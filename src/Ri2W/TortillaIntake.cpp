/*******************************************************************************
 *
 * File: ChipsIntake.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software. 
 *
 ******************************************************************************/
#include "TortillaIntake.h"

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Time.h"

#include "rfutilities/MacroStepFactory.h"
#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/******************************************************************************/

TortillaIntake::TortillaIntake(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Tortilla Intake Control [%s] =========================", 
				control_name.c_str());
    motor_roller = nullptr; 
	motor_handoff = nullptr;
	light_sensor = nullptr;

    motor_roller_target_power = 0.0;
	motor_roller_command_power = 0.0;
	motor_roller_actual_power = 0.0;
    motor_roller_in_max_power = 1.0;
    motor_roller_out_max_power = -1.0;

	motor_handoff_target_power = 0.0;
	motor_handoff_command_power = 0.0;
	motor_handoff_actual_power = 0.0;
    motor_handoff_in_max_power = 1.0;
    motor_handoff_out_max_power = -1.0;

    light_is_triggered = false;
	stop_handoff_at_light = false;

	hardware_is_ready = false;

    new MacroStepProxy<MSTortillaIntakePower>(control_name, "IntakePower", this);
	new MacroStepProxy<MSTortillaHandoffPower>(control_name, "HandoffPower", this);

	const char *name = nullptr;

    xml->QueryFloatAttribute("motor_roller_in_max_power", &motor_roller_in_max_power);
    xml->QueryFloatAttribute("motor_roller_out_max_power", &motor_roller_out_max_power);
    xml->QueryFloatAttribute("motor_handoff_in_max_power", &motor_handoff_in_max_power);
    xml->QueryFloatAttribute("motor_handoff_out_max_power", &motor_handoff_out_max_power);
    
    comp = xml->FirstChildElement("motor");
	while(comp != nullptr)
    {
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		else if(strcmp(name, "motor_roller") == 0)
		{
			Advisory::pinfo("  creating speed controller for rollor motor");
			motor_roller = HardwareFactory::createMotor(comp);
		}
		else if(strcmp(name, "motor_handoff") == 0)
		{
			Advisory::pinfo("  creating speed controller for handoff motor");
			motor_handoff = HardwareFactory::createMotor(comp);
		}
		else
		{
			Advisory::pwarning("found unexpected motor with name %s");
		}

		comp = comp -> NextSiblingElement("motor");
	}

	comp = xml->FirstChildElement("limit_switch");
	while(comp != nullptr)
    {
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected limit_switch with no name attribute");
		}
		else if(strcmp(name, "light_sensor") == 0)
		{
			Advisory::pinfo("  creating light sensor for handoff");
			light_sensor = HardwareFactory::createLimitSwitch(comp);
		}
		else
		{
			Advisory::pwarning("found unexpected limit_switch with name %s");
		}

		comp = comp -> NextSiblingElement("limit_switch");
	}

	comp = xml->FirstChildElement("oi");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected oir with no name attribute");
		}
		else if (strcmp(name, "roller_in") == 0)
		{
			Advisory::pinfo("  connecting roller in channel");
			float value = 0.0;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
					std::bind(&TortillaIntake::setMomentaryRollerPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "roller_out") == 0)
		{
			Advisory::pinfo("  connecting roller out channel");
			float value = 0.0;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
					std::bind(&TortillaIntake::setMomentaryRollerPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "roller_stop") == 0)
		{
			Advisory::pinfo("connecting %s channel", name);
			OIController::subscribeDigital(comp, 
					std::bind(&TortillaIntake::setMomentaryRollerPower, this, 0.0, std::placeholders::_1));
		}
		else if (strcmp(name, "handoff_in") == 0)
		{
			Advisory::pinfo("  connecting handoff in channel");
			bool stop_at_light = true;
			comp->QueryBoolAttribute("stop_at_light", &stop_at_light);
			float value = 0.0;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
					std::bind(&TortillaIntake::setMomentaryHandoffPower, this, value, stop_at_light, std::placeholders::_1));
		}
		else if (strcmp(name, "handoff_out") == 0)
		{
			Advisory::pinfo("  connecting handoff out channel");
			float value = 0.0;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
					std::bind(&TortillaIntake::setMomentaryHandoffPower, this, value, false, std::placeholders::_1));
		}
		else if (strcmp(name, "handoff_stop") == 0)
		{
			Advisory::pinfo("connecting %s channel", name);
			OIController::subscribeDigital(comp, 
					std::bind(&TortillaIntake::setMomentaryHandoffPower, this, 0.0, false, std::placeholders::_1));
		}
		else
		{
			Advisory::pwarning("found unexpected oi with name %s");
		}
		
		comp = comp->NextSiblingElement("oi");
	}

}

/*******************************************************************************
 *
 ******************************************************************************/
TortillaIntake::~TortillaIntake(void)
{
	// Be sure to reference each motor
	if (motor_roller != nullptr)
	{
		delete motor_roller;
		motor_roller = nullptr;
	}

	if (motor_handoff != nullptr)
	{
		delete motor_handoff;
		motor_handoff = nullptr;
	}

	if (light_sensor != nullptr)
	{
		delete light_sensor;
		light_sensor = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void TortillaIntake::controlInit(void)
{ 
	// Make sure every motor has current limiting. No magic smoke
	bool is_ready = true;

	if( motor_roller == nullptr )
	{
		Advisory::pwarning("%s missing required component -- motor_roller", getName().c_str());
		is_ready = false;
	}
	if( motor_handoff == nullptr )
	{
		Advisory::pwarning("%s missing required component -- motor_handoff", getName().c_str());
		is_ready = false;
	}

	if (light_sensor == nullptr)
	{
		Advisory::pwarning("%s missing required component -- light_sensor", getName().c_str());
		is_ready = false;
	}
	
	hardware_is_ready = is_ready;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void TortillaIntake::disabledInit(void)
{
	motor_roller_target_power = 0.0;
	motor_handoff_target_power = 0.0;
	motor_roller_command_power = 0.0;
	motor_handoff_command_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void TortillaIntake::autonomousInit(void)
{
	motor_roller_target_power = 0.0;
	motor_handoff_target_power = 0.0;
	motor_roller_command_power = 0.0;
	motor_handoff_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void TortillaIntake::teleopInit(void)
{
	motor_roller_target_power = 0.0;
	motor_handoff_target_power = 0.0;
	motor_roller_command_power = 0.0;
	motor_handoff_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void TortillaIntake::testInit(void)
{
	motor_roller_target_power = 0.0;
	motor_handoff_target_power = 0.0;
	motor_roller_command_power = 0.0;
	motor_handoff_command_power = 0.0;
}

/*******************************************************************************
 *
 * @brief	Set the roller target power to the given value
 * 
 * This method can be mapped to an analog OI input, it is also called by 
 * methods that set power based on button presses and by macro steps.
 *  
 * @param	value	the new value for the roller target power, this is the percent
 *                  duty cycle that the motor should run at, the value will be
 *                  limited between motor_roller_in_max_power and motor_roller_out_max_power
 *                  by this method.
 * 
 ******************************************************************************/
void TortillaIntake::setRollerPower(float value)
{
	motor_roller_target_power = gsu::Filter::limit(value, motor_roller_in_max_power, motor_roller_out_max_power);
}

/*******************************************************************************
 *
 * @brief	Set the target motor power to a given value while a button is pressed
 * 
 * This method sets the target motor value to the provided value when a button 
 * is pressed and sets it to 0.0 when the button is released. It can be mapped to 
 * button presses that are intended to set motor power for short bursts.
 * 
 * @param 	value	the motor power value as a percent of the duty cycle from 
 * 					motor_min_control to motor_max_control
 * 
 * @param	pressed	if true the motor target power will be set to the specified
 * 					value, if false the motor target power will be set to 0.0,
 * 					default = true
 * 
 ******************************************************************************/
void TortillaIntake::setMomentaryRollerPower(float value, bool pressed)
{
	Advisory::pinfo("TortillaIntake::setMomentaryRollerPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setRollerPower(value);
	}
	else
	{
		setRollerPower(0.0);
	}
}

/*******************************************************************************
 *
 * @brief	Set the handoff target power to the given value
 * 
 * This method can be mapped to an analog OI input, it is also called by 
 * methods that set power based on button presses and by macro steps.
 *  
 * @param	value	the new value for the handoff target power, this is the percent
 *                  duty cycle that the motor should run at, the value will be
 *                  limited between motor_handoff_in_max_power and motor_handoff_out_max_power
 *                  by this method.
 * 
 ******************************************************************************/
void TortillaIntake::setHandoffPower(float value)
{
	motor_handoff_target_power = gsu::Filter::limit(value, motor_handoff_in_max_power, motor_handoff_out_max_power);
}

/*******************************************************************************
 *
 * @brief	Set the handoff target motor power to a given value while a button is pressed
 * 
 * This method sets the target motor value to the provided value when a button 
 * is pressed and sets it to 0.0 when the button is released. It can be mapped to 
 * button presses that are intended to set motor power for short bursts.
 * 
 * @param 	value	the motor power value as a percent of the duty cycle from 
 * 					motor_min_control to motor_max_control
 * 
 * @param	stop_at_light	if true, the handoff motor should not go in
 *                          when the light sensor is detecting a game piece
 * 
 * @param	pressed	if true the motor target power will be set to the specified
 * 					value, if false the motor target power will be set to 0.0,
 * 					default = true
 * 
 ******************************************************************************/
void TortillaIntake::setMomentaryHandoffPower(float value, bool stop_at_light, bool pressed)
{
	Advisory::pinfo("TortillaIntake::setMomentaryHandofffPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setHandoffPower(value);
		stop_handoff_at_light = stop_at_light;
	}
	else
	{
		setHandoffPower(0.0);
		stop_handoff_at_light = false;
	}

}

/*******************************************************************************	
 *
 ******************************************************************************/
void TortillaIntake::publish()
{
	SmartDashboard::PutNumber(getName() + " roller target power: ", motor_roller_target_power);
	SmartDashboard::PutNumber(getName() + " roller command_power: ", motor_roller_command_power);
	SmartDashboard::PutNumber(getName() + " roller actual_power: ", motor_roller_actual_power);
	SmartDashboard::PutNumber(getName() + " handoff target power: ", motor_handoff_target_power);
	SmartDashboard::PutNumber(getName() + " handoff command_power: ", motor_handoff_command_power);
	SmartDashboard::PutNumber(getName() + " handoff actual_power: ", motor_handoff_actual_power);

	SmartDashboard::PutBoolean(getName() + " light sensor on: ", light_is_triggered);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void TortillaIntake::doPeriodic()
{
	if(hardware_is_ready == false) 
	{
		Advisory::pinfo("intake is not ready");
		return;
	}

	// Update all motors
	motor_roller->doUpdate();
	motor_handoff->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//
	motor_roller_actual_power = motor_roller->getPercent();
	motor_handoff_actual_power = motor_handoff->getPercent();

	light_is_triggered = light_sensor->isPressed();

	//
	// Keep things safe
	//
	if (getPhase() == DISABLED)
	{
		motor_roller_target_power = 0.0;
		motor_handoff_target_power = 0.0;
		motor_roller_command_power = 0.0;
		motor_handoff_command_power = 0.0;
	}

	motor_roller_command_power = motor_roller_target_power;
	motor_handoff_command_power = motor_handoff_target_power;
	if (stop_handoff_at_light && light_is_triggered)
	{
		motor_handoff_command_power = gsu::Filter::limit(motor_handoff_command_power, 0.0, motor_handoff_out_max_power);
	}

    motor_roller->setPercent(motor_roller_command_power);
    motor_handoff->setPercent(motor_handoff_command_power);
}


// macros

/*******************************************************************************
 *
 ******************************************************************************/
MSTortillaIntakePower::MSTortillaIntakePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (TortillaIntake*)control;
	xml->QueryFloatAttribute("power", &power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSTortillaIntakePower::init(void)
{
	parent_control->setMomentaryRollerPower(power);
	parent_control->setMomentaryHandoffPower(power, true); // stop at light
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSTortillaIntakePower::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSTortillaHandoffPower::MSTortillaHandoffPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (TortillaIntake*)control;
	xml->QueryFloatAttribute("power", &power);
	xml->QueryBoolAttribute("stop_at_light", &stop_at_light);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSTortillaHandoffPower::init(void)
{
	parent_control->setMomentaryHandoffPower(power, stop_at_light); // stop at light
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSTortillaHandoffPower::update(void)
{
	return next_step;
}
