/*******************************************************************************
 *
 * File: ChipsShooter.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Notes:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
// The corresponding .h file
#include "ChipsShooter.h"

// Do not remove - all subsystems
#include <math.h>

#include "gsinterfaces/Path.h"

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Angles.h"
#include "gsinterfaces/Time.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Path.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfhardware/LimitSwitch.h"


#include "frc/smartdashboard/SmartDashboard.h" // WPI


using namespace tinyxml2;
using namespace frc;

//12 tooth gear driving 44 tooth
//(135/2816) = degrees to ticks!!1/1/1

/*
og PID settings
200 - I zone
1 - loop period ms
1 - peak output
2.5 - kD
0.0012497901916503906 - kI
0.25 - kP
*/
//60000/2048
//TODO add limits for open loop that only allows up below top and only down at limit
//851, 1701, 2552, 3402 <- velocity values

//shooter top position -> 0.8
//shooter bottom position -> -54
// shooter limits 0-50 (max max is 54)
//

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
ChipsShooter::ChipsShooter(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)	
{
	Advisory::pinfo("========================= Creating ChipsShooter Control [%s] =========================",
	            control_name.c_str());

	// Be sure to initialize all method variables
	//motor declare
	fly_motor = nullptr; // This is an object, so it gets initialized to nullptr
	joint_motor = nullptr;

	bottom_limit = nullptr;
	top_limit = nullptr;

    joint_sensor = nullptr;;
	joint_pid = nullptr;

	//joint stuff
	joint_target_power = 0.0;
	joint_command_power = 0.0;
	joint_actual_power = 0.0;
	joint_min_power = -0.5;
	joint_max_power = 0.5;
	joint_step_power = 0.1;
	
	joint_target_position = 0.0;
	joint_command_position = 0.0;
	joint_actual_position = 0.0;
    Joint_actual_raw = 0.0;
	joint_actual_wraps = 0;
	joint_min_position = 10.0;
	joint_max_position = 150.0;
	joint_max_current = 0.0;
	joint_position_step = 3.0;

	//shooter stuff
	fly_target_power = 0.0; // Initialize to 0.0 so you don't have any accidents
	fly_command_power = 0.0;
	fly_actual_power = 0.0; // Power is on a scale from 0.0 to 1.0
	fly_min_power = -0.2;
	fly_max_power = 1.0;
	fly_power_step = 0.05;

	fly_target_velocity = 0.0;
	fly_command_velocity = 0.0;
	fly_actual_velocity = 0.0;
	fly_min_velocity = -1000.0;
	fly_max_velocity = 6300.0; //subject to change based on motor? yk
	fly_velocity_step = 250.0;

	bottomPressed = false;
	topPressed = false;

	isFlyOn = false;
	active = false;
	is_closed_loop = false;

	const char *name = nullptr;
	XMLElement* comp = nullptr;
	for (uint8_t i = 0; i < NUM_SETPOINTS; i++ )
	{
		setpoint_defined[i] = false;
		setpoint_name[i] = "unused";
        setpoint_fly_velocity[i] = 0.0;
		setpoint_joint_position[i] = 0.0;
	}
	setpoint_shift = false;

	//
	// Register Macro Steps - these are specific functions that are used in autonomous
	//

	new MacroStepProxy<MSShooterClosedLoop>(control_name, "ShooterClosedLoop", this);
	new MacroStepProxy<MSShooterSetpoint>(control_name, "ShooterSetpoint", this);
#if 0
	new MacroStepProxy<MSShooterToggle>(control_name, "ShooterToggle", this);
	new MacroStepProxy<MSLimelightPipeline>(control_name, "LLPipeline", this); // surely this will not conflict with the swerve macrostep
	new MacroStepProxy<MSShooterPivotDown>(control_name, "ShooterPivotDown", this);
	new MacroStepProxy<MSShooterPivotStop>(control_name, "ShooterPivotStop", this);
#endif

	//
	// Parse XML
	//
	// Read in any variables set in the xml
	xml->QueryBoolAttribute("closed_loop", &is_closed_loop);

	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
		XMLElement *subcomp;
		subcomp = comp->FirstChildElement("setpoint");
		while (subcomp != nullptr)
		{
			subcomp -> QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < NUM_SETPOINTS)
			{
				name = subcomp->Attribute("name");
				if (name != nullptr)
				{
					setpoint_name[setpoint_index] = std::string(name);
				}
				else
				{
					setpoint_name[setpoint_index] = std::string("setpoint ") + std::to_string(setpoint_index);
				}
				subcomp ->QueryFloatAttribute("velocity", &setpoint_fly_velocity[setpoint_index]);
				subcomp ->QueryFloatAttribute("position", &setpoint_joint_position[setpoint_index]);
				setpoint_defined[setpoint_index] = true;
				Advisory::pinfo("setpoint %d: name = %-15s  velocity = %6.2f position = %6.2f", setpoint_index, 
					setpoint_name[setpoint_index].c_str(), setpoint_fly_velocity[setpoint_index], &setpoint_joint_position[setpoint_index]);
			}
			subcomp = subcomp->NextSiblingElement("setpoint");
		}
	}

	// Read in motors and other hardware	
	comp = xml-> FirstChildElement("motor");	
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name == nullptr)
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		else if(strcmp(name, "flywheel") == 0)
		{
			Advisory::pinfo("  creating speed controller for flywheel");
			fly_motor = HardwareFactory::createMotor(comp);
			comp->QueryFloatAttribute("power_step", &fly_power_step);
			comp->QueryFloatAttribute("power_min", &fly_min_power);
			comp->QueryFloatAttribute("power_max", &fly_max_power);
			comp->QueryFloatAttribute("velocity_step", &fly_velocity_step);
			comp->QueryFloatAttribute("velocity_min", &fly_min_velocity);
			comp->QueryFloatAttribute("velocity_max", &fly_max_velocity);
		}
		else if(strcmp(name, "joint") == 0)
		{
			Advisory::pinfo("  creating speed controller for joint");
			joint_motor = HardwareFactory::createMotor(comp);
			comp->QueryFloatAttribute("power_step", &joint_step_power);
			comp->QueryFloatAttribute("power_min", &joint_min_power);
			comp->QueryFloatAttribute("power_max", &joint_max_power);
			comp->QueryFloatAttribute("position_step", &joint_position_step);
			comp->QueryFloatAttribute("position_min", &joint_min_position);
			comp->QueryFloatAttribute("position_max", &joint_max_position);
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with name=%s", name);
		}

		comp = comp -> NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("limit_switch");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected limit switch with no name attribute");
		}
		else if (strcmp(name, "bottomLimit") == 0)
		{
			Advisory::pinfo("  creating shooter bottom limit for %s", name);
			bottom_limit = HardwareFactory::createLimitSwitch(comp);
		}
		else if (strcmp(name, "topLimit") == 0)
		{
			Advisory::pinfo("  creating shooter top limit bottom for %s", name);
			top_limit = HardwareFactory::createLimitSwitch(comp);
		}
		else
		{
			Advisory::pwarning("  found unexpected limit switch with name attribute %s", getName().c_str());
		}		
		
		comp = comp->NextSiblingElement("limit_switch");
	}

	comp = xml-> FirstChildElement("aps");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected aps (absolute position sensor) with no name attribute");
		}
		else if (strcmp(name, "joint_sensor") == 0)
		{
			Advisory::pinfo("  creating aps for %s", name);
			joint_sensor = HardwareFactory::createAbsPosSensor(comp);
		}
		else
		{
			Advisory::pwarning("  found unexpected aps (absolute position sensor) with name attribute %s", getName().c_str());
		}		
		
		comp = comp->NextSiblingElement("aps");
	}

	comp = xml-> FirstChildElement("pid");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected pid with no name attribute");
		}
		else if (strcmp(name, "joint_pid") == 0)
		{
			Advisory::pinfo("  creating pid for %s", name);
			joint_pid = HardwareFactory::createPid(comp);
		}
		else
		{
			Advisory::pwarning("  found unexpected aps (absolute position sensor) with name attribute %s", getName().c_str());
		}		
		
		comp = comp->NextSiblingElement("pid");
	}

	// read in OI channels - these connect a button on the controller to an action
	// essentially, when a button is pressed, it sends a signal to the robot to do an action
	// here, we connect the signals
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pwarning("  found unexpected oi with no name attribute");
		}
		else if (strcmp(name, "setClosedLoop")==0)
		{
			Advisory::pinfo("connecting closed loop channel");
			bool value = false;
			comp->QueryBoolAttribute("value", &value);
			OIController::subscribeDigital(comp, 
					std::bind(&ChipsShooter::setClosedLoop, this, value, std::placeholders::_1));
		}

		else if (strcmp(name, "fly_step_up") == 0)
		{
			Advisory::pinfo(" connecting fly step up channel");
			OIController::subscribeDigital(comp, this, CMD_FLY_STEP_UP);
		}
		else if (strcmp(name, "fly_step_down") == 0)
		{
			Advisory::pinfo(" connecting fly step down channel");
			OIController::subscribeDigital(comp, this, CMD_FLY_STEP_DOWN);
		}

		else if (strcmp(name, "pivot_step_down") == 0)
		{
			Advisory::pinfo(" connecting pivot step down channel");
			OIController::subscribeDigital(comp, this, CMD_PIVOT_DOWN);
		}
		else if (strcmp(name, "pivot_step_up") == 0)
		{
			Advisory::pinfo(" connecting pivot step up channel");
			OIController::subscribeDigital(comp, this, CMD_PIVOT_UP);
		}

		else if ((strncmp(name, "setpoint",8)==0)&& (name[8] >= '0')&& (name[8] <= '3'))
		{
			Advisory::pinfo("connecting %s channel", name);
			OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8]-'0'));
		}
		else if (strcmp(name, "dpad_setpoints") == 0)
		{
			Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
			OIController::subscribeInt(comp, this, CMD_DPAD_SETPOINTS);
		}
		else if (strcmp(name, "setpoint_shift") == 0)
		{
			Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
			OIController::subscribeDigital(comp, 
					std::bind(&ChipsShooter::setSetpointShift, this, std::placeholders::_1));
		}
		else
		{
			Advisory::pwarning("  found unexpected oi with name=%s", name);
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Destructor: Release any resources allocated by this object
 * Any object that you created (motor, sensor, etc), put it here
 * 
 ******************************************************************************/
ChipsShooter::~ChipsShooter(void)
{
	if (fly_motor != nullptr)
	{
		delete fly_motor;
		fly_motor = nullptr;
	}

	if (joint_motor != nullptr)
	{
		delete joint_motor;
		joint_motor = nullptr;
	}

	if (bottom_limit != nullptr)
	{
		delete bottom_limit;
		bottom_limit = nullptr;
	}

	if (top_limit != nullptr)
	{
		delete top_limit;
		top_limit = nullptr;
	}

	if (joint_sensor != nullptr)
	{
		delete joint_sensor;
		joint_sensor = nullptr;
	}

	if (joint_pid != nullptr)
	{
		delete joint_pid;
		joint_pid = nullptr;
	}
}

/*******************************************************************************
 * 
 ******************************************************************************/
void ChipsShooter::controlInit(void)
{ 
	bool is_ready = true;
	if (fly_motor == nullptr)
	{
		Advisory::pwarning("%s shooter missing component fly_motor", getName().c_str());
		is_ready = false;
	}
	
	if(joint_motor == nullptr)
	{
		Advisory::pwarning("%s joint missing component joint_motor", getName().c_str());
		is_ready = false;
	}
    
	if(bottom_limit == nullptr)
	{
		Advisory::pwarning("%s joint missing component bottom_limit", getName().c_str());
		is_ready = false;
	}
    
	if(top_limit == nullptr)
	{
		Advisory::pwarning("%s joint missing component top_limit", getName().c_str());
		is_ready = false;
	}
    
	if(joint_sensor == nullptr)
	{
		Advisory::pwarning("%s joint missing component joint_sensor", getName().c_str());
		is_ready = false;
	}

	if(joint_pid == nullptr)
	{
		Advisory::pwarning("%s joint missing component joint_pid", getName().c_str());
		is_ready = false;
	}

	active = is_ready;	
}

/*******************************************************************************	
 *
 * This runs whenever the robot is disabled (end of a match AND for a second between auton and telop)
 * Reset target and command power to 0
 * Do not reset fly1_power here. fly1_power is the output power reported by the motor itself
 * 
 ******************************************************************************/
void ChipsShooter::disabledInit(void)
{
	fly_target_power = 0.0;
	fly_command_power = 0.0;
	fly_target_velocity = 0.0;
	fly_command_velocity = 0.0;
}

/*******************************************************************************	
 *
 * This runs at the beginning of autonomous
 * Reset power to 0
 * 
 ******************************************************************************/
void ChipsShooter::autonomousInit(void)
{
	fly_target_power = 0.0;
	fly_command_power = 0.0;
	fly_target_velocity = 0.0;
	fly_command_velocity = 0.0;

	joint_target_position = joint_actual_position;
	joint_actual_power = 0.0;
	joint_target_power = 0.0;
	joint_command_power = 0.0;
}

/*******************************************************************************	
 *
 * This runs at the beginning of teleop
 * Reset power to 0
 * 
 ******************************************************************************/
void ChipsShooter::teleopInit(void)
{
	fly_target_power = 0.0;
	fly_command_power = 0.0;
	fly_target_velocity = 0.0;
	fly_command_velocity = 0.0;
	isFlyOn = false;

	joint_target_position = joint_actual_position;
	joint_command_position = joint_actual_position;
	joint_actual_power = 0.0;
	joint_target_power = 0.0;
	joint_command_power = 0.0;

	setpoint_index = 0;
}

/*******************************************************************************	
 *
 * This runs at the beginning when you run the robot in Test mode.
 * We don't typically use this mode
 * Reset power to 0 just in case
 *
 ******************************************************************************/
void ChipsShooter::testInit(void)
{
	fly_target_power = 0.0;
	fly_command_power = 0.0;
	fly_target_velocity = 0.0;
	fly_command_velocity = 0.0;
}

/*******************************************************************************
 * 
 ******************************************************************************/
bool ChipsShooter::isClosedLoop(void)
{
	return is_closed_loop;
}

/*******************************************************************************
 * 
 ******************************************************************************/
bool ChipsShooter::flyOn(void)
{
	return isFlyOn;
}

/*******************************************************************************
 * 
 ******************************************************************************/
void ChipsShooter::setClosedLoop(bool state, bool pressed)
{
	if (pressed)
	{
		if(is_closed_loop != state)
		{
			is_closed_loop = state;
		}
	}
}

/*******************************************************************************
 * 
 ******************************************************************************/
void ChipsShooter::setSetpointShift(bool pressed)
{
	setpoint_shift = pressed;
}

/*******************************************************************************
 * 
 ******************************************************************************/
void ChipsShooter::applySetpoint(int i)
{
	if ((i >= 0) && (i < NUM_SETPOINTS))
	{
		isFlyOn = true;
		setpoint_index = i;
		fly_target_velocity = setpoint_fly_velocity[i];
		joint_target_position = setpoint_joint_position[i];
		Advisory::pinfo("apply shooter setpoint %d, name = %s, velocity = %6.2f position = %6.2f", setpoint_index, 
			setpoint_name[setpoint_index].c_str(), fly_target_velocity, joint_target_position);
	}
	else
	{
		Advisory::pwarning("setpoint out of range!!");
	}
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * Analog: A controller input measured on a continuous -1.0 to 1.0 scale.
 * For our controllers (as of 2023) this is only the joysticks
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void ChipsShooter::setAnalog(int id, float val)
{
	// unused
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * Digital: A controller input measured as true/false.
 * This is all buttons on the controller (other than the D-Pad which is separate)
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void ChipsShooter::setDigital(int id, bool val)
{	
	Advisory::pinfo("set digital %d", id);

	switch (id)
	{
	    case CMD_PIVOT_UP:
			if(val)
			{				
				if(is_closed_loop)
				{
				
					joint_target_position +=joint_position_step;
					joint_target_position = gsu::Filter::limit(joint_target_position, joint_min_position, joint_max_position);	
				}
				else
				{
					joint_target_power = joint_step_power;
					joint_target_power = gsu::Filter::limit(joint_target_power, joint_min_power, joint_max_power);
				}				
			}
			else
			{
				joint_target_power = 0.0;
			}
	 	break;

	    case CMD_PIVOT_DOWN:
			if(val)
			{				
				if(is_closed_loop)
				{
				
					joint_target_position -= joint_position_step;
					joint_target_position = gsu::Filter::limit(joint_target_position, joint_min_position, joint_max_position);
					
				}
				else
				{
					joint_target_power = -joint_step_power;
					joint_target_power = gsu::Filter::limit(joint_target_power, joint_min_power, joint_max_power);
				}				
			}
			else
			{
				joint_target_power = 0.0;
			}
	 	break;

		case CMD_FLY_STEP_UP:
		{
			if(val)
			{				
				if(is_closed_loop)
				{
					fly_target_velocity += fly_velocity_step;
					fly_target_velocity = gsu::Filter::limit(fly_target_velocity, fly_min_velocity, fly_max_velocity);
				}
				else
				{
					fly_target_power += fly_power_step;
					fly_target_power = gsu::Filter::limit(fly_target_power, fly_min_power,fly_max_power);
				}				
			}
		} break;

		case CMD_FLY_STEP_DOWN:
		{
			if (val)
			{
				if(is_closed_loop)
				{
				
					fly_target_velocity -= fly_velocity_step;
					fly_target_velocity = gsu::Filter::limit(fly_target_velocity, fly_min_velocity, fly_max_velocity);
				}
				else
				{
					fly_target_power -= fly_power_step;
					fly_target_power = gsu::Filter::limit(fly_target_power, fly_min_power,fly_max_power);
				}				
			}
			else
			{
				joint_target_power = 0.0;
			}
		} break;

		case CMD_SETPOINT_0:
			if (val)
			{
				applySetpoint(0);
			}
		break;

		case CMD_SETPOINT_1:
			if (val)
			{
				applySetpoint(1);
			}
		break;

		case CMD_SETPOINT_2:
			if (val)
			{
				applySetpoint(2);
			}
		break;

		case CMD_SETPOINT_3:
			if (val)
			{
				applySetpoint(3);
			}
		break;

		default:
			break;
	}
}

/*******************************************************************************
 * setInt connects a button on the D-Pad to a command
******************************************************************************/
void ChipsShooter::setInt(int id, int val)
{
	Advisory::pinfo("shooter setint %d", val);
	switch (id)
	{
		case CMD_DPAD_SETPOINTS:
		{
			if (val == 0)
			{
				applySetpoint(setpoint_shift?4:0);
			}
			else if (val == 90)
			{
				applySetpoint(setpoint_shift?5:1);
			}
			else if (val == 180)
			{
				applySetpoint(setpoint_shift?6:2);
			}
			else if (val == 270)
			{
				applySetpoint(setpoint_shift?7:3);
			}
		} break;

		default: // do nothing
			break;
	}
}

/*******************************************************************************
 * Functions that set variables. These are typically only used in autonomous.
 ******************************************************************************/
void ChipsShooter::setMotorPower(float power)
{
	// in autonomous, you can't directly set a variable's value
	// so, we need a function to do it instead
	fly_target_power = power;
}

/*******************************************************************************
 * 
 ******************************************************************************/
int ChipsShooter::getSetpointIndex()
{
	return setpoint_index;
}

/*******************************************************************************	
 * Puts variables on SmartDashboard. Add all of your variables for now and you can delete them later
 ******************************************************************************/
void ChipsShooter::publish()
{
	SmartDashboard::PutBoolean(" shooter: closed loop", is_closed_loop);

	SmartDashboard::PutNumber( " fly: target power: ", fly_target_power);
	SmartDashboard::PutNumber( " fly: actual power: ", fly_actual_power);
	
	SmartDashboard::PutNumber( " fly: target velocity: ", fly_target_velocity);
	SmartDashboard::PutNumber( " fly: actual velocity: ", fly_actual_velocity);

	SmartDashboard::PutBoolean(" fly: fly on", isFlyOn);

	SmartDashboard::PutNumber(" joint: target power",  joint_target_power);
	SmartDashboard::PutNumber(" joint: command power", joint_command_power);
	SmartDashboard::PutNumber(" joint: actual power ", joint_actual_power);

	SmartDashboard::PutNumber(" joint: target position",  joint_target_position);
	SmartDashboard::PutNumber(" joint: command position", joint_command_position);
	SmartDashboard::PutNumber(" joint: actual position ", joint_actual_position);
    SmartDashboard::PutNumber(" joint: actual raw",       Joint_actual_raw);
	SmartDashboard::PutNumber(" joint: actual wraps",     joint_actual_wraps);

	SmartDashboard::PutBoolean(" joint: bottom limit", bottomPressed);
	SmartDashboard::PutBoolean(" joint: top limit", topPressed);	
}

/*******************************************************************************
 *
 * This method will be called once a period (every 20 ms) to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void ChipsShooter::doPeriodic()
{
	if (active == false)
	{
		Advisory::pinfo("doperiodic not ready");
		return;
	}

	//
	// Get Inputs
	//
	fly_motor->doUpdate();
	joint_motor->doUpdate();
	
	joint_actual_power = joint_motor->getPercent();

	fly_actual_velocity = fly_motor->getVelocity();
	fly_actual_power = fly_motor->getPercent();

    Joint_actual_raw = joint_sensor->getRaw();
	joint_actual_wraps = joint_sensor->getWrapCount();
	joint_actual_position = joint_sensor->getPositionWithWraps();

	topPressed = top_limit->isPressed();
	bottomPressed = bottom_limit->isPressed();

	//
	// Keep things safe
	//
	if (getPhase() == DISABLED)
	{
		joint_target_position = joint_actual_position;
		joint_target_power = 0.0;

		fly_target_velocity = 0.0;
		fly_target_power = 0.0;
	}

	//
	// Process Data
	//	
	if (is_closed_loop)
	{
		// Pivot Joint
		joint_command_position = joint_target_position;
		joint_target_power = 0.0;
	    joint_command_power = joint_pid->calculateCommand(joint_command_position, joint_actual_position);
		if(bottomPressed)
		{
			joint_command_power = gsu::Filter::limit(joint_command_power, 0.0, joint_max_power);
		}

		if(topPressed)
		{
			joint_command_power = gsu::Filter::limit(joint_command_power, joint_min_power, 0.0);
		}

		// Flywheel
    	fly_command_velocity = fly_target_velocity;
		fly_target_power = 0.0;
        fly_command_power = 0.0;
		if (fly_command_velocity != 0.0) 
		{
			isFlyOn = true;
		}
		else 
		{
			isFlyOn = false;
		}
	}
	else
	{
		// pivot Joint
  	    joint_command_power = joint_target_power;
        joint_target_position = joint_actual_position;
        joint_command_position = joint_actual_position;
		if(bottomPressed)
		{
			joint_command_power = gsu::Filter::limit(joint_command_power, 0.0, joint_max_power);
		}

		if(topPressed)
		{
			joint_command_power = gsu::Filter::limit(joint_command_power, joint_min_power, 0.0);
		}

        // Flywheel
	    fly_command_power = fly_target_power;
        fly_target_velocity = 0.0;
        fly_command_velocity = 0.0;

		if (fly_command_power != 0.0) 
		{
			isFlyOn = true;
		}
		else 
		{
			isFlyOn = false;
		}
	}

	//
	// Set Outputs
	//
	joint_motor->setPercent(joint_command_power);

	if (is_closed_loop)
	{
		fly_motor->setVelocity(fly_command_velocity);
	}
	else
	{
		fly_motor->setPercent(fly_command_power);
	}
}

// macros :)

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterClosedLoop::MSShooterClosedLoop(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (ChipsShooter *)control;
	xml->QueryBoolAttribute("state", &state);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterClosedLoop::init(void)
{	
	parent_control->setClosedLoop(state);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterClosedLoop::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterSetpoint::MSShooterSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (ChipsShooter *)control;
	xml->QueryIntAttribute("setpoint_index", &setpoint);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterSetpoint::init(void)
{	
	parent_control->applySetpoint(setpoint);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterSetpoint::update(void)
{
	return next_step;
}

#if 0

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterToggle::MSShooterToggle(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (ChipsShooter *)control;
}


/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterToggle::init(void)
{	
	parent_control->shootToggle();
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterToggle::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSLimelightPipeline::MSLimelightPipeline(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (ChipsShooter *)control;
	xml->QueryIntAttribute("pipeline", &pipeline);
}


/*******************************************************************************
 *
 ******************************************************************************/
void MSLimelightPipeline::init(void)
{	
	parent_control->setLimelightPipeline(pipeline);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSLimelightPipeline::update(void)
{
	return next_step;
}
}

MSShooterPivotDown::MSShooterPivotDown(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (ChipsShooter *)control;
}
/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterPivotDown::init(void)
{	

}
/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterPivotDown::update(void)
{
	parent_control->setDigital(ChipsShooter::CMD_FLY_STEP_DOWN, true);
	return next_step;
}

MSShooterPivotStop::MSShooterPivotStop(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (ChipsShooter *)control;
}
/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterPivotStop::init(void)
{	

}
/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterPivotStop::update(void)
{
	parent_control->setDigital(ChipsShooter::CMD_FLY_STEP_DOWN, false);
	return next_step;
}

#endif
