/*******************************************************************************
 *
 * File: Swerve.cpp 
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include "Swerve.h"

#include "gsinterfaces/Time.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Angles.h"
#include "gsutilities/Filter.h"
#include "rfhardware/HardwareFactory.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/RobotUtil.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
WPILibSwerve::WPILibSwerve(std::string control_name, XMLElement *xml)
    : PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating swerve Control =========================");

	const char *name = nullptr;

	front_left = nullptr;
	front_right = nullptr;
	back_left = nullptr;
	back_right = nullptr;

	gyro = nullptr;
	is_gyro_connected = false;
	heading = 0.0;
	pitch = 0.0;
	roll = 0.0;

	max_drive_velocity = 0.0;
	max_rot_velocity = 0.0;

	forward_cmd = 0.0;
	strafe_cmd = 0.0;
	turn_cmd = 0.0;
	
	forward_input = 0.0;
	strafe_input = 0.0;
	turn_input = 0.0;
	forward_axis = 0.0;
	strafe_axis = 0.0;
	power_cmd = 0.0;

	using_new_input_mode = false;

	drive_scheme = 0; // field oriented
	default_drive_scheme = 0;

	limelight = nullptr;

	ll_target_visible = 0.0;
	ll_target_angle_horizontal = 0.0;
	ll_target_angle_vertical = 0.0;
	ll_target_area = 0.0;
	ll_target_skew = 0.0;
	ll_pipeline = 0;

	ll_forward_cmd = 0.0;
	ll_strafe_cmd = 0.0;
	ll_turn_cmd = 0.0;
	ll_rot_kp = 0.015;
	ll_strafe_kp = 0.02;


    //
	// Register Macro Steps
	//
	new MacroStepProxy<MSDriveFieldRelative>(control_name, "DriveFieldRelative", this);
	new MacroStepProxy<MSDriveRobotRelative>(control_name, "DriveRobotRelative", this);
	new MacroStepProxy<MSDrivetoPosition>(control_name, "DrivetoPosition", this);
	new MacroStepProxy<MSRotatetoHeading>(control_name, "RotatetoHeading", this);
	new MacroStepProxy<MSSetDriveScheme>(control_name, "SetDriveScheme", this);
	new MacroStepProxy<MSSetLLPipeline>(control_name, "SetLLPipeline", this);
	new MacroStepProxy<MSXWheels>(control_name, "XWheels", this);
	new MacroStepProxy<MSGyroReset>(control_name, "GyroReset", this);

    //
	// Parse XML
	//

	xml->QueryIntAttribute("default_drive_scheme", &default_drive_scheme);
	Advisory::pinfo("default drive scheme: %d", default_drive_scheme);
	xml->QueryBoolAttribute("using_new_input_mode", &using_new_input_mode);
	Advisory::pinfo("using new input mode? %s", using_new_input_mode ? "true" : "false");
	xml->QueryFloatAttribute("ll_rot_kp", &ll_rot_kp);
	Advisory::pinfo("limelight rotation kp = %.2f", ll_rot_kp);
	xml->QueryFloatAttribute("ll_strafe_kp", &ll_strafe_kp);
	Advisory::pinfo("limelight strafe kp = %.2f", ll_strafe_kp);

    // init modules (hardware)
	comp = xml-> FirstChildElement("swerve_module");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "front_left") == 0)
			{
				Advisory::pinfo("  creating controller for FRONT LEFT module");
				front_left = new WPILibSwerveModule(0, comp);
				if (max_drive_velocity == 0.0)
				{
					max_drive_velocity = front_left->getMaxDriveVelocity();
					max_rot_velocity = max_drive_velocity / (corner_to_corner * M_PI) * 360.0;
				}
				front_left->setMaxRotVelocity(max_rot_velocity);
				Advisory::pinfo("max drive velocity: %.2f in/s = %.2f ft/s", max_drive_velocity, max_drive_velocity/12.0f);
				Advisory::pinfo("max rot velocity: %.2f deg/s = %.2f rotations/s", max_rot_velocity, max_rot_velocity/360.0f);
			}
			else if(strcmp(name, "front_right") == 0)
			{
				Advisory::pinfo("  creating controller for FRONT RIGHT module");
				front_right = new WPILibSwerveModule(1, comp);
				if (max_drive_velocity == 0.0)
				{
					max_drive_velocity = front_right->getMaxDriveVelocity();
					max_rot_velocity = max_drive_velocity / (corner_to_corner * M_PI) * 360.0;
				}
				front_right->setMaxRotVelocity(max_rot_velocity);
			}
			else if(strcmp(name, "back_left") == 0)
			{
				Advisory::pinfo("  creating controller for BACK LEFT module");
				back_left = new WPILibSwerveModule(2, comp);
				if (max_drive_velocity == 0.0)
				{
					max_drive_velocity = back_left->getMaxDriveVelocity();
					max_rot_velocity = max_drive_velocity / (corner_to_corner * M_PI) * 360.0;
				}
				back_left->setMaxRotVelocity(max_rot_velocity);
			}
			else if(strcmp(name, "back_right") == 0)
			{
				Advisory::pinfo("  creating controller for BACK RIGHT module");
				back_right = new WPILibSwerveModule(3, comp);
				if (max_drive_velocity == 0.0)
				{
					max_drive_velocity = back_right->getMaxDriveVelocity();
					max_rot_velocity = max_drive_velocity / (corner_to_corner * M_PI) * 360.0;
				}
				back_right->setMaxRotVelocity(max_rot_velocity);
			}
			else
			{
				Advisory::pwarning("found unexpected swerve module with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected swerve module with no name attribute");
		}
		comp = comp->NextSiblingElement("swerve_module");
	}
	
    comp = xml->FirstChildElement("gyro");
    if (comp != nullptr)
    {
		name = comp->Attribute("type");
		if (strcmp(name, "NavX") == 0 || strcmp(name, "Pigeon") == 0)
		{
			Advisory::pinfo("  creating gyro");
			gyro = HardwareFactory::createGyro(comp);
			Advisory::pinfo("  created gyro");
			if (gyro->isConnected()) // pigeon always returns true
			{
				Advisory::pinfo("gyro is connected");
			}
			else
			{
				Advisory::pinfo("gyro is NOT connected!!!");
			}
		}
		else
		{
			Advisory::pinfo("unsupported gyro connected! please connect a navx or a pigeon to use swerve");
		}
    }

    comp = xml->FirstChildElement("limelight");
    if (comp != nullptr)
    {
        limelight = HardwareFactory::createLimelight(comp);
        Advisory::pinfo("  created limelight");
    }

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				OIController::subscribeAnalog(comp, 
					std::bind(&WPILibSwerve::setForward, this, std::placeholders::_1));
			}
			else if (strcmp(name, "strafe") == 0)
			{
				Advisory::pinfo("  connecting strafe channel");
				OIController::subscribeAnalog(comp, 
					std::bind(&WPILibSwerve::setLateral, this, std::placeholders::_1));
			}
			else if (strcmp(name, "turn") == 0)
			{
				Advisory::pinfo("  connecting turn channel");
				OIController::subscribeAnalog(comp, 
					std::bind(&WPILibSwerve::setRotation, this, std::placeholders::_1));
			}
			else if (strcmp(name, "power") == 0)
			{
				Advisory::pinfo("  connecting power channel");
				OIController::subscribeAnalog(comp, this, CMD_POWER);
			}
			else if (strcmp(name, "gyro_reset") == 0)
			{
				Advisory::pinfo("  connecting gyro reset channel");
				OIController::subscribeDigital(comp, this, CMD_GYRO_RESET);
			}
			else if (strcmp(name, "x_wheels") == 0)
			{
				Advisory::pinfo("  connecting x wheels channel");
				OIController::subscribeDigital(comp, this, CMD_X_WHEELS);
			}
			else if (strcmp(name, "field_drive") == 0)
			{
				Advisory::pinfo("  connecting x wheels channel");
				OIController::subscribeDigital(comp, this, CMD_FIELD_ORIENTED_MODE);
			}
			else if (strcmp(name, "robot_drive") == 0)
			{
				Advisory::pinfo("  connecting x wheels channel");
				OIController::subscribeDigital(comp, this, CMD_ROBOT_ORIENTED_MODE);
			}
			else if (strcmp(name, "reset_wheels") == 0)
			{
				Advisory::pinfo("  connecting reset wheels channel");
				OIController::subscribeDigital(comp, this, CMD_RESET_WHEELS);
			}
			else if (strcmp(name, "toggle_scheme") == 0)
			{
				Advisory::pinfo("  connecting toggle drive scheme channel");
				OIController::subscribeDigital(comp, this, CMD_FIELD_ORIENTED_MODE);
			}
			else if (strcmp(name, "ll_rot_hold") == 0)
			{
				Advisory::pinfo("  connecting limelight rotate (hold) channel");
				OIController::subscribeDigital(comp, this, CMD_LL_ROT_HOLD);
			}
			else if (strcmp(name, "ll_strafe_hold") == 0)
			{
				Advisory::pinfo("  connecting limelight strafe (hold) channel");
				OIController::subscribeDigital(comp, this, CMD_LL_STRAFE_HOLD);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
		
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
WPILibSwerve::~WPILibSwerve(void)
{
	if (front_left != nullptr)
	{
		delete front_left;
		front_left = nullptr;
	}
	if (front_right != nullptr)
	{
		delete front_right;
		front_right = nullptr;
	}
	if (back_left != nullptr)
	{
		delete back_left;
		back_left = nullptr;
	}
	if (back_right != nullptr)
	{
		delete back_right;
		back_right = nullptr;
	}
	if (gyro != nullptr)
	{
		delete gyro;
		gyro = nullptr;
	}
	if (limelight != nullptr)
	{
		delete limelight;
		limelight = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void WPILibSwerve::controlInit(void)
{
	if (gyro == nullptr)
	{
		Advisory::pinfo("(%s) swerve created without gyro", getName().c_str());
	}
	if (limelight == nullptr)
	{
		Advisory::pinfo("(%s) swerve created without limelight", getName().c_str());
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void WPILibSwerve::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void WPILibSwerve::disabledInit(void)
{
	setDriveScheme(default_drive_scheme);
	xWheels();
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void WPILibSwerve::autonomousInit(void)
{
	setDriveScheme(DRIVE_AUTON);
	resetWheels();
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void WPILibSwerve::teleopInit(void)
{
	setDriveScheme(default_drive_scheme);
	forward_axis = 0.0;
	strafe_axis = 0.0;
	power_cmd = 0.0;
	resetWheels();
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void WPILibSwerve::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void WPILibSwerve::setAnalog(int id, float val)
{			

	switch (id)
	{	
		case CMD_FORWARD:
		{
			forward_input = val;
		} break;
		
		case CMD_STRAFE:
		{
			strafe_input = val;
		} break;

		case CMD_TURN:
		{
			turn_input = val;
		} break;

		case CMD_POWER:
		{
			power_cmd = val;
		} break;
			
		default:
			break;
	}
}

void WPILibSwerve::setForward(float val)
{
		Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, val);
		forward_input = val;
}
void WPILibSwerve::setLateral(float val)
{
		Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, val);
		strafe_input = val;
}
void WPILibSwerve::setRotation(float val)
{
		Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, val);
		turn_input = val;
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void WPILibSwerve::setDigital(int id, bool val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_GYRO_RESET:
		{
			if (val)
			{
				resetHeading();
				Advisory::pinfo("gyro reset!!! new heading: %f", gyro->getAngle());
			}
		} break;

		case CMD_X_WHEELS:
		{
			if (val)
			{
				drive_scheme = DRIVE_X_WHEELS;
				Advisory::pinfo("Drive Scheme set to X");
			}
		} break;

		case CMD_FIELD_ORIENTED_MODE:
		{
			if (val)
			{
				drive_scheme = DRIVE_FIELD_RELATIVE;
				Advisory::pinfo("Drive Scheme set to Field Relative");
			}
		} break;
		case CMD_ROBOT_ORIENTED_MODE:
		{
			if (val)
			{
				drive_scheme = DRIVE_ROBOT_RELATIVE;
				Advisory::pinfo("Drive Scheme set to Robot Relative");
			}
		} break;
		case CMD_RESET_WHEELS:
		{
			if (val)
			{
				drive_scheme = DRIVE_RESET_WHEELS;
				Advisory::pinfo("wheels facing forward");
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;

		case CMD_LL_ROT_HOLD:
		{
			if (val)
			{
				drive_scheme = DRIVE_ROTATE_TO_LIMELIGHT;
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;

		case CMD_LL_STRAFE_HOLD:
		{
			if (val)
			{
				drive_scheme = DRIVE_STRAFE_TO_LIMELIGHT;
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;

		default:
			break;
	}
}

/*******************************************************************************	

 ******************************************************************************/
void WPILibSwerve::setInt(int id, int val)
{
}

/*****************************************************************************
 * kinematics: normalize wheel speeds
 * input: array of swerve module states
 * output: array of swerve module states, normalized so that all wheel speeds are <= the maximum
 ****************************************************************************/
void WPILibSwerve::normalizeSpeeds(wpi::array<frc::SwerveModuleState, 4> states)
{
	kinematics.DesaturateWheelSpeeds(&states, units::meters_per_second_t(max_drive_velocity * INCHES_TO_METERS));
}

/*****************************************************************************
 * kinematics: chassis speeds to module states
 * input: chips coordinate system - x velocity (in/s), y velocity (in/s), rot velocity (deg/s)
 * output: array of module states (fl, fr, bl, br) (target speed (m/s), target angle (rad))
 ****************************************************************************/
wpi::array<frc::SwerveModuleState, 4> WPILibSwerve::chassisToModuleStates(float xSpeed, float ySpeed, float rot)
{
	xSpeed *= INCHES_TO_METERS;
	ySpeed *= INCHES_TO_METERS;
	rot = gsu::Angle::toRadians(rot);
	
	// invert y and rot commands to convert from chips to wpilib coordinate system
	return kinematics.ToSwerveModuleStates(
		frc::ChassisSpeeds::Discretize(
			frc::ChassisSpeeds{units::meters_per_second_t(xSpeed), units::meters_per_second_t(-ySpeed), units::radians_per_second_t(-rot)},
			units::second_t(this->getPeriod())
			)		
		);
}

/*****************************************************************************
 * kinematics: chassis speeds to module states
 * input: wpilib coordinate system - chassisspeeds object
 * output: array of module states (fl, fr, bl, br) (target speed (m/s), target angle (rad))
 ****************************************************************************/
wpi::array<frc::SwerveModuleState, 4> WPILibSwerve::chassisToModuleStates(frc::ChassisSpeeds speeds)
{
	return kinematics.ToSwerveModuleStates(frc::ChassisSpeeds::Discretize(speeds, units::second_t(this->getPeriod())));
}

/*****************************************************************************
 * forward kinematics: module states to chassis speeds
 * input: array of module states (fl, fr, bl, br) (target speed (m/s), target angle (rad))
 * output: wpilib coordinate system - chassisspeeds object
 ****************************************************************************/
frc::ChassisSpeeds WPILibSwerve::moduleToChassisSpeeds(wpi::array<frc::SwerveModuleState, 4> states)
{
	frc::ChassisSpeeds wpi_speeds = kinematics.ToChassisSpeeds(states);
	return frc::ChassisSpeeds{wpi_speeds.vx, wpi_speeds.vy, wpi_speeds.omega};
}

/*****************************************************************************
 * inverse kinematics: field relative speeds to chassis speeds
 * input: chips coordinate system - x (in/s), y (in/s), rot (deg/s) speeds, robot heading (cw +)
 * output: wpilib coordinate system - chassisspeeds object
 ****************************************************************************/
frc::ChassisSpeeds WPILibSwerve::fieldToChassisSpeeds(float x, float y, float omega, float robot_angle)
{
	return frc::ChassisSpeeds::FromFieldRelativeSpeeds(
		units::meters_per_second_t(x * INCHES_TO_METERS),
		units::meters_per_second_t(-y * INCHES_TO_METERS), 
		units::radians_per_second_t(gsu::Angle::toRadians(-omega)), 
		Rotation2d(units::degree_t(robot_angle)));
}

/*****************************************************************************
 * Locks wheels in an "X" position
 ****************************************************************************/
void WPILibSwerve::xWheels(void)
{
	front_left->setSteeringPosition(-45.0);
	front_right->setSteeringPosition(45.0);
	back_left->setSteeringPosition(45.0);
	back_right->setSteeringPosition(-45.0);
}

/*****************************************************************************
 * Rotates all wheels to face forward & resets odometry to a pose of (0 in, 0 in, current heading)
 ****************************************************************************/
void WPILibSwerve::resetWheels(void)
{
	front_left->setSteeringPosition(0.0);
	front_right->setSteeringPosition(0.0);
	back_left->setSteeringPosition(0.0);
	back_right->setSteeringPosition(0.0);

	front_left->resetEncoders();
	front_right->resetEncoders();
	back_left->resetEncoders();
	back_right->resetEncoders();


	resetOdometry(
		frc::Pose2d{
			0_in, 0_in,	units::degree_t(-1 * heading)}
	);
}

/*****************************************************************************
 * Updates module motors and sensors
 ****************************************************************************/
void WPILibSwerve::updateModules(void)
{
	front_left->update();
	front_right->update();
	back_left->update();
	back_right->update();
}

/*****************************************************************************
 * Sets Limelight pipeline to specified index
 * @param index
 ****************************************************************************/
void WPILibSwerve::setPipeline(int index)
{
	if (limelight != nullptr)
	{
		limelight->setPipeline(index);
		ll_pipeline = index;
		Advisory::pinfo("set ll pipeline to %i", index);
	}
}

/*****************************************************************************
 * @return index of the default drive scheme 
 ****************************************************************************/
int WPILibSwerve::getDefaultDriveScheme(void)
{
	return default_drive_scheme;
}

/*****************************************************************************
 * Sets drive scheme to specified index
 * @param index 
 ****************************************************************************/
void WPILibSwerve::setDriveScheme(int index)
{
	drive_scheme = index;
	Advisory::pinfo("set drive scheme to %i", index);
}

/*****************************************************************************
 * @return true if gyro is connected and sending data to the roborio
 ****************************************************************************/
bool WPILibSwerve::isGyroConnected(void)
{
	if (gyro != nullptr)
	{
		is_gyro_connected = gyro->isConnected();
	}
	else
	{
		is_gyro_connected = false;
	}
	return is_gyro_connected;
}

/*****************************************************************************
 * @return robot heading (+ CW)
 ****************************************************************************/
float WPILibSwerve::getHeading(void)
{
	heading = gyro->getAngle();
	return heading;
		Advisory::pinfo("(%s) position", getName());

}

/*****************************************************************************
 * @return robot heading as a Rotation2d object (+ CCW) [-180.0, 180.0]
 ****************************************************************************/
frc::Rotation2d WPILibSwerve::getHeadingRotation2d(void)
{
	heading = gyro->getAngle();
	return gyro->getRotation2d();
		Advisory::pinfo("(%s) position", getName());

}

/*****************************************************************************
 * Resets gyro heading to 0
 ****************************************************************************/
void WPILibSwerve::resetHeading(void)
{
	gyro->reset();
}

/*****************************************************************************
 * @return robot pitch (+ tilt backwards) [-180.0, 180.0]
 ****************************************************************************/
float WPILibSwerve::getPitch(void)
{
	pitch = gyro->getPitch();
	return pitch;
}

/*****************************************************************************
 * @return robot roll (+ tilt left) [-180.0, 180.0]
 ****************************************************************************/
float WPILibSwerve::getRoll(void)
{
	roll = gyro->getAngle();
	return roll;
}


/*****************************************************************************
 * Sets odometry to the specified pose
 * @param pose current pose of the robot as a wpilib Pose2d object
 ****************************************************************************/
void WPILibSwerve::resetOdometry(frc::Pose2d pose)
{
	odometry.ResetPosition(
		gyro->getRotation2d(),
		{front_left->getModulePosition(), front_right->getModulePosition(),
		back_left->getModulePosition(), back_right->getModulePosition()},
		pose

	);
}

/*****************************************************************************
 * @return current robot pose as a wpilib Pose2d object, as calculated by the odometry
 ****************************************************************************/
frc::Pose2d WPILibSwerve::getCurrentPose(void)
{
	return robot_pose;
}

/*******************************************************************************
 * @param xCmd (+ forward) [-1.0, 1.0]
 * @param yCmd (+ right) [-1.0, 1.0]
 * @param rotCmd (+ CW) [-1.0, 1.0]
 ******************************************************************************/
void WPILibSwerve::robotRelativeDrive(float xCmd, float yCmd, float rotCmd)
{
	if (abs(xCmd) > 0.03 || abs(yCmd) > 0.03 || abs(rotCmd) > 0.03)
	{ 
		// 0. convert robot relative cmds to robot relative speeds (wpilib)
		// 1. convert robot rel. cmds to wheel speeds and angles (inverse kinematics)
		module_states = chassisToModuleStates(
			xCmd * max_drive_velocity, 
			-yCmd * max_drive_velocity, 
			-rotCmd * max_rot_velocity);

		// 2. normalize wheel speeds to max=
		normalizeSpeeds(module_states);

		// 3. command each module to desired state
		front_left->setState(module_states[0]);
		front_right->setState(module_states[1]);
		back_left->setState(module_states[2]);
		back_right->setState(module_states[3]);
	}
	else
	{
		front_left->setDrivePower(0.0);
		front_right->setDrivePower(0.0);
		back_left->setDrivePower(0.0);
		back_right->setDrivePower(0.0);
	}
}

/*******************************************************************************
 * @param xCmd (+ forward) [-1.0, 1.0]
 * @param yCmd (+ right) [-1.0, 1.0]
 * @param rotCmd (+ CW) [-1.0, 1.0]
 ******************************************************************************/
void WPILibSwerve::fieldRelativeDrive(float xCmd, float yCmd, float rotCmd)
{
	if (abs(xCmd) > 0.03 || abs(yCmd) > 0.03 || abs(rotCmd) > 0.03)
	{
		// 0. convert field relative cmds to robot relative speeds (wpilib)
		// 1. convert robot rel. cmds to wheel speeds and angles (inverse kinematics)
		module_states = chassisToModuleStates(
			fieldToChassisSpeeds(
				xCmd * max_drive_velocity,
				-yCmd * max_drive_velocity,
				-rotCmd * max_rot_velocity,
				heading
			)
		);

		// 2. normalize wheel speeds to max
		normalizeSpeeds(module_states);

		// 3. command each module to desired state
		front_left->setState(module_states[0]);
		front_right->setState(module_states[1]);
		back_left->setState(module_states[2]);
		back_right->setState(module_states[3]);
	}
	else
	{

		front_left->setDrivePower(0.0);
		front_right->setDrivePower(0.0);
		back_left->setDrivePower(0.0);
		back_right->setDrivePower(0.0);
	}
}

/*******************************************************************************	
 *
 ******************************************************************************/
void WPILibSwerve::publish()
{
	SmartDashboard::PutNumber("FL position target: ", front_left->getEncoderTarget());
	SmartDashboard::PutNumber("FL position actual: ", front_left->getEncoderPosition());
	SmartDashboard::PutNumber("FL position error: ", front_left->getEncoderError());
	
	SmartDashboard::PutNumber("FR position target: ", front_right->getEncoderTarget());
	SmartDashboard::PutNumber("FR position actual: ", front_right->getEncoderPosition());
	SmartDashboard::PutNumber("FR position error: ", front_right->getEncoderError());
	
	SmartDashboard::PutNumber("BL position target: ", back_left->getEncoderTarget());
	SmartDashboard::PutNumber("BL position actual: ", back_left->getEncoderPosition());
	SmartDashboard::PutNumber("BL position error: ", back_left->getEncoderError());
	
	SmartDashboard::PutNumber("BR position target: ", back_right->getEncoderTarget());
	SmartDashboard::PutNumber("BR position actual: ", back_right->getEncoderPosition());
	SmartDashboard::PutNumber("BR position error: ", back_right->getEncoderError());

	SmartDashboard::PutNumber("swerve drive scheme: ", drive_scheme);
	
	SmartDashboard::PutNumber("gyro heading: ", heading);
	SmartDashboard::PutNumber("gyro pitch: ", pitch);
	SmartDashboard::PutNumber("gyro roll: ", roll);

	SmartDashboard::PutNumber("fwd cmd: ", forward_cmd);
	SmartDashboard::PutNumber("strafe cmd: ", strafe_cmd);
	SmartDashboard::PutNumber("turn cmd: ", turn_cmd);

}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed
 * 
 ******************************************************************************/
void WPILibSwerve::doPeriodic()
{
   updateModules();

    // read sensors
	if (gyro != nullptr && isGyroConnected())
	{
		heading = gyro->getAngle();
		pitch = gyro->getPitch();
		roll = gyro->getRoll();
	}
	else
	{
		heading = 0.0;
		pitch = 0.0;
		roll = 0.0;
	}

    if (getPhase() == ControlPhase::AUTON)
	{
		//Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__);
		robot_pose = odometry.Update(
			gyro->getRotation2d(),
			{front_left->getModulePosition(), front_right->getModulePosition(),
			back_left->getModulePosition(), back_right->getModulePosition()}
		);
	}


	forward_cmd = forward_input;
	strafe_cmd = strafe_input;
	turn_cmd = turn_input;

	switch (drive_scheme)
	{
		case DRIVE_FIELD_RELATIVE:
		{
			fieldRelativeDrive(forward_cmd, strafe_cmd, turn_cmd);
		} break;

		case DRIVE_ROBOT_RELATIVE:
		{
			robotRelativeDrive(forward_cmd, strafe_cmd, turn_cmd);
		} break;

		case DRIVE_X_WHEELS:
		{
			xWheels();
		} break;

        case DRIVE_RESET_WHEELS:
		{
			resetWheels();
		} break;

		case DRIVE_AUTON:
		{
			// do nothing, modules will be commanded from inside macrosteps
		} break;

		case DRIVE_PATH_FOLLOWING:
		{
			
		} break;

		default:
		  break;
	}
}


/*******************************************************************************
 * macros
 ******************************************************************************/

/*******************************************************************************
 * Commands the robot to drive from a start to end position relative to the field
 * 
 * @param initial_x initial x position (inches) (+ forward)
 * @param initial_y initial y position (inches) (+ right)
 * @param initial_heading initial heading (degrees) (+ CW)
 * 
 * @param target_x target x position (inches) (+ forward)
 * @param target_y target y position (inches) (+ right)
 * @param target_heading target heading (degrees) (+ CW)
 * 
 * @param tol_x tolerance (inches)
 * @param tol_y tolerance (inches)
 * @param tol_heading tolerance (degrees)
 * 
 * @param x_control velocity controller for the x axis
 * @param y_control velocity controller for the y axis
 * @param rot_control velocity controller for heading
 * 
 * @note controller documentation in rfutilities\\SimpleTrapCntl.h. 
 * velocity and acceleration use inches/degrees, everything else uses % power
 * 
 ******************************************************************************/
MSDrivetoPosition::MSDrivetoPosition(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (WPILibSwerve *)control;
	XMLElement *comp;

	// query xml inputs
	xml->QueryFloatAttribute("initial_x", &initial_x);
	xml->QueryFloatAttribute("initial_y", &initial_y);
	initial_heading = -0.999f;
	xml->QueryFloatAttribute("initial_heading", &initial_heading);
	Advisory::pinfo("initial position created, x = %2.f in, y = %.2f in, heading = %.2f deg", initial_x, initial_y, initial_heading);

	xml->QueryFloatAttribute("target_x", &target_x);
	xml->QueryFloatAttribute("target_y", &target_y);
	xml->QueryFloatAttribute("target_heading", &target_heading);
	Advisory::pinfo("target position created, x = %2.f in, y = %.2f in, heading = %.2f deg", target_x, target_y, target_heading);
	
	xml->QueryFloatAttribute("tol_x", &tol_x);
	xml->QueryFloatAttribute("tol_y", &tol_y);
	xml->QueryFloatAttribute("tol_heading", &tol_heading);
	Advisory::pinfo("x tolerance: %.2f in, y tolerance: %.2f in, heading tolerance: %.2f deg", tol_x, tol_y, tol_heading);

	comp = xml->FirstChildElement("x_control");
	x_control = HardwareFactory::createTrapCntl(comp);
	comp = xml->FirstChildElement("y_control");
	y_control = HardwareFactory::createTrapCntl(comp);
	comp = xml->FirstChildElement("rot_control");
	rot_control = HardwareFactory::createTrapCntl(comp);

}

/*******************************************************************************
 *
 *****************************************************************************/
void MSDrivetoPosition::init(void)
{
	Advisory::pinfo("initial heading?????? %f", initial_heading);
	if (initial_heading == -0.999f)
	{
		// if no initial heading value read from xml,
		// set initial heading to current robot heading
		initial_heading = parent_control->getHeading();
		Advisory::pinfo("no initial heading found, setting init heading to %.2f", initial_heading);
	}
	parent_control->resetOdometry(
		frc::Pose2d{
			units::inch_t(initial_x),
			units::inch_t(-1 * initial_y),
			units::degree_t(-1 * initial_heading)
		}
	);
	current_x = initial_x;
	current_y = initial_y;
	current_heading = initial_heading;
	parent_control->setDriveScheme(parent_control->DRIVE_PATH_FOLLOWING);
	x_control->resetControlValue(target_x, initial_x);
	y_control->resetControlValue(target_y, initial_y);
	rot_control->resetControlValue(target_heading, initial_heading);
	Advisory::pinfo("MSDrivetoPosition started");
	Advisory::pinfo(
		"x initial: %.2f in, y initial: %.2f in, heading initial: %.2f deg",
		initial_x, initial_y, initial_heading
	);
}

/*******************************************************************************
 *
 *****************************************************************************/
MacroStep * MSDrivetoPosition::update(void)
{
	// update current pose, convert to x, y, heading values
	// invert y and heading to convert from wpilib to chips coordinate system
	frc::Pose2d current_pose = parent_control->getCurrentPose();
	current_x = current_pose.X().value() * METERS_TO_INCHES;
	current_y = -1 * current_pose.Y().value() * METERS_TO_INCHES;
	current_heading = -1 * current_pose.Rotation().Degrees().value();

	if ((fabs(target_x - current_x) < tol_x) && // if at target position
		(fabs(target_y - current_y) < tol_y) &&
		(fabs(target_heading - current_heading) < tol_heading))
	{
		Advisory::pinfo("MSDrivetoPosition finished");
		Advisory::pinfo(
			"x target: %.2f in, y target: %.2f in, heading target: %.2f deg",
			target_x, target_y, target_heading
		);
		Advisory::pinfo(
			"x current: %.2f in, y current: %.2f in, heading current: %.2f deg",
			current_x, current_y, current_heading
		);
		Advisory::pinfo("----------------");
		parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
		parent_control->setDriveScheme(parent_control->DRIVE_AUTON);
		parent_control->xWheels();
		return next_step;
	}

	// if not at target position
	x_cmd = x_control->calculateControlValue(target_x, current_x);
	y_cmd = y_control->calculateControlValue(target_y, current_y);
	rot_cmd = rot_control->calculateControlValue(target_heading, current_heading);
	// command the base to drive at (field relative) target commands
	//Advisory::pinfo("x current: %.2f in, x cmd: %.2f, y current: %.2f, y cmd: %.2f, rot current: %.2f, rot cmd: %.2f", current_x, x_cmd, current_y, y_cmd, current_heading, rot_cmd); // todo delete
	parent_control->fieldRelativeDrive(
		x_cmd,
		y_cmd,
		rot_cmd
	);
	return this;
}

/*******************************************************************************
 * Drives at specified field relative commands for a specified length of time.
 *****************************************************************************/
MSDriveFieldRelative::MSDriveFieldRelative(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (WPILibSwerve *)control;

	xml->QueryFloatAttribute("forward", &forward);
	xml->QueryFloatAttribute("strafe", &strafe);
	xml->QueryFloatAttribute("turn", &turn);
}

/*******************************************************************************
 *
 *****************************************************************************/
void MSDriveFieldRelative::init()
{
	start_time = gsi::Time::getTime();
	Advisory::pinfo("MSDriveFieldRelative started with commands fwd = %.2f, strafe = %.2f, turn = %.2f", forward, strafe, turn);
}

/*******************************************************************************
 *
 *****************************************************************************/
MacroStep *MSDriveFieldRelative::update()
{
	current_time = gsi::Time::getTime();
	if (current_time - start_time < time)
	{
		parent_control->fieldRelativeDrive(forward, strafe, turn);
		return this;
	}
	parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
	Advisory::pinfo("MSDriveFieldRelative ended after %.3f seconds", current_time - start_time);
	return next_step;
}

/*******************************************************************************
 * Drives at specified robot relative commands for a specified length of time.
 *****************************************************************************/
MSDriveRobotRelative::MSDriveRobotRelative(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (WPILibSwerve *)control;

	xml->QueryFloatAttribute("forward", &forward);
	xml->QueryFloatAttribute("strafe", &strafe);
	xml->QueryFloatAttribute("turn", &turn);
}

/*******************************************************************************
 *
 *****************************************************************************/
void MSDriveRobotRelative::init()
{
	start_time = gsi::Time::getTime();
	Advisory::pinfo("MSDriveRobotRelative started with commands fwd = %.2f, strafe = %.2f, turn = %.2f", forward, strafe, turn);
}

/*******************************************************************************
 *
 *****************************************************************************/
MacroStep *MSDriveRobotRelative::update()
{
	current_time = gsi::Time::getTime();
	if (current_time - start_time < time)
	{
		parent_control->robotRelativeDrive(forward, strafe, turn);
		return this;
	}
	parent_control->robotRelativeDrive(0.0, 0.0, 0.0);
	Advisory::pinfo("MSDriveRobotRelative ended after %.3f seconds", current_time - start_time);
	return next_step;
}

/*******************************************************************************
 * Rotates to a specified heading
 * 
 * @note controller documentation in rfutilities\\SimpleTrapCntl.h. 
 * velocity and acceleration use degrees, everything else uses % power
 * 
 *****************************************************************************/
MSRotatetoHeading::MSRotatetoHeading(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (WPILibSwerve *)control;
	XMLElement *comp;

	xml->QueryFloatAttribute("ending", &ending);
	xml->QueryFloatAttribute("tol", &tol);
	comp = xml->FirstChildElement("rot_control");
	rot_control = HardwareFactory::createTrapCntl(comp);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSRotatetoHeading::init()
{
	current = parent_control->getHeading();
	rot_control->resetControlValue(ending, current);
	is_at_endpoint = false;
	Advisory::pinfo("MSRotatetoHeading started, initial = %.2f, target = %.2f", current, ending);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSRotatetoHeading::update()
{
	current = parent_control->getHeading();
	if (fabs(ending - current) < tol)
	{
		Advisory::pinfo("MSRotatetoHeading finished at heading %.2f, target = %.2f", current, ending);
		parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
		parent_control->setDriveScheme(parent_control->DRIVE_AUTON);
		parent_control->xWheels();
		return next_step;
	}
	cmd = rot_control->calculateControlValue(ending, current);
	parent_control->fieldRelativeDrive(0.0, 0.0, cmd);
	//Advisory::pinfo("current heading = %.2f, cmd = %.2f", current, cmd);
	return this;
}

/*******************************************************************************
 * Sets the drive scheme
 * @param scheme drive scheme index
 ******************************************************************************/
MSSetDriveScheme::MSSetDriveScheme(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (WPILibSwerve *)control;
    scheme = 0;
    xml->QueryIntAttribute("scheme", &scheme);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetDriveScheme::init()
{
    parent_control->setDriveScheme(scheme);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSetDriveScheme::update()
{
    return next_step;
}

/*******************************************************************************
 * Sets the limelight pipeline
 * @param pipeline pipeline index [0-7]
 ******************************************************************************/
MSSetLLPipeline::MSSetLLPipeline(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (WPILibSwerve *)control;
    pipeline = 0;
    xml->QueryIntAttribute("pipeline", &pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetLLPipeline::init()
{
    parent_control->setPipeline(pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSetLLPipeline::update()
{
    return next_step;
}


/*******************************************************************************
 * Locks the wheels in an "X" shape
 ******************************************************************************/
MSXWheels::MSXWheels(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (WPILibSwerve *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSXWheels::init(void)
{
	parent_control->setDriveScheme(parent_control->DRIVE_X_WHEELS);
	parent_control->xWheels();
	parent_control->setDriveScheme(parent_control->getDefaultDriveScheme());
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSXWheels::update(void)
{
	return next_step;
}

/*******************************************************************************
 * resets gyro to a heading of 0.0
 ******************************************************************************/
MSGyroReset::MSGyroReset(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (WPILibSwerve *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSGyroReset::init(void)
{
	parent_control->resetHeading();
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSGyroReset::update(void)
{
	return next_step;
}