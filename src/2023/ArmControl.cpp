/*******************************************************************************
 *
 * File: ArmControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Time.h"
#include "2023/ArmControl.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/DataLogger.h"
#include "rfhardware/Motor_TalonSrx.h"
#include "rfhardware/Motor_TalonFx.h"
#include "rfhardware/AbsPosSensor.h"
#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
ArmControl::ArmControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
	, ENCODER_TO_IN ((2048 * 12) / 2.314)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating %s Control =========================",
	            control_name.c_str());

	//-------------MOTORS---------------
	wrist_motor = nullptr;
	shoulder_motor = nullptr;
    extension_motor = nullptr;	
	arm_homed = false;
	home_count = 0;

	motor_min_control = -1.0;
	motor_max_control = 1.0;

    wrist_motor_max_current = 100.0;  /// Please, please, please lets pick good numbers
    shoulder_motor_max_current = 100.0;
    extension_motor_max_current = 100.0;   /// so that we do not burn up any intake motors

	wrist_motor_forward_power = 0.0;
    wrist_motor_backward_power = 0.0;
    shoulder_motor_forward_power = 0.0;
	shoulder_motor_backward_power = 0.0;
    extension_motor_forward_power = 0.0;
	extension_motor_backward_power = 0.0;

	motor_max_cmd_delta = 0.25;

	wrist_motor_target_power = 0.0;
	shoulder_motor_target_power = 0.0;
    extension_motor_target_power = 0.0;

	wrist_motor_command_power = 0.0;
	shoulder_motor_command_power = 0.0;
    extension_motor_command_power = 0.0;

	//----------SENSORS-----------

	m_wristFwdLimitPressed = false;
	m_wristBackLimitPressed = false;
	m_shoulderFwdLimitPressed = false;
	m_shoulderBackLimitPressed = false;
	m_extensionFwdLimitPressed = false;
	m_extensionBackLimitPressed = false;
	
	m_limit_shoulder_fwd = nullptr;
	m_limit_shoulder_back = nullptr;

	shoulder_fwd_limit_state = false;
	shoulder_back_limit_state = false;



	//encoder stuff
	shoulder_pos_sensor = nullptr;
	m_wrist_cancoder = nullptr;
	wrist_pid = nullptr;
	shoulder_pid = nullptr;
	is_closed_loop = false;
	min_position_wrist = -95.0;
	max_position_wrist = 95.0;
	wrist_target_position = 0.0; //may need to replace values with the normal resting place inches
	wrist_actual_position = 0.0; //do both
	wrist_position_error = 0.0;
	wrist_offset_angle = -111.0;

	min_position_extension = 0.0;
	max_position_extension = 22.5; 
	extension_actual_position = 0.0;
	extension_target_position = 0.0;
	extension_encoder_actual_position = 0.0;
	extension_encoder_raw_position = 0.0;

	min_position_shoulder = -120.0;
	max_position_shoulder = 120.0; 
	shoulder_target_position = 0.0;
	shoulder_calibrated_position = 0.0;
	shoulder_raw_position = 0.0;
	shoulder_position_error = 0.0;

	wrist_nudge_forward_step = 1.0;
	wrist_nudge_backward_step = -1.0;
	shoulder_nudge_forward_step = 1.0;
	shoulder_nudge_backward_step = -1.0;
	extension_nudge_forward_step = 1.0;
	extension_nudge_backward_step = -1.0;

	//setpoint things
 
	for (uint8_t i = 0; i < NUM_SETPOINTS; i++)
	{

		setpoint_name [i] = "unknown";
		wrist_setpoint_position [i] = 0.0;
		shoulder_setpoint_position [i] = 0.0;
		extension_setpoint_position [i] = 0.0;
		setpoint_defined [i] = false;

	}

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSWrist>(control_name, "Wrist", this);
	new MacroStepProxy<MSShoulder>(control_name, "Shoulder", this);
	new MacroStepProxy<MSExtension>(control_name, "Extension", this);
	new MacroStepProxy<MSChooseSetpoint>(control_name, "ChooseSetpoint", this);


    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &motor_max_cmd_delta);
    //Advisory::pinfo("  max cmd delta = %f", motor_max_cmd_delta);

	xml->QueryFloatAttribute("min_position_wrist", &min_position_wrist);
	xml->QueryFloatAttribute("max_position_wrist", &max_position_wrist);

	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp!=nullptr)
		{
			int setpoint_index = -1;
			setpoint_comp->QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < NUM_SETPOINTS)
			{
				name = setpoint_comp->Attribute("name");
				if (name != nullptr)
				{
					setpoint_name[setpoint_index] = std::string(name);
				}
				else
				{
					setpoint_name[setpoint_index] = std::string("setpoint_") + std::to_string(setpoint_index);
				}

				setpoint_comp->QueryFloatAttribute("wrist_position", &wrist_setpoint_position[setpoint_index]);
				setpoint_comp->QueryFloatAttribute("shoulder_position", &shoulder_setpoint_position[setpoint_index]);
				setpoint_comp->QueryFloatAttribute("extension_position", &extension_setpoint_position[setpoint_index]);
				
				setpoint_defined[setpoint_index] = true;

				Advisory::pinfo(" -- setpoint %2d: %20s   wrist = %7.2f, shoulder = %7.2f, extension = %7.2f",
					setpoint_index, setpoint_name[setpoint_index].c_str(),
					wrist_setpoint_position[setpoint_index],
					shoulder_setpoint_position[setpoint_index],
					extension_setpoint_position[setpoint_index]);
			}
			else
			{
				Advisory::pinfo("setpoint with index out of range -- %d", setpoint_index);
			}

    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "wrist_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for wrist motor");
				wrist_motor = HardwareFactory::createMotor(comp);
				comp->QueryUnsignedAttribute("max_current", & wrist_motor_max_current);
				Advisory::pinfo("	max_current=%u", wrist_motor_max_current); 
			}
			else if(strcmp(name, "shoulder_motor") == 0)
			{
    			Advisory::pinfo("  creating speed controller for Shoulder Motor");
				shoulder_motor = HardwareFactory::createMotor(comp);
				comp -> QueryUnsignedAttribute("max_current", & shoulder_motor_max_current);
				Advisory::pinfo("	max_current=%u", shoulder_motor_max_current);
			}
            else if(strcmp(name, "extension_motor") == 0)
			{
    			Advisory::pinfo("  creating speed controller for Extension Motor");
				extension_motor = HardwareFactory::createMotor(comp);
				//extension_motor->setSensorScale(ENCODER_TO_IN, extension_height_offset); // converts from encoder units to inches
				comp -> QueryUnsignedAttribute("max_current", & extension_motor_max_current);
				Advisory::pinfo("	max_current=%u", extension_motor_max_current);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
	
	comp = xml-> FirstChildElement("cancoder");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating cancoder for Wrist Motor");
		m_wrist_cancoder = HardwareFactory::createCancoder(comp);
    }

	comp = xml-> FirstChildElement("aps");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating aps sensor for Shoulder Motor");
		shoulder_pos_sensor = HardwareFactory::createAbsPosSensor(comp);
		if (shoulder_pos_sensor == nullptr)
		{
			Advisory::pinfo("shoulder pos sensor not found");
		}
		
    }

	comp = xml-> FirstChildElement("wrist_pid");
	if (comp != nullptr)
	{
	    wrist_pid = HardwareFactory::createPid(comp);
		//Advisory::pinfo("wrist_pid: %x",wrist_pid);
    }

	comp = xml-> FirstChildElement("shoulder_pid");
	if (comp != nullptr)
	{
	    shoulder_pid = HardwareFactory::createPid(comp);
		Advisory::pinfo("shoulder_pid: %x",shoulder_pid);
    }
	
	comp = xml->FirstChildElement("limit_switch");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "shoulder_fwd_limit") == 0)
			{
				Advisory::pinfo("  creating shoulder fwd limit for %s", name);
				m_limit_shoulder_fwd = HardwareFactory::createLimitSwitch(comp);
			}
			else if (strcmp(name, "shoulder_back_limit") == 0)
			{
				Advisory::pinfo("  creating shoulder back limit for %s", name);
				m_limit_shoulder_back = HardwareFactory::createLimitSwitch(comp);
			}
			else
			{
				Advisory::pwarning("  found digital_input tag with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found digital_input tag with no name attribute");
		}
		comp = comp->NextSiblingElement("digital_input");
	}
    
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "wrist_forward") == 0)
			{
				Advisory::pinfo("  connecting wrist forward channel");
				comp->QueryFloatAttribute("value", & wrist_motor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_WRISTFORWARD);
			}
			else if (strcmp(name, "wrist_backward") == 0)
			{
				Advisory::pinfo("  connecting wrist backward channel");
				comp->QueryFloatAttribute("value", & wrist_motor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_WRISTBACKWARD);
			}
			else if (strcmp(name, "shoulder_forward") == 0)
			{
				Advisory::pinfo("  connecting shoulder forward channel");
				comp->QueryFloatAttribute("value", & shoulder_motor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_SHOULDERFORWARD);
			}
			else if(strcmp(name, "shoulder_backward") == 0)
			{
				Advisory::pinfo("  connecting shoulder backward channel");
				comp->QueryFloatAttribute("value", & shoulder_motor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_SHOULDERBACKWARD);
			}
            else if(strcmp(name, "extension_forward") == 0)
			{
				Advisory::pinfo("  connecting extension forward channel");
				comp->QueryFloatAttribute("value", & extension_motor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_EXTENSIONFORWARD);
			}
            else if(strcmp(name, "extension_backward") == 0)
			{
				Advisory::pinfo("  connecting extension backward channel");
				comp->QueryFloatAttribute("value", & extension_motor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_EXTENSIONBACKWARD);
			}			
            else if (strcmp(name, "stop") == 0)
			{
				Advisory::pinfo("  connecting stop channel");
				OIController::subscribeDigital(comp, this, CMD_STOP);
			}
			else if ((strncmp(name, "setpoint", 8) == 0)
				&& (name[8] >= '0') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8] - '0'));
			}
			else if (strcmp(name, "closed_loop_state") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if(strcmp(name, "closed_loop_off") == 0)
			{
				Advisory::pinfo("  connecting closed loop off channel");
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_OFF);
			}

			else if (strcmp(name, "analog_setpoint") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeAnalog(comp, this, CMD_ANALOG_SETPOINT);
			}

			else if(strcmp(name, "wrist_nudge_forward") == 0)
			{
				Advisory::pinfo("  connecting wrist_nudge_forward channel");
				comp->QueryFloatAttribute("nudge_value", &wrist_nudge_forward_step);
				comp->QueryFloatAttribute("ol_value", &wrist_motor_forward_power);  

				wrist_motor_forward_power = RobotUtil::limit(motor_min_control, motor_max_control, 
						wrist_motor_forward_power);

				OIController::subscribeDigital(comp, this, CMD_NUDGE_WRIST_FWD);
			}
			else if(strcmp(name, "wrist_nudge_backward") == 0)
			{
				Advisory::pinfo("  connecting wrist_nudge_backwardchannel");
				comp->QueryFloatAttribute("nudge_value", &wrist_nudge_backward_step);
				comp->QueryFloatAttribute("ol_value", &wrist_motor_backward_power);

				wrist_motor_backward_power = RobotUtil::limit(motor_min_control,    
						motor_max_control, wrist_motor_backward_power);

				OIController::subscribeDigital(comp, this, CMD_NUDGE_WRIST_BCK);
			}
			else if(strcmp(name, "shoulder_nudge_forward") == 0)
			{
				Advisory::pinfo("  connecting shoulder_nudge_forward channel");
				comp->QueryFloatAttribute("nudge_value", &shoulder_nudge_forward_step);
				comp->QueryFloatAttribute("ol_value", &shoulder_motor_forward_power);  

				shoulder_motor_forward_power = RobotUtil::limit(motor_min_control, motor_max_control, 
						shoulder_motor_forward_power);

				OIController::subscribeDigital(comp, this, CMD_NUDGE_SHOULDER_FWD);
			}
			else if(strcmp(name, "shoulder_nudge_backward") == 0)
			{
				Advisory::pinfo("  connecting shoulder_nudge_backwardchannel");
				comp->QueryFloatAttribute("nudge_value", &shoulder_nudge_backward_step);
				comp->QueryFloatAttribute("ol_value", &shoulder_motor_backward_power);

				shoulder_motor_backward_power = RobotUtil::limit(motor_min_control,    
						motor_max_control, shoulder_motor_backward_power);

				OIController::subscribeDigital(comp, this, CMD_NUDGE_SHOULDER_BCK);
			}
			else if(strcmp(name, "extension_nudge_forward") == 0)
			{
				Advisory::pinfo("  connecting extension_nudge_forward channel");
				comp->QueryFloatAttribute("nudge_value", &extension_nudge_forward_step);
				comp->QueryFloatAttribute("ol_value", &extension_motor_forward_power);  

				extension_motor_forward_power = RobotUtil::limit(motor_min_control, motor_max_control, 
						extension_motor_forward_power);

				OIController::subscribeDigital(comp, this, CMD_NUDGE_EXTENSION_FWD);
			}
			else if(strcmp(name, "extension_nudge_backward") == 0)
			{
				Advisory::pinfo("  connecting extension_nudge_backwardchannel");
				comp->QueryFloatAttribute("nudge_value", &extension_nudge_backward_step);
				comp->QueryFloatAttribute("ol_value", &extension_motor_backward_power);

				extension_motor_backward_power = RobotUtil::limit(motor_min_control,    
						motor_max_control, extension_motor_backward_power);

				OIController::subscribeDigital(comp, this, CMD_NUDGE_EXTENSION_BCK);
			}


		}
		
		comp = comp->NextSiblingElement("oi");
		
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
ArmControl::~ArmControl(void)
{
	// Be sure to reference each motor
	if (wrist_motor != nullptr)
	{
		delete wrist_motor;
		wrist_motor = nullptr;
	}
	if (shoulder_motor != nullptr)
	{
		delete shoulder_motor;
		shoulder_motor = nullptr;
	}
    if (extension_motor != nullptr)
	{
		delete extension_motor;
		extension_motor = nullptr;
	}
	if (m_wrist_cancoder != nullptr)
	{
		delete m_wrist_cancoder;
		m_wrist_cancoder = nullptr;
	}
}
/********************************************************************************
 * 
 *******************************************************************************/
bool ArmControl::isClosedLoop(void)
{
	return is_closed_loop;
}
/*******************************************************************************
 * 
 *******************************************************************************/
void ArmControl::applySetPoint(bool on, int idx)
{
	if (on && (idx >= 0) && (idx < NUM_SETPOINTS))
	{
		if (setpoint_defined[idx] == true)
		{
			setClosedLoop(true);
			extension_target_position = extension_setpoint_position[idx];
			wrist_target_position = wrist_setpoint_position[idx];
			shoulder_target_position = shoulder_setpoint_position[idx];
			
			Advisory::pinfo("%s applySetpoint: idx=%d, targ=%f, %f, %f", getName().c_str(), idx, shoulder_target_position, extension_target_position, wrist_target_position);
			//Advisory::pinfo("%s applyWristSetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, wrist_target_position);
			//Advisory::pinfo("%s applyShoulderSetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, shoulder_target_position);
			//Advisory::pinfo("%s applyExtensionSetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, extension_target_position);
		}
		else
		{
			Advisory::pinfo("%s applySetpoint: rejected, setpoint not defined for index %d", getName().c_str(), idx);
		}
	}
}
/*******************************************************************************
 *
 ******************************************************************************/
void ArmControl::controlInit(void)
{ 
	// Make sure every notor has current limiting. No magic smoke
	bool is_ready = true;

	if( wrist_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- wrist_motor", getName().c_str());
		is_ready = false;
	}

	if( shoulder_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- shoulder_motor", getName().c_str());
		is_ready = false;
	}

	if( extension_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- extension_motor", getName().c_str());
		is_ready = false;
	}

	if( m_wrist_cancoder == nullptr )
	{
		Advisory::pwarning("%s missing required component -- CANCoder", getName().c_str());
		is_ready = false;
	}

	active = is_ready;

	if (is_ready)
	{
		if (wrist_motor_max_current < 100.0)
		{
			wrist_motor->setCurrentLimit(wrist_motor_max_current);
			wrist_motor->setCurrentLimitEnabled(true);
		}
		if (shoulder_motor_max_current < 100.0)
		{
			shoulder_motor->setCurrentLimit(shoulder_motor_max_current);
			shoulder_motor->setCurrentLimitEnabled(true);
		}
		if (extension_motor_max_current < 100.0)
		{
			extension_motor->setCurrentLimit(extension_motor_max_current);
			extension_motor->setCurrentLimitEnabled(true);
		}
	}


	wrist_target_position = wrist_actual_position;
	extension_target_position = extension_actual_position;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArmControl::updateConfig()
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void ArmControl::disabledInit(void)
{
	// Be sure to set each motor to zero power when disabled
    wrist_motor_target_power = 0.0;
    wrist_motor_command_power = 0.0;
	shoulder_motor_target_power = 0.0;
    shoulder_motor_command_power = 0.0;
    extension_motor_target_power = 0.0;
    extension_motor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void ArmControl::autonomousInit(void)
{
	//Advisory::pinfo("Autonomous init!!!!!!!!!!!");
	// All motors should have zero power at start of autonomous 
      // so that the robot doesn't run away when you enable
    wrist_motor_target_power = 0.0;
    wrist_motor_command_power = 0.0;
	shoulder_motor_target_power = 0.0;
    shoulder_motor_command_power = 0.0;
    extension_motor_target_power = 0.0;
    extension_motor_command_power = 0.0;

    setClosedLoop(false);
	extension_target_position = extension_encoder_actual_position;
	shoulder_target_position = shoulder_calibrated_position;
	wrist_target_position = wrist_actual_position;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void ArmControl::teleopInit(void)
{
	// All motors should have zero power at start of teleop 
      // so that the robot doesn't run away when you enable
	wrist_motor_target_power = 0.0;
    wrist_motor_command_power = 0.0;
	shoulder_motor_target_power = 0.0;
    shoulder_motor_command_power = 0.0;
    extension_motor_target_power = 0.0;
    extension_motor_command_power = 0.0;

	setClosedLoop(false);
	extension_target_position = extension_encoder_actual_position;
	shoulder_target_position = shoulder_calibrated_position;
	wrist_target_position = wrist_actual_position;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void ArmControl::testInit(void)
{
}
/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void ArmControl::initLogFile(void)
{
	arm_log->openSegment();

	arm_log->log("%s, %s, %s, %s, %s, ",
                    "current_time",
                    "wrist_target_position",
					"wrist_actual_position",
					"extension_target_position",
					"extension_actual_position",
 					"wrist_motor_target_power",
					"wrist_motor_command_power",
					"shoulder_motor_target_power",
					"shoulder_motor_command_power"
					"extension_motor_target_power",
					"extension_motor_command_power");
    arm_log->log("\n");
    arm_log->flush();
}

/**********************************************************************
 *
 * This method is used to update the log files, called from
 * doPeriodic
 *
 **********************************************************************/
void ArmControl::updateLogFile(void)
{
	arm_log->log("%f, %f, %f, %f, %f, ",
                    gsi::Time::getTime(),
                    wrist_target_position,
					wrist_actual_position,
					extension_target_position,
					extension_actual_position,
                    wrist_motor_target_power, 
					wrist_motor_command_power,
					shoulder_motor_target_power, 
					shoulder_motor_command_power,
					extension_motor_target_power, 
					extension_motor_command_power);
    arm_log->log("\n");
    arm_log->flush();	
} 
/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void ArmControl::setAnalog(int id, float val)
{
	//Advisory::pinfo("setAnalog(%d, %f)", id, val);
	switch(id)
	{
		case CMD_ANALOG_SETPOINT:
		{
			applySetPoint(true, (int)val);
		} break;

		default:
		{
			// do nothing
		} break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void ArmControl::setDigital(int id, bool val)
{
	// Add commands as necessary
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_WRISTFORWARD:
		{
			if (val == true)
			{
				wrist_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, wrist_motor_forward_power);
			}
            else
			{
				wrist_motor_target_power = 0.0;
			}
		} break;
			
		case CMD_WRISTBACKWARD:
		{
			if (val == true)
			{
				wrist_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, wrist_motor_backward_power);
			}
            else
			{
				wrist_motor_target_power = 0.0;
			}
		} break;
			
		case CMD_STOP:
		{
			if (val == true)
			{
				wrist_motor_target_power = 0.0;
                shoulder_motor_target_power = 0.0;
                extension_motor_target_power = 0.0;
			}
		} break;

		case CMD_SHOULDERFORWARD:
		{
			if(val == true)
			{
				shoulder_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, shoulder_motor_forward_power);
			}
			else
			{
				shoulder_motor_target_power = 0.0;
			}
		} break;

		case CMD_SHOULDERBACKWARD:
		{
			if(val == true)
			{
				shoulder_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, shoulder_motor_backward_power);
			}
			else
			{
				shoulder_motor_target_power = 0.0;
			}
		} break;

        case CMD_EXTENSIONFORWARD:
		{
			if(val == true)
			{
				extension_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, extension_motor_forward_power);
		//Advisory::pinfo("Extension Forward: %.2f", extension_motor_target_power);
			}
			else
			{
				extension_motor_target_power = 0.0;
			}
		} break;

        case CMD_EXTENSIONBACKWARD:
		{
			if(val == true)
			{
				extension_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, extension_motor_backward_power);
		//Advisory::pinfo("Extension Backward: %.2f", extension_motor_target_power);
			}
			else
			{
				extension_motor_target_power = 0.0;
		//Advisory::pinfo("Extension Backward OFF!!!!!!!!!!!!: %.2f", extension_motor_target_power);
			}
		} break;

		case CMD_SETPOINT_0: 	applySetPoint(val, 0);  break;

		case CMD_SETPOINT_1:	applySetPoint(val, 1);	break;

		case CMD_SETPOINT_2:	applySetPoint(val, 2);	break;

		case CMD_CLOSED_LOOP_STATE: 
		{	
			setClosedLoop(!isClosedLoop()); // toggle
		} break;

		case CMD_CLOSED_LOOP_TOGGLE:
		{	
			// if pressed, set closed loop state to the opposite of its current state (true / false)
			if(val == true)
			{
				setClosedLoop(!isClosedLoop());
				wrist_target_position = wrist_actual_position;
			}
			
		} break;

		case CMD_CLOSED_LOOP_ON:
		{	
			if(val == true)
			{
				setClosedLoop(true);
			}
		} break;

		case CMD_CLOSED_LOOP_OFF:
		{
			if(val == true)
			{
				setClosedLoop(false);
			}
		} break;

		case CMD_NUDGE_WRIST_FWD:
        {
            if (val == true)
            {
    			wrist_motor_target_power = wrist_motor_forward_power;
    			wrist_target_position  = RobotUtil::limit( 
             		min_position_wrist, max_position_wrist,
             		wrist_target_position+wrist_nudge_forward_step);
            }
            else
            {
    			wrist_motor_target_power = 0.0;
            }
        } break;

        case CMD_NUDGE_WRIST_BCK:
        {
            if (val == true)
            {
    			wrist_motor_target_power = wrist_motor_backward_power;
    			wrist_target_position  = RobotUtil::limit( 
             		min_position_wrist, max_position_wrist,
             		wrist_target_position+wrist_nudge_backward_step);
            }
            else
            {
    			wrist_motor_target_power = 0.0;
            }
        } break;

		case CMD_NUDGE_SHOULDER_FWD:
        {
            if (val == true)
            {
    			shoulder_motor_target_power = shoulder_motor_forward_power;
    			shoulder_target_position  = RobotUtil::limit( 
             		min_position_shoulder, max_position_shoulder,
             		shoulder_target_position+shoulder_nudge_forward_step);
            }
            else
            {
    			shoulder_motor_target_power = 0.0;
            }
        } break;

        case CMD_NUDGE_SHOULDER_BCK:
        {
            if (val == true)
            {
    			shoulder_motor_target_power = shoulder_motor_backward_power;
    			shoulder_target_position  = RobotUtil::limit( 
             		min_position_shoulder, max_position_shoulder,
             		shoulder_target_position+shoulder_nudge_backward_step);
            }
            else
            {
    			shoulder_motor_target_power = 0.0;
            }
        } break;

		case CMD_NUDGE_EXTENSION_FWD:
        {
            if (val == true)
            {
    			extension_motor_target_power = extension_motor_forward_power;
    			extension_target_position  = RobotUtil::limit( 
             		min_position_extension, max_position_extension,
             		extension_target_position+extension_nudge_forward_step);
            }
            else
            {
    			extension_motor_target_power = 0.0;
            }
        } break;

        case CMD_NUDGE_EXTENSION_BCK:
        {
            if (val == true)
            {
    			extension_motor_target_power = extension_motor_backward_power;
    			extension_target_position  = RobotUtil::limit( 
             		min_position_extension, max_position_extension,
             		extension_target_position+extension_nudge_backward_step);
            }
            else
            {
    			extension_motor_target_power = 0.0;
            }
        } break;


		default:
		{
			Advisory::pcaution("setAnalog() was called for an unknown id, id=%d", id);
		} break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArmControl::setInt(int id, int val)
{
}

void ArmControl::setWristPower(float power)
{
	wrist_motor_target_power = power;
}
void ArmControl::setShoulderPower(float power)
{
	shoulder_motor_target_power = power;
}
void ArmControl::setExtensionPower(float power)
{
    extension_motor_target_power = power;
}
void ArmControl::setExtensionPosition(float position)
{
	extension_target_position = position;
}
float ArmControl::getPosition()
{
	return 0;
}
/*******************************************************************************
 * 
 ******************************************************************************/
void ArmControl::setClosedLoop(bool closed)
{
	Advisory::pinfo("in set closed loop");
	if (is_closed_loop != closed)
	{
		is_closed_loop = closed;

		if (is_closed_loop)
		{
		    // wrist_motor->setControlMode(Motor::POSITION);
			extension_motor->setControlMode(Motor::POSITION);

			if( wrist_pid == nullptr )
			{
				Advisory::pwarning("%s missing required component -- Wrist PID", getName().c_str());
				Advisory::pwarning("%s Cannot do closed loop control, returning to open loop", getName().c_str());
				is_closed_loop = false;
				wrist_motor->setControlMode(Motor::PERCENT);
				return;
			}

		    //Advisory::pinfo("setting %s closed loop mode", getName().c_str());

		}
		else
		{

           	//Advisory::pinfo("setting %s open loop mode", getName().c_str());
		}
	}
	//Advisory::pinfo("exiting closed loop");

}
/*******************************************************************************	
 *
 ******************************************************************************/
void ArmControl::publish()
{
	// Put whatever you want to see in here
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " wrist target power: ", wrist_motor_target_power);
	SmartDashboard::PutNumber(getName() + " wrist command_power: ", wrist_motor_command_power);
    SmartDashboard::PutBoolean(getName() + " wrist forward state: ", m_wristFwdLimitPressed);
    SmartDashboard::PutBoolean(getName() + " wrist back state: ", m_wristBackLimitPressed);
	SmartDashboard::PutNumber(getName() + " Wrist Motor Position Target (encoder): ", wrist_target_position);
	SmartDashboard::PutNumber(getName() + " Wrist Motor Position Actual (Encoder): ", wrist_actual_position);

	SmartDashboard::PutNumber(getName() + " shoulder target_power: ", shoulder_motor_target_power);
	SmartDashboard::PutNumber(getName() + " shoulder command_power: ", shoulder_motor_command_power);
	SmartDashboard::PutNumber(getName() + " extension target_power: ", extension_motor_target_power);
	SmartDashboard::PutNumber(getName() + " extension command_power: ", extension_motor_command_power);
	SmartDashboard::PutBoolean(getName() + " shoulder forward state: ", m_shoulderFwdLimitPressed);
    SmartDashboard::PutBoolean(getName() + " shoulder back state: ", m_shoulderBackLimitPressed);
	SmartDashboard::PutBoolean(getName() + " extension forward state: ", m_extensionFwdLimitPressed);
    SmartDashboard::PutBoolean(getName() + " extension back state: ", m_extensionBackLimitPressed);
	SmartDashboard::PutBoolean(getName() + " shoulder forward state: ", shoulder_fwd_limit_state);
    SmartDashboard::PutBoolean(getName() + " shoulder back state: ", shoulder_back_limit_state);
	SmartDashboard::PutBoolean(getName() + " Closed Loop: ", is_closed_loop);
	SmartDashboard::PutNumber(getName() + " Extension Motor Position Target (in): ", extension_target_position);
	SmartDashboard::PutNumber(getName() + " Extension Motor Position Actual (Encoder): ", extension_encoder_actual_position);
	SmartDashboard::PutNumber(getName() + " Extension Motor Position Raw (Encoder): ", extension_encoder_raw_position);
	//SmartDashboard::PutNumber(getName() + " Extension Motor Current: ", extension_motor->getSupplyCurrent());
	SmartDashboard::PutNumber(getName() + " Shoulder Motor Position Calibrated (Pot Sensor): ", shoulder_calibrated_position);
	SmartDashboard::PutNumber(getName() + " Shoulder Motor Position Raw (Pot Sensor): ", shoulder_raw_position);
	SmartDashboard::PutNumber(getName() + " Shoulder Motor Position Error (Encoder): ", shoulder_position_error);
	SmartDashboard::PutNumber(getName() + " Shoulder Motor Position Target (degrees -120->120): ", shoulder_target_position);
	SmartDashboard::PutNumber(getName() + " Wrist Motor Position Raw (raw): ", m_wrist_cancoder->getAbsoluteAngle() + 180.0);

}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void ArmControl::doPeriodic()
{
	if(active == false)
	{
		Advisory::pinfo("DoPeriodic is not ready");
		return;
	}

	// Update all motors
	wrist_motor->doUpdate();
	shoulder_motor->doUpdate();
    extension_motor->doUpdate();
	
	//
	// Get inputs -- this is just for reporting
	//
	extension_encoder_actual_position = extension_motor->getPosition();
	extension_encoder_raw_position = extension_motor->getRawPosition();

	shoulder_calibrated_position = shoulder_pos_sensor->getPosition();
	shoulder_raw_position = shoulder_pos_sensor->getRaw();

	wrist_actual_position = m_wrist_cancoder->getAbsoluteAngle() + 180.0;
 
    // if (wrist_actual_position < 180.0)
	// {
	// 	wrist_actual_position -= 360.0;
	// }
	// else if (wrist_actual_position < -180)
	// {
	// 	wrist_actual_position += 360.0;
	// }
	wrist_actual_position += wrist_offset_angle;
 
	m_wristFwdLimitPressed = wrist_motor->isUpperLimitPressed(); // TODO fix logic, why coming backwards off the hardware?
	m_wristBackLimitPressed = wrist_motor->isLowerLimitPressed();

	m_shoulderFwdLimitPressed = shoulder_motor->isUpperLimitPressed(); // TODO fix logic, why coming backwards off the hardware?
	m_shoulderBackLimitPressed = shoulder_motor->isLowerLimitPressed();

	m_extensionFwdLimitPressed = extension_motor->isUpperLimitPressed(); // TODO fix logic, why coming backwards off the hardware?
	m_extensionBackLimitPressed = extension_motor->isLowerLimitPressed();
	
	//shoulder_fwd_limit_state = !m_limit_shoulder_fwd->isPressed(); // TODO fix logic, why coming backwards off the hardware?
	//shoulder_back_limit_state = !m_limit_shoulder_back->isPressed();

	if (m_extensionBackLimitPressed == true)
	{
		extension_motor->resetPosition();
		arm_homed = true;
		home_count = 1000;
	}

	/*

	if (shoulder_fwd_limit_state == true)
	{
		shoulder_motor_command_power = gsu::Filter::limit(shoulder_motor_command_power, -0.5, 0.0);
	}

	if (shoulder_back_limit_state == true)
	{
        shoulder_motor_command_power = gsu::Filter::limit(shoulder_motor_command_power, 0.0, 0.5);
	}
	*/
	//Psudocode for a built in software limit on the wrist/shoulder/extension motors
	//if (hood_actual_pos < 12.5) hood_command_power = gsu::Filter::limit(hood_command_power, -1.0, 0.0);
	//if (hood_actual_pos > 29.25) hood_command_power = gsu::Filter::limit(hood_command_power,  0.0, 1.0);

	//
	// All processing happens in the motor class
	//

	//
	// Set Outputs
	//

 	
	wrist_position_error = wrist_target_position - wrist_actual_position;
	shoulder_position_error = shoulder_target_position - shoulder_calibrated_position;

	if (is_closed_loop)// Command the position
	{
		if ((! arm_homed)  && (home_count < 50))
		{
			home_count++;
             extension_target_position = extension_encoder_actual_position - 0.1;
		}

		wrist_motor_command_power = gsu::Filter::step(wrist_motor_command_power, 0.25, 
		    wrist_pid->calculateCommand(wrist_target_position, wrist_actual_position));
		
		shoulder_motor_command_power = gsu::Filter::step(shoulder_motor_command_power, 0.25,
		    shoulder_pid->calculateCommand(shoulder_target_position, shoulder_calibrated_position));		
	}
	else
	{
    	if ((! arm_homed)  && (home_count < 50))
		{
			home_count++;
            extension_motor_command_power = extension_motor_backward_power;
		}
		else
		{
			extension_motor_command_power = extension_motor_target_power;
		}

		wrist_motor_command_power = wrist_motor_target_power;
		shoulder_motor_command_power = shoulder_motor_target_power;
	}



   	if (m_wristFwdLimitPressed == true)
	{
		wrist_motor_command_power = gsu::Filter::limit(wrist_motor_command_power, -1.0, 0.0);
	}

	if (m_wristBackLimitPressed == true)
	{
        wrist_motor_command_power = gsu::Filter::limit(wrist_motor_command_power, 0.0, 1.0);
	}

	if (m_shoulderFwdLimitPressed == true)
	{
		shoulder_motor_command_power = gsu::Filter::limit(shoulder_motor_command_power, -1.0, 0.0);
	}

	if (m_shoulderBackLimitPressed == true)
	{
        shoulder_motor_command_power = gsu::Filter::limit(shoulder_motor_command_power, 0.0, 1.0);
	}

	if (m_extensionFwdLimitPressed == true)
	{
		extension_motor_command_power = gsu::Filter::limit(extension_motor_command_power, -1.0, 0.0);
        extension_target_position  = RobotUtil::limit(min_position_extension, max_position_extension,
             		extension_target_position);
	}
	if (m_extensionBackLimitPressed == true)
	{
        extension_motor_command_power = gsu::Filter::limit(extension_motor_command_power, 0.0, 1.0);
        extension_target_position  = RobotUtil::limit(min_position_extension, max_position_extension,
             		extension_target_position);
	}


	if (is_closed_loop)// Command the position
	{	
		wrist_motor->setPercent(wrist_motor_command_power);
		extension_motor->setPosition(extension_target_position);
		shoulder_motor->setPercent(shoulder_motor_command_power);
	}
	else
	{
		wrist_motor->setPercent(wrist_motor_command_power);
		extension_motor->setPercent(extension_motor_command_power);
		shoulder_motor->setPercent(shoulder_motor_command_power);
	}

	//updateLogFile();

}

/*******************************************************************************
 *
 ******************************************************************************/
MSWrist::MSWrist(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	wrist_power = 0.3;

	m_parent_control = (ArmControl *)control;

	xml->QueryFloatAttribute("power", &wrist_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSWrist::init(void)
{
	m_parent_control->setWristPower(wrist_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSWrist::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShoulder::MSShoulder(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	shoulder_power = 0.3;

	m_parent_control = (ArmControl *)control;

	xml->QueryFloatAttribute("power", &shoulder_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShoulder::init(void)
{
	m_parent_control->setShoulderPower(shoulder_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShoulder::update(void)
{
	return next_step;
}
/*******************************************************************************
 *
 ******************************************************************************/
MSExtension::MSExtension(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	extension_power = 0.3;

	m_parent_control = (ArmControl *)control;

	xml->QueryFloatAttribute("power", &extension_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSExtension::init(void)
{
	m_parent_control->setExtensionPower(extension_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSExtension::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSChooseSetpoint::MSChooseSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_parent_control = (ArmControl *)control;
	xml->QueryIntAttribute("setpoint_index", &setpoint_index);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSChooseSetpoint::init(void)
{
	//Advisory::pinfo("Macrostep init!!!!! %d !!!!", setpoint_index );
	m_parent_control->applySetPoint(true, setpoint_index);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSChooseSetpoint::update(void)
{
	return next_step;
}