/*******************************************************************************
 *
 * File: SummerIntake.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "SummerIntake.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
SummerIntake::SummerIntake(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Summer Intake Control [☀️] =========================",
	            control_name.c_str());

	rollers = nullptr;
	deploy = nullptr;

	roller_in_power = 0.0;
	roller_out_power = 0.0;
	roller_target_power = 0.0;
	roller_command_power = 0.0;

	deploy_forward_power = 0.0;
	deploy_backward_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSSetRollerPower>(control_name, "SetRollerPower", this);
	new MacroStepProxy<MSSetDeployPower>(control_name, "SetDeployPower", this);

	//
	// Parse XML
	//

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "rollers") == 0)
			{
				Advisory::pinfo("  creating speed controller for roller motor");
				rollers = HardwareFactory::createMotor(comp);
			}
			else if(strcmp(name, "deploy") == 0)
			{
    			Advisory::pinfo("  creating speed controller for deploy motor");
				deploy = HardwareFactory::createMotor(comp);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
	
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "roller_in") == 0)
			{
				Advisory::pinfo("  connecting roller in channel");
				comp->QueryFloatAttribute("value", &roller_in_power);
				OIController::subscribeDigital(comp, this, CMD_ROLLER_IN);
			}
			else if (strcmp(name, "roller_out") == 0)
			{
				Advisory::pinfo("  connecting roller out channel");
				comp->QueryFloatAttribute("value", &roller_out_power);
				OIController::subscribeDigital(comp, this, CMD_ROLLER_OUT);
			}
			else if (strcmp(name, "deploy_forward") == 0)
			{
				Advisory::pinfo("  connecting deploy forward channel");
				comp->QueryFloatAttribute("value", &deploy_forward_power);
				OIController::subscribeDigital(comp, this, CMD_DEPLOY_FORWARD);
			}
			else if (strcmp(name, "deploy_backward") == 0)
			{
				Advisory::pinfo("  connecting deploy backward channel");
				comp->QueryFloatAttribute("value", &deploy_backward_power);
				OIController::subscribeDigital(comp, this, CMD_DEPLOY_BACKWARD);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
		
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
SummerIntake::~SummerIntake(void)
{
	if (rollers != nullptr)
	{
		delete rollers;
		rollers = nullptr;
	}
	if (deploy != nullptr)
	{
		delete deploy;
		deploy = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void SummerIntake::controlInit(void)
{ 
	roller_target_power = 0.0;
	roller_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;	
}

/*******************************************************************************
 *
 ******************************************************************************/
void SummerIntake::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void SummerIntake::disabledInit(void)
{
	roller_target_power = 0.0;
	roller_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;	
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void SummerIntake::autonomousInit(void)
{
	roller_target_power = 0.0;
	roller_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;	
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void SummerIntake::teleopInit(void)
{
	roller_target_power = 0.0;
	roller_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;	
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void SummerIntake::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void SummerIntake::setAnalog(int id, float val)
{
	// Add commands as necessary
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void SummerIntake::setDigital(int id, bool val)
{
	// Add commands as necessary
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_ROLLER_IN:
		{
			if (val)
			{
				roller_target_power = roller_in_power;
			}
			else
			{
				roller_target_power = 0.0;
			}
		} break;
			
		case CMD_ROLLER_OUT:
		{
			if (val)
			{
				roller_target_power = roller_out_power;
			}
			else
			{
				roller_target_power = 0.0;
			}
		} break;
			
		case CMD_DEPLOY_FORWARD:
		{
			if (val)
			{
				deploy_target_power = deploy_forward_power;
			}
			else
			{
				deploy_target_power = 0.0;
			}
		} break;

		case CMD_DEPLOY_BACKWARD:
		{
			if(val)
			{
				deploy_target_power = deploy_backward_power;
			}
			else
			{
				deploy_target_power = 0.0;
			}
		} break;

		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void SummerIntake::setInt(int id, int val)
{
}

void SummerIntake::setRollerPower(float power)
{
	roller_target_power = power;
}

void SummerIntake::setDeployPower(float power)
{
	deploy_target_power = power;
}
/*******************************************************************************	
 *
 ******************************************************************************/
void SummerIntake::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " roller target_power: ", roller_target_power);
	SmartDashboard::PutNumber(getName() + " roller command_power: ", roller_command_power);
	SmartDashboard::PutNumber(getName() + " deploy target_power: ", deploy_target_power);
	SmartDashboard::PutNumber(getName() + " deploy command_power: ", deploy_command_power);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void SummerIntake::doPeriodic()
{
	// Update all motors
	rollers->doUpdate();
	deploy->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	roller_command_power = rollers->getPercent();
	deploy_command_power = deploy->getPercent();

	//
	// All processing happens in the motor class
	//

	//
	// Set Outputs
	//

	rollers->setPercent(roller_target_power);
	deploy->setPercent(deploy_target_power);

}

/*******************************************************************************
 *
 ******************************************************************************/
MSSetRollerPower::MSSetRollerPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	power = 0.0;
	m_parent_control = (SummerIntake *)control;

	xml->QueryFloatAttribute("power", &power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetRollerPower::init(void)
{
	m_parent_control->setRollerPower(power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSSetRollerPower::update(void)
{
	return next_step;
}


/*******************************************************************************
 *
 ******************************************************************************/
MSSetDeployPower::MSSetDeployPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	power = 0.0;
	m_parent_control = (SummerIntake *)control;

	xml->QueryFloatAttribute("power", &power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetDeployPower::init(void)
{
	m_parent_control->setDeployPower(power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSSetDeployPower::update(void)
{
	return next_step;
}
