/*******************************************************************************
 *
 * File: Intake.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Time.h"
#include "Intake.h"
#include "rfutilities/MacroStepFactory.h"
#include "frc/DoubleSolenoid.h"
#include "frc/smartdashboard/SmartDashboard.h" //WPI
#include "frc/Solenoid.h" //TEST
#include "frc/PneumaticsControlModule.h" //TEST

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
Intake::Intake(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Intake Control [%s] =========================",
	            control_name.c_str());

	// Be sure to initialize all method variables
	// I left a lot of the old names. Get rid of what you don't use
	motor1 = nullptr;
	motor_min_control = -1.0;
	motor_max_control = 1.0;

    // motor_max_current = 40.0;  /// Please, please, please lets pick good numbers

	motor1_target_power = 0.0;
	motor1_command_power = 0.0;

	motor1_roller_in_max_power = 0.0;
	motor1_roller_out_max_power = 0.0;
	current_power = 0.0;
	intakePosition = nullptr;

	piecePosition = OFF_POS;

	motor_max_cmd_delta = 0.25;

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	// new MacroStepProxy<MSIntakePower>(control_name, "IntakePower", this);
	new MacroStepProxy<MSRollerPower>(control_name, "RollerPower", this);
	new MacroStepProxy<MSCubePosition>(control_name, "CubePosition", this);
	new MacroStepProxy<MSConePosition>(control_name, "ConePosition", this);

	//
	// Parse XML
	//
	
    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &motor_max_cmd_delta);
    Advisory::pinfo("  max cmd delta = %f", motor_max_cmd_delta);

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "intake_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for intake motor");
				motor1 = HardwareFactory::createMotor(comp);
				// comp->QueryUnsignedAttribute("max_current", &motor_max_current);
				// Advisory::pinfo("	max_current=%u", motor_max_current); 
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}

	comp = xml->FirstChildElement("double_solenoid");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "game_piece") == 0)
			{
				Advisory::pinfo("  creating game piece double solenoid for %s", name);
				intakePosition = HardwareFactory::createDoubleSolenoid(comp);
			}
			else
			{
				Advisory::pwarning("  found solenoid with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found solenoid with no name attribute");
		}
		comp = comp->NextSiblingElement("double_solenoid");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "roller_in") == 0)
			{
				Advisory::pinfo("  connecting roller in channel");
				comp->QueryFloatAttribute("value", &motor1_roller_in_max_power);
				OIController::subscribeDigital(comp, this, CMD_ROLLER_IN);
			}
			else if (strcmp(name, "roller_out") == 0)
			{
				Advisory::pinfo("  connecting roller out channel");
				comp->QueryFloatAttribute("value", &motor1_roller_out_max_power);
				current_power = motor1_roller_out_max_power;
				OIController::subscribeDigital(comp, this, CMD_ROLLER_OUT);
			}
			else if (strcmp(name, "roller_out_fast") == 0)
			{
				Advisory::pinfo("  connecting roller out fast channel");
				OIController::subscribeDigital(comp, this, CMD_ROLLER_OUT_FAST);
			}
			else if (strcmp(name, "off_position") == 0)
			{
				Advisory::pinfo("  connecting stop channel");
				OIController::subscribeDigital(comp, this, CMD_OFF);
			}
			else if (strcmp(name, "cube_position") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_CUBE);
			}
			else if (strcmp(name, "cone_position") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_CONE);
			}
			else if (strcmp(name, "toggle_position") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_TOGGLE);
			}
			else if (strcmp(name, "toggle_speed") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_TOGGLE_SPEED);
			}
			
		}
		
		comp = comp->NextSiblingElement("oi");

	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
Intake::~Intake(void)
{
	// Be sure to reference each motor
	if (motor1 != nullptr)
	{
		delete motor1;
		motor1 = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::controlInit(void)
{ 
	// Make sure every motor has current limiting. No magic smoke
	/*
    if ((motor1 != nullptr) && (motor_max_current < 100.0))
    {
        motor1->setCurrentLimit(motor_max_current);
        motor1->setCurrentLimitEnabled(true);
    }
	if ((deploy_motor != nullptr) && (motor_max_current < 100.0))
    {
        motor1->setCurrentLimit(motor_max_current);
        motor1->setCurrentLimitEnabled(true);
    }
	*/
}


/*******************************************************************************
 *
 ******************************************************************************/
void Intake::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::disabledInit(void)
{
	// Be sure to set each motor to zero power when disabled
	motor1_target_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::autonomousInit(void)
{
	// All motors should have zero power at start of autonomous 
      // so that the robot doesn't run away when you enable
	motor1_target_power = 0.0;
	motor1_command_power = 0.0;
	setConePosition(); // TODO make this an xml input
	
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::teleopInit(void)
{
	// All motors should have zero power at start of teleop 
      // so that the robot doesn't run away when you enable
	
	motor1_target_power = 0.0;
	motor1_command_power = 0.0;
	
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void Intake::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Intake::setAnalog(int id, float val)
{
	// Add commands as necessary
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void Intake::setDigital(int id, bool val)
{
	// Add commands as necessary
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_ROLLER_IN:
		{
			if (val)
			{
				motor1_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor1_roller_in_max_power);
			}
			else
			{
				motor1_target_power = 0.0;
			}
		} break;
			
		case CMD_ROLLER_OUT:
		{
			if (val)
			{
				motor1_target_power = RobotUtil::limit(motor_min_control, motor_max_control, current_power);
			}
			else
			{
				motor1_target_power = 0.0;
			}
		} break;

		case CMD_ROLLER_OUT_FAST:
		{
			if (val)
			{
				motor1_target_power = -1.0;
			}
			else
			{
				motor1_target_power = 0.0;
			}
		} break;
			
		case CMD_OFF:
		{
			if (val)
			{
				motor1_target_power = 0.0;
			}
		} break;

		case CMD_CUBE:
		{
			if(val)
			{
				setCubePosition();
				piecePosition = CUBE_POS;
			}
		} break;

		case CMD_CONE:
		{
			if(val)
			{
				setConePosition();
				piecePosition = CONE_POS;
			}
		} break;

		case CMD_TOGGLE:
		{
			if(val)
			{
				togglePosition();
			}
		} break;

		case CMD_TOGGLE_SPEED:
		{
			if(val)
			{
				if (current_power == motor1_roller_out_max_power)
				{
					current_power = -1.0;
				}
				else if (current_power == -1.0)
				{
					current_power = motor1_roller_out_max_power;
				}
			}
		} break;
			
		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::setInt(int id, int val)
{
}

void Intake::setRollerPower(float power)
{
	motor1_target_power = power;
}
void Intake::setConePosition()
{
	if(intakePosition != nullptr)
	{
		intakePosition->Set((frc::DoubleSolenoid::Value) CONE_POS);
		//Advisory::pinfo("========================= setConePosition: %d", (PiecePositionType) intakePosition->Get());
	}
}
void Intake::setCubePosition()
{
	if(intakePosition != nullptr)
	{
		intakePosition->Set((frc::DoubleSolenoid::Value) CUBE_POS);
	}
}
void Intake::togglePosition()
{
	/*
	PiecePositionType currentPosition = (PiecePositionType) intakePosition->Get();
	if (currentPosition == CUBE_POS) {
		piecePosition = CONE_POS;
	}
	else if (currentPosition == CONE_POS) {
		piecePosition = CUBE_POS;
	}
	else {
		piecePosition = CONE_POS;
	}
	*/
	

	// intakePosition->Toggle();
}

/*******************************************************************************	
 *
 ******************************************************************************/
void Intake::publish()
{
	// Put whatever you want to see in here
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " intake target_power: ", motor1_target_power);
	SmartDashboard::PutNumber(getName() + " intake command_power: ", motor1_command_power);
}

/*******************************************************************************	
 *
 ******************************************************************************/
/*
void Intake::setSolenoidState(bool state)
{
	piecePosition = state;
}
*/

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Intake::doPeriodic()
{
	// Update all motors
	motor1->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	motor1_command_power = motor1->getPercent();

	//
	// All processing happens in the motor class
	//

	//
	// Set Outputs
	//

	motor1->setPercent(motor1_target_power);
}

/*******************************************************************************
 *
 ******************************************************************************/

MSIntakePower::MSIntakePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}


/*******************************************************************************
 *
 ******************************************************************************/
void MSIntakePower::init(void)
{

}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSIntakePower::update(void)
{
	// if (gsi::Time::getTime() < deploy_end_time)
	// {
		return this;
	// }

	//parent_control->setRollerPower(0.0);
	return next_step;
}




/*******************************************************************************
 *
 ******************************************************************************/
MSRollerPower::MSRollerPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	// motor1_roller_power = 0.0;
	parent_control = (Intake *)control;

	// xml->QueryFloatAttribute("power", &motor1_roller_power);
	xml->QueryFloatAttribute("roller_power", &motorPower);
}

/*******************************************************************************
 *
 ******************************************************************************/

void MSRollerPower::init(void)
{
	parent_control->setRollerPower(motorPower);
}


/*******************************************************************************
 *
 ******************************************************************************/

MacroStep * MSRollerPower::update(void)
{
	return next_step;
}




/*******************************************************************************
 *
 ******************************************************************************/
MSCubePosition::MSCubePosition(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/

void MSCubePosition::init(void)
{
	//Advisory::pinfo("========================= SETTING CUBE POSITION!! =========================");
	parent_control->setCubePosition();
}


/*******************************************************************************
 *
 ******************************************************************************/

MacroStep * MSCubePosition::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSConePosition::MSConePosition(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	//Advisory::pinfo("========================= CONE POSITION IS WORKING!! =========================");
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/

void MSConePosition::init(void)
{
	parent_control->setConePosition();
}


/*******************************************************************************
 *
 ******************************************************************************/

MacroStep * MSConePosition::update(void)
{
	return next_step;
}
