#ifndef ENDGAME
/*******************************************************************************
 *
 * File: Endgame.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "Endgame.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
Endgame::Endgame(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating ENDGAME Control [%s] =========================",
	            control_name.c_str());

	// Be sure to initialize all method variables
	// I left a lot of the old names. Get rid of what you don't use
	m_motor1 = nullptr;

	motor1_min_control = -1.0;
	motor1_max_control = 1.0;

    motor1_max_current = 10.0;  /// Please, please, please lets pick good numbers

	motor1_forward_power = 0.0;
	motor1_backward_power = 0.0;

	motor1_target_power = 0.0;
	motor1_command_power = 0.0;

	const char *name = nullptr;

	//
	// Parse XML
	//
	
    xml->QueryFloatAttribute("min_control", &motor1_min_control);
    xml->QueryFloatAttribute("max_control", &motor1_max_control);

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "endgame_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for endgame motor");
				m_motor1 = HardwareFactory::createMotor(comp);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				comp->QueryFloatAttribute("value", &motor1_forward_power);
				OIController::subscribeDigital(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "backward") == 0)
			{
				Advisory::pinfo("  connecting backward channel");
				comp->QueryFloatAttribute("value", &motor1_backward_power);
				OIController::subscribeDigital(comp, this, CMD_BACKWARD);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
		
	}
}


/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
Endgame::~Endgame(void)
{
	// Be sure to reference each motor
	if (m_motor1 != nullptr)
	{
		delete m_motor1;
		m_motor1 = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Endgame::controlInit(void)
{ 
	// Make sure every notor has current limiting. No magic smoke

    if ((m_motor1 != nullptr) && (motor1_max_current < 100.0))
    {
        m_motor1->setCurrentLimit(motor1_max_current);
        m_motor1->setCurrentLimitEnabled(true);
    }
	
}

/*******************************************************************************
 *
 ******************************************************************************/
void Endgame::updateConfig(void)
{
	// NOTE unused
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Endgame::disabledInit(void)
{
	motor1_target_power = 0.0;
	motor1_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Endgame::autonomousInit(void)
{
	motor1_target_power = 0.0;
	motor1_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Endgame::teleopInit(void)
{
	motor1_target_power = 0.0;
	motor1_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void Endgame::testInit(void)
{
	motor1_target_power = 0.0;
	motor1_command_power = 0.0;
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Endgame::setAnalog(int id, float val)
{
	// NOTE n/a
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void Endgame::setDigital(int id, bool val)
{
	// Add commands as necessary
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_FORWARD:
		{
			if (val)
			{
				motor1_target_power = motor1_forward_power;
			}
			else
			{
				motor1_target_power = 0.0;
			}
		} break;
			
		case CMD_BACKWARD:
		{
			if (val)
			{
				motor1_target_power = motor1_backward_power;
			}
			else
			{
				motor1_target_power = 0.0;
			}
		} break;

		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Endgame::setInt(int id, int val)
{
	// NOTE n/a
}

/*******************************************************************************	
 *
 ******************************************************************************/
void Endgame::publish()
{
	// Put whatever you want to see in here
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " endgame target_power: ", motor1_target_power);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Endgame::doPeriodic()
{
	// Update all motors
	m_motor1->doUpdate();

	//
	// Set Outputs
	//

	m_motor1->setPercent(motor1_target_power);

}
#endif