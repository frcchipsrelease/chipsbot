/*******************************************************************************
 *
 * File: SwerveModule.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include "2023/SwerveModule.h"

#include "gsinterfaces/Time.h"
#include "gsutilities/Advisory.h"

#include "rfhardware/HardwareFactory.h"
#include "rfutilities/RobotUtil.h"

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a swerve module and connect it to the specified motors
 * and sensor/s
 * 
 ******************************************************************************/
SwerveModule::SwerveModule(XMLElement* xml)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Swerve Module ⛅ =========================");

	// Be sure to initialize all method variables
	drive_motor = nullptr;
    steering_motor = nullptr;

	wheel_diam = 0.0;
    drive_encoder_scale = 0.0; // ☀ swerve base uses mk4i on lowest (fastest) (6.12:1) gear setting,
							 // fission uses mk3 on highest (slowest) (8.16:1) setting,
							 // 2024 robot will use mk4i on highest (slowest) (8.14:1) setting
    steering_encoder_scale = 0.0;

    drive_target_power = 0.0;
    steering_target_power = 0.0;
    drive_command_power = 0.0;
    steering_command_power = 0.0;

    drive_target_velocity = 0.0;
    steering_target_velocity = 0.0;
    drive_command_velocity = 0.0;
    steering_command_velocity = 0.0;

	drive_target_position = 0.0;
	drive_actual_position = 0.0;
	steering_target_position = 0.0;
	steering_actual_position = 0.0;

    cancoder = nullptr;
	cc_actual = 0.0;
	cc_target = 0.0;
	cc_error = 0.0;

    steering_pid = nullptr;

	const char *name = nullptr;

	//
	// Parse XML
	//
	
	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "drive") == 0)
			{
				Advisory::pinfo("  creating speed controller for drive motor");
				drive_motor = HardwareFactory::createMotor(comp);
                comp->QueryFloatAttribute("wheel_diam", &wheel_diam); // inches
				Advisory::pinfo("wheel diam = %f", wheel_diam);
                comp->QueryFloatAttribute("scale", &drive_encoder_scale);
				comp->QueryFloatAttribute("gear_ratio", &drive_gear_ratio);
				if (drive_gear_ratio != 0.0)
				{
					// assuming falcon 500
					drive_encoder_scale = (wheel_diam * M_PI) / (2048 * drive_gear_ratio);
				}
                drive_motor->setSensorScale(drive_encoder_scale, 0.0);
			}
			else if(strcmp(name, "steering") == 0)
			{
    			Advisory::pinfo("  creating speed controller for steering motor");
				steering_motor = HardwareFactory::createMotor(comp);
                comp->QueryFloatAttribute("scale", &steering_encoder_scale);
				comp->QueryFloatAttribute("gear_ratio", &steering_gear_ratio);
				if (steering_gear_ratio != 0.0)
				{
					// assuming falcon 500
					steering_encoder_scale = 360 / (2048 * steering_gear_ratio);
				}
				Advisory::pinfo("steering encoder scale = %f", steering_encoder_scale);
				Advisory::pinfo("2048 ticks = %f degrees", 2048*steering_encoder_scale);
                steering_motor->setSensorScale(steering_encoder_scale, 0.0);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
    
    comp = xml-> FirstChildElement("cancoder");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating cancoder");
		cancoder = HardwareFactory::createCancoder(comp);
    }
	
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
SwerveModule::~SwerveModule(void)
{
	if (drive_motor != nullptr)
	{
		delete drive_motor;
		drive_motor = nullptr;
	}
	if (steering_motor != nullptr)
	{
		delete steering_motor;
		steering_motor = nullptr;
	}
	if (cancoder != nullptr)
	{
		delete cancoder;
		cancoder = nullptr;
	}
}

/*******************************************************************************
 * Getters
 ******************************************************************************/
float SwerveModule::getDrivePower(void)
{
	drive_command_power = drive_motor->getPercent();
	return drive_command_power;
}
float SwerveModule::getDriveTargetPower(void)
{
	return drive_target_power;
}
float SwerveModule::getSteeringPower(void)
{
	steering_command_power = steering_motor->getPercent();
	return steering_command_power;
}
float SwerveModule::getDrivePosition(void)
{
    drive_actual_position = drive_motor->getPosition(); // in
	return drive_actual_position;
}
float SwerveModule::getSteeringPosition(void)
{
    steering_actual_position = steering_motor->getPosition(); // deg
	return steering_actual_position;
}
float SwerveModule::getSteeringPositionTarget(void)
{
	return steering_target_position;
}
float SwerveModule::getSteeringPositionError(void)
{
	return cc_error;
}
float SwerveModule::getDriveVelocity(void)
{
    drive_command_velocity = drive_motor->getVelocity(); // in/s
	return drive_command_velocity;
}
float SwerveModule::getSteeringVelocity(void)
{
    steering_command_velocity = steering_motor->getVelocity(); // deg/s
	return steering_command_velocity;
}
/**
 * Called at the beginning of a match. 
 * Assuming robot is facing straight forward away from alliance wall, 
 * and all wheels are facing straight forward
 */
void SwerveModule::resetEncoders(void)
{
	Advisory::pinfo("module reset encoders");
    drive_motor->resetPosition();
	drive_actual_position = 0.0;
	drive_target_position = drive_actual_position;
	Advisory::pinfo("module Drive pos = %f", drive_actual_position);
	steering_motor->resetPosition();
	steering_actual_position = 0.0;
	steering_target_position = steering_actual_position;
	Advisory::pinfo("module steering pos = %f", steering_actual_position);
	cc_actual = cancoder->getAbsoluteAngle();
	cc_target = cc_actual;
	cc_error = 0.0;
}
/**
 * Returns the module's absolute position *in degrees*.
 */
float SwerveModule::getEncoderPosition(void)
{
    cc_actual = cancoder->getAbsoluteAngle();
	return cc_actual;
}

float SwerveModule::getEncoderPositionTarget(void)
{
	return cc_target;
}

/*******************************************************************************
 * Setters
 ******************************************************************************/
void SwerveModule::setDrivePower(float pwr)
{
	drive_target_power = pwr;
	drive_motor->setPercent(drive_target_power);
}
void SwerveModule::setSteeringPower(float pwr)
{
	steering_target_power = pwr;
	steering_motor->setPercent(steering_target_power);
}
void SwerveModule::setDriveVelocity(float vel)
{
	drive_target_velocity = vel;
	drive_motor->setVelocity(drive_target_velocity);
}
/********************************************************************************
 * @param pos Target position (absolute) [-180.0, 180.0)
********************************************************************************/
void SwerveModule::setSteeringPosition(float pos)
{
	steering_actual_position = steering_motor->getPosition();
	cc_actual = cancoder->getAbsoluteAngle();

	cc_target = pos;
	cc_error = cc_target - cc_actual;

	// cmd motor to Steering position + cc error
	steering_target_position = steering_actual_position + cc_error;
	steering_motor->setPosition(steering_target_position);
}


/********************************************************************************
 * @param pwr Target drive power [0.0, 1.0]
 * @param pos Target wheel position (absolute) [-180.0, 180.0)
********************************************************************************/
void SwerveModule::set(float pwr, float pos)
{
	float pwr_reverse = 1.0;
	steering_actual_position = steering_motor->getPosition();
	cc_actual = cancoder->getAbsoluteAngle();

	cc_target = pos;
	cc_error = cc_target - cc_actual;


	// optimize!
	// minimize the distance the wheel needs to rotate
	if (cc_error > 180.0)
	{
		cc_error -= 360.0;
	}
	else if (cc_error < -180.0)
	{
		cc_error += 360.0;
	}
	if (cc_error == -180.0 || cc_error == 180.0)
	{
		// instead of turning the wheel 180 deg, just drive the other way
		cc_error = 0.0;
		pwr_reverse = -1.0;
	}
	// the wheel should never turn more than 90 deg
	if (cc_error > 90.0)
	{
		cc_error -= 180.0;
		pwr_reverse = -1.0;
	}
	else if (cc_error < -90.0)
	{
		cc_error += 180.0;
		pwr_reverse = -1.0;
	}
	

	// cmd motor to Steering position + cc error
	cc_target = cc_error + cc_actual; // for reporting
	steering_target_position = steering_actual_position + cc_error;
	steering_motor->setPosition(steering_target_position);
	
	// cmd motor to Drive pwr * -1.0 (if reversed) or * 1.0
	drive_target_power = pwr * pwr_reverse;
	drive_motor->setPercent(drive_target_power);

}



/*******************************************************************************
 * Other
 ******************************************************************************/
void SwerveModule::update(void)
{
	drive_motor->doUpdate();
	steering_motor->doUpdate();
}