/*******************************************************************************
 *
 * File: LightsControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
//#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" // WPI

// The corresponding .h file
#include "2023/LightsControl.h"

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
LightsControl::LightsControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating LightsControl =========================",
	            control_name.c_str());

	led_strip = nullptr;
	m_led_length = 10;
	front_length = 4;
	back_length = 4;

	port = 0;
	move_count = 0;
	selected_pattern_idx = 0;
    autonomous_pattern = 0;
    disabled_pattern = 0;

	ColorPalette.resize(17, std::vector<int>(3)); // 16 custom colors + black
	pattern_type.resize(16);
	color1_idx.resize(16);
	color2_idx.resize(17);

	const char *name = nullptr;
	int r = 0, g = 0, b = 0;
	int color1 = 0, color2 = 0;


	//
	// Register Macro Steps - these are specific functions that are used in autonomous.
	//
    new MacroStepProxy<MSSetPattern>(control_name, "SetLightsPattern", this);


	//
	// Parse XML
	//
	
	comp = xml-> FirstChildElement("led_strip");
	if (comp != nullptr)
	{
		XMLElement* led_comp;
		comp->QueryIntAttribute("length", &m_led_length);
		Advisory::pinfo("  LED Length: %d", m_led_length);
		comp->QueryIntAttribute("front_length", &front_length);
		Advisory::pinfo("  Front Length: %d", front_length); // delete after debug
		comp->QueryIntAttribute("back_length", &back_length);
		Advisory::pinfo("  Back Length: %d", back_length);
		comp->QueryIntAttribute("port", &port);
		Advisory::pinfo("  LED Port: %d", port);
		comp->QueryIntAttribute("auton_pattern", &autonomous_pattern);
		Advisory::pinfo("  Auton Pattern: %d", port);
		comp->QueryIntAttribute("disabled_pattern", &disabled_pattern);
		Advisory::pinfo("  Disabled Pattern: %d", port);
		led_strip = new LightsStrip(m_led_length, front_length, port);
		
		m_led_length /= 2; // we are so very silly (2 strips of lights, wired in series)

		// TODO this should not be a child of led_strip, it should be separate
		// low prio - jly
		led_comp = comp-> FirstChildElement("color");
		int element_count = 0;
		while (led_comp != nullptr)
		{
			name = led_comp->Attribute("name");
			Advisory::pinfo("  creating new LED Color %s", name); // delete after debug
			r = led_comp->IntAttribute("r");
			g = led_comp->IntAttribute("g");
			b = led_comp->IntAttribute("b");
			
			ColorPalette[element_count] = {r, g, b};

			led_comp = led_comp-> NextSiblingElement("color");
			element_count += 1;
		}

		led_comp = comp-> FirstChildElement("pattern");
		element_count = 0;
		while (led_comp != nullptr)
		{
			name = led_comp->Attribute("name");
			Advisory::pinfo("  creating new LED Pattern %s", name);
			pattern_type[element_count] = led_comp->IntAttribute("type");

			// read in color 1
			color1 = led_comp->IntAttribute("color1"); // index from 0 to 7
			color1_idx[element_count] = color1;
			
			// read in color 2
			color2 = led_comp->IntAttribute("color2"); // index from 0 to 8
			color2_idx[element_count] = color2;

			if(element_count == 0)
			{
				pattern_chooser.SetDefaultOption("*** lights pattern ***", element_count);
			}
			pattern_chooser.AddOption(name, element_count);

			led_comp = led_comp-> NextSiblingElement("pattern");
			element_count += 1;
		}
		frc::SmartDashboard::PutData(&pattern_chooser);
    }
}

/*******************************************************************************	
 * 
 * Destructor: Release any resources allocated by this object
 * Any object that you created (motor, sensor, etc), put it here
 * 
 ******************************************************************************/
LightsControl::~LightsControl(void)
{
	if (led_strip != nullptr)
	{
		led_strip = nullptr;
	}
}

/*******************************************************************************
 * 
 ******************************************************************************/
void LightsControl::controlInit(void)
{ 
	if (led_strip != nullptr)
	{
		led_strip->start();
		Advisory::pinfo("STARTING LED");
	}
	
}

/*******************************************************************************
 *
 ******************************************************************************/
void LightsControl::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * This runs whenever the robot is disabled (end of a match AND
 * for a second between auton and teleop)
 * 
 ******************************************************************************/
void LightsControl::disabledInit(void)
{
    selected_pattern_idx = disabled_pattern;
	led_strip->setLedPattern(ColorPalette[color2_idx[00]], // sets back lights to red
							 ColorPalette[color2_idx[00]],
							 99,
							 0);
}

/*******************************************************************************	
 *
 * This runs at the beginning of autonomou
 * 
 ******************************************************************************/
void LightsControl::autonomousInit(void)
{
    selected_pattern_idx = autonomous_pattern;
	led_strip->setLedPattern(ColorPalette[color1_idx[8]],
							 ColorPalette[color1_idx[8]],
							 99,
							 0);
}

/*******************************************************************************	
 *
 * This runs at the beginning of teleop
 * 
 ******************************************************************************/
void LightsControl::teleopInit(void)
{
	led_strip->setLedPattern(ColorPalette[color2_idx[00]], // sets back lights to red
							 ColorPalette[color2_idx[00]],
							 99,
							 0);
	selected_pattern_idx = pattern_chooser.GetSelected();
	Advisory::pinfo("pattern: %i", selected_pattern_idx);
}

/*******************************************************************************	
 *
 * This runs at the beginning when you run the robot in Test mode.
 * We don't typically use this mode
 *
 ******************************************************************************/
void LightsControl::testInit(void)
{
}


/*******************************************************************************
 * Set functions
 ******************************************************************************/

void LightsControl::setLedData()
{
	led_strip->setLedData();
}

void LightsControl::setLedPattern(int idx)
{
	led_strip->setLedPattern(ColorPalette[color1_idx[idx]],
							 ColorPalette[color2_idx[idx]],
							 pattern_type[idx],
							 move_count);
}

void LightsControl::setSelectedPattern(int idx)
{
	selected_pattern_idx = idx;
}

/*******************************************************************************
 * 
*******************************************************************************/
void LightsControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	//SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	// idk. put current pattern name/idx in here
	SmartDashboard::PutNumber(getName() + " current pattern idx: ", selected_pattern_idx);
}

/*******************************************************************************
 *
 * This method will be called once a period (every 12.5 ms) to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void LightsControl::doPeriodic()
{
	if (getPhase() == AUTON)
	{
		setLedPattern(7); // theater chase red back
	}
	move_count++;
	move_count %= front_length;
	setLedPattern(selected_pattern_idx);
	setLedData();
}

/*******************************************************************************
 *
 ******************************************************************************/
MSSetPattern::MSSetPattern(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_parent_control = (LightsControl *)control;

	xml->QueryIntAttribute("pattern_idx", &pattern);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetPattern::init(void)
{
	m_parent_control->setSelectedPattern(pattern);
	m_parent_control->setLedPattern(pattern);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSSetPattern::update(void)
{
	return next_step;
}

