/*******************************************************************************
 *
 * File: ArcadeDriveControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  MSDriveBridge code based on FRC Team 3683's OpenAutoBalance
 *  https://github.com/FRC3683/OpenAutoBalance
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/

#include "ArcadeDriveControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/RobotUtil.h"

#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"

#include "gsinterfaces/Time.h"

#include "frc/smartDashboard/SmartDashboard.h" //WPI
#include "frc/geometry/Rotation2d.h"
#include "gsutilities/Filter.h"

using namespace tinyxml2;
using namespace frc;

// TODOs


/*******************************************************************************
 *
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 *
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 *
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 *
 * *****************************************************************************/
ArcadeDriveControl::ArcadeDriveControl(std::string control_name, XMLElement *xml)
    : PeriodicControl(control_name)
{
    Advisory::pinfo("========================= Creating %s Control =========================",
                    control_name.c_str());
    XMLElement *comp;

    const char *name;

    m_data_logger = new DataLogger("/robot/logs", "drive", "csv", 50, true);

    is_limelight_control = false;
    m_limelight = nullptr;
    m_limelightHeight = 30.0; // inches
    m_targetHeight = 24.0;    // inches
    // TODO check if we can accurately get distance from a target below the ll
    m_distance_to_target = 20.0; // inches, from limelight to apriltag
    m_limelightAngle = 45.0;     // degrees, TODO what angle do we need to mount at?

    m_target_visible = 0.0;
    m_targetOffsetAngle_Horizontal = 0.0;
    m_targetOffsetAngle_Vertical = 0.0;
    m_targetArea = 0.0;
    m_targetSkew = 0.0;

    avg_distance = 0.0;
    left_distance = 0.0;
    right_distance = 0.0;
    avg_distance_target = 0.0;
    avg_distance_prev = 0.0;
    avg_velocity = 0.0;
    distance_error = 0.0;
    m_heading = 0.0;
    heading_error = 0.0;
    pitch_error = 0.0;
    pitch = 0.0;
    pitch_target = 0.0;
    pitch_offset = 0.0;

    fl_drive_motor = nullptr;
    fr_drive_motor = nullptr;
    drive_width = 1.00; // meters, unused
    drive_max_current = 40.0;

    drive_scheme = 0; // drive_arcade

    gyro = nullptr;

    brake_solenoid = nullptr;

    brake_engaged = false;
    brake_solenoid_invert = false;
    brake_on_disable = false;

    gear_high = false;
    gear_solenoid_invert = false;

    fl_drive_motor_cmd = 0.0;
    fr_drive_motor_cmd = 0.0;

    trn_power = 0.0;
    fwd_power = 0.0;
    strafe_power = 0.0;

    m_low_power_scale = 0.5;
    m_low_power_active = false;

// --------- initialize CARTESIAN TRAJECTORY variables
#ifdef TRAJ2023

    m_heading = 0.0;
    m_heading_raw = 0.0;
    m_heading_offset = 0.0;

    m_prev_time = 0.0; // seconds, unused
    m_vel_cmd_prev = 0.0;

    left_distance = right_distance = avg_distance = 0.0;
    left_velocity = right_velocity = avg_velocity = 0.0;

    m_drive_cmd_distance = 0.0;       // inches
    m_drive_segment_end_distance = 0; // unused

    m_heading_target = 0.0;
    m_heading_target_prev = 0.0;
    m_gyro_cmd_adjusted = 0.0;

    velocity_pid = nullptr;
    angle_pid = nullptr;

    // gains, initialized to zero in constructor, set to real values in update config
    m_drive_dist_kp = 0.0;
    m_drive_dist_kd = 0.0; // unused for now

    initCartesian(0.0, 0.0, m_heading);
    m_x_cmd_vec = new std::vector<float>();
    m_y_cmd_vec = new std::vector<float>();
    m_end_vel_vec = new std::vector<float>();
    m_blend_vec = new std::vector<bool>();
    m_round_dist = new std::vector<float>();
    m_round_pts = new std::vector<int>();
    m_x_cmd_vec_staged = new std::vector<float>();
    m_y_cmd_vec_staged = new std::vector<float>();
    m_end_vel_vec_staged = new std::vector<float>();
    m_blend_vec_staged = new std::vector<bool>();
    m_round_dist_staged = new std::vector<float>();
    m_round_pts_staged = new std::vector<int>();

    m_tolerance_staged = m_tolerance = 3.0; // passed in from xml
    m_reset_coordinates_staged = true;

    m_timeout_time = m_timeout_staged = m_cart_init_time = m_cart_offset_time = m_timeout = 0.0;
    m_initialize_cart = false;

    m_cart_traj_done_state = CCS_RUNNING;

    m_cart_traj = new CartTrajectory(this->getPeriod()); // get period of this control; Q: is it available here

    m_x_cmd = 0.0;
    m_x_meas = 0.0;
    m_y_cmd = 0.0;
    m_y_meas = 0.0;
    m_cart_error = 0.0;
    m_x_offset = 0.0;
    m_y_offset = 0.0;

#endif

    name = xml->Attribute("name");
    if (name == nullptr)
    {
        name = "drive";
        Advisory::pcaution("WARNING: DriveArcade created without name, assuming \"%s\"", name);
    }

    //
    // Register Macro Steps
    //
    new MacroStepProxy<MSDrivePower>(control_name, "DrivePower", this);
    new MacroStepProxy<MSDriveDistance>(control_name, "DriveDistance", this);
    new MacroStepProxy<MSSimpleDriveDistance>(control_name, "SimpleDriveDistance", this);
    new MacroStepProxy<MSDriveAngle>(control_name, "DriveToHeading", this);
    new MacroStepProxy<MSSimpleDriveAngle>(control_name, "SimpleDriveAngle", this);
    new MacroStepProxy<MSDriveDistanceOnHeading>(control_name, "DriveDistanceOnHeading", this);
    new MacroStepProxy<MSDriveDistanceOnCurrentHeading>(control_name, "DriveDistanceOnCurrentHeading", this);
    
    new MacroStepProxy<MSDriveLLRotate>(control_name, "DriveAlign", this);
    new MacroStepProxy<MSDriveBridge>(control_name, "DriveBridge", this);

    new MacroStepProxy<MSDriveBrakeState>(control_name, "DriveBrakes", this);
    new MacroStepProxy<MSSetPipeline>(control_name, "SetPipeline", this);

    new MacroStepProxy<MSDriveCartesian>(control_name, "DriveCartesian", this);

    xml->QueryFloatAttribute("low_power_scale", &m_low_power_scale);
    xml->QueryFloatAttribute("encoder_scale", &encoder_scale);
    Advisory::pinfo("encoder scale: %f", encoder_scale); // why is encoder scale in xml? for fun
    xml->QueryFloatAttribute("min_error", &m_cart_tolerance_nominal);
    Advisory::pinfo("min error (cartesian): %f", m_cart_tolerance_nominal);
    xml->QueryFloatAttribute("close_enough_cart", &m_cart_close_enough_to_end);
    Advisory::pinfo("close enough (cartesian): %f", m_cart_close_enough_to_end);
    xml->QueryFloatAttribute("dist_kp", &m_drive_dist_kp);
    Advisory::pinfo("drive distance kp: %f", m_drive_dist_kp);
    xml->QueryFloatAttribute("dist_kd", &m_drive_dist_kd);
    Advisory::pinfo("drive distance kd: %f", m_drive_dist_kd);

    //
    // Parse the XML
    //
    comp = xml->FirstChildElement("motor");
    while (comp != nullptr)
    {
        name = comp->Attribute("name");
        if (name != nullptr)
        {
            // read in front_left and front_right
            // back_left and back_right are set as followers in the xml
            if (strcmp(name, "front_left") == 0)
            {
                Advisory::pinfo("  creating speed controller for %s", name);
                fl_drive_motor = HardwareFactory::createMotor(comp);
                fl_drive_motor->setSensorScale(encoder_scale, 0.0);
                comp->QueryUnsignedAttribute("max_current", &drive_max_current);
            }
            else if (strcmp(name, "front_right") == 0)
            {
                Advisory::pinfo("  creating speed controller for %s", name);
                fr_drive_motor = HardwareFactory::createMotor(comp);
                fr_drive_motor->setSensorScale(encoder_scale, 0.0);
            }
        }
        comp = comp->NextSiblingElement("motor");
    }

    comp = xml->FirstChildElement("gyro");
    if (comp != nullptr)
    {
        // if type NAVX then
        Advisory::pinfo("  creating gyro");
        gyro = HardwareFactory::createGyro(comp);
        Advisory::pinfo("  created gyro");
        if (!gyro->isConnected())
        {
            Advisory::pinfo("gyro is NOT connected!!!!!!!!!!!!!!!!!!!!");
        }
        else
        {
            Advisory::pinfo("gyro IS connected :) !!!");
        }
    }

    comp = xml->FirstChildElement("double_solenoid");
    while (comp != nullptr)
    {
        name = comp->Attribute("name");
        if (name != nullptr)
        {
            if (strcmp(name, "brake") == 0)
            {
                Advisory::pinfo("  creating brake solenoid for %s", name);
                brake_solenoid = HardwareFactory::createDoubleSolenoid(comp);
            }
            else
            {
                Advisory::pwarning(" found solenoid with name %s", name);
            }
        }
        else
        {
            Advisory::pwarning(" found solenoid with no name attribute");
        }
        comp = comp->NextSiblingElement("double_solenoid");
    }

    comp = xml->FirstChildElement("velocity_pid");
    if (comp != nullptr)
    {
        velocity_pid = HardwareFactory::createPid(comp);
    }
    comp = xml->FirstChildElement("angle_pid");
    if (comp != nullptr)
    {
        angle_pid = HardwareFactory::createPid(comp);
    }

    comp = xml->FirstChildElement("limelight");
    if (comp != nullptr)
    {
        name = comp->Attribute("name");
        if (name != nullptr)
        {
            m_limelight = HardwareFactory::createLimelight(comp);
        }
        comp->QueryFloatAttribute("limelight_height", &m_limelightHeight);
        comp->QueryFloatAttribute("target_height", &m_targetHeight);
        m_heightDifference = m_targetHeight - m_limelightHeight; // TODO maybe use abs distance?
        Advisory::pinfo("height difference %f", m_heightDifference);
        comp->QueryFloatAttribute("limelight_angle", &m_limelightAngle);
    }

    comp = xml->FirstChildElement("oi");
    while (comp != nullptr)
    {
        name = comp->Attribute("name");
        if (name != nullptr)
        {
            if (strcmp(name, "forward") == 0)
            {
                Advisory::pinfo("  connecting to forward channel");
                OIController::subscribeAnalog(comp, this, CMD_FORWARD);
            }
            else if (strcmp(name, "turn") == 0)
            {
                Advisory::pinfo("  connecting to turn channel");
                OIController::subscribeAnalog(comp, this, CMD_TURN);
            }
            else if (strcmp(name, "strafe") == 0)
            {
                Advisory::pinfo("  connecting to strafe channel");
                OIController::subscribeAnalog(comp, this, CMD_STRAFE);
            }
            else if (strcmp(name, "low_power_on") == 0)
            {
                Advisory::pinfo("  connecting to low_power on channel");
                OIController::subscribeDigital(comp, this, CMD_LOW_POWER_ON);
            }
            else if (strcmp(name, "low_power_off") == 0)
            {
                Advisory::pinfo("  connecting to low_power off channel");
                OIController::subscribeDigital(comp, this, CMD_LOW_POWER_OFF);
            }
            else if (strcmp(name, "low_power_toggle") == 0)
            {
                Advisory::pinfo("  connecting to low_power toggle channel");
                OIController::subscribeDigital(comp, this, CMD_LOW_POWER_TOGGLE);
            }
            else if (strcmp(name, "low_power_state") == 0)
            {
                Advisory::pinfo("  connecting to low_power state channel");
                OIController::subscribeDigital(comp, this, CMD_LOW_POWER_STATE);
            }
            else if (strcmp(name, "brake_on") == 0)
            {
                Advisory::pinfo("  connecting to brake_on channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_ON);
            }
            else if (strcmp(name, "brake_off") == 0)
            {
                Advisory::pinfo("  connecting to brake_off channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_OFF);
            }
            else if (strcmp(name, "brake_toggle") == 0)
            {
                Advisory::pinfo("  connecting to brake_toggle channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_TOGGLE);
            }
            else if (strcmp(name, "brake_state") == 0)
            {
                Advisory::pinfo("  connecting to brake_state channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_STATE);
            }
            else if (strcmp(name, "gear_high") == 0)
            {
                Advisory::pinfo("  connecting to gear_high channel");
                OIController::subscribeDigital(comp, this, CMD_GEAR_HIGH);
            }
            else if (strcmp(name, "gear_low") == 0)
            {
                Advisory::pinfo("  connecting to gear_low channel");
                OIController::subscribeDigital(comp, this, CMD_GEAR_LOW);
            }
            else if (strcmp(name, "gear_toggle") == 0)
            {
                Advisory::pinfo("  connecting to gear_toggle channel");
                OIController::subscribeDigital(comp, this, CMD_GEAR_TOGGLE);
            }
            else if (strcmp(name, "gear_state") == 0)
            {
                Advisory::pinfo("  connecting to gear_state channel");
                OIController::subscribeDigital(comp, this, CMD_GEAR_STATE);
            }
            else if (strcmp(name, "limelight_hold") == 0)
            {
                Advisory::pinfo("  connecting to limelight_hold channel");
                OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_HOLD);
            }
            else if (strcmp(name, "pipeline_tape") == 0)
            {
                Advisory::pinfo("  connecting to pipeline_tape channel");
                OIController::subscribeDigital(comp, this, CMD_PIPELINE_TAPE);
            }
            else if (strcmp(name, "pipeline_apriltag") == 0)
            {
                Advisory::pinfo("  connecting to pipeline_apriltag channel");
                OIController::subscribeDigital(comp, this, CMD_PIPELINE_APRILTAG);
            }
            else if (strcmp(name, "pipeline_game_piece") == 0)
            {
                Advisory::pinfo("  connecting to pipeline_game_piece channel");
                OIController::subscribeDigital(comp, this, CMD_PIPELINE_GAME_PIECE);
            }
            else if (strcmp(name, "gyro_reset") == 0)
            {
                Advisory::pinfo("  connecting to gyro_reset channel");
                OIController::subscribeDigital(comp, this, CMD_RESET_GYRO);
            }
            else
            {
                Advisory::pwarning("  unrecognized oi detected 2023: \"%s\"", name);
            }
        }
        comp = comp->NextSiblingElement("oi");
    }
}

/*******************************************************************************
 *
 * Release any resources used by this object
 *
 ******************************************************************************/
ArcadeDriveControl::~ArcadeDriveControl(void)
{
    if (fl_drive_motor != nullptr)
    {
        delete fl_drive_motor;
        fl_drive_motor = nullptr;
    }

    if (fr_drive_motor != nullptr)
    {
        delete fr_drive_motor;
        fr_drive_motor = nullptr;
    }

    if (brake_solenoid != nullptr)
    {
        delete brake_solenoid;
        brake_solenoid = nullptr;
    }

    if (gyro != nullptr)
    {
        delete gyro;
        gyro = nullptr;
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::controlInit()
{
    fwd_power = 0.0;
    trn_power = 0.0;
    strafe_power = 0.0;

    // Make sure every notor has current limiting. No magic smoke
    //bool is_ready = true;

    if ((fl_drive_motor != nullptr) && (fr_drive_motor != nullptr) && (drive_max_current < 100.0))
    {
        fl_drive_motor->setCurrentLimit(drive_max_current);
        fl_drive_motor->setCurrentLimitEnabled(true);
        fr_drive_motor->setCurrentLimit(drive_max_current);
        fr_drive_motor->setCurrentLimitEnabled(true);
    }
    if (fl_drive_motor == nullptr)
    {
        Advisory::pwarning("%s missing required component -- front left drive motor", getName().c_str());
        //is_ready = false;
    }
    if (fr_drive_motor == nullptr)
    {
        Advisory::pwarning("%s missing required component -- front right drive motor", getName().c_str());
        //is_ready = false;
    }
    if (gyro == nullptr)
    {
        Advisory::pwarning("%s missing required component -- drive gyro", getName().c_str());
        //is_ready = false;
    }
    /*
    if (m_limelight == nullptr)
    {
        Advisory::pwarning("%s missing required component -- drive limelight", getName().c_str());
        is_ready = false;
    }
    if (brake_solenoid == nullptr)
    {
        Advisory::pwarning("%s missing required component -- drive brake", getName().c_str());
        is_ready = false;
    }
    */
    // active = is_ready;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::disabledInit()
{
    m_data_logger->close();

    fwd_power = 0.0;
    trn_power = 0.0;
    strafe_power = 0.0;

    if (brake_on_disable)
    {
        brake_engaged = true;
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::autonomousInit()
{
    //logFileInit("Auton");

    resetEncoders();
    if (gyro->isConnected())
    {
        gyro->reset();
    }
    else
    {
        Advisory::pinfo("gyro is NOT connected!!!!!!!!!!!!!!!!!!!!");
    }

    setArcadeMode();
    arcadeDrive(0.0, 0.0);

    //pitch_offset = gyro->getPitch();
    //Advisory::pinfo("pitch offset %f", pitch_offset);

    fwd_power = 0.0;
    trn_power = 0.0;
    strafe_power = 0.0;
    brake_on_disable = true;

#ifdef TRAJ2023
    // reset Cartesian space variables
    // TODO init x and y to field position, depending on which auton we pick
    // read in from auto xml perhaps?
    initCartesian(0.0, 0.0, m_heading);

    target_avg_velocity = 0.0;
    target_left_velocity = 0.0;
    target_right_velocity = 0.0;

    m_heading_target = m_heading_target_prev = m_heading; // degrees

#endif
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::teleopInit()
{
    //logFileInit("Teleop");

    resetEncoders();
    if (!gyro->isConnected())
    {
        Advisory::pinfo("gyro is NOT connected!!!!!!!!!!!!!!!!!!!!");
    }

    setArcadeMode();
    arcadeDrive(0.0, 0.0);

  //  pitch_offset = gyro->getPitch();
  //  Advisory::pinfo("pitch offset %f", pitch_offset);

    fwd_power = 0.0;
    trn_power = 0.0;
    strafe_power = 0.0;
    brake_engaged = false;
    brake_on_disable = false;
    if (m_limelight != nullptr)
    {
        setPipeline(1);
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::testInit()
{
    //logFileInit("Test");

    fwd_power = 0.0;
    trn_power = 0.0;
    strafe_power = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::setInt(int id, int val)
{
}

/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 *
 * Handled command ids are CMD_TURN and CMD_FORWARD, all others are ignored
 *
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 *
 ******************************************************************************/
void ArcadeDriveControl::setAnalog(int id, float val)
{
    switch (id)
    {
    case CMD_TURN:
    {
        trn_power = val;
    }
    break;

    case CMD_FORWARD:
    {
        fwd_power = val;
    }
    break;

    case CMD_STRAFE:
    {
        strafe_power = val;
    }
    break;
    }
}

/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 *
 * Handled command ids are CMD_BRAKE_[ON|OFF|TOGGLE|STATE] and
 * CMD_GEAR_[HIGH|LOW|TOGGLE|STATE], all others are ignored
 *
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 *
 ******************************************************************************/
void ArcadeDriveControl::setDigital(int id, bool button_pressed)
{
    switch (id)
    {
    case CMD_BRAKE_ON:
    {
        if (button_pressed)
            brake_engaged = true;
    }
    break;

    case CMD_BRAKE_OFF:
    {
        if (button_pressed)
            brake_engaged = false;
    }
    break;

    case CMD_BRAKE_TOGGLE:
    {
        if (button_pressed)
            brake_engaged = !brake_engaged;
    }
    break;

    case CMD_BRAKE_STATE:
    {
        brake_engaged = button_pressed;
    }
    break;

    case CMD_GEAR_HIGH:
    {
        if (button_pressed)
            gear_high = true;
    }
    break;

    case CMD_GEAR_LOW:
    {
        if (button_pressed)
            gear_high = false;
    }
    break;

    case CMD_GEAR_TOGGLE:
    {
        if (button_pressed)
            gear_high = !gear_high;
    }
    break;

    case CMD_GEAR_STATE:
    {
        gear_high = button_pressed;
    }
    break;

    case CMD_LOW_POWER_ON:
    {
        if (button_pressed)
            m_low_power_active = true;
    }
    break;

    case CMD_LOW_POWER_OFF:
    {
        if (button_pressed)
            m_low_power_active = false;
    }
    break;

    case CMD_LOW_POWER_TOGGLE:
    {
        if (button_pressed)
            m_low_power_active = !m_low_power_active;
    }
    break;

    case CMD_LOW_POWER_STATE:
    {
        m_low_power_active = button_pressed;
    }
    break;

    case CMD_LIMELIGHT_HOLD:
    {
        if (button_pressed)
        {
            drive_scheme = DRIVE_LIMELIGHT;
        }
        else
        {
            // button should only get pressed in teleop
            drive_scheme = DRIVE_ARCADE;
            arcadeDrive(0.0, 0.0);
        }
        is_limelight_control = button_pressed;
    }
    break;

    case CMD_PIPELINE_TAPE:
    {
        // pipelines are numbered starting at 0 right?
        m_limelight->setPipeline(0);
    }
    break;

    case CMD_PIPELINE_APRILTAG:
    {
        m_limelight->setPipeline(1);
    }
    break;

    case CMD_PIPELINE_GAME_PIECE:
    {
        m_limelight->setPipeline(2);
    }
    break;

    case CMD_RESET_GYRO:
    {
        gyro->reset();
        resetEncoders();
    }
    break;
    }
}

/*************************************************
 * functions that Set values
 *************************************************/
void ArcadeDriveControl::arcadeDrive(float fwd, float turn)
{
    fwd_power = fwd;
    trn_power = turn;
}

void ArcadeDriveControl::driveDistance(float distance)
{
    fl_drive_motor->resetPosition();
    fr_drive_motor->resetPosition();
    avg_distance_target = distance;
    //Advisory::pinfo("setting avg_distance_target to %f", avg_distance_target);
}

void ArcadeDriveControl::driveDistanceMotor(float distance)
{
    // distance in inches
    fl_drive_motor->resetPosition();
    fr_drive_motor->resetPosition();
    fl_drive_motor->setControlMode(Motor::POSITION);
    fr_drive_motor->setControlMode(Motor::POSITION);
    fl_drive_motor->setPosition(distance);
    fr_drive_motor->setPosition(distance);
}

void ArcadeDriveControl::rotateDegrees(float degrees)
{
    heading_error = 0.0;
    m_heading_target = degrees + m_heading;
}

void ArcadeDriveControl::rotateToAngle(float angle)
{
    m_heading_target = angle;
}

void ArcadeDriveControl::balancePitch(float target)
{
    pitch_target = target;
}

void ArcadeDriveControl::distAndRotate(float distance, float angle)
{
    // unused?
}

void ArcadeDriveControl::resetEncoders()
{
    // sets encoders to a value of 0
    fl_drive_motor->resetPosition();
    fr_drive_motor->resetPosition();
}

void ArcadeDriveControl::setBrakeState(bool state)
{
    brake_engaged = state;
    Advisory::pinfo("brakes set to %s", state ? "true" : "false");
}

/*************************************************
 * functions that Get values
 *************************************************/
float ArcadeDriveControl::getAvgEncoderDistance()
{
    // average the distances of left and right
    return ((fl_drive_motor->getPosition() + fr_drive_motor->getPosition()) / 2);
}

float ArcadeDriveControl::getHeading()
{
    // return get heading from navx gyro
    if (gyro->isConnected())
    {
        return gyro->getAngle();
    }
    else
    {
        return 0.0;
    }
}

float ArcadeDriveControl::getTurnRate()
{
    // return get heading from navx gyro
    if (gyro->isConnected())
    {
        return gyro->getRate();
    }
    else
    {
        return 0.0;
    }
}

float ArcadeDriveControl::getPitch()
{
    // use this if gyro is mounted facing fwd or back
    // note: may need to invert the sign;
    // return get heading from navx gyro
    if (gyro->isConnected())
    {
        return gyro->getPitch();
    }
    else
    {
        return 0.0;
    }
}

float ArcadeDriveControl::getRoll()
{
    // use this if gyro is mounted facing to the side
    // note: may need to invert the sign
    // TODO un-invert this
    // return get heading from navx gyro
    if (gyro->isConnected())
    {
        return -gyro->getRoll();
    }
    else
    {
        return 0.0;
    }
}

bool ArcadeDriveControl::isAtHeadingTarget(float tolerance)
{
    return (abs(m_heading - m_heading_target) < tolerance);
}

bool ArcadeDriveControl::isAtDistanceTarget(float tolerance)
{
    return (abs(avg_distance - avg_distance_target) < tolerance);
}

bool ArcadeDriveControl::isAtPitchTarget(float tolerance)
{
    return (abs(pitch - pitch_target) < tolerance);
}

bool ArcadeDriveControl::isGyroConnected(void)
{
    return gyro->isConnected();
}

float ArcadeDriveControl::getDistanceFromLLTarget()
{
    return m_distance_to_target;
}

float ArcadeDriveControl::getLLHZAngle()
{
    return m_targetOffsetAngle_Horizontal;
}

/*******************************************************************************
 * CARTESIAN TRAJECTORY
 *******************************************************************************/
#ifdef TRAJ2023

// SEARCH function definitions

void ArcadeDriveControl::setCartesianMode()
{
    drive_scheme = DRIVE_CARTESIAN; // this is how enums work right
    fl_drive_motor->setControlMode(Motor::VELOCITY);
    fr_drive_motor->setControlMode(Motor::VELOCITY);
}
void ArcadeDriveControl::setArcadeMode()
{
    drive_scheme = DRIVE_ARCADE;
    fl_drive_motor->setControlMode(Motor::PERCENT);
    fr_drive_motor->setControlMode(Motor::PERCENT);
}
void ArcadeDriveControl::setLimelightMode()
{
    drive_scheme = DRIVE_LIMELIGHT;
    fl_drive_motor->setControlMode(Motor::PERCENT);
    fr_drive_motor->setControlMode(Motor::PERCENT);
}
void ArcadeDriveControl::setClosedLoopMode()
{
    drive_scheme = DRIVE_CLOSED_LOOP;
    fl_drive_motor->setControlMode(Motor::PERCENT);
    fr_drive_motor->setControlMode(Motor::PERCENT);
}

void ArcadeDriveControl::doVelocityControlLaw(float left_target_vel, float right_target_vel, float gyro_contrib)
{
    // gyro contrib in m/s (velocity)
    target_left_velocity = left_target_vel + gyro_contrib; // signs may need to be swapped
    target_right_velocity = right_target_vel - gyro_contrib;
}

void ArcadeDriveControl::getRobotState()
{
    //  float current_time; // seconds

    // current_time = getPhaseElapsedTime(); // seconds
    avg_distance_prev = avg_distance; // stores distance before updating

    m_heading = gyro->getAngle();
    left_distance = fl_drive_motor->getPosition();
    right_distance = fr_drive_motor->getPosition();
    left_velocity = fl_drive_motor->getVelocity();
    right_velocity = fr_drive_motor->getVelocity();

    avg_distance = (left_distance + right_distance) / 2;
    avg_velocity = (left_velocity + right_velocity) / 2;
    // update on Cartesian using internal variables
    updateCartesian(m_heading, avg_distance - avg_distance_prev, &m_x_meas, &m_y_meas);

    // return current_time;
}

/**********************************************************************
 * The periodic function that executes when robot is in cartesian mode
 **********************************************************************/
void ArcadeDriveControl::doCartesianMode()
{
    float pos = 0, vel = 0, end = 0;

    m_delta_th = 0.0;
    m_gyro_cmd_adjusted = 0;

    getRobotState();

    if (m_initialize_cart == true)
    {
        initializeCartesianModeExec();
        m_initialize_cart = false;
    }
    if (true)
    {
        // update distances
        m_cart_traj->Update(getPhaseElapsedTime() - m_cart_init_time - m_cart_offset_time);

        // JLY gets target position, velocity, heading from trajectory at current time
        m_x_cmd = m_cart_traj->GetX();
        m_y_cmd = m_cart_traj->GetY();

        m_drive_cmd_distance = m_cart_traj->GetCurrentDist();

        m_heading_target = m_cart_traj->GetSegmentHeading();
        m_gyro_cmd_adjusted = m_heading_target;

        pos = m_cart_traj->GetCurrentDist();
        vel = m_cart_traj->GetCurrentVelocity();
        end = m_cart_traj->GetEndDist();
        // end get

        // this part is copied straight from 2017. hopefully it works
        if (errorFromCartesian(m_x_meas, m_y_meas, m_x_cmd, m_y_cmd, vel, m_vel_cmd_prev, pos, end,
                               &m_gyro_cmd_adjusted, &m_cart_error) == true)
        {
            m_heading_target = m_gyro_cmd_adjusted;
        }
        else
        {
            getShortestHeadingCorrection(&m_heading_target, m_heading, 0.0);
        }
        // end 2017

        // approach with a position loop wrapping a velocity loop and feed forward model
        target_left_velocity = target_right_velocity = m_cart_error * m_drive_dist_kp;
        if (std::abs(m_cart_error) < m_tolerance) // if close enough, end forward drive, but allow orientation adjustment
        {
            target_left_velocity = target_right_velocity = 0;
        }

        m_delta_th = velocity_pid->calculateCommand(m_heading_target, m_heading); // this should work

        //**** End specific to this control mode ***

        doVelocityControlLaw(target_left_velocity, target_right_velocity, m_delta_th); // this should work
        //**** End general control law ****

        // this should work
        if (vel != 0)
        {
            m_vel_cmd_prev = vel;
        }
    }
    else
    {
        target_left_velocity = target_right_velocity = 0.0;
        Advisory::pwarning("MODE_DISTANCE: gyro not ready, not moving\n");
    }

    calcCartesianSegmentDone(); // update completeness for macro step
}

/*******************************************************************************
 * Update x and y from heading and distance
 ******************************************************************************/
void ArcadeDriveControl::updateCartesian(float heading, float d_dist, float *x, float *y)
{
    *x += d_dist * std::cos(heading / RobotUtil::DEGREE_PER_RADIAN);
    *y += d_dist * std::sin(heading / RobotUtil::DEGREE_PER_RADIAN);
}

//***********************************************************************************
// select minimal effort to heading, where sometimes
// actually -2 deg not 358 deg
//***********************************************************************************
float ArcadeDriveControl::getShortestHeadingCorrection(float *desired, float measured, float mag)
{
    float error1, error2, error3;
    // float omag = mag, odes = *desired;
    //  test code
    error1 = unwindAngleError(*desired, measured);
    error2 = error1 + 180;
    error3 = error1 - 180;

    if (std::abs(error2) <= std::abs(error3) && std::abs(error2) <= std::abs(error1))
    {
        *desired += 180.0;
        mag *= -1.0;
    }
    else if (std::abs(error3) <= std::abs(error1) && std::abs(error3) <= std::abs(error2))
    {
        *desired -= 180;
        mag *= -1.0;
    }
    //	Advisory::pinfo("   getSHC: err [%.3f, %.3f, %.3f] mag [%.3f, %.3f] angle [%.3f, %.3f, %.3f]", error1, error2,
    // error3, omag, mag, odes, *desired, measured);

    return (mag);
}

//***********************************************************************************
// Determine error of an angle, generally looking for the cases where answer is
// actually -2 deg not 358 deg
// 2017 copy
//***********************************************************************************
float ArcadeDriveControl::unwindAngleError(float des, float mea)
{
    int count = 0;
    float angle_error = des - mea;

    while (std::abs(angle_error) >
           180.0) // unwind angle errors > 180 - like when new_angle = -179.9 and desired_angle = 180
    {
        angle_error -= RobotUtil::sign(angle_error) * 360;
        count++;
        if (count > 100) // this would be an error of 18000 deg, major malfunction or lots of loops, prevents (unlikely)
                         // infinite loop
        {
            Advisory::pinfo("unwind failure");
            angle_error = 0;
            break;
        }
    }
    return (angle_error);
}

float ArcadeDriveControl::getCartesianX()
{
    return m_x_meas;
}

float ArcadeDriveControl::getCartesianY()
{
    return m_y_meas;
}

// initialize cartesian variables
void ArcadeDriveControl::initCartesian(float x, float y, float theta)
{
    m_x_cmd = x;
    m_x_meas = y;
    m_y_cmd = x;
    m_y_meas = y;
    m_heading_offset = theta;
    m_cart_error = 0.0;
    avg_distance_prev = 0.0;
    m_gyro_cmd_adjusted = 0.0;
    m_cart_offset_time = 0.0;

    Advisory::pinfo("***init cartesian****");
}

/*******************************************************************************
 * checks whether cartesian segment is complete
 *******************************************************************************/
int ArcadeDriveControl::cartesianSegmentDone()
{
    // gsi::MutexScopeLock block(getLock());
    return (this->m_cart_traj_done_state);
}

/*******************************************************************************
 * checks whether cartesian segment is complete
 *******************************************************************************/
void ArcadeDriveControl::calcCartesianSegmentDone()
{
    float end = m_cart_traj->GetEndDist();
    float dist = m_cart_traj->GetCurrentDist();

    // check for at end of trajectory
    if (std::abs(end - dist) < m_cart_close_enough_to_end && std::abs(m_cart_error) < m_tolerance)
    {
        m_tolerance = m_cart_tolerance_nominal;
        m_cart_traj_done_state = CCS_DONE;
        Advisory::pinfo("cart segment transitioning from completions @ time %.3f", getPhaseElapsedTime());
    }
    else if (getPhaseElapsedTime() > m_timeout_time)
    {
        m_cart_traj_done_state = CCS_TIMEOUT;
        Advisory::pinfo("cart segment transitioning from timeout %.3f, %.3f", getPhaseElapsedTime(), m_timeout_time);
    }
    else // this is were failure modes can get captured
    {
        m_cart_traj_done_state = CCS_RUNNING;
    }
}

/*******************************************************************************
 * requests an initialization of cartesian mode; change for 2019, do all init
 * in doPeriodic.  This is called from main thread/macro
 *******************************************************************************/
void ArcadeDriveControl::initializeCartesianModeCmd(float timeout, std::vector<float> *x_cmd, std::vector<float> *y_cmd,
                                                    std::vector<float> *end_vel, std::vector<bool> *blend,
                                                    std::vector<int> *round_pts, std::vector<float> *round_dist,
                                                    float tolerance, float ignore_start, float ignore_end,
                                                    bool reset_coordinates)
{
    // gsi::MutexScopeLock block(getLock()); // ???

    // will stage initialization by setting a flag, which will request encoders initialize trajectory ...
    m_timeout_staged = timeout;

    m_reset_coordinates_staged = reset_coordinates;

    // reset trajectory possibly non-zero coordinates
    clearCartTrajectoryStage(reset_coordinates, m_x_cmd, m_y_cmd); // no np here

    m_adjust_heading_start = ignore_start; // don't stage.  Change to staging if needed
    m_adjust_heading_end = ignore_end;

    m_x_cmd_vec_staged->assign(x_cmd->begin(), x_cmd->end()); // gets the values to be in global scope
    m_y_cmd_vec_staged->assign(y_cmd->begin(), y_cmd->end());
    m_end_vel_vec_staged->assign(end_vel->begin(), end_vel->end());
    m_blend_vec_staged->assign(blend->begin(), blend->end());
    m_round_pts_staged->assign(round_pts->begin(), round_pts->end());
    m_round_dist_staged->assign(round_dist->begin(), round_dist->end());

    m_tolerance_staged = tolerance; // what's good enough for this traj

    m_initialize_cart = true; // ok we are initialized
    drive_scheme = DRIVE_CARTESIAN;
}

/*******************************************************************************
 * requests an initialization of cartesian mode; change for 2019, do all init
 * in doPeriodic
 *******************************************************************************/
void ArcadeDriveControl::initializeCartesianModeExec()
{
    Advisory::pinfo("initExec xy %.3f, %.3f", m_x_cmd, m_y_cmd);

    float phase_time = getPhaseElapsedTime();
    // will stage initialization by setting a flag, which will request encoders initialize trajectory ...
    m_timeout = m_timeout_staged;

    clearCartTrajectory(); // no np here
    m_x_cmd_vec->assign(m_x_cmd_vec_staged->begin(), m_x_cmd_vec_staged->end());
    m_y_cmd_vec->assign(m_y_cmd_vec_staged->begin(), m_y_cmd_vec_staged->end());
    m_end_vel_vec->assign(m_end_vel_vec_staged->begin(), m_end_vel_vec_staged->end());
    m_blend_vec->assign(m_blend_vec_staged->begin(), m_blend_vec_staged->end());
    m_round_dist->assign(m_round_dist_staged->begin(), m_round_dist_staged->end());
    m_round_pts->assign(m_round_pts_staged->begin(), m_round_pts_staged->end());

    // clear points from cart trajectory
    m_cart_traj->ClearTrajectory(m_reset_coordinates_staged, m_x_cmd, m_y_cmd);

    // add points to cart trajectories
    for (unsigned int i = 0; i < m_x_cmd_vec->size(); i++)
    {
        m_cart_traj->AddPoint(m_x_cmd_vec->at(i), m_y_cmd_vec->at(i), m_end_vel_vec->at(i), m_blend_vec->at(i),
                              m_round_pts->at(i), m_round_dist->at(i));
    }

    // initialize trajectory
    m_cart_traj->Initialize();

    // determine when this trajectory times out
    m_timeout_time = phase_time + m_cart_traj->GetTotalTime() + m_timeout_staged;
    m_cart_init_time = phase_time;

    m_tolerance = m_tolerance_staged; // what's good enough for this traj

    fl_drive_motor->setControlMode(Motor::VELOCITY);
    fr_drive_motor->setControlMode(Motor::VELOCITY);

    // set state to running
    m_cart_traj_done_state = CCS_RUNNING;
}

void ArcadeDriveControl::clearCartTrajectory()
{
    m_x_cmd_vec->clear();
    m_y_cmd_vec->clear();
    m_end_vel_vec->clear();
    m_blend_vec->clear();
    m_round_dist->clear();
    m_round_pts->clear();
}

void ArcadeDriveControl::clearCartTrajectoryStage(bool reset_coordinates, float x, float y)
{
    float x_stage = 0.0, y_stage = 0.0;
    if (reset_coordinates == true)
    {
        x_stage = x;
        y_stage = y;
    }
    m_x_cmd_vec_staged->clear();
    m_y_cmd_vec_staged->clear();
    m_end_vel_vec_staged->clear();
    m_blend_vec_staged->clear();
    m_round_dist_staged->clear();
    m_round_pts_staged->clear();

    m_x_cmd_vec_staged->push_back(x_stage);
    m_y_cmd_vec_staged->push_back(y_stage);
}

/*******************************************************************************
 * Use cartesian data to determine magnitude and angle of error in cartesian
 ******************************************************************************/
bool ArcadeDriveControl::errorFromCartesian(float x_act, float y_act, float x_cmd, float y_cmd, float vel,
                                            float vel_prev, float pos, float end, float *desired_angle, float *mag)
{
    float delta[2];
    bool ret_val = false;
    float vel_dir = vel;

    // we know, we're mixing units
    delta[0] = x_cmd - x_act; // delta x
    delta[1] = y_cmd - y_act; // delta y
    *mag = std::sqrt(delta[0] * delta[0] + delta[1] * delta[1]);

    // because we always zero the angle when starting, when driving backward, the results needs a little adjustment when
    // driving backward
    if (RobotUtil::sign(vel) == 0)
    {
        vel_dir = vel_prev;
    }
    *desired_angle = std::atan2(RobotUtil::sign(vel_dir) * delta[1], RobotUtil::sign(vel_dir) * delta[0]) *
                     RobotUtil::DEGREE_PER_RADIAN;

    if (pos < end - m_adjust_heading_end && pos > m_adjust_heading_start)
    {
        ret_val = true;
    }

    *mag = getShortestHeadingCorrection(desired_angle, m_heading, *mag);

    return (ret_val);
}

#endif

void ArcadeDriveControl::setPipeline(int pipeline)
{
    m_limelight->setPipeline(pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::publish()
{
    //	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
    //	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

    SmartDashboard::PutNumber(getName() + " forward power: ", fwd_power);
    SmartDashboard::PutNumber(getName() + " turn power: ", trn_power);
    SmartDashboard::PutNumber(getName() + " strafe: ", strafe_power);
    SmartDashboard::PutNumber(getName() + " left distance (in): ", fl_drive_motor->getPosition());
    SmartDashboard::PutNumber(getName() + " right distance (in): ", fr_drive_motor->getPosition());
    SmartDashboard::PutNumber(getName() + " avg distance (in): ", getAvgEncoderDistance());
    SmartDashboard::PutNumber(getName() + " heading error (deg): ", heading_error);
    SmartDashboard::PutNumber(getName() + " pitch error (deg): ", pitch_error);
    SmartDashboard::PutNumber(getName() + " distance error (in): ", distance_error);
    SmartDashboard::PutNumber(getName() + " distance target (in): ", avg_distance_target);

    if (brake_solenoid != nullptr)
    {
        SmartDashboard::PutBoolean(getName() + " brake: ", brake_engaged);
    }

    if (fl_drive_motor != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " fl cmd: ", fl_drive_motor_cmd);
        SmartDashboard::PutNumber(getName() + " fl target vel: ", target_left_velocity);
        SmartDashboard::PutNumber(getName() + " fl actual velocity: ", left_velocity);
    }

    if (fr_drive_motor != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " fr cmd: ", fr_drive_motor_cmd);
        SmartDashboard::PutNumber(getName() + " fr target vel: ", target_right_velocity);
        SmartDashboard::PutNumber(getName() + " fr actual velocity: ", right_velocity);
    }

    if (gyro != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " gyro angle: ", gyro->getAngle());
        SmartDashboard::PutNumber(getName() + " gyro rate: ", gyro->getRate());
        SmartDashboard::PutNumber(getName() + " gyro pitch: ", gyro->getPitch());
        SmartDashboard::PutNumber(getName() + " gyro roll: ", -gyro->getRoll());
        SmartDashboard::PutBoolean(getName() + " gyro is connected: ", gyro->isConnected());

        // jr  If it is a NavX, then we can call the extra
        // NavX functions
        auto navx_gyro = dynamic_cast<rfh::Gyro_NavX *>(gyro);
        if (navx_gyro != nullptr)
        {
            SmartDashboard::PutNumber(getName() + " gyro isMoving: ", navx_gyro->isMoving());
        }
    }

    SmartDashboard::PutNumber(getName() + " Limelight Horizontal: ", m_targetOffsetAngle_Horizontal);
    SmartDashboard::PutNumber(getName() + " Limelight Vertical: ", m_targetOffsetAngle_Vertical);
    // SmartDashboard::PutNumber(getName() + " Limelight Area: ", m_targetArea);
    SmartDashboard::PutNumber(getName() + " Limelight Skew: ", m_targetSkew);
    SmartDashboard::PutNumber(getName() + " Limelight Distance to Target (in): ", m_distance_to_target);
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::logFileInit(std::string phase)
{
    Advisory::pinfo("initializing log file for %s", phase.c_str());

    m_data_logger->openSegment();

    m_data_logger->log("%s, %s, %s, ",
                       "time", "phase", "pet");

    m_data_logger->log("%s, %s, %s,  %s, %s,   %s, %s, ",
                       "fwd_power", "trn_power", "strafe_power",
                       "fl_drive_motor_cmd", "fr_drive_motor_cmd",
                       "brake_engaged", "gear_high");

    m_data_logger->log("\n");

    m_data_logger->flush();
}

/*******************************************************************************
 *
 ******************************************************************************/
void ArcadeDriveControl::logFileAppend(void)
{
    m_data_logger->log("%f, %d, %f, ",
                       gsi::Time::getTime(), this->getPhase(), this->getPhaseElapsedTime());

    m_data_logger->log("%f, %f, %f,  %f, %f,   %d, %d, ",
                       fwd_power, trn_power, strafe_power,
                       fl_drive_motor_cmd, fr_drive_motor_cmd,
                       brake_engaged, gear_high);

    m_data_logger->log("\n");
}

/*******************************************************************************
 *
 * Sets the actuator values every period
 *
 ******************************************************************************/
void ArcadeDriveControl::doPeriodic()
{
    //
    // Read Sensors
    //

    if (m_limelight != nullptr)
    {
        m_limelight->getData(m_target_visible,
                             m_targetOffsetAngle_Horizontal,
                             m_targetOffsetAngle_Vertical,
                             m_targetArea,
                             m_targetSkew);
    }
    // jly this formula works but see if we can make it a bit more accurate (maybe)
    m_distance_to_target = m_heightDifference / std::tan((m_targetOffsetAngle_Vertical + m_limelightAngle) / RAD_TO_DEG);

    avg_distance = (fr_drive_motor->getPosition() + fl_drive_motor->getPosition()) / 2;

    if (gyro != nullptr && gyro->isConnected())
    {
        m_heading = gyro->getAngle();
        pitch = gyro->getPitch() - pitch_offset;
    }
    else
    {
        m_heading = 0.0;
        pitch = 0.0;
    }

    if (drive_scheme == DRIVE_ARCADE)
    {
        //
        // Calculate Values
        //
        fl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power - strafe_power);
        fr_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power + strafe_power);

        // Normalize the drive command values
        float normalize_max = fabs(fl_drive_motor_cmd);
        if (fabs(fr_drive_motor_cmd) > normalize_max)
        {
            normalize_max = fabs(fr_drive_motor_cmd);
        }

        if (normalize_max > 1.0)
        {
            fl_drive_motor_cmd /= normalize_max;
            fr_drive_motor_cmd /= normalize_max;
        }

        // apply low power factor
        if (m_low_power_active)
        {
            fl_drive_motor_cmd *= m_low_power_scale;
            fr_drive_motor_cmd *= m_low_power_scale;
        }

        // don't drive motors against the brake;
        if (brake_engaged)
        {
            fl_drive_motor_cmd = 0.0;
            fr_drive_motor_cmd = 0.0;
        }

        // who needs nullptr checks am i right
        fl_drive_motor->setPercent(fl_drive_motor_cmd);
        fr_drive_motor->setPercent(fr_drive_motor_cmd);
    }
    else if (drive_scheme == DRIVE_LIMELIGHT)
    {
        if (fabs(m_targetOffsetAngle_Horizontal) > 0.07)
        {
            //                trn_power = (m_targetOffsetAngle_Horizontal) * 0.025;
            trn_power = angle_pid->calculateCommand(m_targetOffsetAngle_Horizontal, 0.0);
            if (fabs(trn_power) > 1.0)
            {
                trn_power /= trn_power;
            }
            else if (fabs(trn_power) < 0.03)
            {
                trn_power /= trn_power;
                trn_power *= 0.03;
            }
            //  fwd_power = 0.0;
        }
        else
        {
            trn_power = 0.0;
        }

        //
        // Calculate Values
        //

        // ignore fwd commands, only use rot commands
        // fl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, -trn_power);
        // fr_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, trn_power);

        // turn power is set by this drive mode, fwd power is set by the driver
        fl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power);
        fr_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power);

        // apply low power factor
        if (m_low_power_active)
        {
            fl_drive_motor_cmd *= m_low_power_scale;
            fr_drive_motor_cmd *= m_low_power_scale;
        }

        // don't drive motors against the brake;
        if (brake_engaged)
        {
            fl_drive_motor_cmd = 0.0;
            fr_drive_motor_cmd = 0.0;
        }

        // who needs nullptr checks am i right
        fl_drive_motor->setPercent(fl_drive_motor_cmd);
        fr_drive_motor->setPercent(fr_drive_motor_cmd);
    }
    else if (drive_scheme == DRIVE_CARTESIAN)
    {
        doCartesianMode(); // sets target values for everything
        // update motors with vel command
        // TODO check that this is in m/s
        fl_drive_motor->setVelocity(target_left_velocity);
        fr_drive_motor->setVelocity(target_right_velocity);

        // don't drive motors against the brake;
        if (brake_engaged)
        {
            fl_drive_motor_cmd = 0.0;
            fr_drive_motor_cmd = 0.0;
        }
    }
    else if (drive_scheme == DRIVE_CLOSED_LOOP)
    {
        // this is closed loop position on the motors
        // all processing done in motor class
        // i think
        // :)
    }

    //
    // Set Outputs
    //
    if (brake_solenoid != nullptr)
    {
        if (brake_engaged)
        {
            brake_solenoid->Set(frc::DoubleSolenoid::Value::kForward);
        }
        else
        {
            brake_solenoid->Set(frc::DoubleSolenoid::Value::kReverse);
        }
    }

    //logFileAppend();
}
// ----------------------------
// MACROSTEPS (normal)
// ----------------------------

/**********************************************
 * TEST MACROS
 **********************************************/
MSDrivePower::MSDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    fwd = xml->FloatAttribute("forward");
    trn = xml->FloatAttribute("turn");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDrivePower::init()
{
    parent_control->setArcadeMode();
    parent_control->arcadeDrive(fwd, trn);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDrivePower::update()
{
    return next_step;
}

/*****************************************/

MSDriveDistance::MSDriveDistance(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    // THIS WORKS
    parent_control = (ArcadeDriveControl *)control;
    dist = xml->FloatAttribute("distance");
    //Advisory::pinfo("ms drive distance: %f", dist);
    m_wait = xml->BoolAttribute("wait");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveDistance::init()
{
    parent_control->setArcadeMode();
    parent_control->resetEncoders(); // init distance to 0

    at_dist_count = 0;

    // just for reporting purposes
    parent_control->driveDistance(dist);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveDistance::update()
{
    if (m_wait == true)
    {
        if (!(parent_control->isAtDistanceTarget(6.0))) // if not at target
        {
            //    at_dist_count = 0;
            dist_error = dist - parent_control->getAvgEncoderDistance();
            fwd = dist_error * 0.007;
            if (abs(fwd) > 1.0)
            {
                fwd /= abs(fwd);
            }
            if (abs(fwd) < 0.07)
            {
                fwd /= abs(fwd);
                fwd *= 0.07;
            }

            //Advisory::pinfo("distance error: %f, power: %f", dist_error, fwd);
            fwd = gsu::Filter::limit(fwd, -0.5, 0.5);
            parent_control->arcadeDrive(fwd, 0.0); 
            
            return this;
        }
        else if (at_dist_count < 10)
        {
            at_dist_count++;
            return this;
        }
    }

    // once robot reaches target
    parent_control->arcadeDrive(0.0, 0.0);
    return next_step;
}



/*****************************************/

MSSimpleDriveDistance::MSSimpleDriveDistance(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    tol = 2.0; // default values
    timeout = 15.0; // unused, todo implement maybe? low priority

    parent_control = (ArcadeDriveControl *)control;
    xml->QueryFloatAttribute("distance", & dist);
    xml->QueryFloatAttribute("power", & power); // signed!!!!!!!
    xml->QueryFloatAttribute("tolerance", & tol);
    xml->QueryFloatAttribute("timeout", & timeout);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSimpleDriveDistance::init()
{
    parent_control->setArcadeMode();
    parent_control->resetEncoders(); // init distance to 0

    // just for reporting purposes
    parent_control->driveDistance(dist);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSimpleDriveDistance::update()
{
    if (!parent_control->isGyroConnected())
    {
        Advisory::pinfo("gyro not connected!");
        return next_step;
    }
    if (!(parent_control->isAtDistanceTarget(tol))) // if not at target
    {
        parent_control->arcadeDrive(power, 0.0); 
        //Advisory::pinfo("ms simple drive distance: %f, target: %f", parent_control->getAvgEncoderDistance(), dist);
        
        return this;
    }

    // once robot reaches target
    parent_control->arcadeDrive(0.0, 0.0);
    Advisory::pinfo("ending ms simple drive distance :3c, distance = %f", parent_control->getAvgEncoderDistance());
    return next_step;
}


/*****************************************/

MSDriveAngle::MSDriveAngle(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    angle = xml->FloatAttribute("target_heading");
    kp = xml->FloatAttribute("kp");
    xml->QueryFloatAttribute("tolerance", &tol);
    xml->QueryFloatAttribute("timeout", &timeout);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveAngle::init()
{
    parent_control->setArcadeMode();

    // just for reporting purposes
    parent_control->rotateToAngle(angle);

    at_angle_count = 0;
    start_time = gsi::Time::getTime();
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveAngle::update()
{
    if ((timeout > 0.001f) && (gsi::Time::getTime() - start_time) > timeout)
    {
        Advisory::pinfo("timeout, %f", timeout);
        return next_step;
    }
    if (!(parent_control->isAtHeadingTarget(tol))) // if not at target
    {
        angle_error = angle - parent_control->getHeading();
        // Advisory::pinfo("not at target, error = %f", angle_error);
        trn = -1 * angle_error * kp;
        trn = gsu::Filter::limit(trn, -1.0, 1.0);
        Advisory::pinfo("yippee!!!!, heading = %f, trn = %f", parent_control->getHeading(), trn);
        
        parent_control->arcadeDrive(0.0, trn);
        return this;
    }
    /*else if (at_angle_count < 200)
    {
        at_angle_count++;
        return this;
    }*/
    // once robot reaches target
    Advisory::pinfo("at target, angle = %f", parent_control->getHeading());
    parent_control->arcadeDrive(0.0, 0.0);
    return next_step;
}

/*****************************************/

MSSimpleDriveAngle::MSSimpleDriveAngle(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    tol = 2.0; // default values

    parent_control = (ArcadeDriveControl *)control;
    angle = xml->FloatAttribute("angle");
    power = xml->FloatAttribute("power"); // input as positive pretty please :)
    tol = xml->FloatAttribute("tolerance");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSimpleDriveAngle::init()
{
    count = 0;
    if (angle > 0.0)
    {
        // if we want to turn right,
        // turn power needs to be negative
        power *= -1;
    }

    parent_control->setArcadeMode();
    parent_control->resetEncoders();

    starting_angle = parent_control->getHeading();
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSimpleDriveAngle::update()
{
    if (!parent_control->isGyroConnected())
    {
        Advisory::pinfo("gyro not connected!");
        return next_step;
    }
    angle_traveled = parent_control->getHeading() - starting_angle;
    angle_error = angle - angle_traveled;

    //Advisory::pinfo("ms simple drive angle: %f, angle traveled: %f, target: %f", parent_control->getHeading(), angle_traveled, angle);

    if (abs(angle_traveled) < abs(angle) - tol)
    {
        if (abs(angle_error) < 20) // when close to target, switch to using kp
        {
            parent_control->arcadeDrive(0.0, power/abs(power) * angle_error * 0.019); // tune kp for memory
        }
        else
        {
            parent_control->arcadeDrive(0.0, power);
        }
        return this;
    }
    else if (abs(angle_traveled) > abs(angle) + tol)
    {
        parent_control->arcadeDrive(0.0, -power/4);
        return this;
    }
    else if (count < 5)
    {
        parent_control->arcadeDrive(0.0, 0.0);
        count++;
        //Advisory::pinfo("ms simple angle count: %i", count);
        return this;
    }

    // once robot reaches target
    parent_control->arcadeDrive(0.0, 0.0);
    return next_step;
}


/*****************************************/

MSDriveAngleDistance::MSDriveAngleDistance(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    dist_tol = 2.0;
    angle_tol = 2.0;

    parent_control = (ArcadeDriveControl *)control;
    dist = xml->FloatAttribute("distance");
    //Advisory::pinfo("dist: %f", dist);
    dist_power = xml->FloatAttribute("distance_power");
    //Advisory::pinfo("dist power: %f", dist_power);
    dist_tol = xml->FloatAttribute("distance_tolerance");
    //Advisory::pinfo("dist tolerance: %f", dist_tol);
    angle = xml->FloatAttribute("angle");
    //Advisory::pinfo("angle: %f", angle);
    angle_power = xml->FloatAttribute("angle_power"); // input as a positive value
    //Advisory::pinfo("angle power: %f", angle_power);
    angle_tol = xml->FloatAttribute("angle_tolerance");
    //Advisory::pinfo("angle tolerance: %f", angle_tol);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveAngleDistance::init()
{
    //int count = 0;
    if (angle > 0.0)
    {
        // if we want to turn right,
        // turn power needs to be negative
        angle_power *= -1;
    }

    parent_control->setArcadeMode();
    parent_control->resetEncoders();

    //float starting_angle = parent_control->getHeading();

    // just for reporting purposes
    parent_control->driveDistance(dist);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveAngleDistance::update()
{/*
    angle_traveled = parent_control->getHeading() - starting_angle;


    if (abs(angle_traveled) < abs(angle))
    {
        trn = angle_power;
    }
    else if (abs(angle_traveled) > abs(angle))
    {
        trn = -angle_power/4;
    }
    else if (count < 5)
    {
        trn = 0.0;
        count++;
        Advisory::pinfo("angle count: %i", count);
    }

    if (!(parent_control->isAtDistanceTarget(dist_tol) && abs(angle_traveled) < angle_tol))
    {
        parent_control->arcadeDrive(fwd, trn);
        return this;
    }

    // once robot reaches target
    */
    parent_control->arcadeDrive(0.0, 0.0);
    return next_step;
}


/**********************************************************
 **********************************************************/

MSDriveLLRotate::MSDriveLLRotate(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    tol = 2.0;
    
    parent_control = (ArcadeDriveControl *)control;
    xml->QueryFloatAttribute("power", &power); // input as positive pretty please :)
    xml->QueryFloatAttribute("tolerance", &tol);
    xml->QueryFloatAttribute("kp", &kp);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveLLRotate::init()
{
    count = 0;
    cycle_count = 0;
    trn = 0.0;
    parent_control->setArcadeMode();
    parent_control->resetEncoders();

    angle = parent_control->getLLHZAngle();

    Advisory::pinfo("MSDriveLLRotate::init target angle = %f", angle);
    // if (angle > 0.0) // turn right
    // {
    //     power = -power;
    // }
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveLLRotate::update()
{
    if (!parent_control->isGyroConnected())
    {
        Advisory::pinfo("gyro not connected!");
        return next_step;
    }

    if (cycle_count++ > 50)
    {
        parent_control->arcadeDrive(0.0, 0.0);
        return next_step;
    }

    /*
    angle_error = parent_control->getLLHZAngle();
    if (abs(angle_error) > 2.0)
    {
        trn = angle_error * -1 * 0.025;
        parent_control->arcadeDrive(0.0, trn);
        Advisory::pinfo("ll rotate error: %f, power: %f", angle_error, trn);
        return this;
    }
    */
    angle_traveled = parent_control->getHeading();
    angle_error = angle - angle_traveled;

    Advisory::pinfo("MSDriveLLRotate::update target angle = %f, actual = %f, error = %f, tol = %f, count = %d, cycles = %d", angle, angle_traveled, angle_error, tol, count, cycle_count);

    if (abs(angle_error) < tol)
    {
        parent_control->arcadeDrive(0.0, 0.0);

        if (count++ > 5)
        {
            return next_step;
        }
    }
    else
    {
        count = 0;
        trn = angle_error * kp;
        trn = gsu::Filter::limit(trn, -power, power);
        parent_control->arcadeDrive(0.0, -trn);

        return this;
    }

    return this;

    // if (abs(angle_traveled) < abs(angle) - tol)
    // {
    //     if (abs(angle_error) < 20)
    //     {
    //         fwd = -angle_error * 0.015;
    //         if (abs(fwd) < 0.1)
    //         {
    //             fwd /= abs(fwd);
    //             fwd *= 0.1;
    //         }
    //         parent_control->arcadeDrive(0.0, fwd);
    //     }
    //     else
    //     {
    //         parent_control->arcadeDrive(0.0, power);
    //     }
    //     return this;
    // }
    // else if (abs(angle_traveled) > abs(angle) + tol)
    // {
    //     parent_control->arcadeDrive(0.0, -power/4);
    //     return this;
    // }
    // else if (count < 5)
    // {
    //     parent_control->arcadeDrive(0.0, 0.0);
    //     count++;
    //     Advisory::pinfo("ms ll rotate count: %i", count);
    //     return this;
    // }

    // parent_control->arcadeDrive(0.0, 0.0);
    // return next_step;
}
/**********************************************************
 * 
 **********************************************************/

MSDriveBridge::MSDriveBridge(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    // get vals from xml
    m_wait = xml->BoolAttribute("wait");
    reverse = xml->BoolAttribute("reverse");
    fast_power = xml->FloatAttribute("fast_power");
    slow_power = xml->FloatAttribute("slow_power");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveBridge::init()
{
    pitch_on_bridge = 11.0;
    pitch_balanced = 9.0;
    state = 0;
    count = 0;
    max_count = 5;
    if (reverse)
    {
        fast_power = -fast_power;
        slow_power = -slow_power;
    }
    start_time = gsi::Time::getTime();
}

/*******************************************************************************
 * borrowed from 3683's openautobalance <3
 ******************************************************************************/
MacroStep *MSDriveBridge::update()
{
    if (m_wait == true)
    {
        if (!parent_control->isGyroConnected())
        {
            Advisory::pinfo("gyro not connected!");
            return next_step;
        }
        pitch = parent_control->getPitch(); // getroll for ahoy, getpitch for memory
        if ((gsi::Time::getTime() - start_time) > 10.0)
        {
            // lets not balance on the opponents bridge ok? :)
            return next_step;
        }
        switch (state)
        {
            // drive forwards to approach station, exit when tilt is detected
            case 0:
                if (abs(pitch) > pitch_on_bridge)
                {
                    count++;
                    //Advisory::pinfo("state 0, count %i", count);
                }
                if (count > max_count)
                {
                    state = 1;
                    count = 0;
                    fwd = slow_power;
                    break;
                }
                // otherwise, if not contacting the bridge yet:
                fwd = fast_power;
                break;

            // driving up charge station, drive slower, stopping when level
            case 1:
                if (abs(pitch) < pitch_balanced)
                {
                    //count++; 
                    //Advisory::pinfo("state 1, count %i", count);
                   
                    state = 2;
                    count = 0;
                    fwd = 0.0;
                    break;
                }
                if (count > max_count)
                {
                    state = 2;
                    count = 0;
                    fwd = 0.0;
                    break;
                }
                // if not on bridge yet
                fwd = slow_power;
                break;

            // on charge station, stop motors and wait for end of auto
            case 2:
                max_count = 0;
                if(abs(pitch) <= pitch_balanced)
                { 
                    count++;
                    //Advisory::pinfo("state 2, count %i", count);
                }
                if (count > max_count)
                {
                    state = 4;
                    count = 0;
                    fwd = 0.0;
                    parent_control->arcadeDrive(0.0, 0.0);
                    Advisory::pinfo("balanced???????????");
                    return next_step;
                    break;
                }
                if (pitch >= pitch_balanced)
                {
                    fwd = 0.1; // TODO adjust value
                    break;
                }
                else if (pitch <= -pitch_balanced)
                {
                    fwd = -0.1; // TODO adjust value
                    break;
                }
                break;            

            case 4:
                return next_step;

            default:
                fwd = 0.0;
                break;
        }
        Advisory::pinfo("stage: %i, pitch: %f, fwd power: %f", state, pitch, fwd);
        parent_control->arcadeDrive(fwd, 0.0);
        return this;
    }
    parent_control->arcadeDrive(0.0, 0.0);
    return next_step;
}

/**********************************************************
 *
 **********************************************************/

MSDriveBrakeState::MSDriveBrakeState(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    // get vals from xml
    state = xml->BoolAttribute("state");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveBrakeState::init()
{
    parent_control->setBrakeState(state);
    Advisory::pinfo("brakes engadged in auton %s", state ? "true" : "false");
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveBrakeState::update()
{
    return next_step;
}


/*****************************************/

MSDriveDistanceOnHeading::MSDriveDistanceOnHeading(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    
    tol = 2.0;

    xml->QueryFloatAttribute("target_heading", &target_heading);
    xml->QueryFloatAttribute("max_fwd_power", &max_fwd_power);
    xml->QueryFloatAttribute("distance", &target_dist);
    xml->QueryFloatAttribute("tol", &tol);
    xml->QueryFloatAttribute("kp", &kp);
    xml->QueryFloatAttribute("trn_kp", &trn_kp);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveDistanceOnHeading::init()
{
    parent_control->setArcadeMode();
    parent_control->resetEncoders(); // init distance to 0

    // just for reporting purposes
    parent_control->driveDistance(target_dist);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveDistanceOnHeading::update()
{
    if (!parent_control->isGyroConnected())
    {
        Advisory::pinfo("gyro not connected!");
        return next_step;
    }
    if (!(parent_control->isAtDistanceTarget(tol))) // if not at target
    {
        dist_error = target_dist - parent_control->getAvgEncoderDistance();
        heading_error = target_heading - parent_control->getHeading();
        fwd = dist_error * kp;// todo tune for memory
        fwd = gsu::Filter::limit(fwd, -max_fwd_power, max_fwd_power);
        trn = -1 * heading_error * trn_kp * abs(fwd);
        trn = gsu::Filter::limit(trn, -0.15, 0.15);
        Advisory::pinfo("yippee!!, distance = %f, heading = %f, fwd = %f, trn = %f", parent_control->getAvgEncoderDistance(), parent_control->getHeading(), fwd, trn);
        parent_control->arcadeDrive(fwd, trn);
        return this;
    }
    // once robot reaches target
    parent_control->arcadeDrive(0.0, 0.0);
    Advisory::pinfo("ending ms drive dist on heading, distance = %f, heading = %f", parent_control->getAvgEncoderDistance(), parent_control->getHeading());
    return next_step;
}

/*****************************************/

MSDriveDistanceOnCurrentHeading::MSDriveDistanceOnCurrentHeading(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    
    tol = 2.0;

    xml->QueryFloatAttribute("max_fwd_power", &max_fwd_power);
    xml->QueryFloatAttribute("distance", &target_dist);
    xml->QueryFloatAttribute("tol", &tol);
    xml->QueryFloatAttribute("kp", &kp);
    xml->QueryFloatAttribute("trn_kp", &trn_kp);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveDistanceOnCurrentHeading::init()
{
    target_heading = parent_control->getHeading();
    Advisory::pinfo("target heading: %f", target_heading);
    parent_control->setArcadeMode();
    parent_control->resetEncoders(); // init distance to 0

    // just for reporting purposes
    parent_control->driveDistance(target_dist);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSDriveDistanceOnCurrentHeading::update()
{
    if (!parent_control->isGyroConnected())
    {
        Advisory::pinfo("gyro not connected!");
        return next_step;
    }
    if (!(parent_control->isAtDistanceTarget(tol))) // if not at target
    {
        dist_error = target_dist - parent_control->getAvgEncoderDistance();
        heading_error = target_heading - parent_control->getHeading();
        fwd = dist_error * kp;// todo tune for memory
        fwd = gsu::Filter::limit(fwd, -max_fwd_power, max_fwd_power);
        trn = -1 * heading_error * trn_kp * abs(fwd);
        trn = gsu::Filter::limit(trn, -0.15, 0.15);
        Advisory::pinfo("yippee!!, distance = %f, heading = %f, fwd = %f, trn = %f", parent_control->getAvgEncoderDistance(), parent_control->getHeading(), fwd, trn);
        parent_control->arcadeDrive(fwd, trn);
        return this;
    }

    // once robot reaches target
    parent_control->arcadeDrive(0.0, 0.0);
    Advisory::pinfo("ending ms drive dist on current heading, distance = %f, heading = %f", parent_control->getAvgEncoderDistance(), parent_control->getHeading());
    return next_step;
}


// =============================================================================
// NOTE: everything below this is copied from robonauts 2017
// TODO pray to god that this works
// =============================================================================

// =============================================================================
// === MSCartesianMode Methods
// =============================================================================
/*******************************************************************************
 * CARTESIAN MACROS
 ******************************************************************************/
MSCartesianMode::MSCartesianMode(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSCartesianMode::init()
{
    parent_control->setCartesianMode();
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSCartesianMode::update()
{
    return next_step;
}

// =============================================================================
// === MSDriveCartesian Methods
// =============================================================================
/*******************************************************************************
 * JLY read in values for all segments and add to arrays
 ******************************************************************************/
MSDriveCartesian::MSDriveCartesian(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    XMLElement *segment;
    float x = 0, y = 0, end_vel = 0;
    float round_dist = 0.0;
    int round_pts = 0;
    bool blend = false;

    // initialize member variables
    m_reset_coordinates = false;
    m_vision_exit_distance = 0;
    m_vision_exit_criteria = false;

    // m_init_time = parent_control->getPhaseElapsedTime();
    m_x_cmd = new std::vector<float>();
    m_y_cmd = new std::vector<float>();
    m_end_vel = new std::vector<float>();
    m_blend = new std::vector<bool>();
    m_round_pts = new std::vector<int>();
    m_round_dist = new std::vector<float>();

    m_tolerance = 3.0;
    m_timeout = 1.5;             // timeout is multipled by the time of the trajectory (1.0 is timeout when the trajectory
                                 // completes, perhaps not allowing robot to get there)
    m_ignore_start_inches = 4.0; // give a couple inches to allow angles to be manageable
    m_ignore_end_inches = 4.0;   // give a couple inches to allow angles to be manageable

    // end initialize member variables

    xml->QueryFloatAttribute("tolerance", &m_tolerance); // rotate is the angle to turn (in absolute)
    xml->QueryFloatAttribute("time_out", &m_timeout);
    xml->QueryFloatAttribute("ignore_start", &m_ignore_start_inches);
    xml->QueryFloatAttribute("ignore_end", &m_ignore_end_inches);
    xml->QueryBoolAttribute("reset_coordinates", &m_reset_coordinates);
    xml->QueryFloatAttribute("x_distance_exit", &m_vision_exit_distance);
    xml->QueryBoolAttribute("vision_exit", &m_vision_exit_criteria);

    segment = xml->FirstChildElement("seg");
    while (segment != nullptr)
    {
        segment->QueryFloatAttribute("x", &x);
        segment->QueryFloatAttribute("y", &y);
        segment->QueryFloatAttribute("end_vel", &end_vel);
        segment->QueryBoolAttribute("blend", &blend);
        segment->QueryIntAttribute("round_points", &round_pts);
        segment->QueryFloatAttribute("round_dist", &round_dist);
        m_x_cmd->push_back(x);
        m_y_cmd->push_back(y);
        m_end_vel->push_back(end_vel);
        m_blend->push_back(blend);
        m_round_dist->push_back(round_dist);
        m_round_pts->push_back(round_pts);
        segment = segment->NextSiblingElement("seg");
    }
}

/*******************************************************************************
 * JLY puts values in global scope
 ******************************************************************************/
void MSDriveCartesian::init()
{
    parent_control->initializeCartesianModeCmd(m_timeout, m_x_cmd, m_y_cmd, m_end_vel, m_blend, m_round_pts, m_round_dist,
                                               m_tolerance, m_ignore_start_inches, m_ignore_end_inches, m_reset_coordinates);

    if (m_reset_coordinates == true)
    {
        parent_control->resetEncoders();
    }
    // m_init_time = parent_control->getPhaseElapsedTime();
}

/*******************************************************************************
 * JLY don't go to next step until trajectory is done. processing happens in docartesianmode()
 ******************************************************************************/
MacroStep *MSDriveCartesian::update()
{
    // TODO this should work, remove ::Cartcompletestatus if not (replace with uint8_t ??)
    ArcadeDriveControl::CartCompleteStatus ccs = (ArcadeDriveControl::CartCompleteStatus)parent_control->cartesianSegmentDone();
    // note: cartesian, the exit criteria will be managed in the main thread
    MacroStep *return_val = this;

    switch (ccs)
    {
    case ArcadeDriveControl::CCS_RUNNING:
        return_val = this;
        break;
    case ArcadeDriveControl::CCS_DONE:
        Advisory::pinfo("MSDriveCartesian::done");
        return_val = next_step;
        break;
    case ArcadeDriveControl::CCS_TIMEOUT:
        Advisory::pinfo("MSDriveCartesian::timeout");
        return_val = next_step;
        break;
    case ArcadeDriveControl::CCS_ABORT:
        abort();
        parent_control->setArcadeMode();
        return_val = nullptr;
        break;
    default:
        abort();
        parent_control->setArcadeMode();
        return_val = nullptr;
    }
    /*
        if (parent_control->checkUserAbortButton() == true) // drive commanded abort
        {
            Advisory::pinfo("MSDriveCartesian::driver abort");
            parent_control->exitDriveHybridMode();
            abort();
            parent_control->setDrivePowerMode();
            parent_control->drivePower(0.0, 0.0);
            return_val = nullptr;
        }
        */
    // TODO

    return return_val;
}

MSSetPipeline::MSSetPipeline(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (ArcadeDriveControl *)control;
    pipeline = 0;
    xml->QueryIntAttribute("pipeline", &pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetPipeline::init()
{

    parent_control->setPipeline(pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSetPipeline::update()
{
    return next_step;
}