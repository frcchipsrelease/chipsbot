/*******************************************************************************
 *
 * File: SwerveDriveControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *  MSDriveBalance code based on FRC Team 3683's OpenAutoBalance
 *  https://github.com/FRC3683/OpenAutoBalance
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include "SwerveDriveControl.h"

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Time.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
SwerveDriveControl::SwerveDriveControl(std::string control_name, XMLElement *xml)
    : PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating 2023 Summer Swerve ☀️ Control =========================");

	const char *name = nullptr;

	front_right = nullptr;
	front_left = nullptr;
	back_left = nullptr;
	back_right = nullptr;
	gyro = nullptr;

	heading = 0.0;
	pitch = 0.0;
	roll = 0.0;

	l = 1.0;
	w = 1.0;
	r = 1.0;

	forward_cmd = 0.0;
	strafe_cmd = 0.0;
	turn_cmd = 0.0;
	power_cmd = 0.0;
	forward_input = 0.0;
	strafe_input = 0.0;

	using_new_input_mode = false;

	drive_scheme = 0; // field oriented
	default_drive_scheme = 0;

	limelight = nullptr;

	ll_target_visible = 0.0;
	ll_target_angle_horizontal = 0.0;
	ll_target_angle_vertical = 0.0;
	ll_target_area = 0.0;
	ll_target_skew = 0.0;
	ll_pipeline = 0;

	ll_forward_cmd = 0.0;
	ll_strafe_cmd = 0.0;
	ll_turn_cmd = 0.0;


	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSXWheels>(control_name, "XWheels", this);
	new MacroStepProxy<MSSetLLPipeline>(control_name, "SetPipeline", this);
	new MacroStepProxy<MSSetDriveScheme>(control_name, "SetDriveScheme", this);
	new MacroStepProxy<MSFieldDrivePower>(control_name, "DrivePower", this);
	new MacroStepProxy<MSFieldDriveHeading>(control_name, "DriveHeading", this);
	new MacroStepProxy<MSFieldRotateByAngle>(control_name, "DriveAngle", this);
	new MacroStepProxy<MSDriveBalance>(control_name, "DriveBalance", this);

	//
	// Parse XML
	//

	xml->QueryFloatAttribute("wheel_base", &l);
	xml->QueryFloatAttribute("track_width", &w);
	r = std::sqrt(l*l + w*w);
	xml->QueryIntAttribute("default_drive_scheme", &default_drive_scheme);
	xml->QueryBoolAttribute("using_new_input_mode", &using_new_input_mode);

	comp = xml-> FirstChildElement("swerve_module");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "front_right") == 0)
			{
				Advisory::pinfo("  creating controller for FRONT RIGHT module");
				front_right = new SwerveModule(comp);
			}
			else if(strcmp(name, "front_left") == 0)
			{
				Advisory::pinfo("  creating controller for FRONT LEFT module");
				front_left = new SwerveModule(comp);
			}
			else if(strcmp(name, "back_left") == 0)
			{
				Advisory::pinfo("  creating controller for BACK LEFT module");
				back_left = new SwerveModule(comp);
			}
			else if(strcmp(name, "back_right") == 0)
			{
				Advisory::pinfo("  creating controller for BACK RIGHT module");
				back_right = new SwerveModule(comp);
			}
			else
			{
				Advisory::pwarning("found unexpected swerve module with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected swerve module with no name attribute");
		}
		comp = comp -> NextSiblingElement("swerve_module");
	}
	
    comp = xml->FirstChildElement("gyro");
    if (comp != nullptr)
    {
        // if type NAVX or Pigeon then
        Advisory::pinfo("  creating gyro");
        gyro = HardwareFactory::createGyro(comp);
        Advisory::pinfo("  created gyro");
        if (gyro->isConnected())
        {
            Advisory::pinfo("gyro IS connected :)");
        }
        else
        {
            Advisory::pinfo("gyro is NOT connected !!!!!!!!!!!!!!!!!");
        }
    }

    comp = xml->FirstChildElement("limelight");
    if (comp != nullptr)
    {
        limelight = HardwareFactory::createLimelight(comp);
        Advisory::pinfo("  created limelight");
    }

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				OIController::subscribeAnalog(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "strafe") == 0)
			{
				Advisory::pinfo("  connecting strafe channel");
				OIController::subscribeAnalog(comp, this, CMD_STRAFE);
			}
			else if (strcmp(name, "turn") == 0)
			{
				Advisory::pinfo("  connecting turn channel");
				OIController::subscribeAnalog(comp, this, CMD_TURN);
			}
			else if (strcmp(name, "power") == 0)
			{
				Advisory::pinfo("  connecting power channel");
				OIController::subscribeAnalog(comp, this, CMD_POWER);
			}
			else if (strcmp(name, "gyro_reset") == 0)
			{
				Advisory::pinfo("  connecting gyro reset channel");
				OIController::subscribeDigital(comp, this, CMD_GYRO_RESET);
			}
			else if (strcmp(name, "x_wheels") == 0)
			{
				Advisory::pinfo("  connecting x wheels channel");
				OIController::subscribeDigital(comp, this, CMD_X_WHEELS);
			}
			else if (strcmp(name, "reset_wheels") == 0)
			{
				Advisory::pinfo("  connecting reset wheels channel");
				OIController::subscribeDigital(comp, this, CMD_RESET_WHEELS);
			}
			else if (strcmp(name, "toggle_scheme") == 0)
			{
				Advisory::pinfo("  connecting toggle drive scheme channel");
				OIController::subscribeDigital(comp, this, CMD_FIELD_ORIENT_TOGGLE);
			}
			else if (strcmp(name, "ll_rot_hold") == 0)
			{
				Advisory::pinfo("  connecting limelight rotate (hold) channel");
				OIController::subscribeDigital(comp, this, CMD_LL_ROT_HOLD);
			}
			else if (strcmp(name, "ll_strafe_hold") == 0)
			{
				Advisory::pinfo("  connecting limelight strafe (hold) channel");
				OIController::subscribeDigital(comp, this, CMD_LL_STRAFE_HOLD);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
		
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
SwerveDriveControl::~SwerveDriveControl(void)
{
	if (front_right != nullptr)
	{
		delete front_right;
		front_right = nullptr;
	}
	if (front_left != nullptr)
	{
		delete front_left;
		front_left = nullptr;
	}
	if (back_left != nullptr)
	{
		delete back_left;
		back_left = nullptr;
	}
	if (back_right != nullptr)
	{
		delete back_right;
		back_right = nullptr;
	}
	if (gyro != nullptr)
	{
		delete gyro;
		gyro = nullptr;
	}
	if (limelight != nullptr)
	{
		delete limelight;
		limelight = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void SwerveDriveControl::controlInit(void)
{ 	
}

/*******************************************************************************
 *
 ******************************************************************************/
void SwerveDriveControl::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void SwerveDriveControl::disabledInit(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void SwerveDriveControl::autonomousInit(void)
{
	setPipeline(0); // default
	resetWheels(); // surely this will not cause problems
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void SwerveDriveControl::teleopInit(void)
{
	front_right->resetEncoders();
	Advisory::pinfo("FR encoders reset");
	front_left->resetEncoders();
	Advisory::pinfo("FL encoders reset");
	back_left->resetEncoders();
	Advisory::pinfo("BL encoders reset");
	back_right->resetEncoders();
	Advisory::pinfo("BR encoders reset");
	//setPipeline(0); // default
	setDriveScheme(default_drive_scheme);
	forward_axis = 0.0;
	strafe_axis = 0.0;
	power_cmd = 0.0;
	resetWheels();
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void SwerveDriveControl::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void SwerveDriveControl::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_FORWARD:
		{
			forward_input = val;
		} break;
		
		case CMD_STRAFE:
		{
			strafe_input = val;
		} break;

		case CMD_TURN:
		{
			turn_cmd = val;
		} break;

		case CMD_POWER:
		{
			power_cmd = val;
		} break;
			
		default:
			break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void SwerveDriveControl::setDigital(int id, bool val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_GYRO_RESET:
		{
			if (val)
			{
				gyro->reset();
				Advisory::pinfo("gyro reset!!! new heading: %f", gyro->getAngle());
			}
		} break;

		case CMD_X_WHEELS:
		{
			if (val)
			{
				drive_scheme = DRIVE_X_WHEELS;
				Advisory::pinfo("wheels set to X position");
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;

		case CMD_RESET_WHEELS:
		{
			if (val)
			{
				drive_scheme = DRIVE_RESET_WHEELS;
				Advisory::pinfo("wheels facing forward");
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;
		
		case CMD_FIELD_ORIENT_TOGGLE: // toggles between field- and robot-oriented drive
		{
			if (val)
			{
				if (drive_scheme) // if drive scheme is not 0 (field oriented)
				{
					Advisory::pinfo("setting drive scheme to FIELD ORIENTED");
					drive_scheme = DRIVE_FIELD;
				}
				else
				{
					Advisory::pinfo("setting drive scheme to ROBOT ORIENTED");
					drive_scheme = DRIVE_ROBOT;
				}
			}
		} break;

		case CMD_LL_ROT_HOLD:
		{
			if (val)
			{
				drive_scheme = DRIVE_LL_ROT;
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;

		case CMD_LL_STRAFE_HOLD:
		{
			if (val)
			{
				drive_scheme = DRIVE_LL_STRAFE;
			}
			else
			{
				drive_scheme = default_drive_scheme;
			}
		} break;
		
		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void SwerveDriveControl::setInt(int id, int val)
{
}

/*****************************************************************************
 * Locks wheels in an "X" position
 ****************************************************************************/
void SwerveDriveControl::xWheels(void)
{
	front_right->setSteeringPosition(45.0);
	front_left->setSteeringPosition(-45.0);
	back_left->setSteeringPosition(45.0);
	back_right->setSteeringPosition(-45.0);
	front_right->setDrivePower(0.0);
	front_left->setDrivePower(0.0);
	back_left->setDrivePower(0.0);
	back_right->setDrivePower(0.0);
}

/*****************************************************************************
 * Rotates all wheels to face forward
 ****************************************************************************/
void SwerveDriveControl::resetWheels(void)
{
	front_right->setSteeringPosition(0.0);
	front_left->setSteeringPosition(0.0);
	back_left->setSteeringPosition(0.0);
	back_right->setSteeringPosition(0.0);
}

/*****************************************************************************
 * Updates wheel motors and sensors
 ****************************************************************************/
void SwerveDriveControl::updateModules(void)
{
	front_right->update();
	front_left->update();
	back_left->update();
	back_right->update();
}

/*****************************************************************************
 * Sets Limelight pipeline to specified index
 * @param index 
 ****************************************************************************/
void SwerveDriveControl::setPipeline(int index)
{
	limelight->setPipeline(index);
	ll_pipeline = index;
	Advisory::pinfo("set ll pipeline to %i", index);
}

/*****************************************************************************
 * Sets drive scheme to specified index
 * @param index 
 ****************************************************************************/
void SwerveDriveControl::setDriveScheme(int index)
{
	drive_scheme = index;
	Advisory::pinfo("set drive scheme to %i", index);
}

/*****************************************************************************
 * Returns true if gyro is connected and sending data to the roborio
 ****************************************************************************/
bool SwerveDriveControl::isGyroConnected(void)
{
	if (gyro != nullptr)
	{
		gyro_connected = gyro->isConnected();
	}
	else
	{
		gyro_connected = false;
	}
	return gyro_connected;
}


/*****************************************************************************
 * Returns the heading of the robot
 ****************************************************************************/
float SwerveDriveControl::getHeading(void)
{
	heading = gyro->getAngle();
	return heading;
}

/*****************************************************************************
 * Returns the pitch of the robot
 ****************************************************************************/
float SwerveDriveControl::getPitch(void)
{
	pitch = gyro->getPitch();
	return pitch;
}

/*****************************************************************************
 * Returns the roll of the robot
 ****************************************************************************/
float SwerveDriveControl::getRoll(void)
{
	roll = gyro->getAngle();
	return roll;
}

/*******************************************************************************
 *
 * calculations from https://www.chiefdelphi.com/t/paper-4-wheel-independent-drive-independent-steering-swerve/107383
 * 
 * @param xCmd [-1.0, 1.0] - positive is right
 * 
 * @param yCmd [-1.0, 1.0] - positive is forward
 *
 * @param rot Rotational speed (rad/s) - positive is CW
 * 
 ******************************************************************************/
void SwerveDriveControl::robotRelativeDrive(float xCmd, float yCmd, float rot)
{
	if (abs(xCmd) > 0.03 || abs(yCmd) > 0.03 || abs(rot) > 0.03)
	{
		// 1. convert input to speeds
		float robot_xcmd = xCmd;
		float robot_ycmd = yCmd;
		
		// 2. convert robot rel. cmds to wheel speeds and angles (inverse kinematics)
		// todo make function
		float a = robot_xcmd - rot * l/r; 
		float b = robot_xcmd + rot * l/r;
		float c = robot_ycmd - rot * w/r;
		float d = robot_ycmd + rot * w/r;

		wheelSpeeds[0] = std::sqrt(b*b + c*c);	wheelAngles[0] = std::atan2(b, c) * RAD_TO_DEG;
		wheelSpeeds[1] = std::sqrt(b*b + d*d);	wheelAngles[1] = std::atan2(b, d) * RAD_TO_DEG;
		wheelSpeeds[2] = std::sqrt(a*a + d*d);	wheelAngles[2] = std::atan2(a, d) * RAD_TO_DEG;
		wheelSpeeds[3] = std::sqrt(a*a + c*c);	wheelAngles[3] = std::atan2(a, c) * RAD_TO_DEG;

		// 3. normalize wheel speeds to max
		// speeds are now [0.0, 1.0]
		// angles are [-180.0, 180.0) (cw +)
		normalizeSpeeds(wheelSpeeds);

		// 4. command each module to desired state
		front_right->set(wheelSpeeds[0], wheelAngles[0]);
		front_left->set(wheelSpeeds[1], wheelAngles[1]);
		back_left->set(wheelSpeeds[2], wheelAngles[2]);
		back_right->set(wheelSpeeds[3], wheelAngles[3]);
	}
	else
	{
		front_right->setDrivePower(0.0);
		front_left->setDrivePower(0.0);
		back_left->setDrivePower(0.0);
		back_right->setDrivePower(0.0);
	}
}

/*******************************************************************************
 * 
 * @param xCmd [-1.0, 1.0] - positive is right
 * 
 * @param yCmd [-1.0, 1.0] - positive is forward
 *
 * @param rot [-1.0, 1.0] - positive is CW
 * 
 * calculations from https://www.chiefdelphi.com/t/paper-4-wheel-independent-drive-independent-steering-swerve/107383
 * 
 ******************************************************************************/
void SwerveDriveControl::fieldRelativeDrive(float xCmd, float yCmd, float rot)
{
	if (abs(xCmd) > 0.03 || abs(yCmd) > 0.03 || abs(rot) > 0.03)
	{
		// 1. convert field rel cmds to robot rel cmds
		float heading_rad = heading * DEG_TO_RAD;
		float robot_xcmd = -1 * yCmd * sin(heading_rad) + xCmd * cos(heading_rad);
		float robot_ycmd = yCmd * cos(heading_rad) + xCmd * sin(heading_rad);
		
		// 2. convert robot rel. cmds to wheel speeds and angles (inverse kinematics)
		// todo make function
		float a = robot_xcmd - rot * l/r; 
		float b = robot_xcmd + rot * l/r;
		float c = robot_ycmd - rot * w/r;
		float d = robot_ycmd + rot * w/r;

		wheelSpeeds[0] = std::sqrt(b*b + c*c);	wheelAngles[0] = std::atan2(b, c) * RAD_TO_DEG;
		wheelSpeeds[1] = std::sqrt(b*b + d*d);	wheelAngles[1] = std::atan2(b, d) * RAD_TO_DEG;
		wheelSpeeds[2] = std::sqrt(a*a + d*d);	wheelAngles[2] = std::atan2(a, d) * RAD_TO_DEG;
		wheelSpeeds[3] = std::sqrt(a*a + c*c);	wheelAngles[3] = std::atan2(a, c) * RAD_TO_DEG;

		// 3. normalize wheel speeds to max
		// speeds are now [0.0, 1.0]
		// angles are [-180.0, 180.0) (cw +)
		normalizeSpeeds(wheelSpeeds);

		// 4. command each module to desired state
		front_right->set(wheelSpeeds[0], wheelAngles[0]);
		front_left-> set(wheelSpeeds[1], wheelAngles[1]);
		back_left->  set(wheelSpeeds[2], wheelAngles[2]);
		back_right-> set(wheelSpeeds[3], wheelAngles[3]);
	}
	else
	{
		front_right->setDrivePower(0.0);
		front_left->setDrivePower(0.0);
		back_left->setDrivePower(0.0);
		back_right->setDrivePower(0.0);
	}
}

void SwerveDriveControl::normalizeSpeeds(float (speeds)[4])
{
	float max = speeds[0];
	if (speeds[1] > max) {max = speeds[1];}
	if (speeds[2] > max) {max = speeds[2];}
	if (speeds[3] > max) {max = speeds[3];}

	if (max > 1)
	{
		speeds[0] /= max;
		speeds[1] /= max;
		speeds[2] /= max;
		speeds[3] /= max;
	}
}



/*******************************************************************************	
 *
 ******************************************************************************/
void SwerveDriveControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());
	SmartDashboard::PutNumber(getName() + " FRONT LEFT drive pwr: ", front_left->getDrivePower());
	SmartDashboard::PutNumber(getName() + " FRONT LEFT steering pwr: ", front_left->getSteeringPower());
	SmartDashboard::PutNumber(getName() + " FRONT LEFT cc pos actual: ", front_left->getEncoderPosition());
	SmartDashboard::PutNumber(getName() + " FRONT LEFT cc pos target: ", front_left->getEncoderPositionTarget());
	SmartDashboard::PutNumber(getName() + " FRONT LEFT cc pos error: ", front_left->getSteeringPositionError());
	
	SmartDashboard::PutNumber(getName() + " FRONT RIGHT drive pwr: ", front_right->getDrivePower());
	SmartDashboard::PutNumber(getName() + " FRONT RIGHT steering pwr: ", front_right->getSteeringPower());
	SmartDashboard::PutNumber(getName() + " FRONT RIGHT cc pos actual: ", front_right->getEncoderPosition());
	SmartDashboard::PutNumber(getName() + " FRONT RIGHT cc pos target: ", front_right->getEncoderPositionTarget());
	SmartDashboard::PutNumber(getName() + " FRONT RIGHT cc pos error: ", front_right->getSteeringPositionError());
	
	SmartDashboard::PutNumber(getName() + " BACK LEFT drive pwr: ", back_left->getDrivePower());
	SmartDashboard::PutNumber(getName() + " BACK LEFT steering pwr: ", back_left->getSteeringPower());
	SmartDashboard::PutNumber(getName() + " BACK LEFT cc pos actual: ", back_left->getEncoderPosition());
	SmartDashboard::PutNumber(getName() + " BACK LEFT cc pos target: ", back_left->getEncoderPositionTarget());
	SmartDashboard::PutNumber(getName() + " BACK LEFT cc pos error: ", back_left->getSteeringPositionError());
	
	SmartDashboard::PutNumber(getName() + " BACK RIGHT drive pwr: ", back_right->getDrivePower());
	SmartDashboard::PutNumber(getName() + " BACK RIGHT steering pwr: ", back_right->getSteeringPower());
	SmartDashboard::PutNumber(getName() + " BACK RIGHT cc pos actual: ", back_right->getEncoderPosition());
	SmartDashboard::PutNumber(getName() + " BACK RIGHT cc pos target: ", back_right->getEncoderPositionTarget());
	SmartDashboard::PutNumber(getName() + " BACK RIGHT cc pos error: ", back_right->getSteeringPositionError());
	
	SmartDashboard::PutNumber(getName() + " heading: ", heading);
	SmartDashboard::PutNumber(getName() + " pitch: ", pitch);
	SmartDashboard::PutNumber(getName() + " roll: ", roll);
	SmartDashboard::PutString(getName() + " gyro connected: ", gyro_connected ? "true" : "false");
	SmartDashboard::PutNumber(getName() + " drive scheme: ", drive_scheme);
	SmartDashboard::PutString(getName() + " using new input mode? ", using_new_input_mode ? "true" : "false");
	
	SmartDashboard::PutString(getName() + " limelight target visible? ", ll_target_visible ? "true" : "false");
	SmartDashboard::PutNumber(getName() + " limelight target angle HZ: ", ll_target_angle_horizontal);
	SmartDashboard::PutNumber(getName() + " limelight pipeline: ", ll_pipeline);

	SmartDashboard::PutNumber(getName() + " fwd cmd: ", forward_cmd);
	SmartDashboard::PutNumber(getName() + " strafe cmd: ", strafe_cmd);
	SmartDashboard::PutNumber(getName() + " turn cmd: ", turn_cmd);
	
	
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed
 * 
 ******************************************************************************/
void SwerveDriveControl::doPeriodic()
{
	// Read Sensors
	isGyroConnected();
	if (gyro != nullptr && gyro_connected)
	{
		heading = gyro->getAngle();
		pitch = gyro->getPitch();
		roll = gyro->getRoll();
	}
	else
	{
		heading = 0.0;
	}

    if (limelight != nullptr)
    {
        limelight->getData(ll_target_visible,
						   ll_target_angle_horizontal,
						   ll_target_angle_vertical,
						   ll_target_area,
						   ll_target_skew);
    }

	// if using_new_input_mode:
	if (using_new_input_mode)
	{
		/*
		calculate magnitude from distance formula
		if magnitude > threshold: save x and y axis in new variable
		else: do nothing
		
		fwd cmd = y axis * power (from r trigger axis)
		strafe cmd = x axis * power
		*/
		if (std::sqrt(forward_input*forward_input + strafe_input*strafe_input) > 0.65)
		{
			forward_axis = forward_input; // stores joystick cmd in axis variable
			strafe_axis = strafe_input;
		}
		// calculate new commands
		forward_cmd = forward_axis * power_cmd; // power is r trigger axis value [0.0, 1.0]
		strafe_cmd = strafe_axis * power_cmd;
	}
	// else:
	// do nothing! feed the joystick axis values into drive method
	else
	{
		forward_cmd = forward_input;
		strafe_cmd = strafe_input;
	}

	// Command Modules
	if (drive_scheme == DRIVE_LL_STRAFE) // = 2
	{
		if (abs(ll_target_angle_horizontal) > 0.07)
		{
			ll_strafe_cmd = ll_target_angle_horizontal * 0.02;
			ll_strafe_cmd = gsu::Filter::limit(ll_strafe_cmd, -0.2, 0.2);
		}
		else
		{
			ll_strafe_cmd = 0.0;
		}
		robotRelativeDrive(ll_strafe_cmd, forward_cmd, turn_cmd);
	}
	else if (drive_scheme == DRIVE_LL_ROT) // = 3
	{
		if (abs(ll_target_angle_horizontal) > 0.07)
		{
			ll_turn_cmd = ll_target_angle_horizontal * 0.015;
			ll_turn_cmd = gsu::Filter::limit(ll_turn_cmd, -0.2, 0.2);
		}
		else
		{
			ll_turn_cmd = 0.0;
		}
		fieldRelativeDrive(strafe_cmd, forward_cmd, ll_turn_cmd);
	}
	else if (getPhase() == TELEOP)
	{
		if (drive_scheme == DRIVE_FIELD) // = 0
		{
			fieldRelativeDrive(strafe_cmd, forward_cmd, turn_cmd);
		}
		else if (drive_scheme == DRIVE_ROBOT) // = 1
		{
			robotRelativeDrive(strafe_cmd, forward_cmd, turn_cmd);
		}
		else if (drive_scheme == DRIVE_X_WHEELS)
		{
			xWheels();
		}
		else if (drive_scheme == DRIVE_RESET_WHEELS)
		{
			resetWheels();
		}
	}
	// else if phase = auton: do nothing, motors will be controlled by auton routine

	// else if: trajectory mode
	// do: ????
	updateModules();
}

/*******************************************************************************
 * Drives (field-oriented) at a specified power for a specified time. 
 * 
 * If you want to do something else simultaneously (like intake a game piece), 
 * you MUST use MSSplit!!!
 * @todo jly find a workaround for this
 * 
 * @param . learn to read
 ******************************************************************************/
MSFieldDrivePower::MSFieldDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (SwerveDriveControl *)control;

	xml->QueryFloatAttribute("fwd", &fwd);
	xml->QueryFloatAttribute("strafe", &strafe);
	xml->QueryFloatAttribute("rot", &rot);
	xml->QueryFloatAttribute("time", &time);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSFieldDrivePower::init(void)
{
	Advisory::pinfo("MSFieldDrivePower: str = %f, fwd = %f, rot = %f for t = %f s", strafe, fwd, rot, time);
	parent_control->fieldRelativeDrive(strafe, fwd, rot);
	start_time = gsi::Time::getTime();
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSFieldDrivePower::update(void)
{
	// i have to do this because field oriented updates constantly based on heading >:(
	if ((gsi::Time::getTime() - start_time) < time)
	{
		parent_control->fieldRelativeDrive(strafe, fwd, rot);
		return this;
	}
	parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
	return next_step;
}

/*******************************************************************************
 * Rotates to a specified heading given a max power.
 * @param heading target heading in degrees
 * @param max_power maximum power...
 ******************************************************************************/
MSFieldDriveHeading::MSFieldDriveHeading(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (SwerveDriveControl *)control;

	target_heading = 0.0;
	max_power = 0.2;
	timeout = 2.0;
	tolerance = 2.0;
	kp = 0.015;

	xml->QueryFloatAttribute("heading", &target_heading);
	xml->QueryFloatAttribute("max_power", &max_power);
	xml->QueryFloatAttribute("timeout", &timeout);
	xml->QueryFloatAttribute("tolerance", &tolerance);
	xml->QueryFloatAttribute("kp", &kp);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSFieldDriveHeading::init(void)
{
	Advisory::pinfo("MSFieldDriveHeading: target = %f, start = %f, power = %f, timeout = %f, tol = %f, kp = %f",
					 target_heading, parent_control->getHeading(), max_power, timeout, tolerance, kp);
	start_time = gsi::Time::getTime();
	parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSFieldDriveHeading::update(void)
{
    if ((timeout > 0.001f) && (gsi::Time::getTime() - start_time) > timeout)
    {
        Advisory::pinfo("timeout, %f", timeout);
		parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
        return next_step;
    }
	actual_heading = parent_control->getHeading();
	heading_error = target_heading - actual_heading;
	while (heading_error >= 180.0) // @todo not this...
	{
		heading_error -= 360.0;
	}
	while (heading_error <= -180.0)
	{
		heading_error += 360.0;
	}
    if (abs(heading_error) > tolerance) // if not at target
    {
		Advisory::pinfo("heading error: %f, heading: %f", heading_error, actual_heading);
        rot_calc = heading_error * kp;
		rot_calc = gsu::Filter::limit(rot_calc, -max_power, max_power);
		parent_control->fieldRelativeDrive(0.0, 0.0, rot_calc);
        return this;
    }
	else
	{
		parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
		Advisory::pinfo("at target, heading = %f", actual_heading);
		return next_step;
	}
}


/*******************************************************************************
 * Rotates by a certain number of degrees given a max power.
 * @param angle angle in degrees
 * @param max_power maximum power...
 ******************************************************************************/
MSFieldRotateByAngle::MSFieldRotateByAngle(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (SwerveDriveControl *)control;

	angle = 0.0;
	max_power = 0.2;
	timeout = 2.0;
	tolerance = 2.0;
	kp = 0.015;

	xml->QueryFloatAttribute("angle", &angle);
	xml->QueryFloatAttribute("max_power", &max_power);
	xml->QueryFloatAttribute("timeout", &timeout);
	xml->QueryFloatAttribute("tolerance", &tolerance);
	xml->QueryFloatAttribute("kp", &kp);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSFieldRotateByAngle::init(void)
{
	Advisory::pinfo("MSFieldRotateByAngle: angle = %f power = %f, timeout = %f, tol = %f, kp = %f",
					 angle, max_power, timeout, tolerance, kp);
	target_heading = parent_control->getHeading() + angle;
	start_time = gsi::Time::getTime();
	parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSFieldRotateByAngle::update(void)
{
    if ((timeout > 0.001f) && (gsi::Time::getTime() - start_time) > timeout)
    {
        Advisory::pinfo("timeout, %f", timeout);
		parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
        return next_step;
    }
	actual_heading = parent_control->getHeading();
	heading_error = target_heading - actual_heading;
	while (heading_error >= 180.0) // @todo not this...
	{
		heading_error -= 360.0;
	}
	while (heading_error <= -180.0)
	{
		heading_error += 360.0;
	}
    if (abs(heading_error) > tolerance) // if not at target
    {
		Advisory::pinfo("heading error: %f, heading: %f", heading_error, actual_heading);
        rot_calc = heading_error * kp;
		rot_calc = gsu::Filter::limit(rot_calc, -max_power, max_power);
		parent_control->fieldRelativeDrive(0.0, 0.0, rot_calc);
        return this;
    }
	else
	{
		parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
		Advisory::pinfo("at target, heading = %f", actual_heading);
		return next_step;
	}
}

/*******************************************************************************
 * Locks the wheels in an "X" shape
 ******************************************************************************/
MSXWheels::MSXWheels(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (SwerveDriveControl *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSXWheels::init(void)
{
	parent_control->xWheels();
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSXWheels::update(void)
{
	return next_step;
}

/*******************************************************************************
 * Sets the limelight pipeline
 * @param pipeline pipeline index [0-7]
 ******************************************************************************/
MSSetLLPipeline::MSSetLLPipeline(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (SwerveDriveControl *)control;
    pipeline = 0;
    xml->QueryIntAttribute("pipeline", &pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetLLPipeline::init()
{
    parent_control->setPipeline(pipeline);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSetLLPipeline::update()
{
    return next_step;
}

/*******************************************************************************
 * Sets the current drive scheme
 * @param scheme drive scheme index
 * (0 = field oriented, 1 = robot oriented, 
 * 2 = limelight oriented rotate, 3 = limelight oriented strafe)
 ******************************************************************************/
MSSetDriveScheme::MSSetDriveScheme(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (SwerveDriveControl *)control;
    scheme = 0;
    xml->QueryIntAttribute("scheme", &scheme);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSSetDriveScheme::init()
{
    parent_control->setDriveScheme(scheme);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep *MSSetDriveScheme::update()
{
    return next_step;
}

/**********************************************************
 * 
 **********************************************************/

MSDriveBalance::MSDriveBalance(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (SwerveDriveControl *)control;
	reverse = false;
	fast_power = 0.3;
	slow_power = 0.1;
	pitch_on_bridge = 11.0;
	pitch_balanced = 7.0;
	max_count = 5;
	xml->QueryBoolAttribute("reverse", &reverse);
	xml->QueryFloatAttribute("fast_power", &fast_power);
	xml->QueryFloatAttribute("slow_power", &slow_power);
	xml->QueryFloatAttribute("pitch_on_bridge", &pitch_on_bridge);
	xml->QueryFloatAttribute("pitch_balanced", &pitch_balanced);
	xml->QueryIntAttribute("max_count", &max_count);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveBalance::init()
{
    state = 0;
    count = 0;
    if (reverse)
    {
        fast_power = -fast_power;
        slow_power = -slow_power;
    }
    start_time = gsi::Time::getTime();
}

/*******************************************************************************
 * borrowed from 3683's openautobalance <3
 ******************************************************************************/
MacroStep *MSDriveBalance::update()
{
	if (!parent_control->isGyroConnected())
	{
		Advisory::pinfo("gyro not connected!");
		return next_step;
	}
	pitch = parent_control->getPitch(); // TODO getroll if mounted sideways, getpitch if mounted forward, invert if needed
	if ((gsi::Time::getTime() - start_time) > 10.0)
	{
		// lets not balance on the opponents bridge ok? :)
		return next_step;
	}
	switch (state)
	{
		// drive forwards to approach station, exit when tilt is detected
		case 0:
			if (abs(pitch) > pitch_on_bridge)
			{
				count++;
				Advisory::pinfo("state 0, count %i", count);
			}
			if (count > max_count)
			{
				state = 1;
				count = 0;
				fwd = slow_power;
				break;
			}
			// otherwise, if not contacting the bridge yet:
			fwd = fast_power;
			break;

		// driving up charge station, drive slower, stopping when level
		case 1:
			if (abs(pitch) < pitch_balanced)
			{
				count++; 
				Advisory::pinfo("state 1, count %i", count);
				
				state = 2;
				count = 0;
				fwd = 0.0;
				break;
			}
			if (count > max_count)
			{
				state = 2;
				count = 0;
				fwd = 0.0;
				break;
			}
			// if not on bridge yet
			fwd = slow_power;
			break;

		// on charge station, stop motors and wait for end of auto
		case 2:
			max_count = 0;
			if(abs(pitch) <= pitch_balanced)
			{ 
				count++;
				Advisory::pinfo("state 2, count %i", count);
			}
			if (count > max_count)
			{
				state = 4;
				count = 0;
				fwd = 0.0;
				parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
				Advisory::pinfo("balanced???????????");
				return next_step;
				break;
			}
			if (pitch >= pitch_balanced)
			{
				fwd = 0.1; // TODO adjust value
				break;
			}
			else if (pitch <= -pitch_balanced)
			{
				fwd = -0.1; // TODO adjust value
				break;
			}
			break;            

		case 4:
			return next_step;

		default:
			fwd = 0.0;
			break;
	}
	Advisory::pinfo("stage: %i, pitch: %f, fwd power: %f", state, pitch, fwd);
	parent_control->fieldRelativeDrive(0.0, fwd, 0.0);
	return this;
    parent_control->fieldRelativeDrive(0.0, 0.0, 0.0);
    return next_step;
}


