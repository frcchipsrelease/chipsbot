/*******************************************************************************
 *
 * File: Shooter24.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Notes:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
// Do not remove - all subsystems
#include <math.h>

#include "gsinterfaces/Path.h"

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Angles.h"
#include "gsinterfaces/Time.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Path.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfhardware/LimitSwitch.h"


#include "frc/smartdashboard/SmartDashboard.h" // WPI

// ok so maybe open loop for nudging position and then setpoints are done with closed loop???

// The corresponding .h file
#include "Shooter24.h"

using namespace tinyxml2;
using namespace frc;

//12 tooth gear driving 44 tooth
//(135/2816) = degrees to ticks!!1/1/1

/*
og PID settings
200 - I zone
1 - loop period ms
1 - peak output
2.5 - kD
0.0012497901916503906 - kI
0.25 - kP
*/
//60000/2048
//TODO add limits for open loop that only allows up below top and only down at limit
//851, 1701, 2552, 3402 <- velocity values

//shooter top position -> 0.8
//shooter bottom position -> -54
// shooter limits 0-50 (max max is 54)
//
/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
Shooter24::Shooter24(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)	
{
	Advisory::pinfo("========================= Creating Shooter24 Control [%s] =========================",
	            control_name.c_str());

	// Be sure to initialize all method variables
	//motor declare
	fly_motor = nullptr; // This is an object, so it gets initialized to nullptr
	joint_motor = nullptr;
	kicker_motor = nullptr;

	//kicker motor
	kicker_target_power = 0.0;
	kicker_actual_power = 0.0;
	
	//joint stuff
	joint_target_position = 0.0;
	joint_actual_position = 0.0;
	joint_target_power = 0.0;
	joint_actual_power = 0.0;
	//change these based on robot
	joint_min_position = -180000.0;
	joint_max_position = 1800.0;
	joint_power_step = 0.2;
	joint_max_current = 0.0;
	position_step = 1.0;
	joint_command_position = 0.0;

	//shooter stuff
	fly1_power_step = 0.05;
	fly1_actual_power = 0.0; // Power is on a scale from 0.0 to 1.0
	fly1_target_power = 0.0; // Initialize to 0.0 so you don't have any accidents
	fly1_command_power = 0.0;
	fly1_velocity_step = 250.0;
	fly1_target_velocity = 0.0;
	fly1_command_velocity = 0.0;
	fly1_actual_velocity = 0.0;
	fly1_min_velocity = 1000.0;
	fly1_max_velocity = 6300.0; //subject to change based on motor? yk
	prev_velocity = 0.0;
	prev_power = 0.0;

	//limelight stuff
	lime_light = nullptr;
	ll_is_visible = 0.0;
	ll_actual_ang_vert = 0.0;
	ll_actual_ang_hori = 0.0;
	ll_actual_area = 0.0;
	ll_actual_ang_roll = 0.0;
	ll_target_height = 0.0;
	ll_mount_height = 0.0;
	ll_mount_angle_vert = 0.0;
	is_ll_target = false;
	ll_height_diff = 0.0;
	ll_target_distance = 0.0;
	ll_target_vel = 0.0;
	ll_target_pos = 0.0;
	
	bottomPressed = false;
	LightOn = false;
	innerLightSen = nullptr;
	lightCycleCount = 0;
	lightCycleMaxCount = 10;
	bottom_limit = nullptr;

	setpoint_index = 0;

	isFlyOn = false;
	active = false;
	is_closed_loop = false;
	homed = false;

	const char *name = nullptr;
	XMLElement* comp = nullptr;
	for (uint8_t i = 0; i < NUM_SETPOINTS; i++ ){
		setpoint_speeds[i] = 0.0;
		fly1_powers[i] = 0.0;
		setpoint_names[i] = "??";
		setpoint_defined[i] = false;

		joint_position[i] = 0.0;
	
	}
	//
	// Register Macro Steps - these are specific functions that are used in autonomous
	//
	new MacroStepProxy<MSKickerState>(control_name, "KickerState", this);
	new MacroStepProxy<MSShooterClosedLoop>(control_name, "ShooterClosedLoop", this);
	new MacroStepProxy<MSShooterSetpoint>(control_name, "ShooterSetpoint", this);
	new MacroStepProxy<MSShooterToggle>(control_name, "ShooterToggle", this);
	new MacroStepProxy<MSLimelightPipeline>(control_name, "LLPipeline", this); // surely this will not conflict with the swerve macrostep
	new MacroStepProxy<MSShooterHome>(control_name, "ShooterHome", this);
	new MacroStepProxy<MSShooterPivotDown>(control_name, "ShooterPivotDown", this);
	new MacroStepProxy<MSShooterPivotStop>(control_name, "ShooterPivotStop", this);


	//
	// Parse XML
	//
	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr){
		XMLElement *subcomp;
		subcomp = comp->FirstChildElement("setpoint");
		while (subcomp != nullptr){
			subcomp -> QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < NUM_SETPOINTS){
				name = subcomp->Attribute("name");
				if (name != nullptr){
					setpoint_names[setpoint_index] = std::string(name);
					
				}else{
					setpoint_names[setpoint_index] = std::string("setpoint ") + std::to_string(setpoint_index);
				}
				subcomp ->QueryFloatAttribute("velocity", &setpoint_speeds[setpoint_index]);
				subcomp ->QueryFloatAttribute("power", &fly1_powers[setpoint_index]);
				subcomp ->QueryFloatAttribute("position", &joint_position[setpoint_index]);
				setpoint_defined[setpoint_index] = true;
				Advisory::pinfo("setpoint %d velocity - %.2f power - %.2f position-%.2f", setpoint_index, setpoint_speeds[setpoint_index], fly1_powers[setpoint_index], &joint_position[setpoint_index]);
			}
			subcomp = subcomp->NextSiblingElement("setpoint");
		}
		
	}
	// Read in any variables set in the xml
	xml->QueryBoolAttribute("closed_loop", &is_closed_loop);
	// Read in motors and other hardware
	comp = xml->FirstChildElement("limelight_curves");
	if (comp != nullptr){
		XMLElement* ll_comp;
		ll_comp = comp->FirstChildElement("line");
		while (ll_comp != nullptr){
			float distance = 0.0;
			float velocity = 0.0;
			float ang = 0.0;
			ll_comp->QueryFloatAttribute("distance", &distance);
			m_distance_table.push_back(distance);
			ll_comp->QueryFloatAttribute("velocity", &velocity);
			m_velocity_table.push_back(velocity);
			ll_comp->QueryFloatAttribute("angle", &ang);
			m_angle_table.push_back(ang);
			Advisory::pinfo(" -- shooter limelight segment: distance = %.2f in, vel = %.0f, angle = %.2f",
					distance, velocity, ang);
    		ll_comp = ll_comp->NextSiblingElement("line");
		}
		ll_velocity_curves.setCurve(m_distance_table, m_velocity_table);
 		ll_velocity_curves.setLimitEnds(true);
		ll_angle_curves.setCurve(m_distance_table, m_angle_table);
 		ll_angle_curves.setLimitEnds(true);
	}
	
	comp = xml-> FirstChildElement("motor");
	
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "flywheel") == 0)
			{
				Advisory::pinfo("  creating speed controller for flywheel");
				fly_motor = HardwareFactory::createMotor(comp);
				if (fly_motor != nullptr)
				{
					fly_motor->setVelocity(0.0);
				}
				//these do nothing rn bruh
				comp->QueryFloatAttribute("step_size", &fly1_power_step);
				comp->QueryFloatAttribute("velo_step", &fly1_velocity_step);
				comp->QueryFloatAttribute("actual", &fly1_actual_power);
			}
			else if(strcmp(name, "joint") == 0){
				Advisory::pinfo("  creating speed controller for joint");
				joint_motor = HardwareFactory::createMotor(comp);
				comp->QueryFloatAttribute("step_size", &joint_power_step);
				comp->QueryFloatAttribute("position_step", &position_step);
				comp->QueryFloatAttribute("max_position", &joint_max_position);
				comp->QueryFloatAttribute("min_position", &joint_min_position);

			}else if(strcmp(name, "kicker") == 0){
				Advisory::pinfo("  creating speed controller for kicker");
				kicker_motor = HardwareFactory::createMotor(comp);
				comp->QueryFloatAttribute("power", &kicker_power);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
	comp = xml-> FirstChildElement("limit_switch");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "shooterLimit") == 0)
			{
				Advisory::pinfo("%s  creating limit switch", getName().c_str());
				bottom_limit = HardwareFactory::createLimitSwitch(comp);
			}else{
				
				Advisory::pwarning("  found unexpected motor with name attribute %s", getName().c_str());
			}
		}else{
			Advisory::pwarning("  found unexpected limit switch with no name attribute");
		}
		comp = comp->NextSiblingElement("limit_switch");
	}
	comp = xml -> FirstChildElement("digital_input");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "lightSen") == 0)
			{
				Advisory::pinfo("%s  creating light sens", getName().c_str());
				innerLightSen = HardwareFactory::createDigitalInput(comp);
				comp->QueryIntAttribute("maxCount", &lightCycleMaxCount);
			}else{
				
				Advisory::pwarning("  found unexpected DigiIn with name attribute %s", getName().c_str());
			}
		}else{
			Advisory::pwarning("  found unexpected DigiIn with no name attribute");
		}
		comp = comp->NextSiblingElement("limit_switch");
	}
	comp = xml-> FirstChildElement("limelight");
	if (comp != nullptr){
		lime_light = HardwareFactory::createLimelight(comp);
		comp->QueryDoubleAttribute("target_height", &ll_target_height);
		comp->QueryDoubleAttribute("mount_height", &ll_mount_height);
		comp->QueryDoubleAttribute("mount_angle", &ll_mount_angle_vert);
		ll_height_diff = ll_target_height - ll_mount_height;
	}

	// read in OI channels - these connect a button on the controller to an action
	// essentially, when a button is pressed, it sends a signal to the robot to do an action
	// here, we connect the signals
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "loop_toggle")==0)
			{
				Advisory::pinfo("connecting loop toggle channel");
				OIController::subscribeDigital(comp, this, CMD_LOOP_TOGGLE);
			}
			else if (strcmp(name, "fly_toggle") == 0)
			{
				Advisory::pinfo("connecting flywheel toggle channel");
				OIController::subscribeDigital(comp, this, CMD_FLY_TOGGLE);
			}
			else if (strcmp(name, "joint_step_up") == 0){
				Advisory::pinfo(" connecting step up channel");
				OIController::subscribeDigital(comp, this, CMD_FLY_STEP_UP);
			}
			else if (strcmp(name, "joint_step_down") == 0){
				Advisory::pinfo(" connecting step down channel");
				OIController::subscribeDigital(comp, this, CMD_FLY_STEP_DOWN);
			}
			else if ((strncmp(name, "setpoint",8)==0)&& (name[8] >= '0')&& (name[8] <= '3')){
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8]-'0'));
			}
			else if (strcmp(name, "kicker_fwd") == 0){
				Advisory::pinfo(" connecting kick fwd channel");
				OIController::subscribeDigital(comp, this, CMD_KICK_FWD);
			}
			else if (strcmp(name, "kicker_back") == 0){
				Advisory::pinfo(" connecting kick back channel");
				OIController::subscribeDigital(comp, this, CMD_KICK_BACK);
			}
			else if (strcmp(name, "limelight_targeting_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_TOGGLE);
			}
			else if (strcmp(name, "dpad_setpoints") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeInt(comp, this, CMD_DPAD_SETPOINTS);
			}else if (strcmp(name, "speed_up") == 0)
			{
				Advisory::pinfo("connecting flywheel step up channel");
				OIController::subscribeDigital(comp, this, CMD_SPEED_UP);
			}else if (strcmp(name, "speed_down") == 0)
			{
				Advisory::pinfo("connecting flywheel step down channel");
				OIController::subscribeDigital(comp, this, CMD_SPEED_DOWN);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}
}
float Shooter24::kicker_command_power = 0.0;
float Shooter24::kicker_power = 0.0;


/*******************************************************************************	
 * 
 * Destructor: Release any resources allocated by this object
 * Any object that you created (motor, sensor, etc), put it here
 * 
 ******************************************************************************/
Shooter24::~Shooter24(void)
{
	if (fly_motor != nullptr)
	{
		delete fly_motor;
		fly_motor = nullptr;
	}
	if (joint_motor != nullptr)
	{
		delete joint_motor;
		joint_motor = nullptr;
	}
	if(kicker_motor != nullptr){
		delete kicker_motor;
		kicker_motor = nullptr;
	}
}

/*******************************************************************************
 * 
 ******************************************************************************/
//uneccessary? oh nvm its for auton !
bool Shooter24::isClosedLoop(void){
	return is_closed_loop;
}
bool Shooter24::flyOn(void){
	return isFlyOn;
}
void Shooter24::setClosedLoop(bool state){
	if(is_closed_loop != state){
		is_closed_loop = state;
	}
	if (state) {
		joint_target_position = joint_actual_position; // don't break the robot
	}
}
void Shooter24::applySetpoint(int i){
	if ((i >= 0) && (i < NUM_SETPOINTS))
	{
		isFlyOn = true;
		setpoint_index = i;
		fly1_target_velocity = setpoint_speeds[i];
		fly1_target_power = fly1_powers[i];
		
		joint_target_position = joint_position[i];
		Advisory::pinfo("shooter setpoint %d pwr = %.1f vel = %.0f angle = %.1f", setpoint_index, fly1_target_power, fly1_target_velocity, joint_target_position);
	}else{
		
		Advisory::pwarning("setpoint out of range!!");
	}
}
void Shooter24::shootToggle(){
	if (isFlyOn){
		fly1_target_velocity = 0.0;
	}else{
		applySetpoint(setpoint_index);
	}

}
void Shooter24::controlInit(void)
{ 
	bool is_ready = true;
	if (fly_motor == nullptr){
		Advisory::pwarning("%s shooter missing component", getName().c_str());
		is_ready = false;
	}
	if(joint_motor == nullptr){
		Advisory::pwarning("%s joint missing component", getName().c_str());
		is_ready = false;
	}
    active = is_ready;	
}

/*******************************************************************************
 *
 ******************************************************************************/
void Shooter24::updateConfig(void)
{
	// unused
}

/*******************************************************************************	
 *
 * This runs whenever the robot is disabled (end of a match AND for a second between auton and telop)
 * Reset target and command power to 0
 * Do not reset fly1_power here. fly1_power is the output power reported by the motor itself
 * 
 ******************************************************************************/
void Shooter24::disabledInit(void)
{
	fly1_target_power = 0.0;
	fly1_command_power = 0.0;
	fly1_target_velocity = 0.0;
	fly1_command_velocity = 0.0;
}

/*******************************************************************************	
 *
 * This runs at the beginning of autonomous
 * Reset power to 0
 * 
 ******************************************************************************/
void Shooter24::autonomousInit(void)
{
	fly1_target_power = 0.0;
	fly1_command_power = 0.0;
	fly1_target_velocity = 0.0;
	fly1_command_velocity = 0.0;

	joint_target_position = joint_actual_position;
	joint_actual_power = 0.0;
	joint_target_power = 0.0;
	joint_command_power = 0.0;

	homed = false;
}

/*******************************************************************************	
 *
 * This runs at the beginning of teleop
 * Reset power to 0
 * 
 ******************************************************************************/
void Shooter24::teleopInit(void)
{
	fly1_target_power = 0.0;
	fly1_command_power = 0.0;
	fly1_target_velocity = 0.0;
	fly1_command_velocity = 0.0;
	isFlyOn = false;
	if (homed)
	{
		is_closed_loop = true;
	}
	else
	{
		is_closed_loop = false;
	}

	joint_target_position = joint_actual_position;
	joint_command_position = joint_actual_position;
	joint_actual_power = 0.0;
	joint_target_power = 0.0;
	joint_command_power = 0.0;

	kicker_target_power = 0.0;
	kicker_actual_power = 0.0;
	kicker_command_power = 0.0;

	setpoint_index = 0;
}

/*******************************************************************************	
 *
 * This runs at the beginning when you run the robot in Test mode.
 * We don't typically use this mode
 * Reset power to 0 just in case
 *
 ******************************************************************************/
void Shooter24::testInit(void)
{
	fly1_target_power = 0.0;
	fly1_command_power = 0.0;
	fly1_target_velocity = 0.0;
	fly1_command_velocity = 0.0;
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * Analog: A controller input measured on a continuous -1.0 to 1.0 scale.
 * For our controllers (as of 2023) this is only the joysticks
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Shooter24::setAnalog(int id, float val)
{
	// unused
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * Digital: A controller input measured as true/false.
 * This is all buttons on the controller (other than the D-Pad which is separate)
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void Shooter24::setDigital(int id, bool val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_SPEED_UP:
		{
			if(val){
				fly1_target_velocity += fly1_velocity_step;
			}
		}
		break;
		case CMD_SPEED_DOWN:
		{
			if(val){
				fly1_target_velocity -= fly1_velocity_step;
			}
		}
		break;
		case CMD_KICK_FWD:
		{
			if(val){
				Advisory::pinfo("kicker forward pressed");
				kicker_target_power = kicker_power;
			}else{
				kicker_target_power = 0.0;
			}
		}
		break;
		case CMD_KICK_BACK:
		{
			if(val){
				kicker_target_power = -kicker_power;
			}else{
				kicker_target_power = 0.0;
			}
		}
		break;
		case CMD_LOOP_TOGGLE:
		{
			if(val){
				joint_target_position = joint_actual_position;
				setClosedLoop(!isClosedLoop());
				Advisory::pinfo("shooter set closed loop %s", isClosedLoop() ? "true" : "false");
			}
		}break;

		case CMD_FLY_TOGGLE:
		{
			if (val)
			{
				if (isFlyOn){
					Advisory::pinfo("toggle fly off");
					fly1_target_velocity = 0.0;
					fly1_target_power = 0.0;
					isFlyOn = false;
				}else if (is_closed_loop){
					Advisory::pinfo("toggle fly on");
					fly1_target_velocity = setpoint_speeds[setpoint_index];
					isFlyOn = true;
				}else{
					Advisory::pinfo("toggle fly on");
					fly1_target_power = fly1_powers[setpoint_index];
					isFlyOn = true;
				}	
			}				
		} break;
		case CMD_FLY_STEP_UP:
		{
			if(val)
			{				
				if(is_closed_loop){
				
					joint_target_position += position_step;
					joint_target_position = RobotUtil::limit(joint_min_position, joint_max_position, joint_target_position);
					
				}else{
					joint_target_power = joint_power_step;
				}				
			}else{
				joint_target_power = 0.0;
			}
		} break;
		case CMD_FLY_STEP_DOWN:
		{
			if (val){
				if(is_closed_loop){
					joint_target_position -= position_step;
					joint_target_position = RobotUtil::limit(joint_min_position, joint_max_position, joint_target_position);
				}else{
					joint_target_power = -joint_power_step;
				}
			}
			else{
				joint_target_power = 0.0;
			}
		} break;
		case CMD_SETPOINT_0:
			if (val){
				applySetpoint(0);
			}
			break;
		case CMD_SETPOINT_1:
			if (val){
				applySetpoint(1);
			}
			break;
			case CMD_SETPOINT_2:
			if (val){
				applySetpoint(2);
			}
			break;
			case CMD_SETPOINT_3:
			if (val){
				applySetpoint(3);
			}
			break;

		case CMD_LIMELIGHT_TOGGLE:
			if (val){
				is_ll_target = !is_ll_target;
				if (is_ll_target) {
					setClosedLoop(true);
				}
				Advisory::pinfo("shooter ll targeting %s", is_ll_target ? "on" : "off");
			}
			break;

		default:
			break;
	}
}

/*******************************************************************************
 * setInt connects a button on the D-Pad to a command
******************************************************************************/

void Shooter24::setInt(int id, int val)
{
	Advisory::pinfo("shooter setint %d", val);
	switch (id)
	{
		case CMD_DPAD_SETPOINTS:
		{
			if (val == 0)
			{
				applySetpoint(0);
			}
			else if (val == 90)
			{
				applySetpoint(1);
			}
			else if (val == 180)
			{
				applySetpoint(2);
			}
			else if (val == 270)
			{
				applySetpoint(3);
			}
		} break;

		default: // do nothing
			break;
	}

}

/*******************************************************************************
 * Functions that set variables. These are typically only used in autonomous.
 ******************************************************************************/

void Shooter24::setMotorPower(float power)
{
	// in autonomous, you can't directly set a variable's value
	// so, we need a function to do it instead
	fly1_target_power = power;
}

int Shooter24::getSetpointIndex(){

	return setpoint_index;
}
bool Shooter24::isHome(){
	return homed;
}

bool Shooter24::isKickerRunning()
{
	return kicker_command_power == kicker_power;
}

void Shooter24::setLimelightPipeline(int idx) {
	lime_light->setPipeline(idx);
}

/*******************************************************************************	
 * Puts variables on SmartDashboard. Add all of your variables for now and you can delete them later
 ******************************************************************************/
void Shooter24::publish()
{
	SmartDashboard::PutNumber( " fly1: target power: ", fly1_target_power);
	SmartDashboard::PutNumber( " fly1: actual power: ", fly1_actual_power);
	
	SmartDashboard::PutNumber( " fly1: target velocity: ", fly1_target_velocity);
	SmartDashboard::PutNumber( " fly1: actual velocity: ", fly1_actual_velocity);

	SmartDashboard::PutNumber(" joint:  target power", joint_target_power);
	SmartDashboard::PutNumber(" joint:  command power", joint_command_power);
	SmartDashboard::PutNumber(" joint:  current power ", joint_motor->getPercent());
	SmartDashboard::PutNumber(" joint:  target position", joint_target_position);
	SmartDashboard::PutNumber(" joint: 	actual position ", joint_actual_position);
	SmartDashboard::PutNumber(" joint:  setpoint pos 0", joint_position[0]);
	SmartDashboard::PutNumber(" joint:  setpoint pos 1", joint_position[1]);
	SmartDashboard::PutNumber(" joint:  setpoint pos 2", joint_position[2]);
	SmartDashboard::PutNumber(" joint:  setpoint pos 3", joint_position[3]);

	SmartDashboard::PutBoolean(" shooter: bottom limit", bottomPressed);
	
	SmartDashboard::PutBoolean(" shooter: light sensor", LightOn);
	SmartDashboard::PutBoolean(" shooter: light sensor adjusted", LightOnAdjusted);
	SmartDashboard::PutBoolean(" shooter: homed", homed);
	SmartDashboard::PutBoolean(" shooter: fly on", isFlyOn);
	SmartDashboard::PutBoolean(" shooter: closed loop", is_closed_loop);
	SmartDashboard::PutBoolean(" shooter: limelight targeting", is_ll_target);
	SmartDashboard::PutNumber(" shooter: setpoint index", setpoint_index);
	
	SmartDashboard::PutNumber(" kicker: target power", kicker_target_power);
	SmartDashboard::PutNumber(" kicker: command power", kicker_command_power);
}

/*******************************************************************************
 *
 * This method will be called once a period (every 20 ms) to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Shooter24::doPeriodic()
{
	if (active == false){
		Advisory::pinfo("doperiodic not ready");
		return;
	}

	kicker_motor ->doUpdate();
	fly_motor->doUpdate();
	joint_motor->doUpdate();

	
	kicker_actual_power = kicker_motor->getPercent();
	
	joint_actual_position = joint_motor->getPosition();
	joint_actual_power = joint_motor->getPercent();

	fly1_actual_velocity = fly_motor->getVelocity();
	fly1_actual_power = fly_motor->getPercent();


	if (bottom_limit != nullptr) {
		bottomPressed = bottom_limit->isPressed();
	}
	else {
		bottomPressed = false;
	}

	if (innerLightSen != nullptr) {
		LightOn = !innerLightSen->Get();
	}
	else {
		LightOn = false;
	}

	if (LightOn)
	{
		lightCycleCount = 1;
	}
	else
	{
		if (lightCycleCount >= 1)
		{
			//Advisory::pinfo("shooter lightsens on - lightcyclecount %d", lightCycleCount);
			lightCycleCount++;
		}
		if (lightCycleCount >= lightCycleMaxCount)
		{
			lightCycleCount = 0;
		}
	}
	if (lightCycleCount != 0)
	{
		LightOnAdjusted = true;
	}
	else
	{
		LightOnAdjusted = false;
	}

	kicker_command_power = kicker_target_power;
	fly1_command_power = fly1_target_power;
	joint_command_power = joint_target_power; //

	if(bottomPressed){
		joint_motor->resetPosition(0.0);
		joint_command_power = gsu::Filter::limit(joint_command_power, 0.0, 1.0);
		//Advisory::pinfo("shooter pivot at limit switch, limiting cmd pwr to %.2f", joint_command_power);
		homed = true;
	}
	if(LightOnAdjusted && !isFlyOn){
		kicker_target_power = 0.0;
		kicker_command_power = kicker_target_power;
	}
	if(!homed && is_closed_loop){
		is_closed_loop = false;
	}

	// limelight yippee!!!!!!!!!!!!!!!!!!!!!!!!!!
	// if (lime_light != nullptr){
	// 	lime_light->getData(ll_is_visible, ll_actual_ang_hori, ll_actual_ang_vert, ll_actual_area, ll_actual_ang_roll);
	// }
	// if (is_ll_target) {
	// 	if (ll_is_visible > 0.5){
	// 		ll_target_distance = ll_height_diff/tan(gsu::Angle::toRadians(ll_actual_ang_vert+ll_mount_angle_vert));
	// 		ll_target_vel = round(ll_velocity_curves.evaluate(ll_target_distance)/75)*75; // jly it works?
	// 		fly1_target_velocity = ll_target_vel;
	// 		ll_target_pos = ll_angle_curves.evaluate(ll_target_distance);
	// 		joint_target_position = ll_target_pos;
	// 	}
	// 	else {
	// 		// if ll isn't visible, maintain current vel & pos
	// 		fly1_target_velocity = fly1_target_velocity;
	// 		joint_target_position = joint_actual_position;
	// 	}
	// }

	if(is_closed_loop)
	{
//		joint_target_position = gsu::Filter::limit(joint_target_position, joint_min_position, joint_max_position);
		joint_command_position = joint_target_position;
	}
	
	
	fly1_target_velocity = gsu::Filter::limit(fly1_target_velocity, -1000.0, 4250.0); // 2024 shooter max vel = 4252 rpm
	fly1_command_velocity = fly1_target_velocity;

	if (is_closed_loop)
	{
		fly_motor->setVelocity(fly1_command_velocity);
		joint_motor->setPosition(joint_command_position);
		if (fly1_command_velocity != 0.0) {
			isFlyOn = true;
		}
		else {
			isFlyOn = false;
		}
	}
	else{
		fly_motor->setPercent(fly1_command_power);
		joint_motor->setPercent(joint_command_power);
		if (fly1_command_power != 0.0) {
			isFlyOn = true;
		}
		else {
			isFlyOn = false;
		}
	}
	
	kicker_motor->setPercent(kicker_command_power);
}

// macros :)
/*******************************************************************************
 *
 ******************************************************************************/
MSKickerState::MSKickerState(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
	xml->QueryBoolAttribute("state", &state);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSKickerState::init(void)
{	
	parent_control->setDigital(parent_control->CMD_KICK_FWD, state);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSKickerState::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterClosedLoop::MSShooterClosedLoop(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
	xml->QueryBoolAttribute("state", &state);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterClosedLoop::init(void)
{	
	parent_control->setClosedLoop(state);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterClosedLoop::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterSetpoint::MSShooterSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
	xml->QueryFloatAttribute("setpoint_index", &setpoint);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterSetpoint::init(void)
{	
	parent_control->applySetpoint(setpoint);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterSetpoint::update(void)
{
	return next_step;
}


/*******************************************************************************
 *
 ******************************************************************************/
MSShooterToggle::MSShooterToggle(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
}


/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterToggle::init(void)
{	
	parent_control->shootToggle();
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterToggle::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSLimelightPipeline::MSLimelightPipeline(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
	xml->QueryIntAttribute("pipeline", &pipeline);
}


/*******************************************************************************
 *
 ******************************************************************************/
void MSLimelightPipeline::init(void)
{	
	parent_control->setLimelightPipeline(pipeline);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSLimelightPipeline::update(void)
{
	return next_step;
}

MSShooterHome::MSShooterHome(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
}
/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterHome::init(void)
{	

}
/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterHome::update(void)
{
	if(!parent_control->isHome()){
		parent_control->setDigital(Shooter24::CMD_FLY_STEP_DOWN, true);
		return this;
	}else{
		parent_control->setDigital(Shooter24::CMD_FLY_STEP_DOWN, false);
		return next_step;

	}
}





MSShooterPivotDown::MSShooterPivotDown(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
}
/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterPivotDown::init(void)
{	

}
/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterPivotDown::update(void)
{
	parent_control->setDigital(Shooter24::CMD_FLY_STEP_DOWN, true);
	return next_step;
}

MSShooterPivotStop::MSShooterPivotStop(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Shooter24 *)control;
}
/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterPivotStop::init(void)
{	

}
/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterPivotStop::update(void)
{
	parent_control->setDigital(Shooter24::CMD_FLY_STEP_DOWN, false);
	return next_step;
}

