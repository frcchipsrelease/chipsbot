/*******************************************************************************
 *
 * File: lightsensortest.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "lightsensortest.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
sensortest::sensortest(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating MY SUBSYSTEM Control [%s] =========================",
	            control_name.c_str());

	light_sensor = nullptr;
	light_sensor_state = false;

	const char *name = nullptr;

	comp = xml->FirstChildElement("digital_input");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			light_sensor = HardwareFactory::createDigitalInput(comp);
		}
		else
		{
			Advisory::pwarning("  found digital_input tag with no name attribute");
		}
		comp = comp->NextSiblingElement("digital_input");
	}

}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
sensortest::~sensortest(void)
{
	// Be sure to reference each motor
	if (light_sensor != nullptr)
	{
		delete light_sensor;
		light_sensor = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void sensortest::controlInit(void)
{
}

/*******************************************************************************
 *
 ******************************************************************************/
void sensortest::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void sensortest::disabledInit(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void sensortest::autonomousInit(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void sensortest::teleopInit(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void sensortest::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void sensortest::setAnalog(int id, float val)
{
	// Add commands as necessary
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void sensortest::setDigital(int id, bool val)
{
	// Add commands as necessary
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		default:
		{
			Advisory::pcaution("setAnalog() was called for an unknown id, id=%d", id);
		} break;
	}
}
/*******************************************************************************
 *
 ******************************************************************************/
void sensortest::setInt(int id, int val)
{
}
/*******************************************************************************	
 *
 ******************************************************************************/
void sensortest::publish()
{
	SmartDashboard::PutString("light sensor state: ", light_sensor_state ? "true" : "false");
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void sensortest::doPeriodic()
{
	light_sensor_state = light_sensor->Get();
}
