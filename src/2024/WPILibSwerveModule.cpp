/*******************************************************************************
 *
 * File: WPILibSwerveModule.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "2024/WPILibSwerveModule.h"

#include "gsinterfaces/Time.h"
#include "gsutilities/Advisory.h"

#include "gsutilities/Angles.h"

#include "rfhardware/HardwareFactory.h"
#include "rfutilities/RobotUtil.h"

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a swerve module and connect it to the specified motors
 * and sensor/s
 * 
 ******************************************************************************/
WPILibSwerveModule::WPILibSwerveModule(XMLElement* xml)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating WPILib Swerve Module =========================");

	drive_motor = nullptr;
    steering_motor = nullptr;

	wheel_diameter = 0.0;
	drive_gear_ratio = 0.0;
    drive_encoder_scale = 0.0;
	max_drive_velocity = 0.0;
	steering_gear_ratio = 0.0;
    steering_encoder_scale = 0.0;

	drive_target_power = 0.0;
	drive_command_power = 0.0;
	drive_target_velocity = 0.0;
	drive_command_power = 0.0;
	steering_target_power = 0.0;
	steering_command_power = 0.0;
	steering_target_position = 0.0;
	steering_actual_position = 0.0;

    cancoder = nullptr;
	cc_actual = 0.0;
	cc_target = 0.0;
	cc_error = 0.0;

    steering_pid = nullptr;

	const char *name = nullptr;

	//
	// Parse XML
	//
	
	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "drive") == 0)
			{
				Advisory::pinfo("  creating speed controller for drive motor");
				drive_motor = HardwareFactory::createMotor(comp);
                comp->QueryFloatAttribute("wheel_diameter", &wheel_diameter);
				Advisory::pinfo("wheel diam = %f", wheel_diameter);
                comp->QueryFloatAttribute("scale", &drive_encoder_scale);
				comp->QueryFloatAttribute("gear_ratio", &drive_gear_ratio);
				if (drive_gear_ratio != 0.0)
				{
					// assuming talon fx motor controller (falcon 500 or kraken x60 motor)
					drive_encoder_scale = (wheel_diameter * M_PI) / (2048 * drive_gear_ratio);
				}
                drive_motor->setSensorScale(drive_encoder_scale, 0.0);
				max_drive_velocity = 6380 / drive_gear_ratio * M_PI * wheel_diameter / 60.0; // in/s
			}
			else if(strcmp(name, "steering") == 0)
			{
    			Advisory::pinfo("  creating speed controller for steering motor");
				steering_motor = HardwareFactory::createMotor(comp);
                comp->QueryFloatAttribute("scale", &steering_encoder_scale);
				comp->QueryFloatAttribute("gear_ratio", &steering_gear_ratio);
				if (steering_gear_ratio != 0.0)
				{
					// assuming talon fx motor controller (falcon 500 or kraken x60 motor)
					steering_encoder_scale = 360 / (2048 * steering_gear_ratio);
				}
				Advisory::pinfo("steering encoder scale = %f", steering_encoder_scale);
				Advisory::pinfo("2048 ticks = %f degrees", 2048*steering_encoder_scale);
                steering_motor->setSensorScale(steering_encoder_scale, 0.0);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
    
    comp = xml-> FirstChildElement("cancoder");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating cancoder");
		cancoder = HardwareFactory::createCancoder(comp);
    }
	
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
WPILibSwerveModule::~WPILibSwerveModule(void)
{
	if (drive_motor != nullptr)
	{
		delete drive_motor;
		drive_motor = nullptr;
	}
	if (steering_motor != nullptr)
	{
		delete steering_motor;
		steering_motor = nullptr;
	}
	if (cancoder != nullptr)
	{
		delete cancoder;
		cancoder = nullptr;
	}
}

/**
 * Called at the beginning of a match. 
 * Assuming robot is facing straight forward away from alliance wall, 
 * and all wheels are facing straight forward
 */
void WPILibSwerveModule::resetEncoders(void)
{
    drive_motor->resetPosition();
	steering_target_position = steering_actual_position;
	cc_actual = cancoder->getAbsoluteAngle();
	cc_target = cc_actual;
	cc_error = 0.0;
}

/*******************************************************************************
 * Setters
 ******************************************************************************/

/**
 * @param vel maximum drive velocity (in/s)
*/
void WPILibSwerveModule::setMaxDriveVelocity(float vel)
{
	max_drive_velocity = vel;
}

/**
 * @param vel maximum rotational velocity (deg/s)
*/
void WPILibSwerveModule::setMaxRotVelocity(float vel)
{
	max_rot_velocity = vel;
}

/**
 * @return maximum drive velocity (in/s)
*/
float WPILibSwerveModule::getMaxDriveVelocity(void)
{
	return max_drive_velocity;
}

/**
 * @return maximum rotational velocity (deg/s)
*/
float WPILibSwerveModule::getMaxRotVelocity(void)
{
	return max_rot_velocity;
}

/**
 * @param pwr desired drive motor power as a float [-1.0, 1.0]
*/
void WPILibSwerveModule::setDrivePower(float pwr)
{
	drive_target_power = pwr;
	drive_motor->setPercent(drive_target_power);
}

/**
 * @param pwr desired steering motor power as a float [-1.0, 1.0]
*/
void WPILibSwerveModule::setSteeringPower(float pwr)
{
	steering_target_power = pwr;
	steering_motor->setPercent(steering_target_power);
}

/**
 * @param vel desired drive velocity (in/s)
*/
void WPILibSwerveModule::setDriveVelocity(float vel)
{
	drive_target_velocity = vel;
	drive_motor->setVelocity(drive_target_velocity);
}

/**
 * @param pos desired wheel position (deg). 0.0 is straight forward, 
 * positive values are to the right (-180.0, 180.0]
*/
void WPILibSwerveModule::setSteeringPosition(float pos)
{
	steering_actual_position = steering_motor->getPosition();
	cc_actual = cancoder->getAbsoluteAngle();

	cc_target = pos;
	cc_error = cc_target - cc_actual;

	// cmd motor to Steering position + cc error
	steering_target_position = steering_actual_position + cc_error;
	steering_motor->setPosition(steering_target_position);
}

/**
 * @param target_state desired module state
*/
void WPILibSwerveModule::setState(frc::SwerveModuleState target_state)
{
	steering_actual_position = steering_motor->getPosition();
	cc_actual = cancoder->getAbsoluteAngle();
	module_target_state = optimizeState(target_state);
	//module_target_state = target_state;
	drive_target_power = module_target_state.speed.value() * METERS_TO_INCHES / max_drive_velocity;
	cc_target = -module_target_state.angle.Degrees().value();
	float shortest_distance = gsu::Angle::toDegrees(gsu::Angle::shortestDistance(gsu::Angle::toRadians(cc_actual),gsu::Angle::toRadians(cc_target)));
	steering_target_position = steering_actual_position + shortest_distance;
	cc_error = shortest_distance;
	Advisory::pinfo("drive_target_power= %f, steering_target_position = %f", drive_target_power, steering_target_position);
	drive_motor->setPercent(drive_target_power);
	steering_motor->setPosition(steering_target_position);
}

/**
 * @param target_power desired drive power [-1.0, 1.0]
 * @param target_pos desired wheel position (-180.0, 180.0]
*/
void WPILibSwerveModule::setState(float target_power, float target_pos)
{
	// invert angle to convert to wpilib coordinate system
	setState(SwerveModuleState{units::meters_per_second_t(target_power * max_drive_velocity * INCHES_TO_METERS), frc::Rotation2d(units::degree_t(-target_pos))});
}


/*******************************************************************************
 * Wrapper functions/getters
 ******************************************************************************/

void WPILibSwerveModule::update(void)
{
	drive_motor->doUpdate();
	steering_motor->doUpdate();
}

/**
 * @param state desired module state (velocity and wheel position)
 * @return optimized module state to minimize change in wheel position
*/
frc::SwerveModuleState WPILibSwerveModule::optimizeState(frc::SwerveModuleState state)
{
	// invert cc angle to convert to wpilib coordinate system
	return frc::SwerveModuleState::Optimize(state, Rotation2d(units::degree_t(-cc_actual)));
}

/**
 * @param state_vel desired wheel velocity (in/s)
 * @param state_pos desired wheel position (deg) (-180.0, 180.0]
 * @return optimized module state to minimize change in wheel position
*/
frc::SwerveModuleState WPILibSwerveModule::optimizeState(float state_vel, float state_pos)
{
	// invert cc angle to convert to wpilib coordinate system
    return frc::SwerveModuleState::Optimize({units::meters_per_second_t(state_vel * INCHES_TO_METERS), units::degree_t(state_pos)},
											Rotation2d(units::degree_t(-cc_actual)));
}

/**
 * @return current module state (velocity, position)
 * @note wpilib uses m/s and rad; swervemodulestate is a wpiib struct so it must use those units
*/
frc::SwerveModuleState WPILibSwerveModule::getState(void)
{
	drive_command_velocity = drive_motor->getVelocity();
	cc_actual = cancoder->getAbsoluteAngle();
	module_actual_state = {units::meters_per_second_t(drive_command_velocity * INCHES_TO_METERS), units::degree_t(cc_actual)};
	return module_actual_state;
}

/**
 * @return current wheel position, received from the cancoder (deg) (-180.0, 180.0]
*/
float WPILibSwerveModule::getEncoderPosition(void)
{
    cc_actual = cancoder->getAbsoluteAngle();
	return cc_actual;
}

/**
 * @return current wheel target position (deg) (-180.0, 180.0]
*/
float WPILibSwerveModule::getEncoderTarget(void)
{
	return cc_target;
}

/**
 * @return current wheel position error (deg)
*/
float WPILibSwerveModule::getEncoderError(void)
{
	cc_error = cc_target - cc_actual;
	return cc_error;
}

/**
 * @return current drive motor power [-1.0, 1.0]
*/
float WPILibSwerveModule::getDrivePower(void)
{
	drive_command_power = drive_motor->getPercent();
	return drive_command_power;
}

/**
 * @return current drive motor velocity (in/s)
*/
float WPILibSwerveModule::getDriveVelocity(void)
{
	drive_command_velocity = drive_motor->getVelocity();
	return drive_command_velocity;
}

/**
 * @return current steering motor power [-1.0, 1.0]
*/
float WPILibSwerveModule::getSteeringPower(void)
{
    return steering_motor->getPercent();
}

/**
 * @return current module position (distance traveled & angle) as a wpilib swervemoduleposition object
*/
frc::SwerveModulePosition WPILibSwerveModule::getModulePosition(void)
{
	// invert cc angle to convert from chips to wpilib coordinate system
	return {units::inch_t(drive_motor->getPosition()),
		units::degree_t(-1 * cancoder->getAbsoluteAngle())};
}