/*******************************************************************************
 *
 * File: HangControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "HangControl.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
HangControl::HangControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	Advisory::pinfo("========================= Creating %s Control =========================",
	    control_name.c_str());

	hooks_motor = nullptr;
	brake_servo = nullptr;
	
	m_is_ready = false;

	hooks_step_size = 0.5;
	hooks_unlock_step_size = 0.75;

	hooks_down_position = -3.0;  // hooks as far down as they go
	hooks_up_position = 3.0;  // hooks as far up as they go
	hooks_stow_position = 0.0;   // hooks just in stow
	hooks_actual_position = 0.0;
	hooks_target_position = 0.0;
	hooks_command_position = 0.0;
	hooks_position_tolerance = 0.25;
	hooks_position_error = 0.0;
	hooks_unlock_position = 0.0;

	hooks_enabled = true;

	brake_command_position = 0.0;
	brake_target_position = 0.0;
	// brake_step_size = 5.0;
	brake_max_position = 60.0;
	brake_min_position = -60.0;
	brake_release_position = -10.0;
	brake_engage_position = 0.0;

	// do_brake_release = false;

	//
	// Register Macro Steps
	//
	// new MacroStepProxy<MSHangPosition>(control_name, "HangPosition", this);

	//
	// Parse XML
	//
	XMLElement *comp;
	const char *name = nullptr;

	comp = xml->FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "hooks_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for hooks_motor");
				hooks_motor = HardwareFactory::createMotor(comp);
				comp->QueryFloatAttribute("hooks_down_position",      &hooks_down_position);
				comp->QueryFloatAttribute("hooks_stow_position",      &hooks_stow_position);
				comp->QueryFloatAttribute("hooks_up_position",        &hooks_up_position);
				comp->QueryFloatAttribute("hooks_step_size",          &hooks_step_size);
				comp->QueryFloatAttribute("hooks_unlock_step_size",   &hooks_unlock_step_size);
				comp->QueryFloatAttribute("hooks_position_tolerance", &hooks_position_tolerance);
				comp->QueryBoolAttribute("hooks_enabled",             &hooks_enabled);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp->NextSiblingElement("motor");
	}

	comp = xml->FirstChildElement("servo");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "brake_servo") == 0)
			{
				Advisory::pinfo("  creating servo controller for brake_servo");
				brake_servo = HardwareFactory::createServo(comp);
				comp->QueryFloatAttribute("brake_step_size",        &brake_step_size);
				comp->QueryFloatAttribute("brake_max_position",     &brake_max_position);
				comp->QueryFloatAttribute("brake_min_position",     &brake_min_position);
				comp->QueryFloatAttribute("brake_release_position", &brake_release_position);
				comp->QueryFloatAttribute("brake_engage_position",  &brake_engage_position);
				brake_target_position = brake_release_position;
			}
			else
			{
				Advisory::pwarning("found unexpected servo with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected servo with no name attribute");
		}
		comp = comp->NextSiblingElement("servo");
	}
	
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "brake_engage") == 0)
			{
			    Advisory::pinfo("  connecting %s channel", name);
			    OIController::subscribeDigital(comp, this, CMD_BRAKE_ENGAGE);
			}
			else if (strcmp(name, "brake_release") == 0)
			{
			    Advisory::pinfo("  connecting %s channel", name);
			    OIController::subscribeDigital(comp, this, CMD_BRAKE_RELEASE);
			}
			else if (strcmp(name, "brake_step_up") == 0)
			{
			    Advisory::pinfo("  connecting %s channel", name);
			    OIController::subscribeDigital(comp, this, CMD_BRAKE_STEP_UP);
			}
			else if (strcmp(name, "brake_step_down") == 0)
			{
			    Advisory::pinfo("  connecting %s channel", name);
			    OIController::subscribeDigital(comp, this, CMD_BRAKE_STEP_DOWN);
			}
			// else if (strcmp(name, "hooks_up") == 0)
			// {
			//     Advisory::pinfo("  connecting %s channel", name);
			//     OIController::subscribeDigital(comp, this, CMD_HOOKS_RAISE);
			// }
			// else if (strcmp(name, "hooks_stow") == 0)
			// {
			//     Advisory::pinfo("  connecting %s channel", name);
			//     OIController::subscribeDigital(comp, this, CMD_HOOKS_LOWER);
			// }
			// else if (strcmp(name, "hooks_down") == 0)
			// {
			//     Advisory::pinfo("  connecting %s channel", name);
			//     OIController::subscribeDigital(comp, this, CMD_HOOKS_STOW);
			// }
			if (strcmp(name, "hooks_step_up") == 0)
			{
			    Advisory::pinfo("  connecting %s channel", name);
			    OIController::subscribeDigital(comp, this, CMD_HOOKS_STEP_RAISE);
			}
			else if (strcmp(name, "hooks_step_down") == 0)
			{
			    Advisory::pinfo("  connecting %s channel", name);
			    OIController::subscribeDigital(comp, this, CMD_HOOKS_STEP_LOWER);
			}			
			// else if (strcmp(name, "dpad_translation") == 0)
			// {
			//     Advisory::pinfo("  connecting dpad translation channel");
			//     OIController::subscribeInt(comp, this, CMD_DPAD_TRANSLATION);
			// }
			else
			{
				Advisory::pwarning("found unexpected io with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected io with no name attribute");
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
HangControl::~HangControl(void)
{
	if (hooks_motor != nullptr)
	{
		delete hooks_motor;
		hooks_motor = nullptr;
	}
	if (brake_servo != nullptr)
	{
		delete brake_servo;
		brake_servo = nullptr;
	}	
 }

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::controlInit(void)
{
    bool is_ready = true;
	
	if(hooks_motor == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- hooks_motor", getName().c_str());
		is_ready = false;
	}

	if(brake_servo == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- brake_servo", getName().c_str());
		is_ready = false;
	}

	m_is_ready = is_ready;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void HangControl::disabledInit(void)
{
	// Be sure to set each motor to zero power when disabled
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void HangControl::autonomousInit(void)
{
	// All motors should have zero power at start of autonomous 
    // so that the robot doesn't run away when you enable

	brake_target_position = brake_release_position;
	brake_servo->SetAngle(brake_release_position);
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void HangControl::teleopInit(void)
{
	// All motors should have zero power at start of teleop 
    // so that the robot doesn't run away when you enable

	brake_target_position = brake_release_position;
	brake_servo->SetAngle(brake_release_position);
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void HangControl::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void HangControl::setAnalog(int id, float val)
{
	// Add commands as necessary
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * 
 * @param button_press	the new value of the digital channel that the was 
 * 						subscribed to, the button is either pressed or released
 * 
 ******************************************************************************/
void HangControl::setDigital(int id, bool button_press)
{
	// Add commands as necessary
	Advisory::pinfo("HangControl::setDigital(%d, %d)", id, button_press);
	switch (id)
	{
		case CMD_BRAKE_ENGAGE: 
		{
			if (button_press)
			{
				brake_target_position = brake_engage_position;
			}
		} break;

		case CMD_BRAKE_RELEASE: 
		{
			if (button_press)
			{
				brake_target_position = brake_release_position;
			}
		} break;

		case CMD_BRAKE_STEP_UP: 
		{
			if (button_press)
			{
				brake_target_position += brake_step_size; 
			}
		} break;

		case CMD_BRAKE_STEP_DOWN: 
		{
			if (button_press)
			{
				brake_target_position -= brake_step_size; 
			}
		} break;

		// case CMD_HOOKS_RAISE: 
		// {
		// 	if (button_press)
		// 	{
		// 		hooks_target_position = hooks_up_position;
		// 	}
		// } break;

		// case CMD_HOOKS_LOWER: 
		// {
		// 	if (button_press)
		// 	{
		// 		hooks_target_position = hooks_down_position;
		// 	}
		// } break;

		// case CMD_HOOKS_STOW: 
		// {
		// 	if (button_press)
		// 	{
		// 		hooks_target_position = hooks_stow_position;
		// 	}
		// } break;

		case CMD_HOOKS_STEP_RAISE: 
		{
			if (button_press)
			{
				hooks_target_position += hooks_step_size;
			}
		} break;

		case CMD_HOOKS_STEP_LOWER:
		{
			if (button_press)
			{
				hooks_target_position -= hooks_step_size;
			}
		} break;

		default:
		{
			Advisory::pcaution("setDigital() was called for an unknown id, id=%d", id);
		} break;
	}
}

/*******************************************************************************
 *
 * This is the callback for OIController::setInt, if the XML config specifies 
 * a DPAD or other int input, the constructor of this object will connect 
 * that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void HangControl::setInt(int id, int val)
{
	// Advisory::pinfo("HangControl::setint(%d=%d)", id, val);
	// switch (id)
	// {
	// 	case CMD_DPAD_TRANSLATION:
	// 	{
	// 		if (val == 0)
	// 		{
	// 			setDigital(CMD_HOOKS_RAISE, true);
	// 		}
	// 		else if (val == 90)
	// 		{
	// 			setDigital(CMD_HOOKS_STOW, true);
	// 		}
	// 		else if (val == 180)
	// 		{
	// 			setDigital(CMD_HOOKS_LOWER, true);
	// 		}
	// 		else if (val == 270)
	// 		{
	// 			setDigital(CMD_BRAKE_ENGAGE, true);
	// 		}
	// 	} break;

	// 	default: // do nothing
	// 		break;
	// }
}

/*******************************************************************************	
 *
 ******************************************************************************/
void HangControl::publish()
{
	SmartDashboard::PutNumber(getName() + " position actual: ", hooks_actual_position);
	SmartDashboard::PutNumber(getName() + " position target: ", hooks_target_position);
	SmartDashboard::PutNumber(getName() + " position command: ", hooks_command_position);
	// SmartDashboard::PutNumber(getName() + " hooks down position: ", hooks_down_position);
	// SmartDashboard::PutNumber(getName() + " hooks stow position: ", hooks_stow_position);
	// SmartDashboard::PutNumber(getName() + " hooks up position: ", hooks_up_position);

	SmartDashboard::PutNumber(getName()  + " brake target position: ", brake_target_position);
	SmartDashboard::PutNumber(getName()  + " brake command position: ", brake_command_position);
	// SmartDashboard::PutBoolean(getName() + " brake locked: ", (brake_command_position == brake_engage_position));
	SmartDashboard::PutNumber(getName()  + " brake engage position: ", brake_engage_position);
	SmartDashboard::PutNumber(getName()  + " brake release position: ", brake_release_position);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void HangControl::doPeriodic()
{
	hooks_motor->doUpdate();
	
	// Get inputs
	hooks_motor->getPercent();
	hooks_actual_position = hooks_motor->getPosition();

	// make sure while disabled, everything is set to not move
	if (getPhase() == DISABLED)
	{
		hooks_target_position  = hooks_actual_position;

//		brake_target_position  = brake_engage_position;
//		brake_command_position = brake_engage_position;

//		do_brake_release = false;
	}

	// Process the Data
	hooks_position_error = hooks_target_position - hooks_actual_position;

//	if (! do_brake_release &&
//		(brake_target_position == brake_release_position) && 
//		(brake_command_position == brake_engage_position))
//	{
//		do_brake_release = true;
//		hooks_unlock_position = hooks_actual_position - hooks_unlock_step_size;
//	}

//	if (do_brake_release == true)
//	{
//		brake_command_position = brake_release_position;
//		hooks_command_position = hooks_unlock_position;
//
//		if (fabs(hooks_unlock_position - hooks_actual_position) < hooks_position_tolerance )
//		{
//			do_brake_release = false;
//		}
//	}
//	else
//	{
		// if at the target, engage the brake
//		if (fabs(hooks_position_error) < hooks_position_tolerance)
//		{
//			brake_command_position = brake_engage_position;
//		}
//		else 
//		{
//			brake_command_position = RobotUtil::limit(brake_min_position, brake_max_position, brake_target_position);
//		}

		// if brake is locked, motor should not be moving
//		if (brake_command_position != brake_release_position)
//		{
//			hooks_command_position = hooks_actual_position;
//		}
//		else
//		{
			hooks_command_position = hooks_target_position;
//		}
//	}

	// Set the Outputs

	if (hooks_enabled)
	{
		hooks_motor->setPosition(hooks_command_position);
		brake_command_position = brake_target_position;
		brake_servo->SetAngle(brake_command_position);
	}
	else
	{
		brake_command_position = brake_target_position;
		brake_servo->SetAngle(brake_command_position);
		hooks_motor->setPercent(0.0);
	}
}
