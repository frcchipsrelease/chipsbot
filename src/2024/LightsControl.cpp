/*******************************************************************************
 *
 * File: LightsControl.cpp
 *
 * Written by:
 *  Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *  NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *  Copywrite and License information can be found in the LICENSE.md file 
 *  distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "LightsControl.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
LightsControl::LightsControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp = nullptr;
	const char *name = nullptr;
	Advisory::pinfo("========================= creating 2024 lights control ☆ =========================",
	            control_name.c_str());

	lights = nullptr;
	intake_sensor = nullptr;
	intake_sensor_state = false;

	xml->QueryFloatAttribute("intake_pattern_time", &intake_pattern_time);

	comp = xml->FirstChildElement("lights_strip");
	if (comp != nullptr)
	{
		lights = HardwareFactory::createLightsStrip(comp);
		length = lights->getLength();
	}
	else
	{
		Advisory::pinfo("lights control created without lights strip, control will not work");
	}

	comp = xml->FirstChildElement("digital_input");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "intake_sensor") == 0)
			{
				intake_sensor = HardwareFactory::createDigitalInput(comp);
			}
			else
			{
				Advisory::pinfo("lights control: digital input found with no name attribute");
			}
		}
		comp = xml->NextSiblingElement("digital_input");
	}
	if (intake_sensor == nullptr)
	{
		Advisory::pinfo("lights control created without intake_sensor");
	}

	// not reading anything else from the config
	// its ok for lights to be hardcoded because their function changes every year anyway
}

bool LightsControl::intake_sensor_state = false;

/*******************************************************************************	
 * 
 ******************************************************************************/
LightsControl::~LightsControl(void)
{
	if (lights != nullptr)
	{
		delete lights;
		lights = nullptr;
	}
	if (intake_sensor != nullptr)
	{
		delete intake_sensor;
		intake_sensor = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void LightsControl::controlInit(void)
{
	lights->start();
}

/*******************************************************************************
 *
 ******************************************************************************/
void LightsControl::updateConfig(void)
{
}

/*******************************************************************************	
 * 
 ******************************************************************************/
void LightsControl::disabledInit(void)
{
	setPattern(DISABLED);
}

/*******************************************************************************	
 * 
 ******************************************************************************/
void LightsControl::autonInit(void)
{
	setPattern(AUTON);
}

/*******************************************************************************	
 * 
 ******************************************************************************/
void LightsControl::teleopInit(void)
{
	setPattern(TELEOP);
}

/*******************************************************************************	
 *
 ******************************************************************************/
void LightsControl::testInit(void)
{
}

/*******************************************************************************	
 *
 ******************************************************************************/
void LightsControl::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void LightsControl::setDigital(int id, bool val)
{
	// Add commands as necessary
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		default:
		{
			Advisory::pcaution("setDigital() was called for an unknown id, id=%d", id);
		} break;
	}
}

void LightsControl::setInt(int id, int val)
{
}

/*******************************************************************************	
 *
 ******************************************************************************/
void LightsControl::publish()
{
	SmartDashboard::PutBoolean(" intake light sensor state: ", intake_sensor_state);
	// SmartDashboard::PutNumber(getName() + " pattern: ", pattern);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed
 * 
 ******************************************************************************/
void LightsControl::doPeriodic()
{
	move_count++;
	move_count %= length;

	if (intake_sensor != nullptr)
	{
		intake_sensor_state = intake_sensor->Get(); // invert if needed
	}
	else
	{
		intake_sensor_state = false;
	}

	if (getPhase() == ControlPhase::DISABLED)
	{
		setPattern(DISABLED);
	}
	else if (getPhase() == ControlPhase::AUTON)
	{
		setPattern(AUTON);
	}
	else if (getPhase() == ControlPhase::TELEOP)
	{
		setPattern(TELEOP);
	}

	if (intake_sensor_state)
	{
		if (getPhase() == ControlPhase::TELEOP)
		{
			intake_count = 1;
			setPattern(INTAKE);
		}
	}
	else
	{
		if (intake_count >= 1)
		{
			setPattern(INTAKE);
			intake_count++;
		}
		if (intake_count >= (intake_pattern_time / this->getPeriod()))
		{
			intake_count = 0;
		}
	}

	// include lights.h in another subsystem, then call lights::setpattern()
	// or control lights directly from here
	switch (pattern)
	{
		case DISABLED:
		{
			// lights->setStaticPattern(lights->GRADIENT, 0, length/2, BLUE, RED);
			// lights->setStaticPattern(lights->GRADIENT, length/2, length, BLUE, RED);
			lights->setStaticPattern(lights->GRADIENT, 0, length, BLUE, RED);
		} break;

		case AUTON:
		{
			lights->setMovingPattern(lights->THEATER_CHASE, 0, length, BLUE, NONE, move_count);
		} break;

		case TELEOP:
		{
			lights->setMovingPattern(lights->LOADING, 0, length, BLUE, NONE, move_count);
		} break;

		case INTAKE:
		{
			lights->setStaticPattern(lights->SOLID, 0, length, ORANGE, NONE);
		} break;

		default:
		  break;
	}
	lights->setData();
}

bool LightsControl::getIntakeSensorState(void)
{
	return intake_sensor_state;
}

void LightsControl::setPattern(int p)
{
	pattern = p;
}