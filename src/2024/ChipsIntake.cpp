/*******************************************************************************
 *
 * File: ChipsIntake.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 * 
 * setpoints:
 * lift lower 1.49998
 * lift upper 280.003143
 * pivot lower -0.02
 * (maybe) pivot upper -16.7618
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Time.h"
#include "ChipsIntake.h"
#include "Shooter24.h"
#include "LightsControl.h"
#include "rfutilities/MacroStepFactory.h"
#include "frc/smartdashboard/SmartDashboard.h" //WPI


using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * Intake::Intake
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 

 ******************************************************************************/
Intake::Intake(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Intake Control [%s] =========================", 
				control_name.c_str());


	// Be sure to initialize all method variables
	// I left a lot of the old names. Get rid of what you don't use
	pivot_motor = nullptr; //closed loop
	lift_motor = nullptr; //closed loop
	roller_motor = nullptr; //open loop
	blooper_motor = nullptr; //open loop
	
	motor_min_control = -1.0;
	motor_max_control = 1.0;

	pivot_motor_target_power = 0.0;
	lift_motor_target_power = 0.0;
	roller_motor_target_power = 0.0;
	blooper_motor_target_power = 0.0;
	pivot_motor_command_power = 0.0;
	lift_motor_command_power = 0.0;
	roller_motor_command_power = 0.0;
	blooper_motor_command_power = 0.0;

	motor_roller_in_max_power = 0.0;
	motor_roller_out_max_power = 0.0;

	pivot_up_slow = 0.15;

	motor_max_cmd_delta = 0.0;

	const char *name = nullptr;

	is_ready = true;

	lift_upper_limit_pressed = false;
	lift_lower_limit_pressed = false;

	pivot_upper_limit_pressed = false;
	pivot_lower_limit_pressed = false;


	lift_limit_up = nullptr;
	lift_limit_down = nullptr;

	pivot_limit_up = nullptr;
	pivot_limit_down = nullptr;



	is_closed_loop = false;
	lift_pid = nullptr;
	pivot_pid = nullptr;
	min_position_lift = -95.0;
	max_position_lift = 95.0;
	lift_target_position = 0.0; //may need to replace values with the normal resting place inches
	lift_actual_position = 0.0; //do both
	lift_position_error = 0.0;
	lift_offset_angle = 0.0;

	min_position_pivot = -95.0;
	max_position_pivot = 95.0;
	pivot_target_position = 0.0; //may need to replace values with the normal resting place inches
	pivot_actual_position = 0.0; //do both
	pivot_position_error = 0.0;
	pivot_offset_angle = 0.0;

	for (uint8_t i = 0; i < NUM_SETPOINTS; i++)
	{
		setpoint_name [i] = "unknown";
		lift_setpoint_position [i] = 0.0;
		pivot_setpoint_position [i] = 0.0;
		setpoint_defined [i] = false;

	}
	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSRollersIn>(control_name, "RollersIn", this);
	new MacroStepProxy<MSRollersOut>(control_name, "RollersOut", this);
	new MacroStepProxy<MSRollersOff>(control_name, "RollersOff", this);
	new MacroStepProxy<MSPivotDown>(control_name, "PivotDown", this);
	new MacroStepProxy<MSPivotUp>(control_name, "PivotUp", this);
	new MacroStepProxy<MSPivotStop>(control_name, "PivotStop", this);
	new MacroStepProxy<MSLiftSetpoint>(control_name, "LiftSetpoint", this);
	new MacroStepProxy<MSBlooperIn>(control_name, "BlooperIn", this);
	new MacroStepProxy<MSBlooperOut>(control_name, "BlooperOut", this);
	new MacroStepProxy<MSBlooperOff>(control_name, "BlooperOff", this);

	//
	// Parse XML
	//
	
    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &motor_max_cmd_delta);
    Advisory::pinfo("max cmd delta = %f", motor_max_cmd_delta);

	xml->QueryFloatAttribute("min_position_lift", &min_position_lift);
	xml->QueryFloatAttribute("max_position_lift", &max_position_lift);
	xml->QueryFloatAttribute("min_position_pivot", &min_position_pivot);
	xml->QueryFloatAttribute("max_position_pivot", &max_position_pivot);
	xml->QueryFloatAttribute("lift_offset_angle", &lift_offset_angle);
	xml->QueryFloatAttribute("lift_position_error", &lift_position_error);
	xml->QueryFloatAttribute("pivot_position_error", &pivot_position_error);
	xml->QueryFloatAttribute("pivot_offset_angle", &pivot_offset_angle);




	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp!=nullptr)
		{
			int setpoint_index = -1;
			setpoint_comp->QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < NUM_SETPOINTS)
			{
				name = setpoint_comp->Attribute("name");
				if (name != nullptr)
				{
					setpoint_name[setpoint_index] = std::string(name);
				}
				else
				{
					setpoint_name[setpoint_index] = std::string("setpoint_") + std::to_string(setpoint_index);
				}

				setpoint_comp->QueryFloatAttribute("position", &lift_setpoint_position[setpoint_index]);
				//setpoint_comp->QueryFloatAttribute("position", &lift_setpoint_position[setpoint_index]);
				//setpoint_comp->QueryFloatAttribute("pivot_position", &pivot_setpoint_position[setpoint_index]);

				
				setpoint_defined[setpoint_index] = true;

				Advisory::pinfo(" -- setpoint %d  position = %.2f", setpoint_index, lift_setpoint_position[setpoint_index]);
				//Advisory::pinfo(" -- setpoint %d  lift_amp = %.2f", setpoint_index, setpoint_name[setpoint_index].c_str(), lift_setpoint_position[setpoint_index]);

				/*Advisory::pinfo(" -- setpoint %2d: %20s   pivot = %7.2f",
					setpoint_index, setpoint_name[setpoint_index].c_str(),
					pivot_setpoint_position[setpoint_index]);*/

			}
			else
			{
				Advisory::pinfo("setpoint with index out of range -- %d", setpoint_index);
			}

    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}

	(comp = xml -> FirstChildElement("motor"));
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
			if (name != nullptr)
		{
			if(strcmp(name, "pivot_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for pivot motor");
				pivot_motor = HardwareFactory::createMotor(comp);
			}
			else if(strcmp(name, "lift_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for lift motor");
				lift_motor = HardwareFactory::createMotor(comp);
			}
			else if(strcmp(name, "roller_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for roller motor");
				roller_motor = HardwareFactory::createMotor(comp);
			}
			else if(strcmp(name, "blooper_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for blooper motor");
				blooper_motor = HardwareFactory::createMotor(comp);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}


/*	comp = xml-> FirstChildElement("lift_pid");
	if (comp != nullptr)
	{
	    lift_pid = HardwareFactory::createPid(comp);
		Advisory::pinfo("lift_pid: %x",lift_pid);
    }

	comp = xml-> FirstChildElement("pivot_pid");
	if (comp != nullptr)
	{
	    pivot_pid = HardwareFactory::createPid(comp);
		Advisory::pinfo("pivot_pid: %x",pivot_pid);
    }
	*/
	comp = xml->FirstChildElement("limit_switch");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "lift_limit_up") == 0)
			{
				Advisory::pinfo("  creating lift limit up for %s", name);
				lift_limit_up = HardwareFactory::createLimitSwitch(comp);
			}
			else if (strcmp(name, "lift_limit_down") == 0)
			{
				Advisory::pinfo("  creating lift limit down for %s", name);
				lift_limit_down = HardwareFactory::createLimitSwitch(comp);
			}
			else if (strcmp(name, "pivot_limit_up") == 0)
			{
				Advisory::pinfo("  creating pivot limit up for %s", name);
				pivot_limit_up = HardwareFactory::createLimitSwitch(comp);
			}
			else if (strcmp(name, "pivot_limit_down") == 0)
			{
				Advisory::pinfo("  creating pivot limit down for %s", name);
				pivot_limit_down = HardwareFactory::createLimitSwitch(comp);
			}
			else
			{
				Advisory::pwarning("  found limit_switch tag with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found  tag with no name attribute");
		}
		comp = comp->NextSiblingElement("limit_switch");
	}
				Advisory::pinfo("  before oi");

	(comp = xml -> FirstChildElement("oi"));
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "roller_in") == 0)
			{
				Advisory::pinfo("  connecting roller in channel");
				comp->QueryFloatAttribute("value", & motor_roller_in_max_power);
				Advisory::pinfo("  motor_roller_in_max_power: %.2f", motor_roller_in_max_power);
				OIController::subscribeDigital(comp, this, ROLLER_IN);
			}
			else if (strcmp(name, "roller_out") == 0)
			{
				Advisory::pinfo("  connecting roller out channel");
				comp->QueryFloatAttribute("value", & motor_roller_out_max_power);
				OIController::subscribeDigital(comp, this, ROLLER_OUT);
			}
			else if (strcmp(name, "pivot_up") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & pivot_up_max_power);
				OIController::subscribeDigital(comp, this, PIVOT_UP);
			}
			else if (strcmp(name, "pivot_up_slow") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & pivot_up_slow);
				OIController::subscribeDigital(comp, this, PIVOT_UP_SLOW);
			}
			else if (strcmp(name, "pivot_down") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & pivot_down_max_power);
				OIController::subscribeDigital(comp, this, PIVOT_DOWN);
			}
			else if (strcmp(name, "roller_stop") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, ROLLER_STOP);
			}
			else if (strcmp(name, "blooper_in") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & motor_roller_in_max_power);
				OIController::subscribeDigital(comp, this, BLOOPER_IN);
			}
			else if (strcmp(name, "blooper_out") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & motor_roller_out_max_power);
				OIController::subscribeDigital(comp, this, BLOOPER_OUT);
			}
			else if (strcmp(name, "lift_up") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & lift_up_max_power);
				OIController::subscribeDigital(comp, this, LIFT_UP);
			}
			else if (strcmp(name, "lift_down") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				comp->QueryFloatAttribute("value", & lift_down_max_power);
				OIController::subscribeDigital(comp, this, LIFT_DOWN);
			}
			//for reporting to smartdashboard
			else if (strcmp(name, "closed_loop_state") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CLOSED_LOOP_STATE);
			}
			else if ((strncmp(name, "setpoint0", 8) == 0)
				&& (name[8] >= '0') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, SETPOINT_0 + (name[8] - '0'));
			}
			else if ((strncmp(name, "setpoint1", 8) == 0)
				&& (name[8] >= '1') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, SETPOINT_1 + (name[8] - '1'));
			}
			else if ((strncmp(name, "setpoint2", 8) == 0)
				&& (name[8] >= '1') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, SETPOINT_2 + (name[8] - '2'));
			}
			else if ((strncmp(name, "setpoint3", 8) == 0)
				&& (name[8] >= '1') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, SETPOINT_3 + (name[8] - '3'));
			}
		}
		
		comp = comp->NextSiblingElement("oi");

	}

}
/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
Intake::~Intake(void)
{
	// Be sure to reference each motor
	if (pivot_motor != nullptr)
	{
		delete pivot_motor;
		pivot_motor = nullptr;
	}
	if (lift_motor != nullptr)
	{
		delete lift_motor;
		(lift_motor = nullptr);
	}
	if (roller_motor != nullptr)
	{
		delete roller_motor;
		roller_motor = nullptr;
	}
	if (blooper_motor != nullptr)
	{
		delete blooper_motor;
		blooper_motor = nullptr;
	}
	if (lift_limit_down != nullptr)
	{
		delete lift_limit_down;
		lift_limit_down = nullptr;
	}
	if (lift_limit_up != nullptr)
	{
		delete lift_limit_up;
		lift_limit_up = nullptr;
	}
	if (pivot_limit_up != nullptr)
	{
		delete pivot_limit_up;
		pivot_limit_up = nullptr;
	}
	if (pivot_limit_down != nullptr)
	{
		delete pivot_limit_down;
		pivot_limit_down = nullptr;
	}
}
/*******************************************************************************
 *
 ******************************************************************************/
bool Intake::isClosedLoop(void)
{
	Advisory::pinfo("in set closed loop");
	return is_closed_loop;
}
/*******************************************************************************
 *
 ******************************************************************************/
void Intake::applySetPoint(bool on, int idx)
{
	if (on && (idx >= 0) && (idx < NUM_SETPOINTS))
	{
		if (setpoint_defined[idx] == true)
		{
			setClosedLoop(true);
			lift_target_position = lift_setpoint_position[idx];
			pivot_target_position = pivot_setpoint_position[idx];
			
			Advisory::pinfo("%s applySetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, lift_target_position, pivot_target_position);
		}
		else
		{
			Advisory::pinfo("%s applySetpoint: rejected, setpoint not defined for index %d", getName().c_str(), idx);
		}
	}
}
/*******************************************************************************
 *
 ******************************************************************************/
void Intake::controlInit(void)
{ 
	// Make sure every motor has current limiting. No magic smoke
	if (is_ready == false) return;

	if( pivot_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- pivot_motor", getName().c_str());
		is_ready == false;
	}

	if( lift_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- lift_motor", getName().c_str());
		is_ready == false;
	}
	if( roller_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- roller_motor", getName().c_str());
		is_ready == false;
	}
	if( blooper_motor == nullptr )
	{
		Advisory::pwarning("%s missing required component -- blooper_motor", getName().c_str());
		is_ready == false;
	}
	
	Advisory::pinfo("setting motor power to 0");
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::disabledInit(void)
{
	// Be sure to set each motor to zero power when disabled
	if (is_ready == false)	return;
	
	pivot_motor_target_power = 0.0;
	lift_motor_target_power = 0.0;
	roller_motor_target_power = 0.0;
	blooper_motor_target_power = 0.0;

	setClosedLoop(true);
	
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::autonomousInit(void)
{
	// All motors should have zero power at start of autonomous 
      // so that the robot doesn't run away when you enable
	pivot_motor_target_power = 0.0;
	lift_motor_target_power = 0.0;
	roller_motor_target_power = 0.0;
	blooper_motor_target_power = 0.0;
	pivot_motor_command_power = 0.0;
	lift_motor_command_power = 0.0;
	roller_motor_command_power = 0.0;
	blooper_motor_command_power = 0.0;
	
	setClosedLoop(true);
	lift_target_position = lift_actual_position;
	pivot_target_position = pivot_actual_position;

}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::teleopInit(void)
{
	// All motors should have zero power at start of teleop 
      // so that the robot doesn't run away when you enable
	if (is_ready == false) return;
	Advisory::pinfo("setting motor power to 0");

	pivot_motor_target_power = 0.0;
	lift_motor_target_power = 0.0;
	roller_motor_target_power = 0.0;
	blooper_motor_target_power = 0.0;
	pivot_motor_command_power = 0.0;
	lift_motor_command_power = 0.0;
	roller_motor_command_power = 0.0;
	blooper_motor_command_power = 0.0;

	setClosedLoop(true);
	lift_target_position = lift_actual_position;
	pivot_target_position = pivot_actual_position;

}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void Intake::testInit(void)
{
}

/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void Intake::initLogFile(void)
{
	lift_log->openSegment();

	lift_log->log("%s, %s, %s, %s, %s, ",
                    "current_time",
                    "lift_target_position",
					"lift_actual_position",
 					"lift_target_power",
					"lift_command_power");
    lift_log->log("\n");
    lift_log->flush();

	pivot_log->openSegment();

	pivot_log->log("%s, %s, %s, %s, %s, ",
                    "current_time",
                    "pivot_target_position",
					"pivot_actual_position",
 					"pivot_target_power",
					"pivot_command_power");
    pivot_log->log("\n");
    pivot_log->flush();
}

/**********************************************************************
 *
 * This method is used to update the log files, called from
 * doPeriodic
 *
 **********************************************************************/
void Intake::updateLogFile(void)
{
	lift_log->log("%f, %f, %f, %f, %f, ",
                    gsi::Time::getTime(),
                    lift_target_position,
					lift_actual_position,
                    lift_motor_target_power, 
					lift_motor_command_power);
    lift_log->log("\n");
    lift_log->flush();	

	pivot_log->log("%f, %f, %f, %f, %f, ",
                    gsi::Time::getTime(),
                    pivot_target_position,
					pivot_actual_position,
                    pivot_motor_target_power, 
					pivot_motor_command_power);
    pivot_log->log("\n");
    pivot_log->flush();
} 

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Intake::setAnalog(int id, float val)
// Add commands as necessary
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void Intake::setDigital(int id, bool val)
{
	switch (id)
	{
		case CLOSED_LOOP_STATE:
		{
			setClosedLoop(!isClosedLoop());
		} break;

		case ROLLER_STOP:
		{
			if (val)
			{
			roller_motor_target_power = 0;
			}
		} break;
		
		case ROLLER_IN:
		{
			if (val)
			{
			roller_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_roller_in_max_power);
			}
			else
			{
				roller_motor_target_power = 0;
			}
		} break;
																																															
		case ROLLER_OUT:
		{
			if (val)
			{
			roller_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_roller_out_max_power);
			}
			else
			{
				roller_motor_target_power = 0;
			}
	    } break;
		
		case BLOOPER_IN:
		{
			if (val)
			{
			blooper_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_roller_in_max_power);
			}
			else
			{
				blooper_motor_target_power = 0;
			}
		} break;
		
		case BLOOPER_OUT:
		{
			if (val)
			{
			blooper_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_roller_out_max_power);
			}
			else
			{
				blooper_motor_target_power = 0;
			}
		} break;
		
		case LIFT_DOWN:
		{
			if (val)
			{
			lift_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, lift_down_max_power);
			setClosedLoop(false);
			}
			else
			{
				lift_motor_target_power = 0;
			}
		} break;
		
		case LIFT_UP:
		{
			if (val)
			{
			lift_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, lift_up_max_power);
			setClosedLoop(false);

			}
			else
			{
				lift_motor_target_power = 0;
			}
	    } break;
		
		case PIVOT_UP:
		{
			if (val)
			{
			pivot_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, pivot_up_max_power);
			}
			else
			{
				pivot_motor_target_power = 0;
			}
		} break;
		
		case PIVOT_UP_SLOW:
		{
			if (val)
			{
			pivot_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, pivot_up_slow);
			}
			else
			{
				pivot_motor_target_power = 0;
			}
		} break;
		
		case PIVOT_DOWN:
		{
			if (val)
			{
			pivot_motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, pivot_down_max_power);
			}
			else
			{
				pivot_motor_target_power = 0;
			}
	} break;
	
	case SETPOINT_0: 	applySetPoint(val, 0);  break;

	case SETPOINT_1: 	applySetPoint(val, 1);  break;

	case SETPOINT_2: 	applySetPoint(val, 2);  break;
	
	case SETPOINT_3: 	applySetPoint(val, 3);  break;

		} 
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::setInt(int id, int val)
{
}

void Intake::setRollerPower(float, int)
{
	roller_motor_target_power = 0.5;
}
int Intake::getSetpointIndex()
{

	return setpoint_index;
}

void togglePosition()
{
}

void Intake::setLiftPosition(float position)
{
	lift_target_position = position;
}
float Intake::getLiftPosition(void)
{
	return lift_actual_position;
}
void Intake::setPivotPosition(float position)
{
	pivot_target_position = position;
}
float Intake::getPivotPosition(void)
{
	return pivot_actual_position;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void Intake::setClosedLoop(bool closed)
{
	Advisory::pinfo("in set closed loop");
	if (is_closed_loop != closed)
	{
		is_closed_loop = closed;

		if (is_closed_loop)
		{
		    lift_motor->setControlMode(Motor::POSITION);
		}
		else
		{
			lift_motor->setControlMode(Motor::PERCENT);
           	//Advisory::pinfo("setting %s open loop mode", getName().c_str());
		}
		pivot_motor->setControlMode(Motor::PERCENT);
		//Advisory::pinfo("setting %s open loop mode", getName().c_str());
	}
	//Advisory::pinfo("exiting closed loop");

}
/*******************************************************************************	
 *
 ******************************************************************************/
void Intake::publish()
{
	// Put whatever you want to see in here
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " pivot target_power: ", pivot_motor_target_power);
	SmartDashboard::PutNumber(getName() + " pivot command_power: ", pivot_motor_command_power);
	SmartDashboard::PutNumber(getName() + " pivot Motor Position Target: ", pivot_target_position);
	SmartDashboard::PutNumber(getName() + " pivot Motor Position Actual: ", pivot_actual_position);
	SmartDashboard::PutBoolean(getName() + " pivot upper state: ", pivot_upper_limit_pressed);
	SmartDashboard::PutBoolean(getName() + " pivot lower state: ", pivot_lower_limit_pressed);
	SmartDashboard::PutNumber(getName() + " lift target power: ", lift_motor_target_power);
	SmartDashboard::PutNumber(getName() + " lift command_power: ", lift_motor_command_power);
	SmartDashboard::PutNumber(getName() + " lift Motor Position Target: ", lift_target_position);
	SmartDashboard::PutNumber(getName() + " lift Motor Position Actual: ", lift_actual_position);
	SmartDashboard::PutNumber(getName() + " roller target power: ", roller_motor_target_power);
	SmartDashboard::PutNumber(getName() + " roller command_power: ", roller_motor_command_power);
	SmartDashboard::PutNumber(getName() + " blooper target power: ", blooper_motor_target_power);
	SmartDashboard::PutNumber(getName() + " blooper command_power: ", blooper_motor_command_power);
	SmartDashboard::PutBoolean(getName() + " lift lower state: ", lift_lower_limit_pressed);
    SmartDashboard::PutBoolean(getName() + " lift upper state: ", lift_upper_limit_pressed);
	SmartDashboard::PutNumber(getName() + " lift motor Position Error: ", lift_position_error);
	SmartDashboard::PutNumber(getName() + " pivot motor Position Error: ", pivot_position_error);
	SmartDashboard::PutBoolean(getName() + " Closed Loop: ", is_closed_loop);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Intake::doPeriodic()
{
	
	// Update all motors
	if(is_ready == false) 
	{
		Advisory::pinfo("intake is not ready");
		return;
	}
		//Advisory::pinfo("dop -----------------------");
	

	pivot_motor -> doUpdate();
	lift_motor -> doUpdate();
	roller_motor -> doUpdate();
	blooper_motor -> doUpdate();
	//
	// Get inputs -- this is just for reporting
	//

	pivot_motor_command_power = pivot_motor -> getPercent();
	lift_motor_command_power = lift_motor -> getPercent();
	roller_motor_command_power = roller_motor -> getPercent();
	blooper_motor_command_power = blooper_motor -> getPercent();

	lift_upper_limit_pressed = lift_limit_up->isPressed();
	lift_lower_limit_pressed = lift_limit_down->isPressed();

	pivot_upper_limit_pressed = pivot_limit_up->isPressed();
	pivot_lower_limit_pressed = pivot_limit_down->isPressed();


	lift_actual_position = lift_motor->getPosition();
	pivot_actual_position = pivot_motor->getPosition();


	//
	// All processing happens in the motor class
	//

	//
	// Set Outputs
	//

	/*if (lift_lower_limit_pressed == true)

		{

			// set motor such that current position is something
			if (lift_limit_down->isReset())
			{

				lift_motor->resetPosition(lift_limit_down->getResetValue());
			

			}

			if (lift_target_position < lift_limit_down->getResetValue())
			{

				lift_target_position = lift_limit_down->getResetValue();
			}
		}*/

		if(lift_lower_limit_pressed == true)
		{

		lift_motor->resetPosition(0.0);
		lift_motor_command_power = gsu::Filter::limit(lift_motor_command_power, 0.0, 1.0);

		}

if (is_closed_loop)// Command the position
	{
		/*if (lift_upper_limit_pressed == true)
		{						
			// set motor such that current position is something
			if (lift_limit_up->isReset())
			{
				lift_motor->resetPosition(lift_limit_up->getResetValue());
			}

			if (lift_target_position > lift_limit_up->getResetValue())
			{
				lift_target_position = lift_limit_up->getResetValue();
			}
		}
			

		if (pivot_upper_limit_pressed == true)
		{
			if (pivot_limit_up->isReset())
			{
				pivot_motor->resetPosition(pivot_limit_up->getResetValue());
			}

			if (pivot_target_position > pivot_limit_up->getResetValue())
			{
				pivot_target_position = pivot_limit_up->getResetValue();
			}
		}
		else if (pivot_lower_limit_pressed == true)
		{	
			if (pivot_limit_down->isReset())
			{
				pivot_motor->resetPosition(pivot_limit_down->getResetValue());
			}

			if (pivot_target_position > pivot_limit_down->getResetValue())
			{
				pivot_target_position = pivot_limit_down->getResetValue();
			}
		}*/

		lift_motor->setPosition(lift_target_position);

		//pivot_motor->setPosition(pivot_target_position);
		//Advisory::pinfo("get lift_target_position: %.2f", lift_target_position);
		pivot_motor_command_power = pivot_motor_target_power;
		if (pivot_upper_limit_pressed == true)
		{
			pivot_motor_command_power = gsu::Filter::limit(pivot_motor_target_power, -1.0, 0.0);
		}
		if (pivot_lower_limit_pressed == true)
		{
			pivot_motor_command_power = gsu::Filter::limit(pivot_motor_target_power, 0.0, 1.0);
		}

		pivot_motor->setPercent(pivot_motor_command_power);
	}
	else
	{
		lift_motor_command_power = lift_motor_target_power;
		pivot_motor_command_power = pivot_motor_target_power;



		if (lift_upper_limit_pressed == true)
		{						
			lift_motor_command_power = gsu::Filter::limit(lift_motor_target_power, -1.0, 0.0);
		}
		if (lift_lower_limit_pressed == true)
		{
			lift_motor_command_power = gsu::Filter::limit(lift_motor_target_power, 0.0, 1.0);
		}
		if (pivot_upper_limit_pressed == true)
		{
			pivot_motor_command_power = gsu::Filter::limit(pivot_motor_target_power, -1.0, 0.0);
		}
		if (pivot_lower_limit_pressed == true)
		{
			pivot_motor_command_power = gsu::Filter::limit(pivot_motor_target_power, 0.0, 1.0);
		}

		pivot_motor->setPercent(pivot_motor_command_power);
		lift_motor->setPercent(lift_motor_command_power);			
		//	Advisory::pinfo("get lift_motor_target_power: %.2f", lift_motor_target_power);
	}

	roller_motor_command_power = roller_motor_target_power;
	if (LightsControl::getIntakeSensorState() && !Shooter24::isKickerRunning())
	{
		roller_motor_command_power =  gsu::Filter::limit(roller_motor_command_power, 0.0, -1.0);
	}

	roller_motor->setPercent(roller_motor_command_power);
	blooper_motor->setPercent(blooper_motor_target_power);


	//for reporting to smartdashboard
	lift_position_error = lift_target_position - lift_actual_position;
	pivot_position_error = pivot_target_position - pivot_actual_position;
}

// macros

/*******************************************************************************
 *
 ******************************************************************************/
MSRollersIn::MSRollersIn(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/

void MSRollersIn::init(void)
{
	parent_control->setDigital(parent_control->ROLLER_IN, true);
}


/*******************************************************************************
 *
 ******************************************************************************/

MacroStep * MSRollersIn::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSRollersOut::MSRollersOut(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSRollersOut::init(void)
{
	parent_control->setDigital(parent_control->ROLLER_OUT, true);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSRollersOut::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSRollersOff::MSRollersOff(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSRollersOff::init(void)
{
	parent_control->setDigital(parent_control->ROLLER_IN, false);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSRollersOff::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSPivotDown::MSPivotDown(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPivotDown::init(void)
{
	parent_control->setDigital(parent_control->PIVOT_DOWN, true);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPivotDown::update(void)
{
	return next_step;
}


/*******************************************************************************
 *
 ******************************************************************************/
MSPivotUp::MSPivotUp(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPivotUp::init(void)
{
	parent_control->setDigital(parent_control->PIVOT_UP, true);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPivotUp::update(void)
{
	return next_step;
}


/*******************************************************************************
 *
 ******************************************************************************/
MSPivotStop::MSPivotStop(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPivotStop::init(void)
{
	parent_control->setDigital(parent_control->PIVOT_DOWN, false);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPivotStop::update(void)
{
	return next_step;
}



/*******************************************************************************
 *
 ******************************************************************************/
MSLiftSetpoint::MSLiftSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
	xml->QueryIntAttribute("idx", &idx);	
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSLiftSetpoint::init(void)
{
	parent_control->applySetPoint(true, idx);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSLiftSetpoint::update(void)
{
	return next_step;
}


/*******************************************************************************
 *
 ******************************************************************************/
MSBlooperIn::MSBlooperIn(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/

void MSBlooperIn::init(void)
{
	parent_control->setDigital(parent_control->BLOOPER_IN, true);
}


/*******************************************************************************
 *
 ******************************************************************************/

MacroStep * MSBlooperIn::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSBlooperOut::MSBlooperOut(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSBlooperOut::init(void)
{
	parent_control->setDigital(parent_control->BLOOPER_OUT, true);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSBlooperOut::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSBlooperOff::MSBlooperOff(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (Intake *)control;
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSBlooperOff::init(void)
{
	parent_control->setDigital(parent_control->BLOOPER_IN, false);
}


/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSBlooperOff::update(void)
{
	return next_step;
}
