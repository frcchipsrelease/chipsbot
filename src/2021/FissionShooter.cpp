/*******************************************************************************
 *
 * File: FissionShooter.h
 *  
 * Fission CHIPS shooter control class. Mirrors the generic ShooterControl class
 * with specialized implementations specific to the 2021 Fission bot
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "gsutilities/Filter.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/MacroStepFactory.h"

#include "rfcontrols/FissionShooter.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;


FissionShooter::FissionShooter(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	Advisory::pinfo("========================= Creating Fission Shooter Control [%s] =========================", 
			control_name.c_str());
	
	// 
	// HARDWARE DEFAULTS
	// 
	kicker_motor = nullptr;
	kicker_percent_target  = 0.0;
	kicker_percent_command  = 0.0;
	kicker_percent_nominal = 0.85; // default
	kicker_velocity_actual = 0.0;

	fly_motor = nullptr;
	fly_percent_step = 0.05;
	fly_percent_target = 0.0;
	fly_percent_command = 0.0;
	fly_percent_nominal = 0.85;
	fly_percent_min = 0.2;
	fly_percent_max = 1.0;

	fly_velocity_step = 5000.0;
	fly_velocity_max = 215000.0;
	fly_velocity_min = 60000.0;
	fly_velocity_target = 0.0;
	fly_velocity_command = 0.0;
	fly_velocity_actual = 0.0; 
	fly_velocity_error = 0.0;

	fly_setpoint[0] = 0.0;
	fly_setpoint[1] = 0.50;
	fly_setpoint[2] = 0.75;
	fly_setpoint[3] = 1.00;
	setpoint_index = 0;

	hood_motor = nullptr;
	hood_position_actual = 0.0;
	hood_position_target = 0.0;
	hood_position_error  = 0.0;
	hood_position_kp = 0.02;
	hood_setpoint[0] = 0.0;
	hood_setpoint[1] = 0.0;
	hood_setpoint[2] = 0.0;
	hood_setpoint[3] = 0.0;
	hood_position_step = 5.0;
	hood_percent_nudge = 0.1;
	hood_gear_ratio = 1.0;
	hood_position_stop_hi = 90.0;
	hood_position_stop_lo = 0.0;
	hood_percent_target = 0.0;
	hood_percent_command = 0.0;

	hood_ls_upper = nullptr;
	hood_ls_lower = nullptr;
	hood_ls_upper_state = false;
	hood_ls_lower_state = false;

	hood_encoder = nullptr;
	cc_resolution = 2048;
    hood_pid = nullptr;

	ll_limelight = nullptr;
	lime_light_is_on = false;
	ll_is_visible = 0.0; // bool as float due to LL's weird API
	ll_actual_area = 0.0; // steradians? Who knows, didn't see in LL documentation 
	ll_actual_angle_vertical = 0.0; // vertical angle deg
	ll_actual_angle_horizontal = 0.0;  // horizontal angle deg
	ll_actual_angle_roll = 0.0; // roll angle deg
	ll_target_height = 0.0;
	ll_mount_height_current = 0.0;
	ll_mount_height_minimum = 0.0;
	ll_mount_angle_vertical = 0.0;
	ll_mount_planar_offset = 0.0;
    ll_target_angle_vertical = -19.9;
    ll_hood_error = 0.0;
    ll_hood_target = 0.0;


	//
	// Logs
	//
	log_auton = new DataLogger("/robot/logs/shooter", "autoshooter", "csv", 10, true);
    log_teleop = new DataLogger("/robot/logs/shooter", "teleshooter", "csv", 10, true);
	log_active = log_auton;

	active = false;
	closed_loop_on = false;

    //
	// Register Macro Steps
	//
	new MacroStepProxy<MSFissionShooter>(control_name, "SetPower", this);
	new MacroStepProxy<MSFissionFlywheelPower>(control_name, "SetFlywheelPower", this);
	new MacroStepProxy<MSFissionKickerPower>(control_name, "SetKickerPower", this);

	//
	// Parse the Controls XML
	//
	XMLElement* comp = nullptr;
	const char* name = nullptr;

	xml->QueryBoolAttribute("closed_loop", &closed_loop_on);
	xml->QueryBoolAttribute("limelight_targeting_state", &lime_light_is_on);


	comp = xml->FirstChildElement("limelight_curve");
			Advisory::pinfo(" limelight comp: %x ", comp);
	if (comp != nullptr)
	{
    	XMLElement *limelight_comp;
		limelight_comp = comp->FirstChildElement("line");
 		while (limelight_comp!=nullptr)
		{
			float vertical_angle = 0.0;
			float position = 0.0;
			limelight_comp->QueryFloatAttribute("vertical_angle", &vertical_angle);
			m_vertical_angle_table.push_back(vertical_angle);
			limelight_comp->QueryFloatAttribute("position", &position);
			m_hood_position_table.push_back(position);
			Advisory::pinfo(" ---------------------- limelight segment: limelight vertical angle = %7.2f motor position = %7.2",
					vertical_angle, position);
    		limelight_comp = limelight_comp->NextSiblingElement("line");
		}
		ll_target_curves.setCurve(m_vertical_angle_table, m_hood_position_table);
 		ll_target_curves.setLimitEnds(true);
	}


	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			XMLElement* subcomp = nullptr;
			if (strcmp(name, "flywheel") == 0)
			{
				Advisory::pinfo("%s  creating flywheel1 motor controller", getName().c_str());
				fly_motor = HardwareFactory::createMotor(comp);
				if( fly_motor != nullptr )
				{
					fly_motor->setVelocity(0.0); 
				}
				comp->QueryFloatAttribute("step_size", &fly_percent_step);
				comp->QueryFloatAttribute("nominal", &fly_percent_nominal);
				subcomp = comp->FirstChildElement("setpoints");
				parseSetPoints( subcomp, fly_setpoint );
			}

			else if (strcmp(name, "kicker") == 0)
			{
				Advisory::pinfo("%s  creating kicker motor controller", getName().c_str());
				kicker_motor = HardwareFactory::createMotor(comp);
				if(kicker_motor != nullptr)
				{
					kicker_motor->setVelocity(0.0); 
				}
				comp->QueryFloatAttribute("nominal", &kicker_percent_nominal);
			}

			else if( strcmp(name,"hood") == 0 )
			{
				Advisory::pinfo("%s  creating hood motor controller", getName().c_str());
				hood_motor = HardwareFactory::createMotor(comp);
				if( hood_motor != nullptr)
				{
					hood_motor->setVelocity(0.0); 
				}
				comp->QueryFloatAttribute("gear_ratio", &hood_gear_ratio);
				comp->QueryFloatAttribute("step_size", &hood_position_step);
				comp->QueryFloatAttribute("low_stop", &hood_position_stop_lo);
				comp->QueryFloatAttribute("high_stop", &hood_position_stop_hi);
				subcomp = comp->FirstChildElement("setpoints");
				parseSetPoints( subcomp, hood_setpoint );
				subcomp = comp->FirstChildElement("limit_switch");
				while( subcomp != nullptr )
				{
					name = subcomp->Attribute("name");
					if (name != nullptr)
					{
						if( strcmp(name,"upper") == 0 )
						{
							hood_ls_upper = HardwareFactory::createLimitSwitch(subcomp);
							Advisory::pinfo("%s  creating hood upper LS", getName().c_str());
						}
						else if( strcmp(name,"lower") == 0 )
						{
							hood_ls_lower = HardwareFactory::createLimitSwitch(subcomp);
							Advisory::pinfo("%s  creating hood lower LS", getName().c_str());
						}
						else
						{
							Advisory::pinfo("%s  unrecognized limit switch spec", getName().c_str());
						}
					}
					else
					{
						Advisory::pinfo("%s  unnamed limit switch spec", getName().c_str());
					}
					subcomp = subcomp->NextSiblingElement("limit_switch");
				}
			}
			else
			{
				Advisory::pinfo("%s -- unrecognized motor '%s'", getName().c_str(), name);
			}
		}
		comp = comp->NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("cancoder");
	if (comp != nullptr)
	{
		hood_encoder = HardwareFactory::createCancoder(comp);
    }

	comp = xml-> FirstChildElement("hood_pid");
	if (comp != nullptr)
	{
	    hood_pid = HardwareFactory::createPid(comp);
    }

	comp = xml-> FirstChildElement("limelight");
	if (comp != nullptr)
	{
		ll_limelight = HardwareFactory::createLimelight(comp);
		comp->QueryDoubleAttribute("target_height", &ll_target_height);
		Advisory::pinfo("  Target Height: %f", ll_target_height);
		comp->QueryDoubleAttribute("limelight_height", &ll_mount_height_minimum);
		Advisory::pinfo("  Limelight Height: %f", ll_mount_height_minimum);
		comp->QueryDoubleAttribute("target_vertical", &ll_target_angle_vertical);
		Advisory::pinfo("  Target Verticak: %f", ll_target_angle_vertical);
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
			// CLOSED LOOP -------------------------------------
			if( strcmp(name, "closed_loop_state") == 0 )
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if (strcmp(name, "closed_loop_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_TOGGLE);
			}
			else if (strcmp(name, "closed_loop_start") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_ON);
			}
			else if (strcmp(name, "closed_loop_stop") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_OFF);
			}

			// LIME LIGHT --------------------------------------
			else if (strcmp(name, "limelight_targeting_state") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_STATE);
			}
			else if (strcmp(name, "limelight_targeting_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_TOGGLE);
			}
			else if (strcmp(name, "limelight_targeting_on") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_ON);
			}
			else if (strcmp(name, "limelight_targeting_off") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_OFF);
			}

			// HOOD --------------------------------------------
			else if (strcmp(name, "hood_step_up") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_HOOD_STEP_UP);
			}
			else if (strcmp(name, "hood_step_down") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_HOOD_STEP_DN);
			}
			else if ((strcmp(name, "hood_rotate_up") == 0) || (strcmp(name, "hood_analog") == 0))
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeAnalog(comp, this, CMD_HOOD_ANALOG);
			}

			// FLYWHEEL -----------------------------------------------
			else if( strcmp(name, "flywheel_on") == 0 )
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_ON);
			}
			else if( strcmp(name, "flywheel_off") == 0 )
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_OFF);
			}
			else if( strcmp(name, "flywheel_step_up") == 0 )
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_STEP_UP);
			}
			else if (strcmp(name, "flywheel_step_down") == 0 )
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_STEP_DN);
			}

			else if ((strncmp(name, "setpoint_", 9) == 0)
				     && (name[9] >= '0') 
					 && (name[9] <= '3')                        )
			{
				Advisory::pinfo("%s  connecting %s channel as setpoint %d", getName().c_str(), name, name[9]-'0');
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[9] - '0'));
			}
			else if (strcmp(name, "dpad_setpoints") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeInt(comp, this, CMD_DPAD_SETPOINTS);
			}

			// KICKER --------------------------------------------------
			else if (strcmp(name, "kicker_forward") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_FWD);
			}
			else if (strcmp(name, "kicker_backward") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_BCK);
			}
			else if (strcmp(name, "kicker_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_TOGGLE);
			}
			else
			{
				Advisory::pinfo("%s unknown oi %s", getName().c_str(), name);
			}
		}
		else
		{
			Advisory::pinfo("%s unnamed oi", getName().c_str());
		}
		comp = comp->NextSiblingElement("oi");


	}

	// Calibrate
	if( hood_encoder != nullptr )
	{
		hood_position_actual = hood_encoder->getAbsoluteAngle();
	}

	Motor_TalonSrx* m_fx;
	m_fx = dynamic_cast<Motor_TalonSrx*>(hood_motor);
    if( m_fx != nullptr )
    {
        cc_resolution = 2048;
        m_fx->setSensorScale( 
			360.0/cc_resolution/hood_gear_ratio,
			hood_position_actual );
    }
	else
	{
		Advisory::pwarning("%s rotational motor WARNING: resolution unset",control_name.c_str());
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
FissionShooter::~FissionShooter(void)
{
	if (fly_motor != nullptr)
	{
		delete fly_motor;
		fly_motor = nullptr;
	}

	if (kicker_motor != nullptr)
	{
		delete kicker_motor;
		kicker_motor = nullptr;
	}

	if (hood_motor != nullptr)
	{
		delete hood_motor;
		hood_motor = nullptr;
	}

	if(ll_limelight != nullptr)
	{
		delete ll_limelight;
		ll_limelight = nullptr;
	}

	if( hood_encoder != nullptr )
	{
		delete hood_encoder;
		hood_encoder = nullptr;
	}

	if( hood_ls_upper != nullptr )
	{
		delete hood_ls_upper;
		hood_ls_upper = nullptr;
	}

	if( hood_ls_lower != nullptr )
	{
		delete hood_ls_lower;
		hood_ls_lower = nullptr;
	}

	if (log_auton != nullptr)
	{
	    log_auton->close();
		delete log_auton;
	    log_auton = nullptr;
	}

	if (log_teleop != nullptr)
	{
	    log_teleop->close();
		delete log_teleop;
	    log_teleop = nullptr;
	}

	log_active = nullptr;
}

/**
 * Parse a setpoints xml item
 */
int FissionShooter::parseSetPoints( tinyxml2::XMLElement* comp, float* setp )
{
	if( comp == nullptr )
	{
		return 0;
	}

	int active_index = 0;
	int num_setpoints = 0;

	XMLElement *setpoint_comp;
	const char* name;
	setpoint_comp = comp->FirstChildElement("setpoint");
	comp->QueryIntAttribute("active", &active_index);
	while (setpoint_comp != nullptr)
	{
		int sp_index = -1;
		setpoint_comp->QueryIntAttribute("index", &sp_index);
		if (sp_index < 0 || sp_index >= FissionShooter::NUM_SETPOINTS)
		{
			Advisory::pinfo("%s setpoint with unset or out of bounds index.\n"
			                "Automatically assigning to %d", getName().c_str(), num_setpoints);
			sp_index = num_setpoints;
		}
		setpoint_comp->QueryFloatAttribute("value", &setp[sp_index]);
		name = setpoint_comp->Attribute("name");
		if (name != nullptr)
		{
			setpoint_lookup.insert( std::pair<std::string,float&>(name,setp[sp_index]) );
		}
		else
		{
			Advisory::pwarning("%s found unnamed setpoint, using default", getName().c_str());
			char default_name[50];
			sprintf(default_name,"SETPOINT_%d",setpoint_lookup.size());
			setpoint_lookup.insert( std::pair<std::string,float&>(default_name,setp[sp_index]) );
		}
		Advisory::pinfo("%s  -- setpoint %2d: %20s   value=%7.2f",getName().c_str(),
			sp_index, name, setp[sp_index] );


		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		if( (++num_setpoints) >= FissionShooter::NUM_SETPOINTS )
		{
			break;
		}
	}

	return active_index;
}

/*******************************************************************************	
 *
 *
 *
 ******************************************************************************/
void FissionShooter::controlInit(void)
{
    bool is_ready = true;
	
	if(fly_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- flywheel motor", getName().c_str());
		is_ready = false;
	}
	else if(kicker_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- kicker motor", getName().c_str());
		is_ready = false;
	}
	else if(hood_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- hood motor", getName().c_str());
		is_ready = false;
	}
	else if(ll_limelight == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- limelight", getName().c_str());
		// is_ready = false; uncomment when ready
	}
	else if( hood_encoder == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- CANCoder", getName().c_str());
		is_ready = false;
	}
	else if( hood_ls_upper == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- Upper Limit Switch", getName().c_str());
		is_ready = false;
	}
	else if( hood_ls_lower == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- Lower Limit Switch", getName().c_str());
		is_ready = false;
	}
	
	active = is_ready;
	ll_height_difference = ll_target_height - ll_mount_height_minimum;
    hood_position_target = hood_position_actual;

}

/*******************************************************************************
 *
 *
 *
 ******************************************************************************/
void FissionShooter::updateConfig(void)
{
	// Unused TBD
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void FissionShooter::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_HOOD_ANALOG:
			hood_percent_target = val;
			break;

		default:
			// Do Nothing
			break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void FissionShooter::setDigital(int id, bool val)
{ 
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);

	switch (id)
	{
		case CMD_CLOSED_LOOP_STATE: 
		{	
			setClosedLoop(val);  
		} break;

		case CMD_CLOSED_LOOP_TOGGLE:
		{	
			if(val == true)
			{
				setClosedLoop(!isClosedLoop());
			}
		} break;

		case CMD_CLOSED_LOOP_ON:
		{	
			if(val == true)
			{
				setClosedLoop(true);
			}
		} break;

		case CMD_CLOSED_LOOP_OFF:
		{
			if(val == true)
			{
				setClosedLoop(false);
			}
		} break;

		case CMD_LIMELIGHT_STATE:
		{
			setLimeLightState(val);
		} break;

		case CMD_LIMELIGHT_TOGGLE:
		{
			if (val == true)
			{
				setLimeLightState( ! getLimeLightState() );
			}
		} break;

		case CMD_LIMELIGHT_ON:
		{
			if (val == true)
			{
				setLimeLightState(true);
			}
		} break;

		case CMD_LIMELIGHT_OFF:
		{
			if (val == true)
			{
				setLimeLightState(false);
			}
		} break;

		case CMD_KICKER_FWD:  
			kicker_percent_target = applyMomentary( val,  kicker_percent_nominal ); 
			break;
		
		case CMD_KICKER_BCK:  
			kicker_percent_target = applyMomentary( val, -kicker_percent_nominal ); 
			break;
		
		case CMD_KICKER_TOGGLE:
			if ( val == true )
			{
				if ( kicker_percent_target == 0.0 )
				{
					kicker_percent_target = kicker_percent_nominal;
				}
				else
				{
					kicker_percent_target = 0.0;
				}
			}
			break;

		case CMD_FLY_ON:
		{
			if (val == true)
			{
				fly_velocity_target = fly_setpoint[setpoint_index];
				fly_percent_target = fly_percent_nominal;
			} 
		} break;
		
		case CMD_FLY_OFF:
		{
		    if (val == true)
			{
				fly_percent_target = 0.0;
				fly_velocity_target = 0.0;
			}
		} break;
		
		case CMD_FLY_STEP_UP:
		{
			if (val == true)
			{
				if(closed_loop_on)
				{
					fly_velocity_target += fly_velocity_step;
					// if the flywheel was off, this will turn it on to min
					fly_velocity_target = gsu::Filter::limit(fly_velocity_target, fly_velocity_min, fly_velocity_max);
				}
				else
				{
					fly_percent_target += fly_percent_step;
					// if the flywheel was off, this will turn it on to min
					fly_percent_target = gsu::Filter::limit(fly_percent_target, fly_percent_min, fly_percent_max);
				}
			}
		} break;
		
		case CMD_FLY_STEP_DN:
		{
			if (val == true)
			{
				if (closed_loop_on)
				{
					fly_velocity_target -= fly_velocity_step;
					// if too slow, just turn it off
					if (fly_velocity_target < fly_velocity_min)
					{
						fly_velocity_target = 0.0;
					}
				}
				else
				{
					fly_percent_target -= fly_percent_step;
					// if too slow, just turn it off
					if (fly_percent_target < fly_percent_min)
					{
						fly_percent_target = 0.0;
					}
				}
			}
		} break;
				
		case CMD_HOOD_STEP_UP:
		{	
		    if (closed_loop_on)
			{
                if (val == true)
                 {
                    hood_position_target += hood_position_step;
                 }
			}
			else
			{
                if (val == true)
                 {
				    hood_percent_target = -hood_percent_nudge;
                 }
                 else
                 {
    				 hood_percent_target = 0.0;
                 }
			}
		} break;

		case CMD_HOOD_STEP_DN:
		{	
		    if (closed_loop_on)
			{
                if (val == true)
                 {
                    hood_position_target -= hood_position_step;
                 }
			}
			else
			{
                if (val == true)
                 {
				    hood_percent_target = hood_percent_nudge;
                 }
                 else
                 {
    				 hood_percent_target = 0.0;
                 }
			}
		} break;

		case CMD_SETPOINT_0:
			if (val == true)
			{
				applySetPoint(0);
			}
			break;
		
		case CMD_SETPOINT_1:
			if (val == true)
			{
				applySetPoint(1);
			}
			break;
		
		case CMD_SETPOINT_2:
			if (val == true)
			{
				applySetPoint(2);
			} 
			break;
		
		case CMD_SETPOINT_3:
			if (val == true)
			{
				applySetPoint(3);
			} 
			break;

		default:
			// Do Nothing
			break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::subscribeInt, if the XML config
 * specifies an int, the constructor of this object will connect that
 * input to this method.
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the int channel that was subscribed to
 *
 ******************************************************************************/
void FissionShooter::setInt(int id, int val)
{
	switch (id)
	{
		case CMD_DPAD_SETPOINTS:
		{
			if (val == 0)
			{
				applySetPoint(0);
			}
			else if (val == 90)
			{
				applySetPoint(1);
			}
			else if (val == 180)
			{
				applySetPoint(2);
			}
			else if (val == 270)
			{
				applySetPoint(3);
			}
		} break;

		default: // do nothing
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
float FissionShooter::applyMomentary(bool on, float val)
{
	if(on)
	{
		return val;
	}
	else
	{
		return 0.0;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::applySetPoint(int index)
{
	setLimeLightState(false);

	setpoint_index = index;
	fly_velocity_target = fly_setpoint[setpoint_index];
	hood_position_target = hood_setpoint[setpoint_index];
}

/*******************************************************************************	
 *
 ******************************************************************************/
float FissionShooter::getHoodPosition()
{
	return hood_position_actual;
}


/*******************************************************************************	
 *
 ******************************************************************************/
int FissionShooter::getSetPointIndex()
{
	return setpoint_index;
}


/*******************************************************************************	
 *
 ******************************************************************************/
void FissionShooter::setFlywheelPercent(float percent)
{
	fly_percent_target = percent;
}

/*******************************************************************************	
 *
 ******************************************************************************/
float FissionShooter::getFlywheelPercent()
{
	return fly_percent_command;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void FissionShooter::setKickerPercent(float percent)
{
	kicker_percent_target = percent;
}
/*******************************************************************************
 *
 ******************************************************************************/
float FissionShooter::getKickerPercent()
{
	return kicker_percent_command;
}

/*******************************************************************************
 *
 ******************************************************************************/
float FissionShooter::getKickerVelocity(void)
{
	return fly_velocity_actual;
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::disabledInit()
{
    log_teleop->close();
    log_auton->close();

	fly_velocity_target = 0.0;
	fly_velocity_command = 0.0;

	hood_percent_target = 0.0;
	hood_percent_command = 0.0;

	kicker_percent_target = 0.0;
	kicker_percent_command = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::autonomousInit()
{
	Advisory::pinfo("%s autonomousInit", getName().c_str());
    FissionShooter::initLogFile();
	log_active = log_auton;
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::teleopInit()
{
    FissionShooter::initLogFile();
	log_active = log_teleop;
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::testInit()
{
}

/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void FissionShooter::initLogFile(void)
{
	log_active->openSegment();

	log_active->log("%s, %s, %s, %s, %s, %s, %s, ",
                    "current_time",
                    "fly_velocity_actual",
                    "fly_velocity_target", 
                    "fly_velocity_command", 
					"fly_velocity_error",
					"fly_percent_target",
					"fly_percent_command");
	log_active->log("%s, %s, %s, ",
					"kicker_velocity_actual",
                    "kicker_percent_target",
                    "kicker_percent_command");
	log_active->log("%s, %s, %s, %s, %s, ",
					"hood_position_actual",
					"hood_position_target",
					"hood_position_error", 
					"hood_percent_target", 
					"hood_percent_command" );
	log_active->log("%s, %s, ",
					"hood_ls_upper_state",
					"hood_ls_lower_state");
	log_active->log("%s, %s, %s, %s, %s ",
					"ll_actual_area",
					"ll_target_altitude",
					"ll_target_azimuth",
                    "ll_hood_error",
                    "hood_percent_command" );
					
    log_active->log("\n");
    log_active->flush();
} 

/**********************************************************************
 *
 * This method is used to update the log files, called from
 * doPeriodic
 *
 **********************************************************************/
void FissionShooter::updateLogFile(void)
{
	log_active->log("%f, %f, %f, %f, %f, %f, %f, ",
                    gsi::Time::getTime(),
                    fly_velocity_actual,
                    fly_velocity_target, 
                    fly_velocity_command, 
					fly_velocity_error,
					fly_percent_target,
					fly_percent_command);
	log_active->log("%f, %f, %f, ",
					kicker_velocity_actual,
					kicker_percent_target,
                    kicker_percent_command);
	log_active->log("%f, %f, %f, %f, ",
					hood_position_actual,
					hood_position_target,
					hood_position_error, 
					hood_percent_target,
					hood_percent_command);
	log_active->log("%b, %b, ",
					hood_ls_upper_state,
					hood_ls_lower_state);
	log_active->log("%sf %f, %f, %f, %f ",
					ll_actual_area,
					ll_actual_angle_vertical,
					ll_actual_angle_horizontal,
                    ll_hood_error,
                    hood_percent_command );
	
    log_active->log("\n");
    log_active->flush();
} 

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName().c_str() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutBoolean(getName() +" closed loop: ", closed_loop_on);

	SmartDashboard::PutNumber(getName() +" flywheel actual velocity: ", fly_velocity_actual);
	SmartDashboard::PutNumber(getName() +" flywheel target velocity: ", fly_velocity_target);
	SmartDashboard::PutNumber(getName() +" flywheel command velocity: ", fly_velocity_command);
	SmartDashboard::PutNumber(getName() +" flywheel error of velocity: ", fly_velocity_error);
	SmartDashboard::PutNumber(getName() +" flywheel target percent: ", fly_percent_target);
	SmartDashboard::PutNumber(getName() +" flywheel command percent: ", fly_percent_command);
	SmartDashboard::PutNumber(getName() +" setpoint index: ", setpoint_index);
	//SmartDashboard::PutNumber(getName() +" flywheel setpt[0] velocity: ", fly_setpoint[0]);
	//SmartDashboard::PutNumber(getName() +" flywheel setpt[1] velocity: ", fly_setpoint[1]);
	//SmartDashboard::PutNumber(getName() +" flywheel setpt[2] velocity: ", fly_setpoint[2]);
	//SmartDashboard::PutNumber(getName() +" flywheel setpt[3] velocity: ", fly_setpoint[3]);
	//SmartDashboard::PutNumber(getName() +" flywheel velocity step size: ",fly_velocity_step);
	//SmartDashboard::PutNumber(getName() +" flywheel percent step size: ", fly_percent_step);
	
	SmartDashboard::PutNumber(getName() +" kicker_percent_target: ", kicker_percent_target);
	SmartDashboard::PutNumber(getName() +" kicker_percent_command: ", kicker_percent_command);
	SmartDashboard::PutNumber(getName() +" kicker_percent_nominal: ", kicker_percent_nominal);
	SmartDashboard::PutNumber(getName() +" kicker_velocity_actual: ", kicker_velocity_actual);

	SmartDashboard::PutNumber(getName() +" hood_percent_target: ", hood_percent_target);
	SmartDashboard::PutNumber(getName() +" hood_percent_command: ", hood_percent_command);
	SmartDashboard::PutNumber(getName() +" hood_position_actual: ", hood_position_actual);
	SmartDashboard::PutNumber(getName() +" hood_position_target: ", hood_position_target);
	SmartDashboard::PutNumber(getName() +" hood_position_error: ", hood_position_error);
	//SmartDashboard::PutNumber(getName() +" hood_setpoint[0]: ", hood_setpoint[0]);
	//SmartDashboard::PutNumber(getName() +" hood_setpoint[1]: ", hood_setpoint[1]);
	//SmartDashboard::PutNumber(getName() +" hood_setpoint[2]: ", hood_setpoint[2]);
	//SmartDashboard::PutNumber(getName() +" hood_setpoint[3]: ", hood_setpoint[3]);
	//SmartDashboard::PutNumber(getName() +" hood_position_step: ", hood_position_step);
	//SmartDashboard::PutNumber(getName() +" hood_gear_ratio: ", hood_gear_ratio);
	//SmartDashboard::PutNumber(getName() +" hood_position_hi: ", hood_position_stop_hi);
	//SmartDashboard::PutNumber(getName() +" hood_position_lo: ", hood_position_stop_lo);
	SmartDashboard::PutBoolean(getName() +" hood_ls_upper_state: ", hood_ls_upper_state);
	SmartDashboard::PutBoolean(getName() +" hood_ls_lower_state: ", hood_ls_lower_state);

	SmartDashboard::PutBoolean(getName() + " ll_targeting: ", lime_light_is_on);
	SmartDashboard::PutNumber(getName() + " ll_target_area: ", ll_actual_area);
	SmartDashboard::PutNumber(getName() + " ll_actual_angle_vertical: ", ll_actual_angle_vertical);
	SmartDashboard::PutNumber(getName() + " ll_actual_angle_horizontal: ", ll_actual_angle_horizontal);
	SmartDashboard::PutNumber(getName() + " ll_hood_error: ", ll_hood_error);
	SmartDashboard::PutNumber(getName() + " ll_hood_target: ", ll_hood_target);
	SmartDashboard::PutNumber(getName() + " ll_distance: ", ll_mount_planar_offset);
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::setClosedLoop(bool closed)
{
	if (closed_loop_on == closed)
	{
		// Already set, return
		return;
	}

	closed_loop_on = closed;

	if (closed_loop_on)
	{
		if(fly_motor != nullptr)
		{
			fly_motor->setControlMode(Motor::VELOCITY);
		}
		Advisory::pinfo("setting Shooter closed  mode");
	}
	else
	{
		if(fly_motor != nullptr)
		{
			fly_motor->setControlMode(Motor::PERCENT);
		}

		setLimeLightState(false);

		Advisory::pinfo("setting Shooter open loop mode");
	}

	// Kicker motor always runs in percent mode.
	// Set here for complete mode initialization via this function
	if(hood_motor != nullptr)
	{
		hood_motor->setControlMode(Motor::PERCENT);
	}

	if(kicker_motor != nullptr)
	{
		kicker_motor->setControlMode(Motor::PERCENT);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void FissionShooter::setLimeLightState(bool value)
{
	Advisory::pinfo("-------------------------------------- setLimeLightState", getName().c_str());

	if (lime_light_is_on != value)
	{
		if (value == true) 
		{
			if (ll_limelight == nullptr)
			{
				Advisory::pinfo("cannot use %s limelight control, lime light is not connected", getName().c_str());
				lime_light_is_on = false;
			}
			else
			{
				setClosedLoop(true);

				if (isClosedLoop())
				{
					Advisory::pinfo("setting %s limelight control on", getName().c_str());
					lime_light_is_on = true;
				}
				else
				{
					Advisory::pinfo("cannot use %s limelight control, closed loop is not connected", getName().c_str());
					lime_light_is_on = false;
				}
				
			}
		}
		else
		{
			Advisory::pinfo("setting %s limelight control off", getName().c_str());
			lime_light_is_on = false;
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
 bool FissionShooter::getLimeLightState(void)
 {
	 return lime_light_is_on;
 }

/*******************************************************************************
 *
 ******************************************************************************/
bool FissionShooter::isClosedLoop(void)
{
	return closed_loop_on;
}

/*******************************************************************************
 *
 ******************************************************************************/
bool FissionShooter::isAtTarget( float fly_tolerance, float hood_tolerance )
{
	bool at_fly  = fabs( fly_velocity_actual  - fly_velocity_target  ) < fly_tolerance;
	bool at_hood = fabs( hood_position_actual - hood_position_target ) < hood_tolerance;
	return at_fly && at_hood;
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void FissionShooter::doPeriodic()
{
	if(active == false)
	{
		Advisory::pinfo("DoPeriodic is not ready");
		return;
	}

	//
	// Update motors first, some motors need updates to get sensor values
	//
	fly_motor->doUpdate();
	kicker_motor->doUpdate();
	hood_motor->doUpdate();

	//	
	// Get Sensor Inputs
	//
	fly_velocity_actual = fly_motor->getVelocity();
	kicker_velocity_actual = kicker_motor->getVelocity();

	hood_position_actual = hood_encoder->getAbsoluteAngle();

	hood_ls_lower_state = !hood_ls_lower->isPressed(); // TODO fix logic, why coming backwards off the hardware?
	hood_ls_upper_state = !hood_ls_upper->isPressed();
	
	if(ll_limelight != nullptr)
	{
		ll_limelight->getData(ll_is_visible, 
						   ll_actual_angle_horizontal, 
						   ll_actual_angle_vertical,
						   ll_actual_area, 
						   ll_actual_angle_roll);
    }

	//
	// Make sure the motors don't jump when enabled
	//
	if (getPhase() == DISABLED)
	{
		fly_velocity_target = 0.0; 
		fly_velocity_command = 0.0; 

		fly_percent_target = 0.0;
		fly_percent_command = 0.0;

		kicker_percent_target = 0.0;
		kicker_percent_command = 0.0;

		hood_percent_target = 0.0;
		hood_percent_command = 0.0;

        hood_position_target = hood_position_actual;
	}

	//
	// Process Data
	//

	if (ll_is_visible > 0.5)
	{
		//ll_mount_planar_offset = - ll_height_difference/tan(ll_actual_angle_vertical/RAD_TO_DEG);
		//ll_hood_target = ll_target_curves.evaluate(ll_mount_planar_offset);
		ll_hood_error = ll_target_angle_vertical - ll_actual_angle_vertical;
	}

	if (closed_loop_on)
	{
		if (lime_light_is_on && ll_is_visible > 0.5)
		{
			//ll_hood_target = ll_target_curves.evaluate(ll_actual_angle_vertical);
			//ll_hood_error = ll_hood_target - hood_position_actual;
			//hood_percent_command = ll_hood_error*hood_position_kp*-1.0;

			// d = (h2-h1)/tan(a1+a2)
 
// Use the verical angle to target (needs work for variable hood)
			//ll_hood_target = ll_target_curves.evaluate(ll_actual_angle_vertical);
			//Advisory::pinfo("Distance Offset: %.2f", ll_mount_planar_offset);
 
// Use area to calculate hood angle
			//ll_hood_target = ll_target_curves.evaluate(ll_actual_area);
			//Advisory::pinfo("Distance Offset: %.2f", ll_mount_planar_offset);

// Constant limelight angle

			ll_hood_error = ll_target_angle_vertical - ll_actual_angle_vertical;
            hood_percent_command = -hood_pid->calculateCommand(ll_target_angle_vertical, ll_actual_angle_vertical);
			/*
			if (fabs(ll_actual_angle_vertical) > 0.1)
			{
				ll_mount_planar_offset = - ll_height_difference/tan(ll_actual_angle_vertical/RAD_TO_DEG);
				ll_hood_target = ll_target_curves.evaluate(ll_mount_planar_offset);
				ll_hood_error = ll_hood_target - hood_position_actual;
				//hood_motor->setPosition(ll_hood_target);
				hood_percent_command = ll_actual_angle_vertical*hood_position_kp*-1.0;
			}
			else
			{
				ll_mount_planar_offset = 0;
				ll_hood_target = ll_target_curves.evaluate(ll_mount_planar_offset);
				//hood_motor->setPosition(ll_hood_target);		
				hood_percent_command = 0.0;		
			}
*/
		}
        else if (lime_light_is_on)
        {
            hood_percent_command = 0.0;
        }
		else
		{
			// flywheel is closed loop on the motor controller, this error value 
			// is only for information
			fly_velocity_error = fly_velocity_target - fly_velocity_actual;
			
			// because the hood sensor is not connected to the hood motor controller,
			// this code must do the closed loop processing.
			// @TODO: RJP says: this could be changed to a PID instead of just a P to 
			//        get better control, P is easy and works for now
	        hood_position_error = hood_position_target - hood_position_actual;
            hood_percent_command = hood_pid->calculateCommand(hood_position_target, hood_position_actual);
		}	
	}
	else
	{
		if( hood_percent_target != 0.0 ) // assumes joystick deadband is properly set.
		{
			hood_percent_command = hood_percent_target;
			hood_position_target = hood_position_actual;
			hood_position_error  = 0.0;
		}
		else // stationkeeping to hold last commanded orientation. We do this to fight against recoils
		{
			hood_position_error = hood_position_target - hood_position_actual;
			hood_percent_command = -hood_pid->calculateCommand(hood_position_target, hood_position_actual);
		}
		fly_velocity_error = 0.0;
	}

    

    // 
	// Convert targets to commands
	//
	// @TODO: RJP says: motor command values should be ramped, large steps can cause motors to spark, thus reducing the life of the motor
	//        test everything else, then add ramps, then test ramps by setting it to small step changes (like 0.02% per cycle),
	//        then change to appropriate value (like 0.2% per cycle), could also ramp based on time instead of based on cycles
	fly_velocity_command = fly_velocity_target;     // should ramp, 
	fly_percent_command = fly_percent_target;       // should ramp
    kicker_percent_command = kicker_percent_target; // should ramp


    if (hood_ls_lower_state == true)
	{
		hood_percent_command = gsu::Filter::limit(hood_percent_command, -1.0, 0.0);
	}

	if (hood_ls_upper_state == true)
	{
        hood_percent_command = gsu::Filter::limit(hood_percent_command, 0.0, 1.0);
	}

    // Software limit
	if (hood_position_actual < 35.5) hood_percent_command = gsu::Filter::limit(hood_percent_command, -1.0, 0.0);
	if (hood_position_actual > 120.0) hood_percent_command = gsu::Filter::limit(hood_percent_command,  0.0, 1.0);

    // for debugging purposes - slows hood down
	// hood_percent_command = gsu::Filter::limit(hood_percent_command, -0.15, 0.15); 

	// Do not allow kicker to inject ball if flywheel is not running
	if ( closed_loop_on )
	{
		if( fly_velocity_actual < fly_velocity_min )
		{
			kicker_percent_command = gsu::Filter::limit(kicker_percent_command, -1.0, 0.0);
		}
	}
	else
	{
		if( fly_percent_command < fly_percent_min )
		{
			kicker_percent_command = gsu::Filter::limit(kicker_percent_command, -1.0, 0.0);
		}
	}

	//
	//	Set Outputs
	//
	hood_motor->setPercent(hood_percent_command);

	kicker_motor->setPercent(kicker_percent_command);

	if (closed_loop_on)
	{
		fly_motor->setVelocity(fly_velocity_command);
	}
	else
	{
        fly_motor->setPercent(fly_percent_command);
	}
	
	updateLogFile();
}

/* ============================================================================================ */
/* ============================================================================================ */
/* ============================================================================================ */

/*******************************************************************************
 *
 ******************************************************************************/
MSFissionShooter::MSFissionShooter(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	fly_velocity_command = 0.0;
	hood_position_command = 0.0;
	kicker_percent_command = 0.0;
	m_wait = false;
	setpoint_index = 0;
	m_closed_loop = false;
	
	m_parent_control = (FissionShooter *)control;
	xml->QueryFloatAttribute("flywheel_vel", &fly_velocity_command);
	xml->QueryFloatAttribute("hood_pos",     &hood_position_command);
	xml->QueryFloatAttribute("kicker_per", &kicker_percent_command);
	xml->QueryIntAttribute("setpoint", &setpoint_index);
	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);
	xml->QueryBoolAttribute("wait", &m_wait);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSFissionShooter::init(void)
{
	m_parent_control->setClosedLoop(m_closed_loop);
	if (setpoint_index < NUM_SETPOINTS - 1)
	{
		m_parent_control->applySetPoint(setpoint_index );
	}
	else
	{
		m_parent_control->applySetPoint(0);
	}	
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSFissionShooter::update(void)
{
    if(m_wait == true)
	{
	    if(!m_parent_control->isAtTarget(1.0,1.0))
	 	{
			return this;
	 	}
	}

	return next_step;
}

/* ============================================================================================ */
/* ============================================================================================ */
/* ============================================================================================ */

/*******************************************************************************
 *
 ******************************************************************************/
MSFissionFlywheelPower::MSFissionFlywheelPower(std::string type,
    tinyxml2::XMLElement *xml, void *control) 
	: MacroStepSequence(type, xml, control)
{
	fly_percent_command = 0.0;
	m_parent_control = (FissionShooter *)control;
	xml->QueryFloatAttribute("flywheel_percent", &fly_percent_command);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSFissionFlywheelPower::init(void)
{
	m_parent_control->setFlywheelPercent(fly_percent_command);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSFissionFlywheelPower::update(void)
{
	return next_step;
}

/* ============================================================================================ */
/* ============================================================================================ */
/* ============================================================================================ */

/*******************************************************************************
 *
 ******************************************************************************/
MSFissionKickerPower::MSFissionKickerPower(std::string type,
    tinyxml2::XMLElement *xml, void *control) 
	: MacroStepSequence(type, xml, control)
{
	kicker_percent_command = 0.0;
	m_parent_control = (FissionShooter *)control;
	xml->QueryFloatAttribute("kicker_percent", &kicker_percent_command);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSFissionKickerPower::init(void)
{
	m_parent_control->setKickerPercent(kicker_percent_command);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSFissionKickerPower::update(void)
{
	return next_step;
}
