/*******************************************************************************
 *
 * File: RobotMain.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "RobotMain.h"

#include "rfcontrols/ControlFactory.h"
#include <frc/RobotBase.h>

#include "rfcontrols/DriveBase.h"

/******************************************************************************
 *
 * Create an instance ot the Robot.
 * 
 * This constructor Registers the default Chips Robot Controls and allows for
 * additional robot specific controls to be Registered with the Control
 * Factory.
 *
 ******************************************************************************/
RobotMain::RobotMain(void) : ChipsBase()
{
    RegisterControls();

    ControlFactory::registerProxy("DriveBase",   new ControlProxy<DriveBase>());
    //
    // Register any robot specific controls
    //
    // example: ControlFactory::registerProxy("my_control", new ControlProxy<MyControl>());
    //     where: 
    //         my_control is what will appear as the control type in the RobotControl.xml file
    //         MyControl  is the name of the class that implements the Control
    //
}

/******************************************************************************
 *
 *  Release all resources used by this instance
 *
 ******************************************************************************/
RobotMain::~RobotMain()
{
}

/******************************************************************************
 *
 * This main is what makes an instance of this class (RobotMain) run as the
 * robot.
 *
 ******************************************************************************/
int main()
{
    return frc::StartRobot<RobotMain>();
}

