/*******************************************************************************
 *
 * File: SwerveModule.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 * 
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "rfutilities/MacroStepFactory.h"

#include "SwerveModule.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control, feedback, and PID 
 * and connect them to the specified inputs
 * clo
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 ******************************************************************************/
SwerveModule::SwerveModule(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	Advisory::pinfo("========================= Creating Swerve Module [%s] =========================", 
			        control_name.c_str() );

	mode = MODE_FULL;

    m_rotational_motor = nullptr;
	m_rotational_gear_ratio = 1.0;
	m_rotational_resolution = 1;
    m_rotational_velocity_max = 1.0;
    m_rotational_velocity_current = 0.0;
    m_rotational_acceleration_max = 1.0;
    m_rotational_acceleration_current = 0.0;

    m_translational_motor = nullptr;
	m_translational_gear_ratio = 1.0;
    m_translational_velocity_max = 1.0;
    m_translational_velocity_current = 0.0;
    m_translational_velocity_target = 0.0;
    m_translational_velocity_error = 0.0;
    m_translational_acceleration_max = 1.0;
    m_translational_acceleration_current = 0.0;
    m_translational_acceleration_target = 0.0;
    m_translational_acceleration_error = 0.0;
	m_translational_reverse = 1;
    
    absolute_rot_cc = nullptr;
    cc_position_target = 0.0;
    cc_position_current = 0.0;
    cc_position_error = 0.0;
    cc_position_min = -180.0;
    cc_position_max = 179.9999;
    cc_velocity_target = 0.0;
    cc_velocity_current = 0.0;
    cc_velocity_error = 0.0;
    cc_velocity_max = 0.0;

    //
	// Parse the Controls XML
	//
	XMLElement* comp;
	const char* name = nullptr;

	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);

	comp = xml->FirstChildElement("motor");
	while( comp != nullptr )
	{
		name = comp->Attribute("name");
		if( name == nullptr )
		{
			Advisory::pwarning("%s  WARNING: could not read motor name.", control_name.c_str() );
		}
		else if( strcmp(name,"translational") == 0 )
		{
			Advisory::pinfo("  creating drive motor for %s", control_name.c_str() );
            m_translational_motor = HardwareFactory::createMotor(comp);
			comp->QueryFloatAttribute("gear_ratio",&m_translational_gear_ratio);
			comp->QueryFloatAttribute("velocity_max",&m_translational_velocity_max);
			comp->QueryFloatAttribute("acceleration_max",&m_translational_acceleration_max);
		}
		else if( strcmp(name,"rotational") == 0 )
		{
			Advisory::pinfo("  creating rotor motor for %s", control_name.c_str() );
            m_rotational_motor = HardwareFactory::createMotor(comp);
			comp->QueryFloatAttribute("gear_ratio",&m_rotational_gear_ratio);
			comp->QueryFloatAttribute("velocity_max",&m_rotational_velocity_max);
			comp->QueryFloatAttribute("acceleration_max",&m_rotational_acceleration_max);
		}
		else
		{
			Advisory::pwarning("%s  WARNING: motor \"%s\" not understood", control_name.c_str(), name );
		}
		comp = comp->NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("cancoder");
	if (comp != nullptr)
	{
		absolute_rot_cc = HardwareFactory::createCancoder(comp);
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "translational") == 0)
			{
				Advisory::pinfo("  connecting to translational channel");
				OIController::subscribeAnalog(comp, this, CMD_FORWARD);
			}
			else if(strcmp(name, "rotational") == 0 )
			{
				Advisory::pinfo("  connecting to rotational channel");
				OIController::subscribeAnalog(comp, this, CMD_ROTATE);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}

	// Calibration
	if( absolute_rot_cc != nullptr )
	{
		cc_position_current = absolute_rot_cc->getAbsoluteAngle();
	}

	// TODO: This seems suspect that we are interacting with a
	// specific version of a motor here
	Motor_TalonFx* m_fx;
	m_fx = dynamic_cast<Motor_TalonFx*>(m_rotational_motor);
    if( m_fx != nullptr )
    {
        m_rotational_resolution = 2048;
        m_fx->setSensorScale( 
			360.0/m_rotational_resolution/m_rotational_gear_ratio,
			cc_position_current );
    }
	else
	{
		Advisory::pwarning("%s rotational motor WARNING: resolution unset",control_name.c_str());
	}
}

float SwerveModule::getAngle() {

	return (float)absolute_rot_cc->getAngle();
}

float SwerveModule::getRate() {

	return (float)absolute_rot_cc->getRate();
}

float SwerveModule::getAbsoluteAngle() {

	return (float)absolute_rot_cc->getAbsoluteAngle();
}

float SwerveModule::getTranslationMotorPosition()
{
	return (float) m_translational_motor->getRawPosition();
}

float SwerveModule::getTranslationMotorVelocity()
{
	return (float) m_translational_motor->getVelocity();
}

float SwerveModule::getRotationMotorVelocity()
{
	return (float) m_rotational_motor->getVelocity();
}

float SwerveModule::getRotationMotorPosition()
{
	return (float) m_rotational_motor->getPosition();
}

float SwerveModule::getCancoderCurrentPosition()
{
	return cc_position_current;
}

float SwerveModule::getCancoderTargetPosition()
{
	return cc_position_target;
}

float SwerveModule::getCancoderErrorPosition()
{
	return cc_position_error;
}

float SwerveModule::getRotationMotorGearRatio()
{
	return m_rotational_gear_ratio;
}

int SwerveModule::getTranslationMotorReverse()
{
	return m_translational_reverse;
}





/*******************************************************************************	
 *
 * The Destructor!
 * Release any resources used by this object.
 * These comments aren't very professional. 
 * 
 ******************************************************************************/
SwerveModule::~SwerveModule(void)
{
	if (m_rotational_motor != nullptr)
	{
		delete m_rotational_motor;
		m_rotational_motor = nullptr;
	}

	if (m_translational_motor != nullptr)
	{
		delete m_translational_motor;
		m_translational_motor = nullptr;
	}

	if (absolute_rot_cc != nullptr)
	{
		delete absolute_rot_cc;
		absolute_rot_cc = nullptr;
	}
}

/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 * 
 * Handled command ids are CMD_TURN and CMD_FORWARD, all others are ignored
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void SwerveModule::setAnalog(int id, float val)
{
	// Advisory::pinfo("========================= SwerveModule::setAnalog() =========================");

	switch (id)
	{
		case CMD_ROTATE:
			cc_position_target = val;
			break;
		case CMD_FORWARD:
			m_translational_velocity_target = val;
			break;

		default:
			break;
	}
}


/*******************************************************************************
 * This will put numbers and values on the FRC Smart Dashboard
 * Isn't that fun? 
 ******************************************************************************/

void SwerveModule::publish()
{

	SmartDashboard::PutNumber(getName() +" cancoder angle: ",
			                   cc_position_current);
	SmartDashboard::PutNumber(getName() +" rot encoder angle: ",
			                   m_rotational_motor->getPosition());
	SmartDashboard::PutNumber(getName() +" rot angle target: ",
			                   cc_position_target);
	SmartDashboard::PutNumber(getName() +" rot angle error: ",
			               	   cc_position_error);
	SmartDashboard::PutNumber(getName() +" rot motor velocity: ",
			                   m_rotational_motor->getVelocity());
	SmartDashboard::PutNumber(getName() +" trans gear_ratio: ",
							   m_rotational_gear_ratio);
	SmartDashboard::PutNumber(getName() +" trans reversed: ",
							   m_translational_reverse);
	SmartDashboard::PutNumber(getName() +" trans position: ",
							   m_translational_motor->getRawPosition());
	SmartDashboard::PutNumber(getName() +" trans velocity: ",
							   m_translational_motor->getVelocity());
}

void SwerveModule::controlInit(void){}
void SwerveModule::disabledInit(){}
void SwerveModule::autonomousInit(){}
void SwerveModule::teleopInit(){}
void SwerveModule::testInit(){}
void SwerveModule::doPeriodic()
{
	if(MODE_FULL) // If slave, master must call doControl() manually
	{
		doControl();
	}
	publish();
}
void SwerveModule::doControl()
{
	// 1) read encoder data for both motor and external encoder
	if( absolute_rot_cc != nullptr )
	{
		cc_position_current = absolute_rot_cc->getAbsoluteAngle();
	}

	// 2) Read commanded orientation (speed/rotation)
	m_translational_reverse = 1;
	cc_position_error = cc_position_target - cc_position_current;
	if( cc_position_error > 180.0 )
	{
		cc_position_error -= 360.0;
	}
	else if( cc_position_error < -180.0 )
	{
		cc_position_error += 360.0;
	}
	if( cc_position_error == -180.0 || cc_position_error == 180.0 )
	{
		cc_position_error = 0.0;
		m_translational_reverse = -1;
	}

	// 3) Figure shortest path between commanded orientation and current
	if( cc_position_error > 90.0 ) // Reverse speed, turn left to go right
	{
		cc_position_error -= 180.0;
		m_translational_reverse = -1;
	}
	else if( cc_position_error < -90.0 ) // Reverse spd, turn right to go left
	{
		cc_position_error += 180.0;
		m_translational_reverse = -1;
	}

	// 4) Command rotation motor
	if( m_rotational_motor != nullptr )
	{
		m_rotational_position_error = cc_position_error;
		m_rotational_position_current = m_rotational_motor->getPosition();
		m_rotational_position_target = m_rotational_position_current
				                     + m_rotational_position_error;
		m_rotational_motor->setPosition(m_rotational_position_target);
		m_rotational_motor->doUpdate();
	}

	// 5) Command translation motor
	// TODO: Condition drive motor to modify speed command based on error
	//       Linear scale, hacky C++ bitswitching exponential quickmath?
	m_translational_velocity_target *= m_translational_reverse;
	if( m_translational_motor != nullptr)
	{
		m_translational_motor->setPercent(m_translational_velocity_target);
		m_translational_motor->doUpdate();
	}
}


void SwerveModule::setDigital(int id, bool val){}
void SwerveModule::setInt(int id, int val){}
