/*******************************************************************************
 *
 * File: Hang.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "Hang.h"
#include "rfutilities/MacroStepFactory.h"
#include <math.h>
#include "rfutilities/DataLogger.h"
#include "frc/smartdashboard/SmartDashboard.h" //WPI
#include "rfcontrols/SolenoidControl.h"

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
Hang::Hang(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
	, ENCODER_TO_IN ((2048 * 12) / 2.314)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Hang Control [%s] =========================",
	           control_name.c_str());

	// Be sure to initialize all method variables
	motor1 = nullptr;

	hang_log = nullptr;
    is_ready = true;

	motor_min_control = -1.0;
	motor_max_control = 1.0;

	lock_solenoid = nullptr;
	lock_state = LOCK_ENGAGED;
	pivot_solenoid = nullptr;
	pivot_state = PIVOT_BACKWARD;

	motor_up_power = 0.0;
	motor_down_power = 0.0;

    motor_max_current = 10.0;

	motor_max_cmd_delta = 0.25;

	hang_target_power = 0.0;
	hang_command_power = 0.0;

	is_closed_loop = false;
	hang_actual_position = 0.0;
	hang_position_inches = 0.0;
	hang_height_offset = 0.0;

	min_position = 0.0;
	max_position = 0.0;
	hang_target_position = 0.0;
 
	for (uint8_t i = 0; i < NUM_SETPOINTS; i++)
	{

		setpoint_name [i] = "unknown";
		setpoint_position [i] = 0.0;
		setpoint_defined [i] = false;

	}

	const char *name = nullptr;

	hang_log = new DataLogger("/robot/logs/hang", "hang", "csv", 10, true);

	//
	// Register Macro Steps
	//
	//new MacroStepProxy<MSIntakeRollerPower>(control_name, "MSSomeMacroSTep1", this);
	//new MacroStepProxy<MSIntakeDeploy>(control_name, "MSHangMacroSTep2", this);

	//
	// Parse XML
	//
	
    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &motor_max_cmd_delta);
	xml -> QueryUnsignedAttribute("max_current", &motor_max_current);

	xml->QueryFloatAttribute("min_position", &min_position);
	xml->QueryFloatAttribute("max_position", &max_position);
	xml->QueryFloatAttribute("height_offset", &hang_height_offset);

	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp!=nullptr)
		{
			int setpoint_index = -1;
			setpoint_comp->QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < NUM_SETPOINTS)
			{
				name = setpoint_comp->Attribute("name");
				if (name != nullptr)
				{
					setpoint_name[setpoint_index] = std::string(name);
				}
				else
				{
					setpoint_name[setpoint_index] = std::string("setpoint_") + std::to_string(setpoint_index);
				}

				setpoint_comp->QueryFloatAttribute("position", &setpoint_position[setpoint_index]);

				setpoint_defined[setpoint_index] = true;

				Advisory::pinfo(" -- setpoint %2d: %20s   position=%7.2f",
					setpoint_index, setpoint_name[setpoint_index].c_str(),
					setpoint_position[setpoint_index]);
			}
			else
			{
				Advisory::pinfo("setpoint with index out of range -- %d", setpoint_index);
			}

    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}
	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "motor_1") == 0)
			{
				Advisory::pinfo("  creating speed controller for motor_1");
				motor1 = HardwareFactory::createMotor(comp);
				motor1->setSensorScale(ENCODER_TO_IN, hang_height_offset); // converts from encoder units to inches
				comp->QueryUnsignedAttribute("max_current", &motor_max_current);
				Advisory::pinfo("	max_current=%u", motor_max_current); 
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
	
	comp = xml->FirstChildElement("solenoid");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "lock") == 0)
			{
				Advisory::pinfo("  creating lock solenoid for %s", name);
				lock_solenoid = HardwareFactory::createSolenoid(comp);
			}
			else if (strcmp(name, "pivot") == 0)
			{
				Advisory::pinfo("  creating pivot solenoid for %s", name);
				pivot_solenoid = HardwareFactory::createSolenoid(comp);
			}
			else
			{
				Advisory::pwarning("  found solenoid with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found solenoid with no name attribute");
		}
		comp = comp->NextSiblingElement("solenoid");
	}
//                OIController::subscribeDigital(comp, this, CMD_BRAKE_RELEASED);

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
		 	if (strcmp(name, "up") == 0)
			{
				Advisory::pinfo("  connecting up channel");
				comp->QueryFloatAttribute("value", &motor_up_power);
				OIController::subscribeDigital(comp, this, CMD_HANG_UP);
			}
			else if(strcmp(name, "down") == 0)
			{
				Advisory::pinfo("  connecting down channel");
				comp->QueryFloatAttribute("value", &motor_down_power);
				OIController::subscribeDigital(comp, this, CMD_HANG_DOWN);
			}
			else if (strcmp(name, "stop") == 0)
			{
				Advisory::pinfo("  connecting stop channel");
				OIController::subscribeDigital(comp, this, CMD_STOP);
			}
			else if ((strncmp(name, "setpoint", 8) == 0)
				&& (name[8] >= '0') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8] - '0'));
			}
			else if (strcmp(name, "closed_loop_state") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if (strcmp(name, "lock_engage") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_LOCK_ENGAGE);
			}
			else if (strcmp(name, "lock_release") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_LOCK_RELEASE);
			}
			else if (strcmp(name, "pivot_forward") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_PIVOT_FORWARD);
			}
			else if (strcmp(name, "pivot_backward") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_PIVOT_BACKWARD);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
	}
}
/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
Hang::~Hang(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	if (motor1 != nullptr)
	{
		delete motor1;
		motor1 = nullptr;
	}
}
/********************************************************************************
 * 
 *******************************************************************************/
bool Hang::isClosedLoop(void)
{
	return is_closed_loop;
}
/*******************************************************************************
 * 
 *******************************************************************************/
void Hang::applySetpoint(bool on, int idx)
{
	if (on && (idx >= 0) && (idx < NUM_SETPOINTS))
	{
		if (setpoint_defined[idx] == true)
		{
			lock_state = LOCK_RELEASED; // jly: do we need to lock afterwards? i'm not sure
			setClosedLoop(true);
			hang_target_position = setpoint_position[idx];
			Advisory::pinfo("%s applySetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, hang_target_position);
			
		}
		else
		{
			Advisory::pinfo("%s applySetpoint: rejected, setpoint not defined for index %d", getName().c_str(), idx);
		}
	}
}
/*******************************************************************************
 *
 ******************************************************************************/
void Hang::controlInit(void)
{ 
	
	if (motor1 == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- motor 1", getName().c_str());
		is_ready = false;
	}
	else
	{
		if (motor_max_current < 100.0) 
		{
			motor1->setCurrentLimit(motor_max_current);
        	motor1->setCurrentLimitEnabled(true);
		}
	}
	if (lock_solenoid == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- lock solenoid", getName().c_str());
		is_ready = false;
	}
	if (pivot_solenoid == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- pivot solenoid", getName().c_str());
		is_ready = false;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Hang::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Hang::disabledInit(void)
{
    hang_log->close();
	hang_target_power = 0.0;
	hang_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Hang::autonomousInit(void)
{
	initLogFile();
	hang_target_power = 0.0;
	hang_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Hang::teleopInit(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	hang_target_power = 0.0;
	hang_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void Hang::testInit(void)
{
}

/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void Hang::initLogFile(void)
{
	hang_log->openSegment();

	hang_log->log("%s, %s, %s, %s, %s, ",
                    "current_time",
                    "hang_target_position",
					"hang_actual_position",
 					"hang_target_power",
					"hang_command_power");
    hang_log->log("\n");
    hang_log->flush();
}

/**********************************************************************
 *
 * This method is used to update the log files, called from
 * doPeriodic
 *
 **********************************************************************/
void Hang::updateLogFile(void)
{
	hang_log->log("%f, %f, %f, %f, %f, ",
                    gsi::Time::getTime(),
                    hang_target_position,
					hang_actual_position,
                    hang_target_power, 
					hang_command_power);
    hang_log->log("\n");
    hang_log->flush();	
} 
/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Hang::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/

void Hang::setDigital(int id, bool val)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_CLOSED_LOOP_STATE:
		{
			setClosedLoop(!isClosedLoop()); // toggle
		} break;
			
		case CMD_HANG_UP: 
		{
			if (val)
			{
				hang_target_power = RobotUtil::limit(motor_min_control, motor_max_control,motor_up_power);
				setClosedLoop(false);
			}
			else
			{
				hang_target_power = 0.0;
			}
		} break;

		case CMD_HANG_DOWN:
		{
			if(val)
			{
				hang_target_power = RobotUtil::limit(motor_min_control, motor_max_control,motor_down_power);
				setClosedLoop(false);
			}
			else
			{
				hang_target_power = 0.0;
				
			}
		} break;

		case CMD_LOCK_ENGAGE:
		{
			if (val) lock_state = LOCK_ENGAGED;
		} break;

		case CMD_LOCK_RELEASE:
		{
			if (val) lock_state = LOCK_RELEASED;
		} break;

		case CMD_PIVOT_FORWARD:
		{
			if (val) pivot_state = PIVOT_FORWARD;
		} break;

		case CMD_PIVOT_BACKWARD:
		{
			if (val) pivot_state = PIVOT_BACKWARD;
		} break;

		case CMD_SETPOINT_0: 	applySetpoint(val, 0);  break;
		
		case CMD_SETPOINT_1: 	applySetpoint(val, 1);  break;
		
		case CMD_SETPOINT_2: 	applySetpoint(val, 2);  break;
		
		case CMD_SETPOINT_3: 	applySetpoint(val, 3);  break;

		case CMD_SETPOINT_4: 	applySetpoint(val, 4);  break;
		
		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Hang::setInt(int id, int val)
{
}

void Hang::setPosition(float position)
{
	hang_target_position = position;
}
float Hang::getPosition()
{
	return hang_position_inches;
}
/*******************************************************************************
 * 
 ******************************************************************************/
void Hang::setClosedLoop(bool closed)
{
	if (is_closed_loop != closed)
	{
		is_closed_loop = closed;

		if (is_closed_loop)
		{
		    motor1->setControlMode(Motor::POSITION);
		    Advisory::pinfo("setting %s closed loop mode", getName().c_str());
		}
		else
		{
         	motor1->setControlMode(Motor::PERCENT); 
           	Advisory::pinfo("setting %s open loop mode", getName().c_str());
		}
	}
}
/*******************************************************************************	
 *
 ******************************************************************************/
void Hang::publish()
{
	// I left a lot of the old stuff. Change and get rid of as needed
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());
	SmartDashboard::PutBoolean(getName() + " Closed Loop: ", is_closed_loop);
	SmartDashboard::PutNumber(getName() + " Hang Position Target (in): ", hang_target_position);
	SmartDashboard::PutNumber(getName() + " Hang Position (Encoder): ", hang_actual_position);
	SmartDashboard::PutNumber(getName() + " Hang Position Actual (in): ", hang_position_inches);

	SmartDashboard::PutNumber(getName() + " Hang Target Power: ", hang_target_power);
	SmartDashboard::PutNumber(getName() + " Hang command Power: ", hang_command_power);	
	SmartDashboard::PutBoolean(getName() + " Hang Locked: ", lock_state);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Hang::doPeriodic()
{
	if (is_ready == false)
	{
		Advisory::pinfo("%s hang is not ready", getName().c_str());
		return;
	}

	motor1->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	hang_position_inches = motor1->getPosition();

	//
	// All processing happens in the motor class :)
	//
	if (getPhase() == DISABLED)
	{
		lock_state = LOCK_ENGAGED;
		pivot_state = PIVOT_BACKWARD;
	}
	//
	// Set Outputs
	//

	if (is_closed_loop)// Command the position
	{
		motor1->setPosition(hang_target_position);
	}
	else
	{
		motor1->setPercent(hang_target_power);
	}

	lock_solenoid->Set(lock_state);
	pivot_solenoid->Set(pivot_state);

	updateLogFile();
}

/*******************************************************************************
 *
 *****************************************************************************
MSSomeMacroSTep1::MSSomeMacroSTep1(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	deploy_power = 0.3;
	deploy_duration = 0.25;

	m_parent_control = (Hang *)control;

	xml->QueryFloatAttribute("power", &deploy_power);
	xml->QueryDoubleAttribute("duration", &deploy_duration);

	if (deploy_duration > 2.0)
	{
		deploy_duration = 0.3;
	}
}


void MSSomeMacroSTep1::init(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	deploy_end_time = gsi::Time::getTime() + deploy_duration;
	m_parent_control->setDeployPower(deploy_power);
}


MacroStep * MSSomeMacroSTep1::update(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	if (gsi::Time::getTime() < deploy_end_time)
	{
		return this;
	}

	m_parent_control->setDeployPower(0.0);
	return next_step;
}

MSHangMacroSTep2::MSHangMacroSTep2(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	m_intake_roller_power = 0.0;
	m_parent_control = (Hang *)control;

	xml->QueryFloatAttribute("power", &m_intake_roller_power);
}

void MSHangMacroSTep2::init(void)
{
	//m_parent_control->setRollerPower(m_intake_roller_power);
}

MacroStep * MSHangMacroSTep2::update(void)
{
	return next_step;
}
*/
