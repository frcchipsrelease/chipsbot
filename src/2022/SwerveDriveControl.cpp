/*******************************************************************************
 *
 * File: SwerveDriveControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

// system
#include <math.h>
#include <algorithm>
#include <cstring>
#include <cstdio>

#include "SwerveDriveControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/RobotUtil.h"

#include "gsutilities/Advisory.h"

#include "gsinterfaces/Time.h"

#include "frc/smartDashboard/SmartDashboard.h" //WPI



using namespace tinyxml2;
using namespace frc;

// TODO: Replace these with gsu::Angle::toRadians() and gsu::Angle::toDegrees()
static constexpr float rad_to_deg = 180.0 / M_PI;
static constexpr float deg_to_rad = M_PI  / 180.0;

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 * *****************************************************************************/

/**
 * Constructor
 */
SwerveDriveScheme::SwerveDriveScheme()
: name()
, type(AUTO_OFF_JUKE)
, pos_wrt_global{0.0,0.0,0.0}
, vel_wrt_global{0.0,0.0,0.0}
, acc_wrt_global{0.0,0.0,0.0}
, T_wrt_global{{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}}
, Q_wrt_global{0.0,0.0,0.0,0.0}
, omg_wrt_local{0.0,0.0,0.0}
, alpha_wrt_local{0.0,0.0,0.0}
{
}

SwerveDriveScheme::SwerveDriveScheme(std::string target_name, tinyxml2::XMLElement* xml)
: SwerveDriveScheme()
{
    name = target_name;
    
    XMLElement* comp;
    const char* value_string;
    
    value_string = xml->Attribute("type");
    if( strcmp(value_string, "juke") == 0 )
    {	
        type = AUTO_OFF_JUKE;
        Advisory::pcaution("SwerveDriveScheme %s of type AUTO_OFF_JUKE, which is already included by default!\n"
                           "\tAdding potential duplicate",
                           name.c_str() );
    }
    if( strcmp(value_string, "tokyo") == 0 )
    {
        type = AUTO_OFF_TOKYO;
        Advisory::pcaution("SwerveDriveScheme %s of type AUTO_OFF_TOKYO, which is already included by default!\n"
                           "\tAdding potential duplicate",
                           name.c_str() );
    }
    if( strcmp(value_string, "global_juke") == 0 )
    {	
        type = AUTO_GLOBAL_JUKE;
        Advisory::pcaution("SwerveDriveScheme %s of type AUTO_GLOBAL_JUKE, which is already included by default!\n"
                           "\tAdding potential duplicate",
                           name.c_str() );
    }
    if( strcmp(value_string, "rotate_wrt_static") == 0 )
    {	
        type = AUTO_ROTATE_STATIC;
    }
    if( strcmp(value_string, "rotate_wrt_mobile") == 0 )
    {	
        type = AUTO_ROTATE_MOBILE;
    }
    if( strcmp(value_string, "sensor") == 0 )
    {
        type = AUTO_ROTATE_SENSOR;
    }
    if( strcmp(value_string, "orbit_static") == 0 )
    {
        type = AUTO_ORBIT_STATIC;
    }
    if( strcmp(value_string, "orbit_mobile") == 0 )
    {
        type = AUTO_ORBIT_MOBILE;
    }

    comp = xml-> FirstChildElement("position");
    if(comp != nullptr )
    {
        value_string = comp->Attribute("value");
        sscanf(value_string, "%f %f %f", &pos_wrt_global[0], &pos_wrt_global[1], &pos_wrt_global[2] );
    }
    comp = xml-> FirstChildElement("velocity");
    if(comp != nullptr )
    {
        value_string = comp->Attribute("value");
        sscanf(value_string, "%f %f %f", &vel_wrt_global[0], &vel_wrt_global[1], &vel_wrt_global[2] );
    }
    comp = xml-> FirstChildElement("acceleration");
    if(comp != nullptr )
    {
        value_string = comp->Attribute("value");
        sscanf(value_string, "%f %f %f", &acc_wrt_global[0], &acc_wrt_global[1], &acc_wrt_global[2] );
    }
    comp = xml-> FirstChildElement("rot_matrix");
    if(comp != nullptr )
    {
        value_string = comp->Attribute("value");
        sscanf(value_string, "%f %f %f "
                             "%f %f %f "
                             "%f %f %f ", 
                &T_wrt_global[0][0], &T_wrt_global[0][1], &T_wrt_global[0][2],
                &T_wrt_global[1][0], &T_wrt_global[1][1], &T_wrt_global[1][2],
                &T_wrt_global[2][0], &T_wrt_global[2][1], &T_wrt_global[2][2] );
    }
    comp = xml-> FirstChildElement("quaternion");
    if(comp != nullptr )
    {
        value_string = comp->Attribute("value");
        sscanf(value_string, "%f %f %f %f", &Q_wrt_global[0], &Q_wrt_global[1], &Q_wrt_global[2], &Q_wrt_global[3] );
    }
        
}

SwerveDriveControl::SwerveDriveControl(std::string control_name, XMLElement* xml)
:	PeriodicControl(control_name)
{
    Advisory::pinfo("========================= Creating Swerve Drive Control [%s] =========================",
                control_name.c_str());


    // Defaults --------------------------------------------------------------

    m_data_logger = new DataLogger("/robot/logs", "drive", "csv", 50, true);

    front_right = nullptr;
    front_left  = nullptr;
    back_left   = nullptr;
    back_right  = nullptr;

    brake_engaged = true;
    speed_high = true;

    // By default, always include {basic juke, field relative juke, basic tokyo}
    drive_scheme.emplace_back();
    drive_scheme[0].name = "Robot Relative JUKE";
    drive_scheme.emplace_back();
    drive_scheme[1].name = "Field Relative JUKE";
    drive_scheme[1].type = SwerveDriveScheme::AUTO_GLOBAL_JUKE;
    drive_scheme.emplace_back();
    drive_scheme[2].name = "Robot Relative TOKYO";
    drive_scheme[2].type = SwerveDriveScheme::AUTO_OFF_TOKYO;
    // TODO: move to input file
    drive_scheme.emplace_back();
    drive_scheme[3].name = "Limelight";
    drive_scheme[3].type = SwerveDriveScheme::AUTO_ROTATE_SENSOR;

    drive_scheme_current = &(drive_scheme[1]);
    drive_scheme_index = 1;

    fwd_cmd = 0.0;
    lat_cmd = 0.0;
    rot_cmd = 0.0;

    fwd_drive = 0.0;
    lat_drive = 0.0;
    rot_drive = 0.0;
    
    low_power_scale = 0.5;

    // Must be set in input file, default is failure
    distance_front_to_back = -1.0;
    distance_left_to_right = -1.0;
    distance_robot_radius = -1.0;

    m_accelerometer = nullptr;
    m_accelerometer_x = 0.0;
    m_accelerometer_y = 0.0;
    m_accelerometer_z = 0.0;

    gyro = nullptr;
    gyro_angle_current = 0.0;
    gyro_angle_target  = 0.0;
    gyro_angle_error   = 0.0;
    gyro_rate_current  = 0.0;
    gyro_rate_target   = 0.0;
    gyro_rate_error    = 0.0;

    ll_limelight = nullptr;
    ll_target_is_visible       = 0.0;
    ll_target_angle_horizontal = 0.0;
    ll_target_angle_vertical   = 0.0;
    ll_target_area             = 0.0;
    ll_target_angle_roll       = 0.0;
    ll_target_hz_offset        = 0.0;

    m_in_auton_mode = false;

    // Begin construction -------------------------------------------------------
    XMLElement *comp;
    const char *name;
    int status;

    name = xml->Attribute("name");
    if (name == nullptr)
    {
        name="drive";
        Advisory::pcaution(
            "WARNING: SwerveDrive created without name, assuming '%s'", name);
    }

    //
    // Register Macro Steps
    //
    new MacroStepProxy<MSSwerveDriveDrivePower>(control_name, "SwerveDrivePower", this);
    new MacroStepProxy<MSSwerveDriveSetScheme>(control_name, "SetDriveScheme", this);
    new MacroStepProxy<MSSwerveDriveRotateAngle>(control_name, "RotateAngle", this);
    

    //
    // Parse the XML
    //

    comp = xml-> FirstChildElement("SwerveModule");
    while (comp != nullptr)
    {
        name = comp->Attribute("name");
        if (name != nullptr)
        {
            Advisory::pinfo("  creating swerve module for %s", name);
            if (strcmp(name, "front_left") == 0)
            {				
                front_left = new SwerveModule(name,comp);
                front_left->mode = SwerveModule::MODE_SLAVE;
            }
            else if (strcmp(name, "front_right") == 0)
            {
                front_right = new SwerveModule(name,comp);
                front_right->mode = SwerveModule::MODE_SLAVE;
            }
            else if (strcmp(name, "back_left") == 0)
            {
                back_left = new SwerveModule(name,comp);
                back_left->mode = SwerveModule::MODE_SLAVE;
            }
            else if (strcmp(name, "back_right") == 0)
            {
                back_right = new SwerveModule(name,comp);
                back_right->mode = SwerveModule::MODE_SLAVE;
            }
            else
            {
                Advisory::pwarning(
                    "  unrecognized SwerveModule detected: '%s'", name);
            }
        }
        comp = comp->NextSiblingElement("SwerveModule");
    }

    status = xml->QueryFloatAttribute("drive_length", &distance_front_to_back);
    if( status == 0 )
    {
        Advisory::pinfo("  Drive base length = %f", distance_front_to_back);
    }
    status = xml->QueryFloatAttribute("drive_width", &distance_left_to_right);
    if( status == 0 )
    {
        Advisory::pinfo("  Drive base width  = %f", distance_left_to_right);
    }
    status = xml->QueryFloatAttribute("drive_radius", &distance_robot_radius);
    if( status == 0 )
    {
        Advisory::pinfo("  Drive base radius  = %f", distance_robot_radius);
    }

    if( distance_robot_radius > 0 )
    {
        if( distance_left_to_right > 0 || distance_front_to_back > 0 )
        {
            Advisory::pinfo("  Redundant base size spec: "
                "use radius or width/length, not both.");

        }
        distance_left_to_right = distance_robot_radius / std::sqrt(2);
        distance_front_to_back = distance_left_to_right;
    }
    else if( distance_left_to_right < 0 )
    {
        Advisory::pfatal("**Missing base width spec!");
    }
    else if( distance_front_to_back < 0 )
    {
        Advisory::pfatal("**Missing base length spec!");
    }
    else
    {
        distance_robot_radius = std::sqrt(  distance_left_to_right*distance_left_to_right
                                          + distance_front_to_back*distance_front_to_back );
    }

    /*
    comp = xml->FirstChildElement("accelerometer");
    if( comp != nullptr )
    {
        Advisory::pinfo("  creating accelerometer");
        m_accelerometer = new BuiltInAccelerometer();
    }
    */
    /*
    comp = xml->FirstChildElement("imu");
    if (comp != nullptr)
    {
        // if type ADIS16448 then
        Advisory::pinfo("  creating imu");
        m_imu = new ADIS16448_IMU();
    }
    */

    comp = xml->FirstChildElement("gyro");
    if (comp != nullptr)
    {
        // if type ADIS16448 then
        Advisory::pinfo("  creating gyro");
        gyro = HardwareFactory::createGyro(comp);
        Advisory::pinfo("  created gyro");
    }

    comp = xml-> FirstChildElement("limelight");
    if (comp != nullptr)
    {
        ll_limelight = HardwareFactory::createLimelight(comp);
        comp->QueryDoubleAttribute("target_hz_offset", &ll_target_hz_offset);
        Advisory::pinfo("  Target LL HZ angle for aiming: %d", ll_target_hz_offset);
    }

    xml->QueryFloatAttribute("low_power_scale", &low_power_scale);
    comp = xml-> FirstChildElement("oi");
    while (comp != nullptr)
    {
        name = comp->Attribute("name");
        if (name != nullptr)
        {
            if (strcmp(name, "forward") == 0)
            {
                Advisory::pinfo("  connecting to forward channel");
                OIController::subscribeAnalog(comp, this, CMD_FORWARD);
            }
            else if (strcmp(name, "lateral") == 0)
            {
                Advisory::pinfo("  connecting to lateral channel");
                OIController::subscribeAnalog(comp, this, CMD_LATERAL);
            }
            else if (strcmp(name, "rotate") == 0)
            {
                Advisory::pinfo("  connecting to rotate channel");
                OIController::subscribeAnalog(comp, this, CMD_ROTATE);
            }
            else if (strcmp(name, "rotate_step_left") == 0)
            {
                Advisory::pinfo("  connecting to rotate step left channel");
                OIController::subscribeDigital(comp, this, CMD_ROT_STEP_LEFT);
            }
            else if (strcmp(name, "rotate_step_right") == 0)
            {
                Advisory::pinfo("  connecting to rotate step right channel");
                OIController::subscribeDigital(comp, this, CMD_ROT_STEP_RIGHT);
            }
            else if (strcmp(name, "brake_on") == 0)
            {
                Advisory::pinfo("  connecting to brake_on channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_ON);
            }
            else if (strcmp(name, "brake_off") == 0)
            {
                Advisory::pinfo("  connecting to brake_off channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_OFF);
            }
            else if (strcmp(name, "brake_toggle") == 0)
            {
                Advisory::pinfo("  connecting to brake_toggle channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_TOGGLE);
            }
            else if (strcmp(name, "brake_state") == 0)
            {
                Advisory::pinfo("  connecting to brake_state momentary channel");
                OIController::subscribeDigital(comp, this, CMD_BRAKE_STATE);
            }
            else if (strcmp(name, "lo_speed") == 0)
            {
                Advisory::pinfo("  connecting to lo_speed ON channel");
                OIController::subscribeDigital(comp, this, CMD_SPEED_LO);
            }
            else if (strcmp(name, "hi_speed") == 0)
            {
                Advisory::pinfo("  connecting to lo_speed OFF channel");
                OIController::subscribeDigital(comp, this, CMD_SPEED_HI);
            }
            else if (strcmp(name, "speed_toggle") == 0)
            {
                Advisory::pinfo("  connecting to speed toggle channel");
                OIController::subscribeDigital(comp, this, CMD_SPEED_TOGGLE);
            }
            else if (strcmp(name, "speed_state") == 0)
            {
                Advisory::pinfo("  connecting to speed momentary channel");
                OIController::subscribeDigital(comp, this, CMD_SPEED_STATE);
            }
            else if (strcmp(name, "control_scheme_next") == 0)
            {
                Advisory::pinfo("  connecting to control_scheme_next channel");
                OIController::subscribeDigital(comp, this, CMD_SCHEME_INCR);
            }            
            else if (strcmp(name, "control_scheme_prev") == 0)
            {
                Advisory::pinfo("  connecting to control_scheme_previous channel");
                OIController::subscribeDigital(comp, this, CMD_SCHEME_DECR);
            }
            else if (strcmp(name, "control_scheme_keypad") == 0)
            {
                Advisory::pinfo("  connecting to control_scheme_keypad selection channel");
                OIController::subscribeInt(comp, this, CMD_SCHEME_INT);
            }
            else if (strcmp(name, "control_scheme_juke") == 0)
            {
                Advisory::pinfo("  connecting to control_scheme JUKE channel");
                OIController::subscribeDigital(comp, this, CMD_JUKE);
            }  
            else if (strcmp(name, "control_scheme_tokyo") == 0)
            {
                Advisory::pinfo("  connecting to control_scheme TOKYO channel");
                OIController::subscribeDigital(comp, this, CMD_TOKYO);
            }  
            else if (strcmp(name, "control_scheme_global_juke") == 0)
            {
                Advisory::pinfo("  connecting to control_scheme field-based JUKE channel");
                OIController::subscribeDigital(comp, this, CMD_GLOBAL_JUKE);
            }  
            else if (strcmp(name, "control_scheme_3") == 0)
            {
                Advisory::pinfo("  connecting to control scheme 3 channel");
                OIController::subscribeDigital(comp, this, CMD_SCHEME_3);
            }
            else if (strcmp(name, "control_scheme_4") == 0)
            {
                Advisory::pinfo("  connecting to control scheme 4 channel");
                OIController::subscribeDigital(comp, this, CMD_SCHEME_4);
            }
            else if (strcmp(name, "control_scheme_5") == 0)
            {
                Advisory::pinfo("  connecting to control scheme 5 channel");
                OIController::subscribeDigital(comp, this, CMD_SCHEME_5);
            }
            else if (strcmp(name, "control_scheme_6") == 0)
            {
                Advisory::pinfo("  connecting to control scheme 6 channel");
                OIController::subscribeDigital(comp, this, CMD_SCHEME_6);
            }
            else if (strcmp(name, "ll_control_hold") == 0)
            {
                Advisory::pinfo("  connecting to ll control rotate channel");
                OIController::subscribeDigital(comp, this, CMD_LL_HOLD);
            }
            else if( strcmp(name, "gyro_calibrate") == 0)
            {
                Advisory::pinfo("  connecting to gyro calibrate channel");
                OIController::subscribeDigital(comp,this,CMD_GYRO_CALIBRATE);
            }
            else if( strcmp(name, "module_calibrate") == 0)
            {
                Advisory::pinfo("  connecting to swerve module calibrate channel");
                OIController::subscribeDigital(comp,this,CMD_MODULE_CALIBRATE);
            }
			else if (strcmp(name, "dpad_setpoints") == 0)
			{
				Advisory::pinfo("connecting dpad");
				OIController::subscribeInt(comp, this, CMD_DPAD_SETPOINTS);
			}
            else 
            {
                Advisory::pwarning("  unrecognized oi detected: \"%s\"", name);
            }
        }
        comp = comp->NextSiblingElement("oi");
    }
}

SwerveDriveControl::~SwerveDriveControl(void)
{
    if( front_right != nullptr )
    {
        delete front_right;
        front_right = nullptr;
    }
    if( front_left != nullptr )
    {
        delete front_left;
        front_left = nullptr;
    }
    if( back_left != nullptr )
    {
        delete back_left;
        back_left = nullptr;
    }
    if( back_right != nullptr )
    {
        delete back_right;
        back_right = nullptr;
    }
    if( m_accelerometer != nullptr )
    {
        delete m_accelerometer;
        m_accelerometer = nullptr;
    }
    /*
    if( m_imu != nullptr )
    {
        delete m_imu;
        m_imu = nullptr;
    }
    */
    if( ll_limelight != nullptr)
    {
        delete ll_limelight;
        ll_limelight = nullptr;
    }
    if( m_data_logger != nullptr )
    {
        m_data_logger->close();
        delete m_data_logger;
        m_data_logger = nullptr;
    }

}

void SwerveDriveControl::controlInit()
{
    fwd_cmd = 0.0;
    lat_cmd = 0.0;
    rot_cmd = 0.0;

    if( front_right == nullptr )
    {
        Advisory::pwarning("%s Drive Base missing Swerve Module -- front_right", getName().c_str());
    }
    if( front_left == nullptr )
    {
        Advisory::pwarning("%s Drive Base missing Swerve Module -- front_left", getName().c_str());
    }
    if( back_left == nullptr )
    {
        Advisory::pwarning("%s Drive Base missing Swerve Module -- back_left", getName().c_str());
    }
    if( back_right == nullptr )
    {
        Advisory::pwarning("%s Drive Base missing Swerve Module -- back_right", getName().c_str());
    }
    if(ll_limelight == nullptr)
    {
        Advisory::pwarning("%s Drive Base missing Swerve Module -- limelight", getName().c_str());
        // is_ready = false; uncomment when ready
    }
}

void SwerveDriveControl::disabledInit()
{
    m_data_logger->close();
    controlInit();

    // If we've been in auton mode, we want to skip the gyro calibration
    if (!m_in_auton_mode)
    {
       // gyro->calibrate();
    }
}

void SwerveDriveControl::autonomousInit()
{
    logFileInit("Auton");
    m_in_auton_mode = true;
}

void SwerveDriveControl::teleopInit()
{
    logFileInit("Teleop");
    m_in_auton_mode = false;
    fwd_cmd = 0.0;
    lat_cmd = 0.0;
    rot_cmd = 0.0;
}

void SwerveDriveControl::testInit()
{
    logFileInit("Test");
}

void SwerveDriveControl::setInt(int id, int val)
{
    // TODO: CMD_SCHEME_INT
    switch (id)
	{
		case CMD_DPAD_SETPOINTS:
		{
			if (val == 0)
			{
				
			}
			else if (val == 90)
			{
				drive_scheme_current = &(drive_scheme[1]); // field oriented
			}
			else if (val == 180)
			{
				
			}
			else if (val == 270)
			{
				drive_scheme_current = &(drive_scheme[0]); //robot relative
			}
		} break;

		default: // do nothing
			break;
	}
}

void SwerveDriveControl::setDriveScheme(uint32_t scheme)
{
    if(drive_scheme.size() > scheme)
    {
        drive_scheme_current = &(drive_scheme[scheme]);
    }
}

void SwerveDriveControl::setAnalog(int id, float val)
{
    switch(id)
    {
    case CMD_FORWARD:
        fwd_cmd = val;
        break;
    case CMD_LATERAL:
        lat_cmd = val;
        break;
    case CMD_ROTATE:
        rot_cmd = val;
        break;
    }
}

void SwerveDriveControl::setDigital(int id, bool press)
{
    switch(id)
    {
    case CMD_BRAKE_ON:
        if(press)
        {
            brake_engaged = true;
        }
        break;
    case CMD_BRAKE_OFF:
        if(press)
        {
            brake_engaged = false;
        }
        break;
    case CMD_BRAKE_TOGGLE:
        if(press)
        {
            brake_engaged = !brake_engaged;
        }
        break;
    case CMD_BRAKE_STATE:
        brake_engaged = press;
        break;
    case CMD_SPEED_HI:
        if(press)
        {
            speed_high = true;
        }
        break;
    case CMD_SPEED_LO:
        if(press)
        {
            speed_high = false;
        }
        break;
    case CMD_SPEED_TOGGLE:
        if(press)
        {
            speed_high = !speed_high;
        }
        break;
    case CMD_SPEED_STATE:
        brake_engaged = press;
        break;
    case CMD_SCHEME_INCR:
        if(press)
        {
            if(drive_scheme_index == drive_scheme.size()-1)
            {
                drive_scheme_index = 0;
            }
            else
            {
                drive_scheme_index++;
            }
            drive_scheme_current = &(drive_scheme[drive_scheme_index]);
            Advisory::pinfo("Set to drive_scheme[%d] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_SCHEME_DECR:
        if(press)
        {
            if(drive_scheme_index == 0)
            {
                drive_scheme_index = drive_scheme.size()-1;
            }
            else
            {
                drive_scheme_index--;
            }
            drive_scheme_current = &(drive_scheme[drive_scheme_index]);
            Advisory::pinfo("Set to drive_scheme[%d] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_JUKE:
        if(press)
        {
            drive_scheme_current = &(drive_scheme[0]);
            Advisory::pinfo("Set to JUKE drive_scheme[%d] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_GLOBAL_JUKE:
        if(press)
        {
            drive_scheme_current = &(drive_scheme[1]);
            Advisory::pinfo("Set to field-relative JUKE drive_scheme[%d] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_TOKYO:
        if(press)
        {
            drive_scheme_current = &(drive_scheme[2]);
            Advisory::pinfo("Set to TOKYO drive_scheme[%d] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_SCHEME_3:
        if(press && drive_scheme.size() > 3)
        {
            drive_scheme_current = &(drive_scheme[3]);
            Advisory::pinfo("Set to drive_scheme[3] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_SCHEME_4:
        if(press && drive_scheme.size() > 4)
        {
            drive_scheme_current = &(drive_scheme[4]);
            Advisory::pinfo("Set to drive_scheme[4] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_SCHEME_5:
        if(press && drive_scheme.size() > 5)
        {
            drive_scheme_current = &(drive_scheme[5]);
            Advisory::pinfo("Set to drive_scheme[5] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_SCHEME_6:
        if(press && drive_scheme.size() > 6)
        {
            drive_scheme_current = &(drive_scheme[6]);
            Advisory::pinfo("Set to drive_scheme[6] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_GYRO_CALIBRATE:
        if(press)
        {
            if(gyro != nullptr )
            {
                gyro->reset();
            }
        }
        break;
    case CMD_LL_HOLD:
        // while button pressed, switch to drive scheme 3 (ll rotate), otherwise use drive scheme juke
        if(press && drive_scheme.size() > 3)
        {
            drive_scheme_current = &(drive_scheme[3]);
            Advisory::pinfo("Set to drive_scheme[3] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        else
        {
            drive_scheme_current = &(drive_scheme[0]);
            Advisory::pinfo("Set to JUKE drive_scheme[%d] = 0x%x", drive_scheme_index, drive_scheme_current);
        }
        break;
    case CMD_MODULE_CALIBRATE:
        if(press)
        {
            calibrate_modules();
        }
    }
}

void SwerveDriveControl::publish()
{
    SmartDashboard::PutNumber(getName() + " GYRO angle: ", gyro_angle_current);

    SmartDashboard::PutString(getName() +" DRIVE_SCHEME__: ", drive_scheme_current->name.c_str() );
    SmartDashboard::PutNumber(getName() + "LL horizontal offset: ", ll_target_angle_horizontal);
    /*SmartDashboard::PutNumber(getName() +" DRIVE_SCHEME index: ", drive_scheme_index );

    SmartDashboard::PutNumber(getName() + " HC forward: ", fwd_cmd);
    SmartDashboard::PutNumber(getName() + " HC lateral: ", lat_cmd);
    SmartDashboard::PutNumber(getName() + " HC rotate:  ", rot_cmd);
    SmartDashboard::PutNumber(getName() + " DRIVE forward: ", fwd_drive);
    SmartDashboard::PutNumber(getName() + " DRIVE lateral: ", lat_drive);
    SmartDashboard::PutNumber(getName() + " DRIVE rotate:  ", rot_drive);

    SmartDashboard::PutNumber(getName() + " MOTOR front right trans vel:  ", front_right->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR front right rot   pos:  ", front_right->m_rotational_position_target);
    SmartDashboard::PutNumber(getName() + " MOTOR front left trans vel:  ", front_left->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR front left rot   pos:  ", front_left->m_rotational_position_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back right trans vel:  ", back_right->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back right rot   pos:  ", back_right->m_rotational_position_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back left trans vel:  ", back_left->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back left rot   pos:  ", back_left->m_rotational_position_target);

    SmartDashboard::PutNumber(getName() + " MOTOR front right rot target:  ", front_right->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR front right rot :  ", front_right->m_rotational_position_target);
    SmartDashboard::PutNumber(getName() + " MOTOR front left trans vel:  ", front_left->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR front left rot   pos:  ", front_left->m_rotational_position_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back right trans vel:  ", back_right->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back right rot   pos:  ", back_right->m_rotational_position_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back left trans vel:  ", back_left->m_translational_velocity_target);
    SmartDashboard::PutNumber(getName() + " MOTOR back left rot   pos:  ", back_left->m_rotational_position_target);*/
}

void SwerveDriveControl::logFileInit(std::string phase)
{
    Advisory::pinfo("initializing log file for %s", phase.c_str());

        m_data_logger->openSegment();

        m_data_logger->log("%s, %s, %s, ",
            "time", "phase", "pet"
        );

        m_data_logger->log("%s, %s, %s, %s, %s, %s, ",
            "fwd_cmd",   "lat_cmd",   "rot_cmd",
            "fwd_drive", "lat_drive", "rot_drive" );

        if (front_right != nullptr)
        {
            m_data_logger->log("%s, %s, %s, %s, %s, %s, %s, %s, %s, ",
                "fr_can_angle", "fr_rot_enc_angle", "fr_rot_angle_target",
                "fr_rot_angle_err", "fr_rot_motor_vel", "fr_trans_gear_ratio",
                "fr_trans_reverse", "fr_trans_pos", "fr_trans_vel");
        }
        if (front_left != nullptr)
        {
            m_data_logger->log("%s, %s, %s, %s, %s, %s, %s, %s, %s, ",
                "fl_can_angle", "fl_rot_enc_angle", "fl_rot_angle_target",
                "fl_rot_angle_err", "fl_rot_motor_vel", "fl_trans_gear_ratio",
                "fl_trans_reverse", "fl_trans_pos", "fl_trans_vel");
        }
        if (back_right != nullptr)
        {
            m_data_logger->log("%s, %s, %s, %s, %s, %s, %s, %s, %s, ",
                "br_can_angle", "br_rot_enc_angle", "br_rot_angle_target",
                "br_rot_angle_err", "br_rot_motor_vel", "br_trans_gear_ratio",
                "br_trans_reverse", "br_trans_pos", "br_trans_vel");
        }
        if (back_left != nullptr)
        {
            m_data_logger->log("%s, %s, %s, %s, %s, %s, %s, %s, %s",
                "bl_can_angle", "bl_rot_enc_angle", "bl_rot_angle_target",
                "bl_rot_angle_err", "bl_rot_motor_vel", "bl_trans_gear_ratio",
                "bl_trans_reverse", "bl_trans_pos", "bl_trans_vel");
        }

        // TODO: Bring these back when it makes sense
        /*
        if (m_accelerometer != nullptr)
        {
            m_data_logger->log("%s, %s, %s, ",
                "m_accelerometer_x", "m_accelerometer_y", "m_accelerometer_z");
        }
        */
        /*
        if (m_imu!= nullptr)
        {
            m_data_logger->log("%s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s, %s,     %s, %s ",
                "m_imu_accel_x", "m_imu_accel_y", "m_imu_accel_z",
                "m_imu_mag_x", "m_imu_mag_y", "m_imu_mag_z",
                "m_imu_rate_x", "m_imu_rate_y", "m_imu_rate_z",
                "m_imu_angle_x", "m_imu_angle_y", "m_imu_angle_z",
                "m_imu_roll", "m_imu_pitch", "m_imu_yaw",
                "m_imu_quat_w", "m_imu_quat_x", "m_imu_quat_y", "m_imu_quat_z",
                "m_imu_bar_press", "m_imu_temperature");
        }
        */
        m_data_logger->log("\n");

        m_data_logger->flush();
}

void SwerveDriveControl::logFileAppend()
{
    m_data_logger->log("%f, %d, %f, ",
           gsi::Time::getTime(), this->getPhase(), this->getPhaseElapsedTime());
    m_data_logger->log("%f, %f, %f, %f, %f, %f, ",
        fwd_cmd,   lat_cmd,   rot_cmd,
        fwd_drive, lat_drive, rot_drive );

    if (front_right != nullptr)
    {
        m_data_logger->log("%f, %f, %f, %f, %f, %f, %d, %f, %f, ",      
            front_right->getCancoderCurrentPosition(),
            front_right->getRotationMotorPosition(),
            front_right->getCancoderTargetPosition(),
            front_right->getCancoderErrorPosition(),
            front_right->getRotationMotorVelocity(),
            front_right->getRotationMotorGearRatio(),
            front_right->getTranslationMotorReverse(),
            front_right->getTranslationMotorPosition(),
            front_right->getTranslationMotorVelocity()
        );
    }

    if (front_left != nullptr)
    {
        m_data_logger->log("%f, %f, %f, %f, %f, %f, %d, %f, %f, ",      
            front_left->getCancoderCurrentPosition(),
            front_left->getRotationMotorPosition(),
            front_left->getCancoderTargetPosition(),
            front_left->getCancoderErrorPosition(),
            front_left->getRotationMotorVelocity(),
            front_left->getRotationMotorGearRatio(),
            front_left->getTranslationMotorReverse(),
            front_left->getTranslationMotorPosition(),
            front_left->getTranslationMotorVelocity()
        );
    }

    if (back_right != nullptr)
    {
        m_data_logger->log("%f, %f, %f, %f, %f, %f, %d, %f, %f, ",      
            back_right->getCancoderCurrentPosition(),
            back_right->getRotationMotorPosition(),
            back_right->getCancoderTargetPosition(),
            back_right->getCancoderErrorPosition(),
            back_right->getRotationMotorVelocity(),
            back_right->getRotationMotorGearRatio(),
            back_right->getTranslationMotorReverse(),
            back_right->getTranslationMotorPosition(),
            back_right->getTranslationMotorVelocity()
        );
    }

    if (back_left != nullptr)
    {
        m_data_logger->log("%f, %f, %f, %f, %f, %f, %d, %f, %f",      
            back_left->getCancoderCurrentPosition(),
            back_left->getRotationMotorPosition(),
            back_left->getCancoderTargetPosition(),
            back_left->getCancoderErrorPosition(),
            back_left->getRotationMotorVelocity(),
            back_left->getRotationMotorGearRatio(),
            back_left->getTranslationMotorReverse(),
            back_left->getTranslationMotorPosition(),
            back_left->getTranslationMotorVelocity()
        );
    }

    m_data_logger->log("\n");
}

void SwerveDriveControl::doPeriodic()
{
    // Advisory::pinfo("  doPeriodic for SwerveDrive" );
    //
    // Read Sensors -----------------------------------------------------------
    //
    // TODO: reorganize sensor reads to its own function
    if( gyro != nullptr)
    {
        gyro_angle_current = gyro->getAngle();
        gyro_rate_current  = gyro->getRate();
    }
    else
    {
        gyro_angle_current = 0.0;
        gyro_rate_current  = 0.0;
    }
    if(ll_limelight != nullptr)
    {
        ll_limelight->getData( ll_target_is_visible, 
                           ll_target_angle_horizontal, 
                           ll_target_angle_vertical,
                           ll_target_area, 
                           ll_target_angle_roll);
    }
    else
    {
        ll_target_is_visible       = 0.0;
        ll_target_angle_horizontal = 0.0;
        ll_target_angle_vertical   = 0.0;
        ll_target_area             = 0.0;
        ll_target_angle_roll       = 0.0;
    }
    

    //
    // Override/condition input for automatic control -------------------------
    //
        if( drive_scheme_current->type == SwerveDriveScheme::AUTO_OFF_JUKE)
        {
            fwd_drive = fwd_cmd;
            lat_drive = lat_cmd;
            rot_drive = rot_cmd;
            update_drive_juke();
        }
        else if( drive_scheme_current->type == SwerveDriveScheme::AUTO_GLOBAL_JUKE)
        {
            // TODO: gyro calibrate?
            float cosg = cos( -gyro_angle_current * deg_to_rad );
            float sing = sin( -gyro_angle_current * deg_to_rad );
            fwd_drive =   fwd_cmd * cosg
                        - lat_cmd * sing;
            lat_drive =   fwd_cmd * sing
                          + lat_cmd * cosg;
            rot_drive = rot_cmd;
            update_drive_juke();
        }
        else if( drive_scheme_current->type == SwerveDriveScheme::AUTO_ROTATE_SENSOR)
        {
            // Ignore rot command, only use trans commands
            // read target_hz_offset from xml, this is for aiming a certain # of degrees to the right/left of the detected target
            if (fabs(ll_target_angle_horizontal - ll_target_hz_offset) > 0.07)
            {
                // Horizontal FOV 54 deg
                // TODO: unhard code FOV
                rot_drive = (ll_target_angle_horizontal - ll_target_hz_offset) / 36.0;
                if( fabs(rot_drive) > 1.0 )
                {
                    rot_drive /= rot_drive;
                    // rot_drive = 1.0;
                }
                else if( fabs(rot_drive) < 0.03 )
                {
                    rot_drive /= rot_drive;
                    rot_drive *= 0.03;
                   // rot_drive = 0.03;
                }
            }
            else
            {
                rot_drive = 0.0;
            }
            fwd_drive = fwd_cmd;
            lat_drive = lat_cmd;
            update_drive_juke();
        }
        else if( drive_scheme_current->type == SwerveDriveScheme::AUTO_OFF_TOKYO )
        {
            fwd_drive = fwd_cmd;
            lat_drive = lat_cmd;
            rot_drive = rot_cmd;
            update_drive_tokyo();
        }
        

    //
    // Calculate values from input --------------------------------------------
    //
    front_right->doControl();
    back_right->doControl();
    back_left->doControl();
    front_left->doControl();

    logFileAppend();
}

MSSwerveDriveDrivePower::MSSwerveDriveDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (SwerveDriveControl *)control;

    forward_power = 0.0;
    lateral_power = 0.0;
    rotate_power =  0.0;

    xml->QueryFloatAttribute("forward", &forward_power);
    xml->QueryFloatAttribute("lateral", &lateral_power);
    xml->QueryFloatAttribute("rotate", &rotate_power);
}

void MSSwerveDriveDrivePower::init(void)
{
    parent_control->setAnalog(SwerveDriveControl::CMD_FORWARD, forward_power);
    parent_control->setAnalog(SwerveDriveControl::CMD_LATERAL, lateral_power);
    parent_control->setAnalog(SwerveDriveControl::CMD_ROTATE,  rotate_power);
}

MacroStep * MSSwerveDriveDrivePower::update(void)
{
    return next_step;
}


MSSwerveDriveSetScheme::MSSwerveDriveSetScheme(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (SwerveDriveControl *)control;

    drive_scheme = 0;

    xml->QueryUnsignedAttribute("scheme", &drive_scheme);
}

void MSSwerveDriveSetScheme::init(void)
{
    parent_control->setDriveScheme(drive_scheme);
    
}

MacroStep * MSSwerveDriveSetScheme::update(void)
{
    return next_step;
}

MSSwerveDriveRotateAngle::MSSwerveDriveRotateAngle(std::string type, tinyxml2::XMLElement *xml, void *control)
    : MacroStepSequence(type, xml, control)
{
    parent_control = (SwerveDriveControl *)control;

    degrees = 0.0;
    target_angle = 0.0;

    xml->QueryFloatAttribute("degrees", &degrees);
}

void MSSwerveDriveRotateAngle::init(void)
{
    target_angle = parent_control->gyro_angle_current + degrees;
    parent_control->rotToAngle(target_angle);
    
}

MacroStep * MSSwerveDriveRotateAngle::update(void)
{
    return next_step;
}
void SwerveDriveControl::update_drive_tokyo()
{
    // If no turn input, strafe only. Write separately to avoid trig round off
    if( rot_drive == 0.0 )
    {
        float speed = std::sqrt( fwd_drive*fwd_drive + lat_drive*lat_drive );
        if( speed > 1.0 )
        {
            speed = 1.0;
        }
        float angle = std::atan2( lat_drive, fwd_drive ) * rad_to_deg;
        front_right->m_translational_velocity_target = speed;
        front_left->m_translational_velocity_target  = speed;
        back_left->m_translational_velocity_target   = speed;
        back_right->m_translational_velocity_target  = speed;
        front_right->cc_position_target = angle; // * rad_to_deg;   // Why are we doing this twice?
        front_left->cc_position_target  = angle; // * rad_to_deg;
        back_left->cc_position_target   = angle; // * rad_to_deg;
        back_right->cc_position_target  = angle; // * rad_to_deg;
        return;
    }

    float turn_radius = distance_left_to_right/rot_drive;
    float turn_sign = (rot_drive<0) ? -1.0 : 1.0;
    
    float xneg = fwd_drive*fabs( turn_radius - distance_left_to_right/2.0 );
    float ypos = fwd_drive*turn_sign*distance_front_to_back/2.0 + lat_drive*fabs(turn_radius);
    float xpos = fwd_drive*fabs( turn_radius + distance_left_to_right/2.0 );
    float yneg = -fwd_drive*turn_sign*distance_front_to_back/2.0 + lat_drive*fabs(turn_radius);

    // Wheel angles
    front_right->cc_position_target = std::atan2( ypos, xneg ) * rad_to_deg;
    front_left->cc_position_target  = std::atan2( ypos, xpos ) * rad_to_deg;
    back_left->cc_position_target   = std::atan2( yneg, xpos ) * rad_to_deg;
    back_right->cc_position_target  = std::atan2( yneg, xneg ) * rad_to_deg;

    // Wheel speeds
    float spds[4];
    float max_spd;
    spds[FRONT_RIGHT] = std::sqrt(xneg*xneg + ypos*ypos) / turn_radius;
    spds[FRONT_LEFT]  = std::sqrt(xpos*xpos + ypos*ypos) / turn_radius;
    spds[BACK_LEFT]   = std::sqrt(xpos*xpos + yneg*yneg) / turn_radius;
    spds[BACK_RIGHT]  = std::sqrt(xneg*xneg + yneg*yneg) / turn_radius;
    max_spd = *std::max_element(spds, spds+4);
    if( max_spd > 1.0 )
    {
        front_right->m_translational_velocity_target = spds[FRONT_RIGHT] / max_spd;
        front_left->m_translational_velocity_target  = spds[FRONT_LEFT]  / max_spd;
        back_left->m_translational_velocity_target   = spds[BACK_LEFT]   / max_spd;
        back_right->m_translational_velocity_target  = spds[BACK_RIGHT]  / max_spd;
    }
    else
    {
        front_right->m_translational_velocity_target = spds[FRONT_RIGHT];
        front_left->m_translational_velocity_target  = spds[FRONT_LEFT];
        back_left->m_translational_velocity_target   = spds[BACK_LEFT];
        back_right->m_translational_velocity_target  = spds[BACK_RIGHT];
    }
}

void SwerveDriveControl::update_drive_juke()
{
    // Assign control input for each motor
    float xpos = fwd_drive + rot_drive * distance_front_to_back/2;
    float xneg = fwd_drive - rot_drive * distance_front_to_back/2;
    float ypos = lat_drive + rot_drive * distance_left_to_right/2;
    float yneg = lat_drive - rot_drive * distance_left_to_right/2;

    // Speeds
    float spds[4];
    float max_spd;
    spds[FRONT_RIGHT] = std::sqrt(xneg*xneg + ypos*ypos);
    spds[FRONT_LEFT]  = std::sqrt(xpos*xpos + ypos*ypos);
    spds[BACK_LEFT]   = std::sqrt(xpos*xpos + yneg*yneg);
    spds[BACK_RIGHT]  = std::sqrt(xneg*xneg + yneg*yneg);

    // if overcommanding, limit speed to 100%
    max_spd = *std::max_element(spds, spds+4);
    if( max_spd < 1.0 )
    {
        max_spd = 1.0;
    }

    // Rotations
    front_right->cc_position_target = std::atan2( ypos, xneg ) * rad_to_deg;
    front_left->cc_position_target  = std::atan2( ypos, xpos ) * rad_to_deg;
    back_left->cc_position_target   = std::atan2( yneg, xpos ) * rad_to_deg;
    back_right->cc_position_target  = std::atan2( yneg, xneg ) * rad_to_deg;

    front_right->m_translational_velocity_target = spds[FRONT_RIGHT] / max_spd;
    front_left->m_translational_velocity_target  = spds[FRONT_LEFT]  / max_spd;
    back_left->m_translational_velocity_target   = spds[BACK_LEFT]   / max_spd;
    back_right->m_translational_velocity_target  = spds[BACK_RIGHT]  / max_spd;
}

void SwerveDriveControl::calibrate_modules()
{
    if( front_left != nullptr
        && front_left->absolute_rot_cc != nullptr )
    {
        front_left->absolute_rot_cc->calibrate();
    }
    if( front_right != nullptr
        && front_right->absolute_rot_cc != nullptr )
    {
        front_right->absolute_rot_cc->calibrate();
    }
    if( back_left != nullptr
        && back_left->absolute_rot_cc != nullptr )
    {
        back_left->absolute_rot_cc->calibrate();
    }
    if( back_right != nullptr
        && back_right->absolute_rot_cc != nullptr )
    {
        back_right->absolute_rot_cc->calibrate();
    }
}

void SwerveDriveControl::rotToAngle(float angle)
{
    // TODO test this
     // Ignore rot command, only use trans commands
            if (fabs(gyro_angle_current - angle) > 0.1)
            {
                // Horizontal FOV 54 deg
                // TODO: unhard code FOV
                rot_drive = (gyro_angle_current - angle) / 54.0;
                if( fabs(rot_drive) > 1.0 )
                {
                    // rot_drive /= rot_drive;
                    rot_drive = 1.0;
                }
                else if( fabs(rot_drive) < 0.03 )
                {
                    // rot_drive /= rot_drive;
                    // rot_drive *= 0.03;
                    rot_drive = 0.03;
                }
            }
            else
            {
                rot_drive = 0.0;
            }
            fwd_drive = fwd_cmd;
            lat_drive = lat_cmd;
            update_drive_juke();
}
