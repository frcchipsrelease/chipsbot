/*******************************************************************************
 *
 * File: Shooter.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements: this code sponsored by programmer jail
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "gsutilities/Filter.h"
#include "gsinterfaces/Path.h"
#include "rfutilities/DataLogger.h"
#include "Shooter.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
Shooter::Shooter(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
	, ENCODER_TO_RPM ((600.0/20480.0) * 0.9)
{
	Advisory::pinfo("========================= Creating Shooter Control [%s] =========================",
	            control_name.c_str());

	//light_sensor = nullptr;
	//light_sensor_state = false;
	
	// ------ FLYWHEEL -------------------
	m_fly1_motor = nullptr;
	fly_power_step = 0.05;
	fly_target_power = 0.0;
	fly_nominal_power = 0.0;
	fly1_command_power	= 0.0;
	is_fly_on = false;
	prev_fly_target = 0.0;
	fly_min_power = -1.0;
	fly_max_power = 1.0;

	fly_target_velocity = 0.0;
	fly_command_velocity = 0.0;
	fly1_actual_velocity = 0.0;
	fly_target_rpm = 0.0;
	fly_actual_rpm = 0.0;
	
	fly_velocity_step = 500.0;
	fly_min_velocity = 1000.0;
	fly_max_velocity = 6300.0;
	fly_velocity_error = 0.0;

	fly_setpoint[0] = 0.0;
	fly_setpoint[1] = 0.50;
	fly_setpoint[2] = 0.75;
	fly_setpoint[3] = 1.00;
	setpoint_index = 0;


	// ------ HOOD -------------------
	m_hood_motor = nullptr;
	m_hood_cancoder = nullptr;
	m_limit_fwd = nullptr;
	m_limit_back = nullptr;
	hood_pid = nullptr;

	limit_fwd_state = false;
	limit_back_state = false;

	hood_target_power		= 0.0;
	hood_command_power		= 0.0;

	hood_target_pos = 15.0; // arbitrary number so the hood doesn't try to go all the way back
	hood_actual_pos = 15.0;
	hood_position_error = 0.0;
	hood_setpoint[0] = 0.0;
	hood_setpoint[1] = 0.0;
	hood_setpoint[2] = 0.0;
	hood_setpoint[3] = 0.0;


	// ------ KICKER -------------------
	m_kicker_motor = nullptr;
	kicker_target_power		= 0.0;
	kicker_command_power	= 0.0;
	kicker_nominal_power = 0.0;
	kicker_idle_power = 0.0;


	// ------ LIMELIGHT -------------------
	ll_limelight = nullptr;
	lime_light_is_on = false;
	ll_is_visible = 0.0;
	ll_actual_area = 0.0;
	ll_actual_angle_vertical = 0.0;
	ll_actual_angle_horizontal = 0.0;
	ll_actual_angle_roll = 0.0;
	ll_target_height = 0.0;
	ll_mount_height = 0.0;
	ll_mount_angle_vertical = 0.0;
	ll_mount_planar_offset = 0.0;
	ll_distance_to_target = 0.0;
	ll_angle_offset = 0.0;
	ll_target_velocity = 0.0;


	// -------- LED -------------------------
	m_led = nullptr;
	m_led_length = 10;
	led_offset = 0;
	move_limit = 0;

	p_main = 1;
	p_sec = 1;
	current_pattern = 2;
	main_pattern = 2;
	sec_pattern = 2;

	dis_led_lock = false; // rename this later



	// ---------- LOGS -----------------------
	gsi::Path::createPath("/robot/logs/shooter");
	log_auton = new DataLogger("/robot/logs/shooter", "autoshooter", "csv", 10, true);
    log_teleop = new DataLogger("/robot/logs/shooter", "teleshooter", "csv", 10, true);
	log_active = log_auton;

	//
	// Register Macro Steps as proxy names to use in auton XML
	//
	new MacroStepProxy<MSShooterFlywheelVelocity>(control_name, "ShooterFlywheelVelocity", this);
	new MacroStepProxy<MSShooterKickerPower>(control_name, "ShooterKickerPower", this);
	new MacroStepProxy<MSHoodPosition>(control_name, "ShooterHoodPosition", this);
	new MacroStepProxy<MSClosedLoopState>(control_name, "ClosedLoopState", this);
	new MacroStepProxy<MSLimelightState>(control_name, "LimelightState", this);

	//
	// Parse XML
	//
	XMLElement* comp = nullptr;
	const char* name = nullptr;
	
	xml->QueryBoolAttribute("closed_loop", &closed_loop_on);
	xml->QueryBoolAttribute("limelight_targeting_state", &lime_light_is_on);


	comp = xml->FirstChildElement("limelight_velocity_curve");
			Advisory::pinfo(" limelight comp: %x ", comp);
	if (comp != nullptr)
	{
    	XMLElement *limelight_comp;
		limelight_comp = comp->FirstChildElement("line");
 		while (limelight_comp!=nullptr)
		{
			float distance = 0.0;
			float velocity = 0.0;
			float angle = 0.0;
			limelight_comp->QueryFloatAttribute("distance", &distance);
			m_distance_table.push_back(distance);
			limelight_comp->QueryFloatAttribute("velocity", &velocity);
			m_velocity_table.push_back(velocity);
			limelight_comp->QueryFloatAttribute("angle", &angle);
			m_angle_table.push_back(angle);
			Advisory::pinfo(" ---------------------- limelight segment: limelight distance = %7.2f flywheel velocity = %7.2f",
					distance, velocity);
    		limelight_comp = limelight_comp->NextSiblingElement("line");
		}
		ll_velocity_curves.setCurve(m_distance_table, m_velocity_table);
 		ll_velocity_curves.setLimitEnds(true);
		ll_angle_curves.setCurve(m_distance_table, m_angle_table);
 		ll_angle_curves.setLimitEnds(true);
	}

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			XMLElement* subcomp = nullptr;
			Advisory::pinfo("----------------- NAME: %s ----------------", name);
			if(strcmp(name, "kicker_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for kicker motor");
				m_kicker_motor = HardwareFactory::createMotor(comp);
				comp->QueryFloatAttribute("nominal", &kicker_nominal_power);
				comp->QueryFloatAttribute("idle", &kicker_idle_power);
			}
			else if(strcmp(name, "fly_motor_1") == 0)
			{
    			Advisory::pinfo("  creating speed controller for Flywheel Motor #1");
				m_fly1_motor = HardwareFactory::createMotor(comp);

				comp->QueryFloatAttribute("power_step_size", &fly_power_step);
				comp->QueryFloatAttribute("velocity_step_size", &fly_velocity_step);
				comp->QueryFloatAttribute("nominal_power", &fly_nominal_power);
				m_fly1_motor->setSensorScale(ENCODER_TO_RPM, 0.0);
				subcomp = comp->FirstChildElement("setpoints");
				parseSetPoints( subcomp, fly_setpoint );
			}
			else if(strcmp(name, "hood_motor") == 0)
			{
    			Advisory::pinfo("  creating speed controller for Hood Motor");
				m_hood_motor = HardwareFactory::createMotor(comp);
				subcomp = comp->FirstChildElement("setpoints");
				parseSetPoints( subcomp, hood_setpoint );
				subcomp = comp->FirstChildElement("limit_switch");
				while( subcomp != nullptr )
				{
					name = subcomp->Attribute("name");
					if (name != nullptr)
					{
						if( strcmp(name,"fwd") == 0 )
						{
							m_limit_fwd = HardwareFactory::createLimitSwitch(subcomp);
							Advisory::pinfo("%s  creating hood forward (upper) LS", getName().c_str());
						}
						else if( strcmp(name,"bck") == 0 )
						{
							m_limit_back = HardwareFactory::createLimitSwitch(subcomp);
							Advisory::pinfo("%s  creating hood backward (lower) LS", getName().c_str());
						}
						else
						{
							Advisory::pinfo("%s  unrecognized limit switch spec", getName().c_str());
						}
					}
					else
					{
						Advisory::pinfo("%s  unnamed limit switch spec", getName().c_str());
					}
					subcomp = subcomp->NextSiblingElement("limit_switch");
				}
			
				
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
			
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("cancoder");
	if (comp != nullptr)
	{
		m_hood_cancoder = HardwareFactory::createCancoder(comp);
    }

	/*
	// UNCOMMENT ONCE WE GET LEDS
	comp = xml-> FirstChildElement("led_strip");
	if (comp != nullptr)
	{
		XMLElement* led_comp;
		m_led = HardwareFactory::createAddressableLED(comp);
		comp->QueryIntAttribute("led_length", &m_led_length);
		Advisory::pinfo("  LED Length: %d", m_led_length);
		m_led_buffer.resize(m_led_length);
		m_led->SetLength(m_led_length);
		m_led->SetData(m_led_buffer);

		comp->QueryBoolAttribute("dual_stripz", &silly_mode);
		Advisory::pinfo("dual strip: %s", silly_mode ? "true" : "false"); // ternary operator go brrrr

		if (silly_mode)
		{
			m_led_length /= 2; // if the LED strip is split into two, treat it like having 2 separate patterns
		}

		led_comp = comp-> FirstChildElement("pattern");
		int count = 0;
		while (led_comp != nullptr)
		{
			name = led_comp->Attribute("name");
			Advisory::pinfo("  creating new LED Pattern %s", name);
			pattern_type.emplace_back();
			pattern_type[count] = led_comp->IntAttribute("type");
			start_1.emplace_back();
			start_1[count] = led_comp->IntAttribute("start_1");
			Advisory::pinfo("Start_1 for pattern %s: %i", name, start_1[count]);
			start_2.emplace_back();
			start_2[count] = led_comp->IntAttribute("start_2");
			Advisory::pinfo("Start_2 for pattern %s: %i", name, start_2[count]);
			start_3.emplace_back();
			start_3[count] = led_comp->IntAttribute("start_3");
			Advisory::pinfo("Start_3 for pattern %s: %i", name, start_3[count]);
			end_1.emplace_back();
			end_1[count] = led_comp->IntAttribute("end_1");
			Advisory::pinfo("End_1 for pattern %s: %i", name, end_1[count]);
			end_2.emplace_back();
			end_2[count] = led_comp->IntAttribute("end_2");
			end_3.emplace_back();
			end_3[count] = led_comp->IntAttribute("end_3");
			pattern_length.emplace_back();
			pattern_length[count] = led_comp->IntAttribute("pattern_length");
			Advisory::pinfo("pattern length: %i", pattern_length[count]);
			pattern_moving.emplace_back();
			pattern_moving[count] = led_comp->BoolAttribute("moving");
			pattern_dashed.emplace_back();
			pattern_dashed[count] = led_comp->BoolAttribute("dashed");
			
			if(count == 0)
			{
				main_pattern_chooser.SetDefaultOption("right pattern", count + 1);
				sec_pattern_chooser.SetDefaultOption("left pattern", count + 1);
			}
			main_pattern_chooser.AddOption(name, count + 1);
			sec_pattern_chooser.AddOption(name, count + 1);

			led_comp = led_comp-> NextSiblingElement("pattern");
			count += 1;
		}
		frc::SmartDashboard::PutData(&main_pattern_chooser);
		frc::SmartDashboard::PutData(&sec_pattern_chooser);
    }
	*/

	comp = xml-> FirstChildElement("hood_pid");
	if (comp != nullptr)
	{
	    hood_pid = HardwareFactory::createPid(comp);
    }

	comp = xml-> FirstChildElement("limelight");
	if (comp != nullptr)
	{
		ll_limelight = HardwareFactory::createLimelight(comp);
		comp->QueryDoubleAttribute("target_height", &ll_target_height);
		Advisory::pinfo("  Target Height: %f", ll_target_height);
		comp->QueryDoubleAttribute("limelight_height", &ll_mount_height);
		Advisory::pinfo("  Limelight Height: %f", ll_mount_height);
		comp->QueryDoubleAttribute("limelight_angle", &ll_mount_angle_vertical);
		Advisory::pinfo("  Limelight Mount Angle: %f", ll_mount_angle_vertical);
		// TODO: Implement me eventually?
		// comp->QueryDoubleAttribute("target_vertical", &ll_target_angle_vertical);
		// Advisory::pinfo("  Target Vertical: %f", ll_target_angle_vertical);
	}


	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if( strcmp(name, "closed_loop_state") == 0 )
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if (strcmp(name, "closed_loop_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_TOGGLE);
			}
			else if (strcmp(name, "closed_loop_start") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_ON);
			}
			else if (strcmp(name, "closed_loop_stop") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_OFF);
			}
			else if (strcmp(name, "kicker") == 0)
			{
				Advisory::pinfo("%s  connecting kicker channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER);
			}
			else if (strcmp(name, "fly_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting fly toggle channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_TOGGLE);
			}
			else if (strcmp(name, "fly_step_up") == 0)
			{
				Advisory::pinfo("%s  connecting fly step up channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_STEP_UP);
			}
			else if (strcmp(name, "fly_step_down") == 0)
			{
				Advisory::pinfo("%s  connecting fly step down channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLY_STEP_DOWN);
			}
			else if (strcmp(name, "hood_control") == 0)
			{
				Advisory::pinfo("%s  connecting hood channel", getName().c_str(), name);
				OIController::subscribeAnalog(comp, this, CMD_HOOD_CONTROL);
			}
			else if (strcmp(name, "limelight_targeting_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_TOGGLE);
			}
			else if (strcmp(name, "dpad_setpoints") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeInt(comp, this, CMD_DPAD_SETPOINTS);
			}
			
			

		}
		
		comp = comp->NextSiblingElement("oi");
		
	}
}


/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * AKA: destructor, delete any hardware and set to nullptrs
 * 
 ******************************************************************************/
Shooter::~Shooter(void)
{
	if (m_kicker_motor != nullptr)
	{
		delete m_kicker_motor;
		m_kicker_motor = nullptr;
	}
	if (m_fly1_motor != nullptr)
	{
		delete m_fly1_motor;
		m_fly1_motor = nullptr;
	}
	if (m_hood_motor != nullptr)
	{
		delete m_hood_motor;
		m_hood_motor = nullptr;
	}
	if (m_hood_cancoder != nullptr)
	{
		delete m_hood_cancoder;
		m_hood_cancoder = nullptr;
	}
}

/**
 * Parse a setpoints xml item
 */
int Shooter::parseSetPoints( tinyxml2::XMLElement* comp, float* setp )
{
	if( comp == nullptr )
	{
		return 0;
	}

	int active_index = 0;
	int num_setpoints = 0;

	XMLElement *setpoint_comp;
	const char* name;
	setpoint_comp = comp->FirstChildElement("setpoint");
	comp->QueryIntAttribute("active", &active_index);
	while (setpoint_comp != nullptr)
	{
		int sp_index = -1;
		setpoint_comp->QueryIntAttribute("index", &sp_index);
		if (sp_index < 0 || sp_index >= Shooter::NUM_SETPOINTS)
		{
			Advisory::pinfo("%s setpoint with unset or out of bounds index.\n"
			                "Automatically assigning to %d", getName().c_str(), num_setpoints);
			sp_index = num_setpoints;
		}
		setpoint_comp->QueryFloatAttribute("value", &setp[sp_index]);
		name = setpoint_comp->Attribute("name");
		if (name != nullptr)
		{
			setpoint_lookup.insert( std::pair<std::string,float&>(name,setp[sp_index]) );
		}
		else
		{
			Advisory::pwarning("%s found unnamed setpoint, using default", getName().c_str());
			char default_name[50];
			sprintf(default_name,"SETPOINT_%d",setpoint_lookup.size());
			setpoint_lookup.insert( std::pair<std::string,float&>(default_name,setp[sp_index]) );
		}
		Advisory::pinfo("%s  -- setpoint %2d: %20s   value=%7.2f",getName().c_str(),
			sp_index, name, setp[sp_index] );


		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		if( (++num_setpoints) >= Shooter::NUM_SETPOINTS )
		{
			break;
		}
	}

	return active_index;
}

/*******************************************************************************
 * Makes sure that all necessary hardware is included in XML
 ******************************************************************************/
void Shooter::controlInit(void)
{ 
	bool is_ready = true;
	
	if(m_fly1_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- flywheel motor #1", getName().c_str());
		is_ready = false;
	}
	if(m_kicker_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- kicker motor", getName().c_str());
		is_ready = false;
	}
	if(m_hood_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- hood motor", getName().c_str());
		is_ready = false;
	}
	if(ll_limelight == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- limelight", getName().c_str());
		is_ready = false;
	}
	if( m_hood_cancoder == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- CANCoder", getName().c_str());
		is_ready = false;
	}
	if( m_limit_fwd == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- Upper Limit Switch", getName().c_str());
		is_ready = false;
	}
	if( m_limit_back == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- Lower Limit Switch", getName().c_str());
		is_ready = false;
	}
	if( hood_pid == nullptr )
	{
		Advisory::pwarning("%s Shooter missing required component -- Hood PID", getName().c_str());
		is_ready = false;
	}
	active = is_ready;
	ll_height_difference = ll_target_height - ll_mount_height;
    hood_target_pos = hood_actual_pos;
	
	if (m_led != nullptr)
	{
		for (int i = 0; i < 52; i++)
		{
			m_led_buffer[i].SetRGB(64, 64, 0); // this does nothing
		}
		
		setLedData();
		m_led->Start();
		Advisory::pinfo("STARTING LED");
	}
	
}

/*******************************************************************************
 *
 ******************************************************************************/
void Shooter::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Shooter::disabledInit(void)
{
    log_teleop->close();
    log_auton->close();
	kicker_target_power	= 0.0;
	fly_target_power	= 0.0;
	hood_target_power	= 0.0;
	kicker_command_power = 0.0;
	fly1_command_power	= 0.0;
	hood_command_power	= 0.0;
	dis_led_lock = false;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Shooter::autonomousInit(void)
{
	main_pattern = main_pattern_chooser.GetSelected();
	Advisory::pinfo("Main Pattern: %i", main_pattern);
	sec_pattern = sec_pattern_chooser.GetSelected();
	Advisory::pinfo("Secondary Pattern: %i", sec_pattern);

	log_active = log_auton;
	initLogFile();
	kicker_target_power	= 0.0;
	fly_target_power	= 0.0;
	hood_target_power	= 0.0;
	kicker_command_power = 0.0;
	fly1_command_power	= 0.0;
	hood_command_power	= 0.0;
}
/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Shooter::teleopInit(void)
{
	main_pattern = main_pattern_chooser.GetSelected();
	Advisory::pinfo("Main Pattern: %i", main_pattern);
	sec_pattern = sec_pattern_chooser.GetSelected();
	Advisory::pinfo("Secondary Pattern: %i", sec_pattern);

	log_active = log_teleop;
	initLogFile();
	Advisory::pinfo("INITIALIZED LOG FILE");
	kicker_target_power	= kicker_idle_power;
	fly_target_power	= 0.0;
	hood_target_power	= 0.0;
	kicker_command_power = 0.0;
	fly1_command_power	= 0.0;
	hood_command_power	= 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void Shooter::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Shooter::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_HOOD_CONTROL:
			Advisory::pinfo("------- HOOD_TARGET_POWER: %f", hood_target_power);
			hood_target_power = val;
			break;

		default:
			// Do Nothing
			break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void Shooter::setDigital(int id, bool val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_CLOSED_LOOP_STATE: 
		{	
			setClosedLoop(val);  
		} break;

		case CMD_CLOSED_LOOP_TOGGLE:
		{	
			// if pressed, set closed loop state to the opposite of its current state (true / false)
			if(val == true)
			{
				setClosedLoop(!isClosedLoop());
				hood_target_pos = hood_actual_pos;
			}
			
		} break;

		case CMD_CLOSED_LOOP_ON:
		{	
			if(val == true)
			{
				setClosedLoop(true);
			}
		} break;

		case CMD_CLOSED_LOOP_OFF:
		{
			if(val == true)
			{
				setClosedLoop(false);
			}
		} break;

		case CMD_LIMELIGHT_TOGGLE:
		{
			// if pressed, set ll targeting state to the opposite of its current state (true / false)
			if (val)
			{
				setLimeLightState(!getLimeLightState());
			}
		} break;

		case CMD_KICKER:
		{
			// while pressed, command kicker to its nominal power
			// otherwise:
			// if no ball in intake, command kicker to 0% power
			// if ball in intake, command kicker to [xml input] to avoid accidentally shooting the ball
			if (val)
			{
				kicker_target_power = kicker_nominal_power;
			}
			else
			{
				kicker_target_power = kicker_idle_power;
			}
		} break;
		
		case CMD_FLY_TOGGLE: 
			{	
				if (val)
				{
					if (closed_loop_on)
					{
						// if the flywheel is on,
						// save the current target velocity
						// and set the target velocity to 0
						if (is_fly_on)
						{
							prev_fly_target = fly_target_velocity;
							m_fly1_motor->setVelocity(0.0);
							is_fly_on = false;
						}
						// if the flywheel is off,
						// turn it on and command it to the previous target velocity
						else
						{
							m_fly1_motor->setVelocity(prev_fly_target);
							is_fly_on = true;
						}
					}
					else
					{
						// if the flywheel is on,
						// save the current target % power
						// and set the target power to 0%
						if (is_fly_on)
						{
							prev_fly_target = fly_target_power;
							setFlywheelPower(0.0);
							is_fly_on = false;
							Advisory::pinfo("TURNING FLYWHEEL OFF");
						}
						// if the flywheel is off,
						// turn it on and command it to the previous target % power
						else
						{
							setFlywheelPower(prev_fly_target);
							is_fly_on = true;
							Advisory::pinfo("TURNING FLYWHEEL ON");
						}
					}
				}
			} break;
		case CMD_FLY_STEP_UP: 
			{	
				if (val)
				{
					if(closed_loop_on)
					{
						// increase the flywheel target velocity by a certain value
						// and limit it so it doesn't go above the max velocity
						fly_target_velocity += fly_velocity_step;
						fly_target_velocity = gsu::Filter::limit(fly_target_velocity, fly_min_velocity, fly_max_velocity);
					}
					else
					{
						// increase the flywheel target % power by a certain value
						// and limit it so it doesn't go above the max % power
						fly_target_power += fly_power_step;
						fly_target_power = gsu::Filter::limit(fly_target_power, fly_min_power, fly_max_power);
					}
				}
			} break;
		case CMD_FLY_STEP_DOWN: 
			{	
				if (val)
				{
					if(closed_loop_on)
					{
						// decrease the flywheel target velocity by a certain value
						// if the target velocity goes below the min, just turn the flywheel off
						fly_target_velocity -= fly_velocity_step;
						if (fly_target_velocity < fly_min_velocity)
						{
							fly_target_power = 0.0;
						}
					}
					
					else
					{
						// decrease the flywheel target % power by a certain value
						// if the target % power goes below the min, just turn the flywheel off
						fly_target_power -= fly_power_step;
						if (fly_target_power < fly_min_power)
						{
							fly_target_power = 0.0;
						}
					}
				}
			} break;

			
		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Shooter::setInt(int id, int val)
{
	switch (id)
	{
		case CMD_DPAD_SETPOINTS:
		{
			if (val == 0)
			{
				applySetPoint(0);
				Advisory::pinfo("APPLYING SETPOINT #0 (FENDER))");
			}
			else if (val == 90)
			{
				applySetPoint(1);
				Advisory::pinfo("APPLYING SETPOINT #1 (AUTON)");
			}
			else if (val == 180)
			{
				applySetPoint(2);
				Advisory::pinfo("APPLYING SETPOINT #2 (???))");
			}
			else if (val == 270)
			{
				applySetPoint(3);
				Advisory::pinfo("APPLYING SETPOINT #3 (SAFEZONE))");
			}
		} break;

		default: // do nothing
			break;
	}
}

void Shooter::setKickerPower(float power)
{
	// percent power
	kicker_target_power = power;
}

void Shooter::setFlywheelPower(float power)
{
	fly_target_power = power;
}

void Shooter::setHoodPower(float power)
{
	hood_target_power = power;
}

void Shooter::setFlywheelVelocity(float velocity)
{
	fly_target_velocity = velocity;
}

void Shooter::setHoodPosition(float position)
{
	hood_target_pos = position;
}

/*******************************************************************************
 *
 ******************************************************************************/
bool Shooter::isAtTarget(int vel_tolerance, float pos_tolerance)
{
	// (true / false) are the flywheel AND hood both at their target?
	return ((fabs(fly_velocity_error) < vel_tolerance) && (fabs(hood_position_error) < pos_tolerance));
}

/*******************************************************************************
 *
 ******************************************************************************/
void Shooter::applySetPoint(int index)
{
	// sets flywheel velocity and hood position to the setpoint values
	setpoint_index = index;

	setClosedLoop(true);
	fly_target_velocity = fly_setpoint[setpoint_index];
	hood_target_pos = hood_setpoint[setpoint_index];
}


/*******************************************************************************
 *
 ******************************************************************************/
void Shooter::setLimeLightState(bool value)
{
	Advisory::pinfo("-------------------------------------- setLimeLightState", getName().c_str());

	if (lime_light_is_on != value)
	{
		if (value == true) 
		{
			// if ll targeting is off,
			// make sure closed loop is on,
			// then turn ll targeting on
			if (ll_limelight == nullptr)
			{
				Advisory::pinfo("cannot use %s limelight control, lime light is not connected", getName().c_str());
				lime_light_is_on = false;
			}
			else
			{
				setClosedLoop(true);

				if (isClosedLoop())
				{
					lime_light_is_on = true;
				}
				else
				{
					Advisory::pinfo("cannot use %s limelight control, closed loop is not connected", getName().c_str());
					lime_light_is_on = false;
				}
				
			}
		} 
		else
		{
			// if ll targeting is on,
			// turn it off
			// but don't touch the closed loop state
			Advisory::pinfo("setting %s limelight control off", getName().c_str());
			lime_light_is_on = false;
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
 bool Shooter::getLimeLightState(void)
 {
	return lime_light_is_on;
 }

/*******************************************************************************
 *
 ******************************************************************************/
bool Shooter::isClosedLoop(void)
{
	return closed_loop_on;
}
/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void Shooter::initLogFile(void)
{
	log_active->openSegment();

	log_active->log("%s, %s, %s, %s, %s, %s, %s, %s, ",
                    "current_time",
                    "fly1_actual_velocity",
                    "fly_target_velocity", 
                    "fly_command_velocity",
					"fly_actual_rpm",
					"fly_velocity_error",
					"fly_target_power",
					"fly1_command_power");
	log_active->log("%s, %s, ",
					"kicker_target_power",
					"kicker_command_power");
	log_active->log("%s, %s, %s, %s, %s, ",
					"hood_actual_pos",
					"hood_target_pos",
					"hood_position_error",
					"hood_target_power",
					"hood_command_power");
	log_active->log("%s, %s, ",
					"limit_fwd_state",
					"limit_back_state");
	log_active->log("%s, %s, %s, %s,",
					"ll_actual_area",
					"ll_actual_angle_vertical",
					"ll_actual_angle_horizontal",
                    "hood_command_power");
					
    log_active->log("\n");
    log_active->flush();
} 

/**********************************************************************
 *
 * This method is used to update the log files, called from
 * doPeriodic
 *
 **********************************************************************/
void Shooter::updateLogFile(void)
{
	
	log_active->log("%f, %f, %f, %f, %f, %f, %f, %f, ",
                    gsi::Time::getTime(),
                    fly1_actual_velocity,
                    fly_target_velocity, 
                    fly_command_velocity,
					fly_actual_rpm, 
					fly_velocity_error,
					fly_target_power,
					fly1_command_power);
					
	log_active->log("%f, %f, ",
					kicker_target_power,
                    kicker_command_power);
	log_active->log("%f, %f, %f, %f, %f, ",
					hood_actual_pos,
					hood_target_pos,
					hood_position_error, 
					hood_target_power,
					hood_command_power);
	log_active->log("%f, %f, ",
					limit_fwd_state,
					limit_back_state);
	log_active->log("%f, %f, %f, %f, ",
					ll_actual_area,
					ll_actual_angle_vertical,
					ll_actual_angle_horizontal,
                    hood_command_power );
	
    log_active->log("\n");
    log_active->flush();
	
} 

/*******************************************************************************	
 *
 * Publishes important values to Smart Dashboard for troubleshooting/info
 * Lots of this is commented out because we don't need it Now but we might need
 * 	it Later.
 *
 ******************************************************************************/
void Shooter::publish()
{
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " kicker command_power: ", kicker_command_power);
	SmartDashboard::PutNumber(getName() + " flywheel target_power: ", fly_target_power);
	SmartDashboard::PutNumber(getName() + " flywheel #1 command_power: ", fly1_command_power);
	
	SmartDashboard::PutNumber(getName() + " closed loop: ", closed_loop_on);
	SmartDashboard::PutNumber(getName() + " flywheel target_velocity: ", fly_target_velocity);
	SmartDashboard::PutNumber(getName() + " flywheel motor #1 actual_velocity: ", fly1_actual_velocity);
	SmartDashboard::PutNumber(getName() + " FLYWHEEL RPM: ", fly_actual_rpm);
	;
	SmartDashboard::PutNumber(getName() + " hood target_position: ", hood_target_pos);
	SmartDashboard::PutNumber(getName() + " hood actual_position: ", hood_actual_pos);
	
	SmartDashboard::PutBoolean(getName() + " limelight targeting on: ", lime_light_is_on);
	SmartDashboard::PutNumber(getName() + " limelight FLYWHEEL TARGET VELOCITY: ", ll_target_velocity);
	SmartDashboard::PutNumber(getName() + " limelight DISTANCE TO TARGET: ", ll_distance_to_target);

    SmartDashboard::PutBoolean(getName() + " fwd_limit: ", limit_fwd_state);
    SmartDashboard::PutBoolean(getName() + " back_limit: ", limit_back_state);

}
/*******************************************************************************
 *
 ******************************************************************************/
void Shooter::setClosedLoop(bool closed)
{
	if (closed_loop_on == closed)
	{
		// Already set, return
		return;
	}

	closed_loop_on = closed;

	if (closed_loop_on)
	{
		// tell the flywheel to use PID for velocity control
		if(m_fly1_motor != nullptr)
		{
			m_fly1_motor->setControlMode(Motor::VELOCITY);
		}
		Advisory::pinfo("setting Shooter closed  mode");
	}
	else
	{
		// tell the flywheel to use commands to a certain % power
		if(m_fly1_motor != nullptr)
		{
			m_fly1_motor->setControlMode(Motor::PERCENT);
		}

		setLimeLightState(false);

		Advisory::pinfo("setting Shooter open loop mode");
	}

	if(m_hood_motor != nullptr)
	{
		m_hood_motor->setControlMode(Motor::PERCENT);
		hood_target_pos = hood_actual_pos;
	}

	if(m_kicker_motor != nullptr)
	{
		m_kicker_motor->setControlMode(Motor::PERCENT);
	}
}

/*******************************************************************************
 * 
 * LED PATTERNS
 * 
 ******************************************************************************/

void Shooter::setLedData()
{
	m_led->SetData(m_led_buffer);
}

void Shooter::setLedSolid(int r, int g, int b, bool silly)
{
	// sets all leds one solid color
	for (int i = 0; i < m_led_length; i++)
	{
		if (silly)
		{
			m_led_buffer[i + m_led_length].SetRGB(r, g, b);
		}
		else
		{
			m_led_buffer[i].SetRGB(r, g, b);
		}
	}
}

void Shooter::setLedDashed(int r1, int g1, int b1, int r2, int g2, int b2, int length, bool silly)
{
	if(r2 == -1)
	{
		// if only the first color is specified, the second color defaults to black (no light output)
		r2 = 0;
		g2 = 0;
		b2 = 0;
	}
	bool is_color = false;
	for (int i = 0; i < m_led_length; i++)
	{
		// j = current index + offset.
		// if j goes outside bounds of the array, it loops back to the beginning
		int j = (i + led_offset) % m_led_length;
		if (i % length == 0)
		{
			// every (length) pixels, switch the color
			is_color = !is_color;
		}
		if (is_color)
		{
			if (silly)
			{
				m_led_buffer[j + m_led_length].SetRGB(r1, g1, b1);
			}
			else
			{
				// if color A, set the pixel to color A
				m_led_buffer[j].SetRGB(r1, g1, b1);
			}
		}
		else
		{
			if (silly)
			{
				m_led_buffer[j + m_led_length].SetRGB(r2, g2, b2);
			}
			else
			{
				// set pixel to color B
				m_led_buffer[j].SetRGB(r2, g2, b2);
			}
		}
	}
}

void Shooter::setLedGradient(int start, int s, int v, int end, bool silly)
{
	for (int i = 0; i < m_led_length; i++)
	{
		// j = current index + offset.
		// if j goes outside bounds of the array, it loops back to the beginning
		int j = (i + led_offset) % m_led_length;
		// for each pixel increment the hue by:
		// difference btwn start and end hue / length of led string
		// this creates a smooth gradient across the entire led string
	
      	const auto pixelHue = (start + (i * (end - start) / (m_led_length - 1))) % 180;
		if (silly)
		{
			m_led_buffer[j + m_led_length].SetHSV(pixelHue, s, v);
		}
		else
		{
			m_led_buffer[j].SetHSV(pixelHue, s, v);
		}
	}
}

void Shooter::setLedRepeating(int r1, int g1, int b1, int r2, int g2, int b2, int length, bool silly)
{
	if(r2 == -1)
	{
		r2 = r1;
		g2 = g1;
		b2 = b1;
		r1 = 0;
		g1 = 0;
		b1 = 0;
	}
	for (int i = 0; i < m_led_length; i++)
	{
		// j = current index + offset.
		// if j goes outside bounds of the array, it loops back to the beginning
		int j = (i + led_offset) % m_led_length;
	
		int r = std::floor(r1 + (i % length) * ((r2 - r1) / (length - 1)));
		int g = std::floor(g1 + (i % length) * ((g2 - g1) / (length - 1)));
		int b = std::floor(b1 + (i % length) * ((b2 - b1) / (length - 1)));

		if (silly)
		{
			m_led_buffer[j + m_led_length].SetRGB(r, g, b);
		}
		else
		{
			m_led_buffer[j].SetRGB(r, g, b);
		}
		
	}
}


void Shooter::setLedRainbow(bool silly)
{
	int r = 0; int g = 0; int b = 0;
	int j;
	for(int i = 0; i < m_led_length; i++)
	{
		j = (i + led_offset) % m_led_length;
		if (j < std::round(m_led_length / 6))
		{
			// I know there are more efficient ways of doing this. However, I have had 5 hours of sleep and I don't care enough to fix this right now.
			r = 80; g = 0; b = 128;
		}
		else if (j < std::round(m_led_length / 3))
		{
			r = 0; g = 0; b = 128;
		}
		else if (j < std::round(m_led_length / 2))
		{
			r = 0; g = 128; b = 0;
		}
		else if (j < std::round(2 * m_led_length / 3))
		{
			r = 128; g = 64; b = 0;
		}
		else if (j < std::round(5 * m_led_length / 6))
		{
			r = 128; g = 16; b = 0;
		}
		else
		{
			r = 128; g = 0; b = 0;
		}
		if (silly)
		{
			m_led_buffer[i + m_led_length].SetRGB(r, g, b);
		}
		else
		{
			m_led_buffer[i].SetRGB(r, g, b);
		}
	}
}

void Shooter::setLedPattern(int pattern, bool silly)
{
	move_limit += 1;
	// pass in the pattern number, decrease it by 1 for the array
	pattern -= 1;

	if(pattern_type.at(pattern) == 1)
	{
			if (pattern_dashed.at(pattern))
			{
				setLedDashed(start_1.at(pattern), start_2.at(pattern), start_3.at(pattern), end_1.at(pattern), end_2.at(pattern), end_3.at(pattern), pattern_length.at(pattern), silly);
			}
			else
			{
				setLedSolid(start_1.at(pattern), start_2.at(pattern), start_3.at(pattern), silly);
			}
	}
	else if (pattern_type.at(pattern) == 2)
	{
			setLedGradient(start_1.at(pattern), start_2.at(pattern), start_3.at(pattern), end_1.at(pattern), silly);
	}
	else if (pattern_type.at(pattern) == 3)
	{
			setLedRepeating(start_1.at(pattern), start_2.at(pattern), start_3.at(pattern), end_1.at(pattern), end_2.at(pattern), end_3.at(pattern), pattern_length.at(pattern), silly);
	}
	else if (pattern_type.at(pattern) == 4)
	{
		setLedRainbow(silly);
	}
	if (pattern_moving.at(pattern))
	{
		// if pattern is moving, each time setLedPattern() is called,
		// the offset increases by 1 pixel. if the offset is longer than the length of the led string,
		// it resets back to 0 and picks up from there.
		if (move_limit % 20 == 0)
		{
			led_offset = (led_offset + 1)% m_led_length;
		}
	}
}


/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Shooter::doPeriodic()
{
	if(active == false)
	{
		Advisory::pinfo("DoPeriodic is not ready");
		return;
	}
	m_fly1_motor->doUpdate();
	m_kicker_motor->doUpdate();
	m_hood_motor->doUpdate();

	//	
	// Get Sensor Inputs
	//
	fly1_actual_velocity = m_fly1_motor->getVelocity();
	fly_actual_rpm = fly1_actual_velocity;
	
	if(fly1_actual_velocity > 0.0)
	{
		is_fly_on = true;
	}
	else
	{
		is_fly_on = false;
	}

	hood_actual_pos = -m_hood_cancoder->getAbsoluteAngle();

	limit_back_state = !m_limit_back->isPressed(); // TODO fix logic, why coming backwards off the hardware?
	limit_fwd_state = !m_limit_fwd->isPressed();


	//
	// Fun LED things
	//

	/*
	if (isAtTarget(15, 0.25)) // ?? hypothetical tolerance values. TODO change these if needed.
	{
		prev_pattern = current_pattern;
		current_pattern = sec_pattern; // leds change pattern if ready to shoot (flywheel speed/hood angle)
	}
	else
	{
		prev_pattern = current_pattern;
		current_pattern = main_pattern;
	}
	
	*/

	
	if (m_led != nullptr)
	{
		//*
		// now the leds work as an rsl lmao
		if (getPhase() == DISABLED)
		{
			if (!dis_led_lock)
			{
				setLedPattern(1, false); // default pattern
				setLedPattern(1, true); // default pattern
				dis_led_lock = true;
			}
		}
		else {
			if (silly_mode)
			{
				if (pattern_moving[main_pattern-1] || pattern_moving[sec_pattern-1] || p_main != main_pattern || p_sec != sec_pattern)
				{
					setLedPattern(main_pattern, false);
					setLedPattern(sec_pattern, true);
					p_main = main_pattern;
					p_sec = sec_pattern;
				}
			}
			else
			{
				setLedPattern(current_pattern, false);
			}
		}
		/*/
	
		/*
		if (silly_mode)
			{
				if (pattern_moving[main_pattern-1] || pattern_moving[sec_pattern-1] || p_main != main_pattern || p_sec != sec_pattern)
				{
					setLedPattern(main_pattern, false);
					setLedPattern(sec_pattern, true);
					p_main = main_pattern;
					p_sec = sec_pattern;
				}
			}
		else
			{
				setLedPattern(current_pattern, false);
			}
		*/

		setLedData();
	}
	

	if(ll_limelight != nullptr)
	{
		ll_limelight->getData(ll_is_visible, 
						   ll_actual_angle_horizontal, 
						   ll_actual_angle_vertical,
						   ll_actual_area, 
						   ll_actual_angle_roll);
		
		// use this if limelight is rotated 90 degrees clockwise
		//ll_limelight->getData(ll_is_visible, 
		//				   ll_actual_angle_vertical, 
		//				   ll_actual_angle_horizontal,
		//				   ll_actual_area, 
		//				   ll_actual_angle_roll);
		//ll_actual_angle_vertical = -ll_actual_angle_vertical;				   
		
    }
	//
	// Make sure the motors don't jump when enabled
	//
	
	
	if (getPhase() == DISABLED)
	{
		fly_target_velocity = 0.0; 
		fly_command_velocity = 0.0; 

		fly_target_power = 0.0;
		fly1_command_power = 0.0;

		kicker_target_power = kicker_idle_power;
		kicker_command_power = 0.0;

		hood_target_power = 0.0;
		hood_command_power = 0.0;

        hood_target_pos = hood_actual_pos;
	}

	//
	// Process Data
	//

	if (ll_is_visible > 0.5)
	{
		ll_mount_planar_offset = ll_height_difference/tan((ll_actual_angle_vertical+ll_mount_angle_vertical)/RAD_TO_DEG);
		ll_distance_to_target = ll_mount_planar_offset;
		// optional, corrects distance to improve accuracy
		// if we decide to use this, it needs tuning for the new robot
		// ll_angle_offset = 25.2 - (ll_actual_angle_vertical+ll_mount_angle_vertical); // change this so 25.2 ISNT hard coded in
		// ll_distance_to_target = ll_mount_planar_offset + 0.017*pow(ll_angle_offset, 3) + 0.316*pow(ll_angle_offset, 2) + 3.0797*ll_angle_offset - 0.0282;
	}
	
	if (closed_loop_on)
	{
		if (lime_light_is_on && ll_is_visible > 0.5)
		{

			// rounds target flywheel velocity to the nearest 75; this is to avoid it constantly changing and breaking the flywheel lmao
			// IF YOU CHANGE UNITS. CHANGE THIS SO YOU DONT KILL THE FLYWHEEL PLEASE
			ll_target_velocity = round(ll_velocity_curves.evaluate(ll_distance_to_target)/75)*75;
			fly_target_velocity = ll_target_velocity;
			hood_target_pos = ll_angle_curves.evaluate(ll_distance_to_target);
			hood_position_error = hood_target_pos - hood_actual_pos;
			hood_command_power = -hood_pid->calculateCommand(hood_target_pos, hood_actual_pos);

		}
        else if (lime_light_is_on)
        {
            // if limelight targeting is on, but no target detected: maintain current velocity AND POSITION.
			fly_target_velocity = fly_target_velocity;
			hood_target_pos = hood_actual_pos; // 09.10 yeah this should work
			hood_command_power = -hood_pid->calculateCommand(hood_target_pos, hood_actual_pos);
        }
		else
		{
			// closed loop (NOT LL) flywheel

			// flywheel is closed loop on the motor controller, this error value 
			// is only for information
			fly_velocity_error = fly_target_velocity - fly1_actual_velocity;
			
			
			if( hood_target_power != 0.0 ) // assumes joystick deadband is properly set.
			{
				// if we're commanding the hood to move WITH THE CONTROLLER:
				// move it, then set target pos to actual pos to maintain the position
				hood_command_power = hood_target_power;
				hood_target_pos = hood_actual_pos;
			}
			else
			{
				// used for setpoints, etc. where we're setting a target hood position
				// use PID to move the hood to a specific position
				hood_target_pos = gsu::Filter::limit(hood_target_pos, 12.3, 29.0); // 9/6
				hood_position_error = hood_target_pos - hood_actual_pos;
				hood_command_power = -hood_pid->calculateCommand(hood_target_pos, hood_actual_pos);
			}
		}
		
	}
	else
	{
		if( hood_target_power != 0.0 ) // assumes joystick deadband is properly set.
		{
			// if we're commanding the hood to move WITH THE CONTROLLER:
			// move it, then set target pos to actual pos to maintain the position
			hood_command_power = hood_target_power;
			hood_target_pos = hood_actual_pos;
			hood_position_error  = 0.0;
		}
		else
		{
			// stationkeeping to hold last commanded orientation. We do this to fight against recoils
			hood_target_pos = gsu::Filter::limit(hood_target_pos, 12.3, 29.0); // 9/6
			hood_position_error = hood_target_pos - hood_actual_pos;
			hood_command_power = -hood_pid->calculateCommand(hood_target_pos, hood_actual_pos);
		}
		fly_velocity_error = 0.0;
	}
	
	// 
	// Convert targets to commands
	//
	// @TODO: RJP says: motor command values should be ramped, large steps can cause motors to spark, thus reducing the life of the motor
	//        test everything else, then add ramps, then test ramps by setting it to small step changes (like 0.02% per cycle),
	//        then change to appropriate value (like 0.2% per cycle), could also ramp based on time instead of based on cycles
	fly_command_velocity = fly_target_velocity;     // should ramp, 
	fly1_command_power = fly_target_power;       // should ramp
    kicker_command_power = kicker_target_power; // should ramp


    if (limit_back_state == true)
	{
		hood_command_power = gsu::Filter::limit(hood_command_power, -1.0, 0.0);
	}

	if (limit_fwd_state == true)
	{
        hood_command_power = gsu::Filter::limit(hood_command_power, 0.0, 1.0);
	}

    // Software limit, makes sure the hood does not go past certain angles
	if (hood_actual_pos < 12.5) hood_command_power = gsu::Filter::limit(hood_command_power, -1.0, 0.0);
	if (hood_actual_pos > 29.25) hood_command_power = gsu::Filter::limit(hood_command_power,  0.0, 1.0);

	// Do not allow kicker to inject ball if flywheel is not running
	if ( closed_loop_on )
	{
		if( fly1_actual_velocity < fly_min_velocity )
		{
			// if flywheel is not running,
			// kicker cannot run forwards (cannot push ball up)
			kicker_command_power = gsu::Filter::limit(kicker_command_power, -1.0, 0.0);
		}
	}
	else
	{
		if( fly1_command_power < fly_min_power )
		{
			// if flywheel is not running,
			// kicker cannot run forwards (cannot push ball up)
			kicker_command_power = gsu::Filter::limit(kicker_command_power, -1.0, 0.0);
		}
	}

	
	//
	// Set Outputs
	//
	m_kicker_motor->setPercent(kicker_command_power);
	if (closed_loop_on)
	{
		m_fly1_motor->setVelocity(fly_command_velocity);
	}
	else
	{
		m_fly1_motor->setPercent(fly1_command_power);
	}
	m_hood_motor->setPercent(hood_command_power);

	updateLogFile();
}


/*******************************************************************************
 *
 ******************************************************************************/
MSShooter::MSShooter(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	flywheel_target_velocity = 0.0;
	kicker_target_power = 0.0;
	hood_target_position = 0.0;
	m_wait = false;
	setpoint_index = 7; // not sure what this is for
	m_closed_loop = false;
	m_ll_control = false;
	
	m_parent_control = (Shooter *)control;
	xml->QueryFloatAttribute("flywheel_vel", &flywheel_target_velocity);
	xml->QueryFloatAttribute("kicker_pow", &kicker_target_power);
	xml->QueryFloatAttribute("hood_pos", &hood_target_position);
	xml->QueryIntAttribute("setpoint", &setpoint_index);
	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);
	xml->QueryBoolAttribute("ll_control", &m_ll_control);
	xml->QueryBoolAttribute("wait", &m_wait);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooter::init(void)
{
	m_parent_control->setClosedLoop(m_closed_loop);
	m_parent_control->setLimeLightState(m_ll_control);
	if (setpoint_index < NUM_SETPOINTS - 1)
	{
		m_parent_control->applySetPoint(setpoint_index + 1);
	}
	else
	{
		m_parent_control->applySetPoint(NUM_SETPOINTS - 1);
	}	
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooter::update(void)
{
    if(m_wait == true)
	{
		// waits for flywheel to spin up before moving to the next step
		// this would be useful if we had Ice's PID tuned correctly
	    //if(!m_parent_control->isAtTarget(1.0))
	 	//{
		//	return this;
	 	//}
	}

	return next_step;
}


/*******************************************************************************
 * 
 * reads in a target flywheel velocity from the xml,
 * then tells the value to the flywheel motors
 *
 ******************************************************************************/
MSShooterFlywheelVelocity::MSShooterFlywheelVelocity(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_flywheel_target_velocity = 0.0;
	m_parent_control = (Shooter *)control;
	xml->QueryFloatAttribute("velocity", &m_flywheel_target_velocity);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterFlywheelVelocity::init(void)
{
	m_parent_control->setFlywheelVelocity(m_flywheel_target_velocity);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterFlywheelVelocity::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 * reads in a target % power from the xml,
 * then tells that % power to the kicker motor
 *
 ******************************************************************************/
MSShooterKickerPower::MSShooterKickerPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_kicker_target_power = 0.0;
	m_parent_control = (Shooter *)control;
	xml->QueryFloatAttribute("power", &m_kicker_target_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterKickerPower::init(void)
{
	m_parent_control->setKickerPower(m_kicker_target_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterKickerPower::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 * reads in a target position from the xml,
 * then tells that position to the hood motor
 *
 ******************************************************************************/
MSHoodPosition::MSHoodPosition(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_hood_target_position = 0.0;
	m_parent_control = (Shooter *)control;
	xml->QueryFloatAttribute("position", &m_hood_target_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSHoodPosition::init(void)
{
	m_parent_control->setHoodPosition(m_hood_target_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSHoodPosition::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 * reads in desired ll targeting state, then tells that to the hood + flywheel
 *
 ******************************************************************************/
MSLimelightState::MSLimelightState(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_ll_state = false;
	m_parent_control = (Shooter *)control;
	xml->QueryBoolAttribute("state", &m_ll_state);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSLimelightState::init(void)
{
	m_parent_control->setLimeLightState(m_ll_state);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSLimelightState::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 * reads in desired closed loop state, then tells that to the hood + flywheel
 *
 ******************************************************************************/
MSClosedLoopState::MSClosedLoopState(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_closed_loop = false;
	m_parent_control = (Shooter *)control;
	xml->QueryBoolAttribute("state", &m_closed_loop);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSClosedLoopState::init(void)
{
	m_parent_control->setClosedLoop(m_closed_loop);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSClosedLoopState::update(void)
{
	return next_step;
}
