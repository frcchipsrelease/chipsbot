/*******************************************************************************
 *
 * File: Intake.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "Intake.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfcontrols/SolenoidControl.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
Intake::Intake(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Intake Control [%s] =========================",
	            control_name.c_str());

	// Be sure to initialize all method variables
	innerMotor= nullptr;
	outerMotor = nullptr;

	intakeMode = false;

	lightSensor = nullptr;
	lightSensorState = false;

	motor_min_control = -1.0;
	motor_max_control = 1.0;

    innerMotor_max_current = 10.0;
    outerMotor_max_current = 10.0;

	motor_max_cmd_delta = 0.25;

	innerMotor_forward_power = 0.0;
	innerMotor_backward_power = 0.0;
	outerMotor_forward_power = 0.0;
	outerMotor_backward_power = 0.0;

	innerMotor_target_power = 0.0;
	outerMotor_target_power = 0.0;
	innerMotor_command_power = 0.0;
	outerMotor_command_power = 0.0;
	innerMotor_nat_power = 0.2;
	outerMotor_nat_power = 0.7;
	innerMotor_shoot_power = 0.5;
	outerMotor_shoot_power = 0.7;

	m_solenoid = nullptr;
	solenoid_state = false;

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSIntakePower>(control_name, "IntakePower", this);
	new MacroStepProxy<MSIntakeShoot>(control_name, "IntakeShoot", this);
	new MacroStepProxy<MSIntakeDeploy>(control_name, "IntakeDeploy", this);


	//
	// Parse XML
	//
	
    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &motor_max_cmd_delta);
    Advisory::pinfo("  max cmd delta = %f", motor_max_cmd_delta);

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "inner_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for inner motor");
				innerMotor = HardwareFactory::createMotor(comp);
				comp->QueryUnsignedAttribute("max_current", & innerMotor_max_current);
				Advisory::pinfo("	max_current=%u", innerMotor_max_current); 
			}
			else if(strcmp(name, "outer_motor") == 0)
			{
    			Advisory::pinfo("  creating speed controller for outer motor");
				outerMotor = HardwareFactory::createMotor(comp);
				comp -> QueryUnsignedAttribute("max_current", & outerMotor_max_current);
				Advisory::pinfo("	max_current=%u", outerMotor_max_current);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
	comp = xml-> FirstChildElement("digital_input");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "light_sensor") == 0)
			{
				Advisory::pinfo("%s  creating digital input light_sensor", getName().c_str());
				lightSensor = HardwareFactory::createDigitalInput(comp);
			}
		}
		comp = comp->NextSiblingElement("digital_input");
	}
	
	comp = xml->FirstChildElement("solenoid");
	if (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "deploy") == 0)
			{
				Advisory::pinfo("  creating deploy solenoid for %s", name);
				m_solenoid = HardwareFactory::createSolenoid(comp);			}
			else
			{
				Advisory::pwarning("  found unknown solenoid with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found solenoid with no name attribute");
		}
	}
	else
	{ 
		Advisory::pwarning("  found no solenoid!");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "inner_forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				comp->QueryFloatAttribute("value", &innerMotor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_INNERFORWARD);
			}
			else if (strcmp(name, "inner_backward") == 0)
			{
				Advisory::pinfo("  connecting backward channel");
				comp->QueryFloatAttribute("value", &innerMotor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_INNERBACKWARD);
			}
			if (strcmp(name, "outer_forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				comp->QueryFloatAttribute("value", &outerMotor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_OUTERFORWARD);
			}
			else if (strcmp(name, "outer_backward") == 0)
			{
				Advisory::pinfo("  connecting backward channel");
				comp->QueryFloatAttribute("value", &outerMotor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_OUTERBACKWARD);
			}
			else if (strcmp(name, "stop") == 0)
			{
				Advisory::pinfo("  connecting stop channel");
				OIController::subscribeDigital(comp, this, CMD_STOP);
			}
			
			else if (strcmp(name, "shoot_inandout") == 0)
			{
				Advisory::pinfo("%s  connecting shoot channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_SHOOT_INANDOUT);
			}

			else if (strcmp(name, "shoot") == 0)
			{
				Advisory::pinfo("%s  connecting shoot state channel");
				comp->QueryFloatAttribute("inner", &innerMotor_shoot_power);
				comp->QueryFloatAttribute("outer", &outerMotor_shoot_power);
				OIController::subscribeDigital(comp,this, CMD_SHOOT);
			}

			else if (strcmp(name, "nat_intake") == 0)
			{
				Advisory::pinfo("%s  connecting nat intake channel");
				comp->QueryFloatAttribute("inner", &innerMotor_nat_power);
				comp->QueryFloatAttribute("outer", &outerMotor_nat_power);
				OIController::subscribeDigital(comp,this, CMD_NAT_INTAKE);
			}

			else if (strcmp(name, "dump") == 0)
			{
				Advisory::pinfo("%s  connecting dump channel");
				comp->QueryFloatAttribute("inner", &innerMotor_backward_power);
				comp->QueryFloatAttribute("outer", &outerMotor_backward_power);
				OIController::subscribeDigital(comp,this, CMD_DUMP);
			}
			else if (strcmp(name, "deploy") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_DEPLOY);
			}

			
		}
		comp = comp->NextSiblingElement("oi");
		
	}
}



/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
Intake::~Intake(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	if (innerMotor != nullptr)
	{
		delete innerMotor;
		innerMotor = nullptr;
	}
	if (outerMotor != nullptr)
	{
		delete outerMotor;
		outerMotor = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::controlInit(void)
{ 
	// I left a lot of the old stuff. Change and get rid of as needed

    if ((innerMotor != nullptr) && (innerMotor_max_current < 100.0))
    {
      	innerMotor->setCurrentLimit(innerMotor_max_current);
        innerMotor->setCurrentLimitEnabled(true);
    }
	if ((outerMotor != nullptr) && (outerMotor_max_current < 100.0))
    {
        outerMotor->setCurrentLimit(outerMotor_max_current);
        outerMotor->setCurrentLimitEnabled(true);
    }
	
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::disabledInit(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	innerMotor_target_power = 0.0;
	innerMotor_command_power = 0.0;
	outerMotor_target_power = 0.0;
	outerMotor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::autonomousInit(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	innerMotor_target_power = 0.0;
	innerMotor_command_power = 0.0;
	outerMotor_target_power = 0.0;
	outerMotor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void Intake::teleopInit(void)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	innerMotor_target_power = 0.0;
	innerMotor_command_power = 0.0;
	outerMotor_target_power = 0.0;
	outerMotor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void Intake::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void Intake::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void Intake::setDigital(int id, bool val)
{
	// I left a lot of the old stuff. Change and get rid of as needed
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_INNERFORWARD:
		{
			if (val)
			{
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_forward_power);
			}
		} break;
			
		case CMD_INNERBACKWARD:
		{
			if (val)
			{
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_backward_power);
			}
		} break;

		case CMD_OUTERFORWARD:
		{
			if (val)
			{
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_forward_power);
			}
		} break;
			
		case CMD_OUTERBACKWARD:
		{
			if (val)
			{
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_backward_power);
			}
		} break;

		case CMD_STOP:
		{
			if (val)
			{
				innerMotor_target_power = 0.0;
				outerMotor_target_power = 0.0;
			}
		} break;

		case CMD_SHOOT_INANDOUT:
		{
			if (val == true)
			{
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_forward_power);
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_forward_power);
			}

		} break;

		case CMD_SHOOT:
		{
			Advisory::pinfo("shoot: %d", val);
			if (val == true)
			{
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_shoot_power);
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_shoot_power);
			}
			else
			{
				innerMotor_target_power = 0;
				outerMotor_target_power = 0;
			}
			
		} break;

		case CMD_NAT_INTAKE:
		{
			if (val == true)
			{
				intakeMode = true;
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_nat_power);
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_nat_power);
			}
			else 
			{
				intakeMode = false;
				innerMotor_target_power = 0;
				outerMotor_target_power = 0;
			}
			/*
			// 3.12.22 TOGGLE INTAKE
			intakeMode = !intakeMode;
			if (intakeMode == true)
			{
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_nat_power);
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_nat_power);
			}
			else
			{
				innerMotor_target_power = 0;
				outerMotor_target_power = 0;
			}
			*/

		} break;

		case CMD_DUMP: 
		{
			if (val == true)
			{
				innerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, innerMotor_backward_power);
				outerMotor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, outerMotor_backward_power);
			}
			else
			{
				innerMotor_target_power = 0;
				outerMotor_target_power = 0;
			}
			
		} break;
		case CMD_DEPLOY:
		{
			solenoid_state = val;
		} break;

		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void Intake::setInt(int id, int val)
{
}

void Intake::setInnerPower(float power)
{
	innerMotor_target_power = power;
}
void Intake::setOuterPower(float power)
{
	outerMotor_target_power = power;
}
void Intake::setIntakePower(float power)
{
	outerMotor_target_power = power;
    innerMotor_target_power = power;
}
void Intake::setSolenoidState(bool state)
{
	solenoid_state = state;
}
/*******************************************************************************	
 *
 ******************************************************************************/
void Intake::publish()
{
	// I left a lot of the old stuff. Change and get rid of as needed
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " innerMotor target_power: ", innerMotor_target_power);
	SmartDashboard::PutNumber(getName() + " innerMotor command_power: ", innerMotor_command_power);
	SmartDashboard::PutNumber(getName() + " outerMotor target_power: ", outerMotor_target_power);
	SmartDashboard::PutNumber(getName() + " outerMotor command_power: ", outerMotor_command_power);
	SmartDashboard::PutBoolean(getName() + "lightSensorState: ", lightSensorState);
	SmartDashboard::PutBoolean(getName() + "INTAKE SOLENOID STATE: ", solenoid_state);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void Intake::doPeriodic()
{
	// I left a lot of the old stuff. Change and get rid of as needed
	innerMotor->doUpdate();
	outerMotor->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	innerMotor_command_power = innerMotor->getPercent();

	//
	// All processing happens in the motor class
	//
	lightSensorState = lightSensor->Get();
	//Advisory::pinfo("got light sensor state= %d", lightSensorState);
	//
	// Set Outputs
	//

	
	if (intakeMode == true)
	{
		if (lightSensorState == false)
		{
			innerMotor_target_power = 0;
			outerMotor_target_power = outerMotor_nat_power*0.99;
		}
	}
	

	innerMotor->setPercent(innerMotor_target_power);
	outerMotor->setPercent(outerMotor_target_power);
	
	

	if(m_solenoid != nullptr)
	{
		
	m_solenoid->Set(solenoid_state);
	}
	

}
/*******************************************************************************
 *
 ******************************************************************************/
MSIntakePower::MSIntakePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{

	inner_power = 0.0;
	outer_power = 0.0;

	m_parent_control = (Intake *)control;

	xml->QueryFloatAttribute("inner_power", &inner_power);
	xml->QueryFloatAttribute("outer_power", &outer_power);
	
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSIntakePower::init(void)
{	
	if (fabs(outer_power>0.1))
	{
		m_parent_control->setDigital(Intake::CMD_NAT_INTAKE, true);
	}
	else
	{
		m_parent_control->setDigital(Intake::CMD_NAT_INTAKE, false);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSIntakePower::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSIntakeShoot::MSIntakeShoot(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	inner_power = 0.0;
	outer_power = 0.0;

	m_parent_control = (Intake *)control;

	xml->QueryFloatAttribute("shoot_inner_power", &inner_power);
	xml->QueryFloatAttribute("shoot_outer_power", &outer_power);
	Advisory::pinfo("shoot inner power: %f", inner_power);
	Advisory::pinfo("shoot outer power: %f", outer_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSIntakeShoot::init(void)
{
	if (fabs(outer_power)>0.1)
	{
		m_parent_control->setDigital(Intake::CMD_SHOOT, true);
	}
	else
	{
		m_parent_control->setDigital(Intake::CMD_SHOOT, false);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSIntakeShoot::update(void)
{
	return next_step;
}



/*******************************************************************************
 *
 ******************************************************************************/
MSIntakeDeploy::MSIntakeDeploy(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	state = false;

	m_parent_control = (Intake *)control;

	xml->QueryBoolAttribute("state", &state);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSIntakeDeploy::init(void)
{
	m_parent_control->setSolenoidState(state);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSIntakeDeploy::update(void)
{
	return next_step;
}
