/*******************************************************************************
 *
 * File: ColorSensor.cpp -- Analog IR Sensor
 *
 * 
 * As of 08/03/2022, this system has been disable by commenting out the references
 * to the color sensor which I believe lives in a Rev Robotics library that we do not 
 * seem to have at the moment.
 * 
 * 
 *  Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/

#include "2020/ColorSensor.h"


using namespace frc;

#ifdef DISABLED

/******************************************************************************
 *
 ******************************************************************************/
ColorSensor::ColorSensor(void)
{
	  m_colorMatcher.AddColorMatch(BLUE_COLOR);
    m_colorMatcher.AddColorMatch(GREEN_COLOR);
    m_colorMatcher.AddColorMatch(RED_COLOR);
    m_colorMatcher.AddColorMatch(YELLOW_COLOR);


}


/******************************************************************************
 *
 ******************************************************************************/
ColorSensor::~ColorSensor()
{

}

/******************************************************************************
 *
 ******************************************************************************/
void ColorSensor::setThreshold(int threshold)
{
	
}

/******************************************************************************
 *
 ******************************************************************************/
void ColorSensor::setLatchCycles(int cycles)
{

}

/******************************************************************************
 *
 ******************************************************************************/
void ColorSensor::setThresholdType(ColorSensor::ThresholdType type)
{
	
}

/******************************************************************************
 *
 ******************************************************************************/
void ColorSensor::update(Color &detectedColor, double &confidence,std::string &colorString)
{
	/**
     * The method GetColor() returns a normalized color value from the sensor and can be
     * useful if outputting the color to an RGB LED or similar. To
     * read the raw color, use GetRawColor().
     * 
     * The color sensor works best when within a few inches from an object in
     * well lit conditions (the built in LED is a big help here!). The farther
     * an object is the more light from the surroundings will bleed into the 
     * measurements and make it difficult to accurately determine its color.
     */
     detectedColor = m_colorSensor.GetColor();

    /**
     * Run the color match algorithm on our detected color
     */
    confidence = 0.0;
    Color matchedColor = m_colorMatcher.MatchClosestColor(detectedColor, confidence);

    if (matchedColor == BLUE_COLOR) {
      colorString = "Blue";
    } else if (matchedColor == RED_COLOR) {
      colorString = "Red";
    } else if (matchedColor == GREEN_COLOR) {
      colorString = "Green";
    } else if (matchedColor == YELLOW_COLOR) {
      colorString = "Yellow";
    } else {
      colorString = "Unknown";
    }

}
#endif
