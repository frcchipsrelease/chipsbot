/*******************************************************************************
 *
 * File: HangControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/MacroStepFactory.h"

#include "rfcontrols/HangControl.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control, feedback, and PID 
 * and connect them to the specified inputs
 * clo
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 ******************************************************************************/
HangControl::HangControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	Advisory::pinfo("========================= Creating Hang Motor [%s] =========================", 
			control_name.c_str());
	
	motor = nullptr;

	m_upper_limit_sw = nullptr;
	m_lower_limit_sw = nullptr;

	m_upper_limit_pressed = false;
	m_lower_limit_pressed = false;
	reset_on_limit_press = true;

	hang_up_power = 0.5;
	hang_down_power = -0.5;
	hang_up_step = 0.01;
	hang_down_step = 0.01;

	m_servo = nullptr;
	m_servo_step_size = 5.0;
	m_servo_target_position = 0.0;
	m_servo_release_position = 35.0;
	m_servo_engage_position = 65.0;

	increment_step = 0.0;
	decrement_step = 0.0;
	delta_position = 0.01;

	m_is_ready = false;

    m_max_velocity = 0.0;
    m_desired_acceleration = 0.0;
    m_desired_deceleration = 0.0;
    initial_position = 0.0;
    min_position = 0.0;
    max_position = 40000.0;
	m_closed_loop = false;
	raw_position = 0.0;
	raw_velocity = 0.0;
	actual_position = 0.0;
    actual_velocity = 0.0;
	target_power = 0.0;
	target_position = 0.0;
	command_power = 0.0;
	max_power_delta = 0.2;
    setpoint_index = 0;	
	
	for (uint8_t i = 0; i < NUM_SETPOINTS; i++)
	{
		setpoint_name[i] = "unknown";
		setpoint_position[i] = 0.0;
		setpoint_defined[i] = false;
	}

    //
	// Register Macro Steps
	//
	new MacroStepProxy<MSHang>(control_name, "SetPower", this);

    hang_log = new DataLogger("/robot/logs/hang", "hang", "csv", 10, true);

	//
	// Parse the Controls XML
	//
	XMLElement *comp;
	const char *name;

	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);

	xml->QueryFloatAttribute("max_velocity", &m_max_velocity);
	xml->QueryFloatAttribute("desired_acceleration", &m_desired_acceleration);
	xml->QueryFloatAttribute("desired_deceleration", &m_desired_deceleration);
	xml->QueryFloatAttribute("initial_position", &initial_position);
	xml->QueryFloatAttribute("min_position", &min_position);
	xml->QueryFloatAttribute("max_position", &max_position);

	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp!=nullptr)
		{
			int setpoint_index = -1;
			setpoint_comp->QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < HangControl::NUM_SETPOINTS)
			{
				name = setpoint_comp->Attribute("name");
				if (name != nullptr)
				{
					setpoint_name[setpoint_index] = std::string(name);
				}
				else 
				{
					setpoint_name[setpoint_index] = std::string("setpoint_") + std::to_string(setpoint_index);
				}

				setpoint_comp->QueryFloatAttribute("position", &setpoint_position[setpoint_index]);
				setpoint_defined[setpoint_index] = true;

				Advisory::pinfo(" -- setpoint %2d: %20s   position=%7.2f",
				setpoint_index, setpoint_name[setpoint_index].c_str(),
				setpoint_position[setpoint_index]);
			}
			else
			{
				Advisory::pinfo("%s setpoint with index out of range -- %d", getName().c_str(), setpoint_index);
			}
    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}

	comp = xml-> FirstChildElement("motor");
	if (comp != NULL)
	{
		Advisory::pinfo("%s  creating motor controller", getName().c_str());
		motor = HardwareFactory::createMotor(comp);
		motor->setPosition(0.0); // set initial position to known value
		if (m_closed_loop)
		{
			Advisory::pinfo("closed loop mode", getName().c_str());
		}
		else
		{
			Advisory::pinfo("open loop mode", getName().c_str());
		}

		motor->setPosition(0.0); // set initial position to known value
	}
	else
	{
		Advisory::pwarning("%s  WARNING: required element \"motor\" not found", getName().c_str());
	}

	comp = xml->FirstChildElement("servo");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating servo");
		m_servo = HardwareFactory::createServo(comp);
		comp->QueryFloatAttribute("engage_position", &m_servo_engage_position);
		comp->QueryFloatAttribute("release_position", &m_servo_release_position);
		comp->QueryFloatAttribute("step_size", &m_servo_step_size);
	}
	m_servo_target_position = m_servo_release_position;

	comp = xml->FirstChildElement("digital_input");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "upper_limit") == 0)
			{
				Advisory::pinfo("  creating digital input for %s", name);
				m_upper_limit_sw = HardwareFactory::createLimitSwitch(comp);
			}
			else if (strcmp(name, "lower_limit") == 0)
			{
				Advisory::pinfo("  creating digital input for %s", name);
				m_lower_limit_sw = HardwareFactory::createLimitSwitch(comp);
			}
			else
			{
				Advisory::pwarning("  unrecognized digital_input tag for %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found digital_input tag with no name attribute");
		}

		comp = comp->NextSiblingElement("digital_input");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
			if ((strncmp(name, "setpoint", 8) == 0)
				&& (name[8] >= '0') && (name[8] <= '9'))
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8] - '0'));
			}
			else if (strcmp(name, "setpoint_idx") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8] - '0'));
			}
			else if(strcmp(name, "hang_up") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				comp->QueryFloatAttribute("ol_power", &hang_up_power);
				comp->QueryFloatAttribute("cl_step", &hang_up_step);
				OIController::subscribeDigital(comp, this, CMD_HANG_UP);
			}
			else if(strcmp(name, "hang_down") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				comp->QueryFloatAttribute("ol_power", &hang_down_power);
				comp->QueryFloatAttribute("cl_step", &hang_down_step);
				OIController::subscribeDigital(comp, this, CMD_HANG_DOWN);
			}
			else if (strcmp(name, "closed_loop") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if (strcmp(name, "servo_release") == 0)
			{
				Advisory::pinfo("  connecting servo release channel");
				OIController::subscribeDigital(comp, this, CMD_RELEASE);
			}
			 else if (strcmp(name, "servo_engage") == 0)
            {
			     Advisory::pinfo("  connecting servo engage channel");
			     OIController::subscribeDigital(comp, this, CMD_ENGAGE);
			}
			else if (strcmp(name, "servo_state") == 0)
			{
			     Advisory::pinfo("  connecting servo state channel");
			     OIController::subscribeDigital(comp, this, CMD_STATE);
			}
			else
			{
				Advisory::pinfo("%s unknown oi %s", getName().c_str(), name);
			}
		}
		else
		{
			Advisory::pinfo("%s unnamed oi", getName().c_str());
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
HangControl::~HangControl(void)
{
	if (motor != nullptr)
	{
		delete motor;
		motor = nullptr;
	}
	
	if (hang_log != nullptr)
	{
	    delete hang_log;
	    hang_log = nullptr;
	}

	if (m_servo != nullptr)
	{
		delete m_servo;
		m_servo = nullptr;
	}
}

/*******************************************************************************	
 *
 *
 *
 ******************************************************************************/
void HangControl::controlInit(void)
{
    bool is_ready = true;
	
	if(motor == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- motor", getName().c_str());
		is_ready = false;
	}

	if(m_servo == nullptr)
	{
		Advisory::pwarning("%s Hang missing required component -- servo", getName().c_str());
		is_ready = false;
	}

    m_traj_profile.setConfiguration(m_max_velocity, m_desired_acceleration, m_desired_deceleration, this->getPeriod());

	m_is_ready = is_ready;
}

/*******************************************************************************
 *
 *
 *
 ******************************************************************************/
void HangControl::updateConfig(void)
{

}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void HangControl::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void HangControl::setDigital(int id, bool val)
{ 
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_CLOSED_LOOP_STATE:
		{
			setClosedLoop(val);

		} break;

		case CMD_HANG_UP:
		{
			if(val)
			{
				target_power = hang_up_power;
			}
			else
			{
				target_power = 0.0;
			}
			
		} break;

		case CMD_HANG_DOWN:
		{
			if(val)
			{
				target_power = hang_down_power;
			}
			else
			{
				target_power = 0.0;
			}
		} break;

		case CMD_RELEASE:
		{
			if(val)
			{
				m_servo_target_position = m_servo_release_position;
			}
		} break;

		case CMD_ENGAGE:
		{
			if(val)
			{
				m_servo_target_position = m_servo_engage_position;
			}
		} break;

		case CMD_STATE:
		{
			if(val)
			{
				m_servo_target_position = m_servo_release_position;
			}
			else
			{
				m_servo_target_position = m_servo_engage_position;
			}

		} break;

		case CMD_SETPOINT_0: 	applySetpoint(val, 0);  break;
		case CMD_SETPOINT_1: 	applySetpoint(val, 1);  break;
		case CMD_SETPOINT_2: 	applySetpoint(val, 2);  break;
		case CMD_SETPOINT_3: 	applySetpoint(val, 3);  break;
		case CMD_SETPOINT_4: 	applySetpoint(val, 4);  break;
		case CMD_SETPOINT_5: 	applySetpoint(val, 5);  break;
		case CMD_SETPOINT_6: 	applySetpoint(val, 6);  break;
        case CMD_SETPOINT_7:    applySetpoint(val, 7);  break;

		case CMD_INCREMENT_POS:
		{
/*			if (setpoint_index < NUM_SETPOINTS - 1)
			{
				applySetpoint(val, setpoint_index + 1);
			}
			else
			{
				applySetpoint(val, NUM_SETPOINTS - 1);
			}*/
		} break;

		case CMD_DECREMENT_POS:
		{
/*			if (setpoint_index > 0)
			{
				applySetpoint(val, setpoint_index - 1);
			}
			else
			{
				applySetpoint(val, 0);
			}*/
		} break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::subscribeInt, if the XML config
 * specifies an int, the constructor of this object will connect that
 * input to this method.
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the int channel that was subscribed to
 *
 ******************************************************************************/
void HangControl::setInt(int id, int val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	if (id == CMD_SETPOINT_IDX)
	{
		if (val >= 0)
		{
			applySetpoint( true, val/45);
		}
		else
		{
			applySetpoint( false, val);
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::applySetpoint(bool on, int idx)
{
	if (on && (idx >= 0) && (idx < NUM_SETPOINTS))
    {
		if (setpoint_defined[idx] == true)
		{
			target_position = setpoint_position[idx];
			Advisory::pinfo("%s applySetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, target_position);
		}
		else
		{
			Advisory::pinfo("%s applySetpoint: rejected, setpoint not defined for index %d", getName().c_str(), idx);
		}
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::setPosition(float val)
{
	target_position = val;
}

/*******************************************************************************
 *
 ******************************************************************************/
float HangControl::getPostion(void)
{
	return actual_position;
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::disabledInit()
{
    hang_log->close();
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::autonomousInit()
{
	Advisory::pinfo("%s autonomousInit", getName().c_str());
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::teleopInit()
{
    HangControl::initLogFile();
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::testInit()
{
}

/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void HangControl::initLogFile(void)
{
    hang_log->openSegment();

    hang_log->log("%s, %s, %s, %s, %s, %s, ",
                    "current_time",
                    "raw_position", "actual_position", "target_position",
                    "raw_velocity", "actual_velocity");

    hang_log->log("%s, %s",
        "traj_position",
        "traj_velocity");

    hang_log->log("\n");
    hang_log->flush();

} 

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName().c_str() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutBoolean(getName() +" closed loop: ", m_closed_loop);

	SmartDashboard::PutNumber(getName() +" raw_position: ", raw_position);
	SmartDashboard::PutNumber(getName() +" actual velocity: ", actual_velocity);
	SmartDashboard::PutNumber(getName() +" actual position: ", actual_position);
	SmartDashboard::PutNumber(getName() +" target position: ", target_position);
	SmartDashboard::PutNumber(getName() +" command position: ", commanded_position);

	SmartDashboard::PutNumber(getName() +" target power: ", target_power);
	SmartDashboard::PutNumber(getName() +" command power: ", command_power);
	SmartDashboard::PutNumber(getName() +" set point idx: ", setpoint_index);

	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " servo target position: ", m_servo_target_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
void HangControl::setClosedLoop(bool closed)
{
	if (m_closed_loop != closed)
	{
		m_closed_loop = closed;

		if (m_closed_loop)
		{
		    Advisory::pinfo("setting Hang closed loop mode");
			motor->setControlMode(Motor::POSITION);
		}
		else
		{
            Advisory::pinfo("setting Hang open loop mode");
			motor->setControlMode(Motor::PERCENT);
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
bool HangControl::isClosedLoop(void)
{
	return m_closed_loop;
}
/*******************************************************************************
 *
 ******************************************************************************/
bool HangControl::isAtTarget(float tolerance)
{
	return (fabs(actual_position - target_position) < tolerance);
}
/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void HangControl::doPeriodic()
{
	if(m_is_ready == false)
	{
		Advisory::pinfo("DoPeriodic is not ready");
		return;
	}

	double current_time = gsi::Time::getTime();
	motor->doUpdate();

	// 
	// Get Sensor Inputs
	//
	m_upper_limit_pressed = motor->isUpperLimitPressed();
	m_lower_limit_pressed = motor->isLowerLimitPressed();

	command_power = motor->getPercent();

	raw_position = motor->getRawPosition();
	raw_velocity = motor->getRawVelocity();
	actual_position = motor->getPosition();
	actual_velocity = motor->getVelocity();

	//
	// Make sure the motor doesn't jump when enabled
	//
	if (getPhase() == DISABLED)
	{
		target_position = actual_position;
		target_power = 0.0;

		m_traj_profile.setInitialPosition(target_position);

		m_servo_target_position = m_servo_release_position;
	}

	//
	// Do some calculations
	//
	m_servo_target_position = RobotUtil::limit(m_servo_release_position, m_servo_engage_position,
			m_servo_target_position);

	// motor should not be moving in up direction
	if (m_servo_target_position != m_servo_release_position)
	{
        if(target_power > 0)
        {
            target_position = actual_position;
            target_power = 0.0;
        }
	}

	//
	// Set the outputs
	//
	m_servo->SetAngle(m_servo_target_position);

	if (m_closed_loop)
	{
		if (m_lower_limit_pressed == true)
		{
			if (reset_on_limit_press == true)
			{
				actual_position = 0.0;
				m_traj_profile.setInitialPosition(target_position);
			}

			reset_on_limit_press = false;
		}
		else
		{
			if (actual_position > 1.0)
			{
				reset_on_limit_press = true;
			}
		}

		target_position = RobotUtil::limit(min_position, max_position, target_position); 
	//m_traj_profile.setTargetPosition(target_position);
	//	commanded_position = m_traj_profile.update();
		motor->setPosition(target_position);
	}
	else
	{
		// make sure transition to closed loop doesn't cause a jump
		target_position = actual_position;
        if (target_power < 0.0 && actual_position <= min_position)
        {
            target_power = 0.0;
        }
       if (target_power > 0.0 && actual_position >= max_position)
        {
            target_power = 0.0;
        }

        motor->setPercent(target_power);        
	}

	hang_log->log("%6.8f, %6.8f, %6.8f, %6.8f, %6.8f, %6.8f, %6.8f, %6.8f, %6.8f",
	                current_time,
	                raw_position, actual_position, target_position,
	                raw_velocity, actual_velocity,
					m_upper_limit_pressed, m_lower_limit_pressed,
					target_power, command_power);

    hang_log->log("%6.8f, %6.8f\n",
        m_traj_profile.getTrajectoryPosition(),
        m_traj_profile.getTrajectoryVelocity());
}

/*******************************************************************************
 *
 ******************************************************************************/
MSHang::MSHang(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_power = 0.0;
	m_wait = false;
	setpoint_index = 7;
	m_closed_loop = false;
	
	m_parent_control = (HangControl *)control;
	xml->QueryFloatAttribute("power", &m_power);
	xml->QueryIntAttribute("setpoint", &setpoint_index);
	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);
	xml->QueryBoolAttribute("wait", &m_wait);


}

/*******************************************************************************
 *
 ******************************************************************************/
void MSHang::init(void)
{
	m_parent_control->setClosedLoop(m_closed_loop);
	if (setpoint_index < NUM_SETPOINTS - 1)
	{
		m_parent_control->applySetpoint(setpoint_index, setpoint_index + 1);
	}
	else
	{
		m_parent_control->applySetpoint(setpoint_index, NUM_SETPOINTS - 1);
	}	
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSHang::update(void)
{
    if(m_wait == true)
	{
	    if(!m_parent_control->isAtTarget(1.0))
	 	{
			return this;
	 	}
	}

	return next_step;
}
