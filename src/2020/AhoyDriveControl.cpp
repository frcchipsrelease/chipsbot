/*******************************************************************************
 *
 * File: AhoyDriveControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "2020/AhoyDriveControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/RobotUtil.h"

#include "gsutilities/Advisory.h"

#include "gsinterfaces/Time.h"

#include "frc/smartDashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 * *****************************************************************************/
AhoyDriveControl::AhoyDriveControl(std::string control_name, XMLElement* xml)
	:	PeriodicControl(control_name)
{
    Advisory::pinfo("========================= Creating Ahoy Drive Control [%s] =========================",
                control_name.c_str());
	XMLElement *comp;

	const char *name;

    m_data_logger = new DataLogger("/robot/logs", "drive", "csv", 50, true);

	is_limelight_control = false;
	m_limelight = nullptr;

	fl_drive_motor = nullptr;
	fr_drive_motor = nullptr;
	bl_drive_motor = nullptr;
	br_drive_motor = nullptr;

    m_accelerometer = nullptr;
    m_accelerometer_x = 0.0;
    m_accelerometer_y = 0.0;
    m_accelerometer_z = 0.0;

#ifndef WIN32
#if 0  // remove IMU support for 2022

    m_imu = nullptr;
    m_imu_accel_x = 0.0;
    m_imu_accel_y = 0.0;
    m_imu_accel_z = 0.0;

    m_imu_mag_x = 0.0;
    m_imu_mag_y = 0.0;
    m_imu_mag_z = 0.0;

    m_imu_rate_x = 0.0;
    m_imu_rate_y = 0.0;
    m_imu_rate_z = 0.0;

    m_imu_angle_x = 0.0;
    m_imu_angle_y = 0.0;
    m_imu_angle_z = 0.0;

    m_imu_roll  = 0.0;
    m_imu_pitch = 0.0;
    m_imu_yaw   = 0.0;

    m_imu_quat_w = 0.0;
    m_imu_quat_x = 0.0;
    m_imu_quat_y = 0.0;
    m_imu_quat_z = 0.0;

    m_imu_bar_press   = 0.0;
    m_imu_temperature = 0.0;
#endif  // #if 0    
#endif  // WIN32

	gear_solenoid = nullptr;
	brake_solenoid = nullptr;

	brake_engaged = false;
	brake_solenoid_invert = false;

	gear_high = false;
	gear_solenoid_invert = false;

	fl_drive_motor_cmd = 0.0;
	fr_drive_motor_cmd = 0.0;
	bl_drive_motor_cmd = 0.0;
	br_drive_motor_cmd = 0.0;

	trn_power = 0.0;
	fwd_power = 0.0;
	strafe_power = 0.0;

	m_low_power_scale = 0.5;
	m_low_power_active = false;
	
	name = xml->Attribute("name");
	if (name == nullptr)
	{
		name="drive";
		Advisory::pcaution(  "WARNING: Ahoy Drive Control created without name, assuming \"%s\"", name);
	}

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSDriveAhoyDrivePower>(control_name, "DrivePower", this);

	xml->QueryFloatAttribute("low_power_scale", &m_low_power_scale);
	xml->QueryBoolAttribute("limelight_control", &is_limelight_control);

	//
	// Parse the XML
	//
	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "front_left") == 0)
			{				
				Advisory::pinfo("  creating speed controller for %s", name);
                fl_drive_motor = HardwareFactory::createMotor(comp);
				//fl_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
			else if (strcmp(name, "front_right") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                fr_drive_motor = HardwareFactory::createMotor(comp);
				//fr_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
			else if (strcmp(name, "back_left") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                bl_drive_motor = HardwareFactory::createMotor(comp);
				//bl_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
			else if (strcmp(name, "back_right") == 0)
			{
				Advisory::pinfo("  creating speed controller for %s", name);
                br_drive_motor = HardwareFactory::createMotor(comp);
				//br_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
		}
		comp = comp->NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("solenoid");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "brake") == 0)
			{				
				Advisory::pinfo("  creating solenoid for %s", name);
                brake_solenoid = HardwareFactory::createSolenoid(comp);
				brake_solenoid_invert = comp->BoolAttribute("invert");
			}
			else if (strcmp(name, "gear") == 0)
			{
				Advisory::pinfo("  creating solenoid for %s", name);
                gear_solenoid = HardwareFactory::createSolenoid(comp);
				gear_solenoid_invert = comp->BoolAttribute("invert");
			}
		}
		comp = comp->NextSiblingElement("solenoid");
	}

    comp = xml-> FirstChildElement("accelerometer");
    if (comp != nullptr)
    {
        // if type built_in then
        Advisory::pinfo("  creating accelerometer");
        m_accelerometer = new BuiltInAccelerometer();
    }

#ifndef WIN32
#if 0  // remove IMU support for 2022

    comp = xml-> FirstChildElement("imu");
    if (comp != nullptr)
    {
        // if type ADIS16448 then
        Advisory::pinfo("  creating imu");
        m_imu = new ADIS16448_IMU();
    }
#endif // #if 0    
#endif


 comp = xml-> FirstChildElement("limelight");
	if (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			m_limelight = HardwareFactory::createLimelight(comp);
			Advisory::pinfo("  Created limelight %s", name);
		}
//		comp->QueryFloatAttribute("target_height", &m_targetHeight);
//		Advisory::pinfo("  Target Height: %f", m_targetHeight);
//		comp->QueryFloatAttribute("limelight_height", &m_limelightHeight);
//		Advisory::pinfo("  Limelight Height: %f", m_limelightHeight);
//		comp->QueryFloatAttribute("calibrated_distance", &m_calibratedDistance);
//		Advisory::pinfo("  Calibrated Distance: %f", m_calibratedDistance);
	}

   comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting to forward channel");
				OIController::subscribeAnalog(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "turn") == 0)
			{
				Advisory::pinfo("  connecting to turn channel");
				OIController::subscribeAnalog(comp, this, CMD_TURN);
			}
			else if (strcmp(name, "strafe") == 0)
			{
				Advisory::pinfo("  connecting to strafe channel");
				OIController::subscribeAnalog(comp, this, CMD_STRAFE);
			}
			else if (strcmp(name, "low_power_on") == 0)
			{
				Advisory::pinfo("  connecting to low_power on channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_ON);
			}
			else if (strcmp(name, "low_power_off") == 0)
			{
				Advisory::pinfo("  connecting to low_power off channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_OFF);
			}
			else if (strcmp(name, "low_power_toggle") == 0)
			{
				Advisory::pinfo("  connecting to low_power toggle channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_TOGGLE);
			}
			else if (strcmp(name, "low_power_state") == 0)
			{
				Advisory::pinfo("  connecting to low_power state channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_STATE);
			}
			else if (strcmp(name, "brake_on") == 0)
			{
				Advisory::pinfo("  connecting to brake_on channel");
				OIController::subscribeDigital(comp, this, CMD_BRAKE_ON);
			}
			else if (strcmp(name, "brake_off") == 0)
			{
				Advisory::pinfo("  connecting to brake_off channel");
				OIController::subscribeDigital(comp, this, CMD_BRAKE_OFF);
			}
			else if (strcmp(name, "brake_toggle") == 0)
			{
				Advisory::pinfo("  connecting to brake_toggle channel");
				OIController::subscribeDigital(comp, this, CMD_BRAKE_TOGGLE);
			}
			else if (strcmp(name, "brake_state") == 0)
			{
				Advisory::pinfo("  connecting to brake_state channel");
				OIController::subscribeDigital(comp, this, CMD_BRAKE_STATE);
			}
			else if (strcmp(name, "gear_high") == 0)
			{
				Advisory::pinfo("  connecting to gear_high channel");
				OIController::subscribeDigital(comp, this, CMD_GEAR_HIGH);
			}
			else if (strcmp(name, "gear_low") == 0)
			{
				Advisory::pinfo("  connecting to gear_low channel");
				OIController::subscribeDigital(comp, this, CMD_GEAR_LOW);
			}
			else if (strcmp(name, "gear_toggle") == 0)
			{
				Advisory::pinfo("  connecting to gear_toggle channel");
				OIController::subscribeDigital(comp, this, CMD_GEAR_TOGGLE);
			}
			else if (strcmp(name, "gear_state") == 0)
			{
				Advisory::pinfo("  connecting to gear_state channel");
				OIController::subscribeDigital(comp, this, CMD_GEAR_STATE);
			}
			else 
			{
				Advisory::pwarning("  unrecognized oi detected: \"%s\"", name);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 *
 * Release any resources used by this object
 * 
 ******************************************************************************/
AhoyDriveControl::~AhoyDriveControl(void)
{
	if (fl_drive_motor != nullptr)
	{
		delete fl_drive_motor;
		fl_drive_motor = nullptr;
	}
	
	if (fr_drive_motor != nullptr)
	{
		delete fr_drive_motor;
		fr_drive_motor = nullptr;
	}
	
	if (bl_drive_motor != nullptr)
	{
		delete bl_drive_motor;
		bl_drive_motor = nullptr;
	}
	
	if (br_drive_motor != nullptr)
	{
		delete br_drive_motor;
		br_drive_motor = nullptr;
	}

	if (gear_solenoid != nullptr)
	{
		delete gear_solenoid;
		gear_solenoid = nullptr;
	}
	
	if (brake_solenoid != nullptr)
	{
		delete brake_solenoid;
		brake_solenoid = nullptr;
	}
	
	if (m_accelerometer != nullptr)
	{
		delete m_accelerometer;
		m_accelerometer = nullptr;
	}

	if (m_imu != nullptr)
	{
		delete m_imu;
		m_imu = nullptr;
	}
}

/*******************************************************************************	
 *
 ******************************************************************************/
void AhoyDriveControl::controlInit()
{
	fwd_power = 0.0;
	trn_power = 0.0;
	strafe_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void AhoyDriveControl::disabledInit()
{
    m_data_logger->close();

	fwd_power = 0.0;
	trn_power = 0.0;
	strafe_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void AhoyDriveControl::autonomousInit()
{
    logFileInit("Auton");

    fwd_power = 0.0;
	trn_power = 0.0;
	strafe_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void AhoyDriveControl::teleopInit()
{
    logFileInit("Teleop");

    fwd_power = 0.0;
	trn_power = 0.0;
	strafe_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void AhoyDriveControl::testInit()
{
    logFileInit("Test");

    fwd_power = 0.0;
	trn_power = 0.0;
	strafe_power = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void AhoyDriveControl::setInt(int id, int val)
{
}

/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 * 
 * Handled command ids are CMD_TURN and CMD_FORWARD, all others are ignored
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void AhoyDriveControl::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_TURN:
     	{
			trn_power = val;
		} break;

		case CMD_FORWARD:
		{
			fwd_power = val;
		} break;

		case CMD_STRAFE:
		{
			strafe_power = val;
		} break;
	}
}

/*******************************************************************************	
 *
 * Sets the state of the control based on the command id and value
 *  
 * Handled command ids are CMD_BRAKE_[ON|OFF|TOGGLE|STATE] and 
 * CMD_GEAR_[HIGH|LOW|TOGGLE|STATE], all others are ignored
 *
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void AhoyDriveControl::setDigital(int id, bool button_pressed)
{
	switch(id)
	{
		case CMD_BRAKE_ON:
		{
			if (button_pressed) brake_engaged = true;
		} break;
			
		case CMD_BRAKE_OFF:
		{
			if (button_pressed) brake_engaged = false;
		} break;
			
		case CMD_BRAKE_TOGGLE:
		{
			if (button_pressed) brake_engaged = !brake_engaged;
		} break;
			
		case CMD_BRAKE_STATE:
		{
            brake_engaged = button_pressed;
		} break;
			
		case CMD_GEAR_HIGH:
		{
			if (button_pressed) gear_high = true;
		} break;
			
		case CMD_GEAR_LOW:
		{
			if (button_pressed) gear_high = false;
		} break;
			
		case CMD_GEAR_TOGGLE:
		{
            if (button_pressed) gear_high = !gear_high;
		} break;
			
		case CMD_GEAR_STATE:
		{
			gear_high = button_pressed;
		} break;

		case CMD_LOW_POWER_ON:
		{
			if (button_pressed) m_low_power_active = true;
		} break;

		case CMD_LOW_POWER_OFF:
		{
			if (button_pressed) m_low_power_active = false;
		} break;

		case CMD_LOW_POWER_TOGGLE:
		{
			if (button_pressed) m_low_power_active = !m_low_power_active;
		} break;

		case CMD_LOW_POWER_STATE:
		{
			m_low_power_active = button_pressed;
		} break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void AhoyDriveControl::publish()
{
//	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
//	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() +" forward: ", fwd_power);
	SmartDashboard::PutNumber(getName() +" turn: ", trn_power);
	SmartDashboard::PutNumber(getName() +" strafe: ", strafe_power);

	if (gear_solenoid != nullptr)
	{
		SmartDashboard::PutBoolean(getName() +" gear: ", gear_high);
	}

	if (brake_solenoid != nullptr)
	{
		SmartDashboard::PutBoolean(getName() +" brake: ", brake_engaged);
	}

	if (fl_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" fl cmd: ", fl_drive_motor_cmd);
	}

	if (fr_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" fr cmd: ", fr_drive_motor_cmd);
	}

	if (bl_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" bl cmd: ", bl_drive_motor_cmd);
	}

	if (br_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" br cmd: ", br_drive_motor_cmd);
	}

    if (m_accelerometer != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " accel_x: ", m_accelerometer_x);
        SmartDashboard::PutNumber(getName() + " accel_y: ", m_accelerometer_y);
        SmartDashboard::PutNumber(getName() + " accel_z: ", m_accelerometer_z);
    }

#ifndef WIN32
#if 0  // remove IMU support for this 2022
    if(m_imu != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " IMU Accel X: ", m_imu->GetAccelX()); // m_accel_x -- from sensor
        SmartDashboard::PutNumber(getName() + " IMU Accel Y: ", m_imu->GetAccelY());
        SmartDashboard::PutNumber(getName() + " IMU Accel Z: ", m_imu->GetAccelZ());

        SmartDashboard::PutNumber(getName() + " IMU Mag X: ", m_imu->GetMagX());  // m_mag_x  -- from sensor
        SmartDashboard::PutNumber(getName() + " IMU Mag Y: ", m_imu->GetMagY());
        SmartDashboard::PutNumber(getName() + " IMU Mag Z: ", m_imu->GetMagZ());

        SmartDashboard::PutNumber(getName() + " IMU Rate X: ", m_imu->GetRateX()); // m_gyro_x  -- from sensor
        SmartDashboard::PutNumber(getName() + " IMU Rate Y: ", m_imu->GetRateY());
        SmartDashboard::PutNumber(getName() + " IMU Rate Z: ", m_imu->GetRateZ());

        SmartDashboard::PutNumber(getName() + " IMU Angle X: ", m_imu->GetAngleX());  // m_integ_gyro_x, m_integ_gyro_x += (gyro_x - m_gyro_offset_x) * dt;
        SmartDashboard::PutNumber(getName() + " IMU Angle Y: ", m_imu->GetAngleY());
        SmartDashboard::PutNumber(getName() + " IMU Angle Z: ", m_imu->GetAngleZ());

        SmartDashboard::PutNumber(getName() + " IMU Roll: ",  m_imu->GetRoll()); // calculated
        SmartDashboard::PutNumber(getName() + " IMU Pitch: ", m_imu->GetPitch());
        SmartDashboard::PutNumber(getName() + " IMU Yaw: ",   m_imu->GetYaw());

        SmartDashboard::PutNumber(getName() + " IMU Quat W: ", m_imu->GetQuaternionW());  // calculated
        SmartDashboard::PutNumber(getName() + " IMU Quat X: ", m_imu->GetQuaternionX());
        SmartDashboard::PutNumber(getName() + " IMU Quat Y: ", m_imu->GetQuaternionY());
        SmartDashboard::PutNumber(getName() + " IMU Quat Z: ", m_imu->GetQuaternionZ());

        SmartDashboard::PutNumber(getName() + " IMU Bar Press: ",   m_imu->GetBarometricPressure());
        SmartDashboard::PutNumber(getName() + " IMU Temperature: ", m_imu->GetTemperature());

		SmartDashboard::PutNumber(getName() + " Limelight Horizontal: ", m_targetOffsetAngle_Horizontal);
		SmartDashboard::PutNumber(getName() + " Limelight Vertical: ", m_targetOffsetAngle_Vertical);
		SmartDashboard::PutNumber(getName() + " Limelight Area: ", m_targetArea);
		SmartDashboard::PutNumber(getName() + " Limelight Skew: ", m_targetSkew);
		SmartDashboard::PutNumber(getName() + " Limelight Distance Offset: ", m_distanceOffset);

    }
#endif // #if 0
#endif //  WIN32
}

/*******************************************************************************
 *
 ******************************************************************************/
void AhoyDriveControl::logFileInit(std::string phase)
{
    Advisory::pinfo("initializing log file for %s", phase.c_str());

    m_data_logger->openSegment();

    m_data_logger->log("%s, %s, %s, ",
        "time", "phase", "pet"
    );

    m_data_logger->log("%s, %s, %s,  %s, %s, %s, %s,   %s, %s, ",
        "fwd_power", "trn_power", "strafe_power",
        "fl_drive_motor_cmd", "fr_drive_motor_cmd", "bl_drive_motor_cmd", "br_drive_motor_cmd",
        "brake_engaged", "gear_high");

    if (m_accelerometer != nullptr)
    {
        m_data_logger->log("%s, %s, %s, ",
            "m_accelerometer_x", "m_accelerometer_y", "m_accelerometer_z");
    }

    if (m_imu!= nullptr)
    {
        m_data_logger->log("%s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s, %s,     %s, %s ",
            "m_imu_accel_x", "m_imu_accel_y", "m_imu_accel_z",
            "m_imu_mag_x", "m_imu_mag_y", "m_imu_mag_z",
            "m_imu_rate_x", "m_imu_rate_y", "m_imu_rate_z",
            "m_imu_angle_x", "m_imu_angle_y", "m_imu_angle_z",
            "m_imu_roll", "m_imu_pitch", "m_imu_yaw",
            "m_imu_quat_w", "m_imu_quat_x", "m_imu_quat_y", "m_imu_quat_z",
            "m_imu_bar_press", "m_imu_temperature");
    }

    m_data_logger->log("\n");

    m_data_logger->flush();
}

/*******************************************************************************
 *
 ******************************************************************************/
void AhoyDriveControl::logFileAppend(void)
{
    m_data_logger->log("%f, %d, %f, ",
        gsi::Time::getTime(), this->getPhase(), this->getPhaseElapsedTime());

    m_data_logger->log("%f, %f, %f,  %f, %f, %f, %f,   %d, %d, ",
        fwd_power, trn_power, strafe_power, 
        fl_drive_motor_cmd, fr_drive_motor_cmd, bl_drive_motor_cmd,br_drive_motor_cmd,
        brake_engaged, gear_high);

    if (m_accelerometer != nullptr)
    {
        m_data_logger->log("%f, %f, %f, ",
            m_accelerometer_x, m_accelerometer_y, m_accelerometer_z);
    }
		
#ifndef WIN32
#if 0  // remove IMU support for this simple control.  Doesn't need it

    if (m_imu!= nullptr)
    {
        m_data_logger->log("%f, %f, %f,   %f, %f, %f,   %f, %f, %f,   %f, %f, %f,   %f, %f, %f,   %f, %f, %f, %f,     %f, %f ",
            m_imu_accel_x, m_imu_accel_y, m_imu_accel_z,
            m_imu_mag_x, m_imu_mag_y, m_imu_mag_z,
            m_imu_rate_x, m_imu_rate_y, m_imu_rate_z,
            m_imu_angle_x, m_imu_angle_y, m_imu_angle_z,
            m_imu_roll, m_imu_pitch, m_imu_yaw,
            m_imu_quat_w, m_imu_quat_x, m_imu_quat_y, m_imu_quat_z,
            m_imu_bar_press, m_imu_temperature);
    }
#endif  // #if 0
#endif

    m_data_logger->log("\n");
}

/*******************************************************************************	
 * 
 * Sets the actuator values every period
 * 
 ******************************************************************************/
void AhoyDriveControl::doPeriodic()
{
	//
	// Read Sensors
	//
    if (m_accelerometer != nullptr)
    {
        m_accelerometer_x = m_accelerometer->GetX();
        m_accelerometer_y = m_accelerometer->GetY();
        m_accelerometer_z = m_accelerometer->GetZ();
    }

 #ifndef WIN32
#if 0 // remove IMU support for 2022
   if (m_imu != nullptr)
    {
        m_imu_accel_x      = m_imu->GetAccelX();
        m_imu_accel_y      = m_imu->GetAccelY();
        m_imu_accel_z      = m_imu->GetAccelZ();

        m_imu_mag_x        = m_imu->GetMagX();
        m_imu_mag_y        = m_imu->GetMagY();
        m_imu_mag_z        = m_imu->GetMagZ();

        m_imu_rate_x       = m_imu->GetRateX();
        m_imu_rate_y       = m_imu->GetRateY();
        m_imu_rate_z       = m_imu->GetRateZ();

        m_imu_angle_x      = m_imu->GetAngleX();
        m_imu_angle_y      = m_imu->GetAngleY();
        m_imu_angle_z      = m_imu->GetAngleZ();

        m_imu_roll         = m_imu->GetRoll();
        m_imu_pitch        = m_imu->GetPitch();
        m_imu_yaw          = m_imu->GetYaw();

        m_imu_quat_w       = m_imu->GetQuaternionW();
        m_imu_quat_x       = m_imu->GetQuaternionX();
        m_imu_quat_y       = m_imu->GetQuaternionY();
        m_imu_quat_z       = m_imu->GetQuaternionZ();

        m_imu_bar_press    = m_imu->GetBarometricPressure();
        m_imu_temperature  = m_imu->GetTemperature();
    }
#endif // #if 0
#endif


	if (is_limelight_control)
	{
		if(m_limelight != nullptr)
		{
	m_limelight->getData(m_target_visible,
						 m_targetOffsetAngle_Horizontal,
						 m_targetOffsetAngle_Vertical,
				 		 m_targetArea,
		 				 m_targetSkew);

			// d = (h2-h1)/tan(a1+a2)
/*			if (fabs(m_targetOffsetAngle_Vertical) > 0.1)
			{
				m_distanceOffset = m_calibratedDistance - m_heightDifference/tan(m_targetOffsetAngle_Vertical*3.14159265/180.0);
				target_velocity = pwl01.evaluate(m_distanceOffset);
				motor->setVelocity(target_velocity);
				Advisory::pinfo("targetVelocity:: %.2f", target_velocity);
			}
				else
				{
					m_distanceOffset = 0;
					target_velocity = pwl01.evaluate(m_distanceOffset);
					motor->setVelocity(target_velocity);				
					Advisory::pinfo("targetVelocity:: %.2f", target_velocity);
				}
			}*/
		}
	}
	else
	{
			
	}

	//
	// Calculate Values
	//
	fl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power - strafe_power);
	fr_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power + strafe_power);
	bl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power + strafe_power);
	br_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power - strafe_power);

    // Normalize the drive command values
	float normalize_max = fabs(fl_drive_motor_cmd);
	if (fabs(fr_drive_motor_cmd) > normalize_max) { normalize_max = fabs(fr_drive_motor_cmd); }
	if (fabs(bl_drive_motor_cmd) > normalize_max) { normalize_max = fabs(bl_drive_motor_cmd); }
	if (fabs(br_drive_motor_cmd) > normalize_max) { normalize_max = fabs(br_drive_motor_cmd); }

    if (normalize_max > 1.0)
	{
		fl_drive_motor_cmd /= normalize_max;
		fr_drive_motor_cmd /= normalize_max;
	    bl_drive_motor_cmd /= normalize_max;
        br_drive_motor_cmd /= normalize_max;
	}

    // apply low power factor
	if (m_low_power_active)
	{
		fl_drive_motor_cmd *= m_low_power_scale;
		fr_drive_motor_cmd *= m_low_power_scale;
	    bl_drive_motor_cmd *= m_low_power_scale;
        br_drive_motor_cmd *= m_low_power_scale;
	}

	// don't drive motors against the brake;
	if (brake_engaged)
	{
		fl_drive_motor_cmd = 0.0;
		fr_drive_motor_cmd = 0.0;
	    bl_drive_motor_cmd = 0.0;
        br_drive_motor_cmd = 0.0;
	}

	//
	// Set Outputs
	//
	if (brake_solenoid != nullptr)
	{
		brake_solenoid->Set(brake_engaged != brake_solenoid_invert);
	}

	if (gear_solenoid != nullptr)
	{
		gear_solenoid->Set(gear_high != gear_solenoid_invert);
	}
	
	if (fl_drive_motor != nullptr)
	{
		fl_drive_motor->setPercent(fl_drive_motor_cmd);
		fl_drive_motor->doUpdate();
	}
	
	if (fr_drive_motor != nullptr)
	{
		fr_drive_motor->setPercent(fr_drive_motor_cmd);
		fr_drive_motor->doUpdate();
	}
	
	if (bl_drive_motor != nullptr)
	{
		bl_drive_motor->setPercent(bl_drive_motor_cmd);
		bl_drive_motor->doUpdate();
	}
	
	if (br_drive_motor != nullptr)
	{
		br_drive_motor->setPercent(br_drive_motor_cmd);
		br_drive_motor->doUpdate();
	}	

	logFileAppend();
}


// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 ******************************************************************************/
MSDriveAhoyDrivePower::MSDriveAhoyDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (AhoyDriveControl *)control;

	forward_power = xml->FloatAttribute("forward");
	turn_power = xml->FloatAttribute("turn");
	strafe_power = xml->FloatAttribute("strafe");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveAhoyDrivePower::init(void)
{
	parent_control->setAnalog(AhoyDriveControl::CMD_FORWARD, forward_power);
	parent_control->setAnalog(AhoyDriveControl::CMD_TURN, turn_power);
	parent_control->setAnalog(AhoyDriveControl::CMD_STRAFE, strafe_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSDriveAhoyDrivePower::update(void)
{
	return next_step;
}
