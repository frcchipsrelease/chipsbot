/*******************************************************************************
 *
 * File: ChipsBase.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "ChipsBase.h"

#include "rfcontrols/IntakeControl.h"
#include "rfcontrols/ControlFactory.h"
#include "2020/AhoyDriveControl.h"
#include "rfcontrols/SimpleArcadeDriveControl.h"
#include "rfcontrols/KiwiDriveControl.h"
#include "rfcontrols/CameraServerControl.h"
#include "rfcontrols/CompressorControl.h"
#include "rfcontrols/MotorControl.h"
#include "rfcontrols/HopperControl.h"
#include "rfcontrols/LimelightControl.h"
#include "rfcontrols/HangControl.h"
#include "rfcontrols/ShooterControl.h"
#include "rfcontrols/RelayControl.h"
#include "rfcontrols/ServoControl.h"
#include "rfcontrols/SolenoidControl.h"
#include "rfcontrols/GameDataControl.h"
#include "rfcontrols/PositionJointControl.h"
#include "rfcontrols/VelocityJointControl.h"

#include "rfhardware/Cancoder.h"

#include "rfutilities/Macro.h"
#include "rfutilities/MacroController.h"
#include "rfutilities/OIController.h"

#include "gsutilities/tinyxml2.h"
#include "gsutilities/Advisory.h"
#include "gsutilities/AdvisoryFile.h"
#include "gsutilities/AdvisoryStdOut.h"
#include "gsutilities/SegmentedFile.h"

#include "frc/liveWindow/LiveWindow.h"

#include <stdlib.h>
#include <string>
#include <iostream>


using namespace tinyxml2;
using namespace gsu;
using namespace frc;

/******************************************************************************
 *
 * Creates a new instance of the robot and initializes some variables
 * 
 * The majority of the setup work happens in RobotInit()
 *
 ******************************************************************************/
ChipsBase::ChipsBase(void)
{
    Advisory::pinfo("Robot Constructor");

    m_macro_controller = nullptr;

    m_auton_file_count        = 0;
    m_auton_select            = 0;
    m_auton_select_mismatch   = -1;
    m_auton_select_loaded     = -1;
    m_auton_enabled           = true;
    m_auton_delay             = 0.0;
    m_auton_delay_enabled     = false;
    m_auton_start_time        = 0.0;
    m_auton_started           = false;
    m_auton_description       = "";

    m_publish_count = 0;

    m_robot_phase = INIT;
}

/******************************************************************************
 *
 * Deletes any controls and other resources allocated by this robot
 *
 ******************************************************************************/
ChipsBase::~ChipsBase()
{
    Advisory::pinfo("Robot Destructor");

	for (ControlThread *control :  m_controls )
	{
		delete control;
	}
	m_controls.clear();

	if (m_macro_controller != nullptr)
	{
		delete m_macro_controller;
		m_macro_controller = nullptr;
	}
}

/******************************************************************************
 *
 * Register the controls that can be used by this robot
 *
 ******************************************************************************/
void ChipsBase::RegisterControls(void)
{
    Advisory::pinfo("Robot Register Controls");

    ControlFactory::registerProxy("arcade_drive",       new ControlProxy<AhoyDriveControl>());
    ControlFactory::registerProxy("simple_arcade_drive",       new ControlProxy<SimpleArcadeDriveControl>());
    ControlFactory::registerProxy("kiwi_drive",         new ControlProxy<KiwiDriveControl>());
    ControlFactory::registerProxy("camera_server",      new ControlProxy<CameraServerControl>());
    ControlFactory::registerProxy("compressor",         new ControlProxy<CompressorControl>());
    ControlFactory::registerProxy("motor",              new ControlProxy<MotorControl>());
    ControlFactory::registerProxy("intake",             new ControlProxy<IntakeControl>()); 
    ControlFactory::registerProxy("HopperControl",      new ControlProxy<HopperControl>());
    ControlFactory::registerProxy("hang",               new ControlProxy<HangControl>());
    ControlFactory::registerProxy("limelight",          new ControlProxy<LimelightControl>());
    ControlFactory::registerProxy("Shooter",     new ControlProxy<ShooterControl>());
    ControlFactory::registerProxy("solenoid",           new ControlProxy<SolenoidControl>());
    ControlFactory::registerProxy("relay",              new ControlProxy<RelayControl>());
    ControlFactory::registerProxy("servo",              new ControlProxy<ServoControl>());
    ControlFactory::registerProxy("game_data",          new ControlProxy<GameDataControl>());
    ControlFactory::registerProxy("position_joint",     new ControlProxy<PositionJointControl>()); 
    ControlFactory::registerProxy("velocity_joint",     new ControlProxy<VelocityJointControl>()); 
}

/******************************************************************************
 *
 * Initializes the robot and controls
 *
 ******************************************************************************/
void ChipsBase::RobotInit(void)
{
#ifndef WIN32
    LiveWindow::DisableAllTelemetry();
#endif

    Advisory::addObserver(new AdvisoryStdOut());
    Advisory::addObserver(new AdvisoryFile(SegmentedFile("/robot/logs/advisories", "message", "txt", 10).openNextSegment()));

    Advisory::pinfo("Robot RobotInit()");

    m_macro_controller = new MacroController();
    m_macro_controller->start();

    LoadRobot("/robot/config/RobotControl.xml");

	for (ControlThread *control :  m_controls )
	{
		control->doControlInit();
	}

	for (ControlThread *control :  m_controls )
	{
		control->start();
	}
}

/******************************************************************************
 *
 * Loads the XML file and calls each class accordingly
 *
 ******************************************************************************/
void ChipsBase::LoadRobot(std::string file)
{
	XMLDocument doc;
	XMLError err;
	err = doc.LoadFile(file.c_str());

	if(err != XML_SUCCESS)
	{
		Advisory::pfatal("could not read robot configuration file \"%s\"", file.c_str());
		return;
	}

	XMLElement *robot = doc.FirstChildElement("robot");
	if (robot == nullptr)
	{
	    Advisory::pfatal("could not read robot configuration file \"%s\", no \"robot\" element",
			file.c_str());
		return;
	}
	
    Advisory::pinfo("############################## Loading Robot ##############################");
	const char *name = robot->Attribute("name");
	if (name != nullptr)
	{
	    Advisory::pinfo("Robot: %s", name);
	}
	else
	{
	    Advisory::pinfo("Robot: unnamed");
	}

    Advisory::pinfo("========================= Loading User Interface =========================");
	XMLElement *oi_xml = robot->FirstChildElement("interface");
	if (oi_xml != nullptr)
	{
		OIController::configure(oi_xml);
	}
	else
	{
	    Advisory::pfatal("ERROR: could not initialize OIController, no xml element named \"interface\"");
		return;
	}
	
    Advisory::pinfo("========================= Loading Robot Controls =========================");
    XMLElement *control_xml = robot->FirstChildElement("control");
	while (control_xml != nullptr)
	{
		const char* type = control_xml->Attribute("type");
		const char *name = control_xml->Attribute("name");
		
		if (name == nullptr)
		{
			name = "unnamed";
		}
		
		if (type != nullptr)
		{
		    Advisory::pinfo("Create %s - %s", type, name);
			ControlThread *control = ControlFactory::create(type, control_xml);

			if (control != nullptr)
			{
				m_controls.push_back(control);
			}
			else
			{
			    Advisory::pwarning("failed to create control %s - %s, make sure the %s type is registered with the factory\n", type, name, type);
			}
		}
		else
		{
		    Advisory::pwarning("could not create control, type is NULL");
		}

		control_xml = control_xml->NextSiblingElement("control");
	}

    Advisory::pinfo("==================== Loading Auton File Names ====================");
    XMLElement *auton_elem = robot->FirstChildElement("auton");
    while (auton_elem != nullptr)
    {
        const char *auton_name = auton_elem->Attribute("name");
        const char *auton_file = auton_elem->Attribute("file");

        if ((auton_name != nullptr) && (auton_file != nullptr))
        {
            if (0 == m_auton_name_list.size())
            {
                m_auton_chooser.SetDefaultOption(auton_name, new AutonIndex(m_auton_name_list.size())); 
            }
            else
            {
                m_auton_chooser.AddOption(auton_name, new AutonIndex(m_auton_name_list.size()));
            }
            m_auton_name_list.push_back(auton_name);
            m_auton_file_list.push_back(auton_file);
        }
        else
        {
            Advisory::pwarning("could not parse auton tag");
        }
        auton_elem = auton_elem->NextSiblingElement("auton");
    }

    m_auton_file_count = m_auton_name_list.size();
    Advisory::pinfo("found %d named auton files", m_auton_file_count);

    if (isAutonAvailable())
    {
        m_auton_enabled = true;

        SmartDashboard::PutData("Auton Selected: ", &m_auton_chooser);
        SmartDashboard::PutBoolean("Auton Enabled: ", m_auton_enabled);
        SmartDashboard::PutBoolean("Auton Delay Enabled: ", false);
        SmartDashboard::PutNumber("Auton Delay: ", 0.0);
        SmartDashboard::PutString("Auton Description: ", "none");
    }

    Advisory::pinfo("====================== Loading Macros Names ========================");
	XMLElement *macro_xml = robot->FirstChildElement("macro");
	while (macro_xml != nullptr)
	{
		const char *macro_name = macro_xml->Attribute("name");
		const char *macro_file = macro_xml->Attribute("file");

		if ((macro_name != nullptr) && (macro_file != nullptr))
		{
            // Don't load them here, they will be loaded in UpdateConfig()
			// m_macro_controller->loadMacro(macro_name, macro_file, this);

			XMLElement *oi_xml = macro_xml->FirstChildElement("oi");
			while (oi_xml != nullptr)
			{
                name = oi_xml->Attribute("name");
                if (name != NULL)
                {
                    if (strcmp(name, "start") == 0)
                    {
                        Advisory::pinfo("  connecting start macro %s to oi button", macro_name);
                        OIController::subscribeDigital(oi_xml, this, CMD_START_MACRO + m_macro_name_list.size());
                    }
                    else if (strcmp(name, "abort") == 0)
                    {
                        Advisory::pinfo("  connecting abort macro %s to oi button", macro_name);
                       OIController::subscribeDigital(oi_xml, this, CMD_ABORT_MACRO + m_macro_name_list.size());
                    }
                }
				oi_xml = oi_xml->NextSiblingElement("oi");
			}

			m_macro_name_list.push_back(macro_name);
			m_macro_file_list.push_back(macro_file);
		}
		else
		{
		    Advisory::pinfo("ERROR: could not load macro file");
		}
		macro_xml = macro_xml->NextSiblingElement("macro");
	}

    Advisory::pinfo("========================== Loading Macros ==========================");
    int macro_index = 0;
    for (std::string file : m_macro_file_list)
    {
        m_macro_controller->loadMacro(m_macro_name_list[macro_index].c_str(), file /*, this*/);
        macro_index++;
    }

    UpdateAutonConfig();

    Advisory::pinfo("############################## Robot Loaded  ##############################");
}

/*******************************************************************************
 *
 * Called during initialization to update the configuration.
 *
 * In previous version of robot main, specific button sequences during
 * disabled could result in the reloading of certain file by calling this
 * method. For now that is mostly handled by checking to see if the
 * selected auton changed.
 *
 * This method may be deleted in the future.
 *
 ******************************************************************************/
void ChipsBase::UpdateConfig(void)
{
    Advisory::pinfo("Robot UpdateConfig()");
}

/*******************************************************************************
 *
 * This method is called during initialization to load the default auton
 * file if any. It will also be called during the disabled period if the
 * selected auton changed.
 *
 ******************************************************************************/
void ChipsBase::UpdateAutonConfig(void)
{
    try
    {
        if (isAutonAvailable())
        {
            Advisory::pinfo("Robot UpdateAutonConfig()");

            AutonIndex * auton_index = m_auton_chooser.GetSelected();
            if (auton_index != nullptr)
            {
                m_auton_select = auton_index->value();
            }

            m_auton_enabled = SmartDashboard::GetBoolean("Auton Enabled: ", m_auton_enabled);
            m_auton_delay_enabled = SmartDashboard::GetBoolean("Auton Delay Enabled: ", m_auton_delay_enabled);
            m_auton_delay = SmartDashboard::GetNumber("Auton Delay: ", m_auton_delay);

            Advisory::pinfo("RobotMain::UpdateAutonConfig -- enabled=%s, select=%d, delay_enabled=%s, delay=%5.3f",
                    m_auton_enabled?"true":"false", m_auton_select, m_auton_delay_enabled?"true":"false", m_auton_delay);

            if ((m_auton_select >= 0) && (m_auton_select < m_auton_file_count))
            {
                m_macro_controller->loadMacro("auton", m_auton_file_list[m_auton_select] /*, this*/);
                m_auton_select_loaded = m_auton_select;
                m_auton_select_mismatch = 0;
                m_auton_description = m_macro_controller->getMacro("auton")->getDescription();
            }
            else
            {
                m_auton_enabled = false;
                m_auton_select = -1;
                m_auton_select_loaded = -1;
                m_auton_description = "none";
                m_auton_delay_enabled = false;
                m_auton_delay = 0.0;

                SmartDashboard::PutBoolean("Auton Enabled: ", m_auton_enabled);
                SmartDashboard::PutBoolean("Auton Delay Enabled: ", m_auton_delay_enabled);
                SmartDashboard::PutNumber("Auton Delay: ", m_auton_delay);
           }

            SmartDashboard::PutString("Auton Description: ", m_auton_description);
        }
    }
    catch (...)
    {
        Advisory::pwarning("exception caught in PacbotBase::UpdateAutonConfig");
    }
}

/******************************************************************************
 *
 * Called one when the robot becomes Disabled
 *
 ******************************************************************************/
void ChipsBase::DisabledInit(void)
{
    m_robot_phase = DISABLED;
    Advisory::pinfo("Robot DisabledInit");

	m_macro_controller->abortAll();

	for (ControlThread *control :  m_controls )
	{
		control->doDisabledInit();
	}
}

/******************************************************************************
 *
 * Called repeatedly while the robot is Disabled
 *
 ******************************************************************************/
void ChipsBase::DisabledPeriodic(void)
{
	OIController::update();

    if (isAutonAvailable())
    {
        AutonIndex * auton_index = m_auton_chooser.GetSelected();
        if (auton_index != nullptr)
        {
            m_auton_select = auton_index->value();
        }

        if ((m_auton_select != m_auton_select_loaded) && (m_auton_select >= 0)
            && (m_auton_select < m_auton_file_count))
        {
            // 40 cycles should be about 2 seconds, that should be enough
            // time for the user to finish adjusting the auton selection
            if(m_auton_select_mismatch++ > 40)
            {
                UpdateAutonConfig();
            }
        }
    }

	publish();
}

/******************************************************************************
 *
 * Called once when the robot enters the Autonomous mode of operation
 *
 ******************************************************************************/
void ChipsBase::AutonomousInit(void)
{
    m_robot_phase = AUTON;
    Advisory::pinfo("Robot AutonomousInit");

    m_macro_controller->abortAll();

    if (isAutonAvailable())
    {
        m_auton_enabled = SmartDashboard::GetBoolean("Auton Enabled: ", m_auton_enabled);
        m_auton_delay_enabled = SmartDashboard::GetBoolean("Auton Delay Enabled: ", m_auton_delay_enabled);
        m_auton_delay = SmartDashboard::GetNumber("Auton Delay: ", m_auton_delay);

        m_auton_started = false;
        if (m_auton_enabled)
        {
            m_auton_start_time = (double)GetTime();

            if (m_auton_delay_enabled)
            {
                m_auton_start_time += m_auton_delay;
            }
        }
//
//        Advisory::pinfo("RobotMain::AutonomousInit -- enabled=%s, select=%d, delay_enabled=%s, delay=%5.3f, start_time=%f, now=%f",
//                m_auton_enabled?"true":"false", m_auton_select, m_auton_delay_enabled?"true":"false", m_auton_delay,
//                		m_auton_start_time, GetTime());
//
    }

    for (ControlThread *control :  m_controls )
	{
		control->doAutonomousInit();
	}
}

/******************************************************************************
 *
 * Called repeatedly while the robot is in the Autonomous mode of operation
 *
 ******************************************************************************/
void ChipsBase::AutonomousPeriodic(void)
{
	OIController::update();

    if ((m_auton_enabled) && ((double)GetTime() >= m_auton_start_time) && (! m_auton_started))
    {
        m_macro_controller->startMacro("auton");
        m_auton_started = true;
    }

    publish();
}

/******************************************************************************
 *
 * Called once when the robot enters the Teleoperated mode of operation
 *
 ******************************************************************************/
void ChipsBase::TeleopInit(void)
{
    m_robot_phase = TELEOP;
    Advisory::pinfo("Robot TeleopInit");

	for (ControlThread *control :  m_controls )
	{
		control->doTeleopInit();
	}
}

/******************************************************************************
 *
 * Called repeatedly while the robot is in the Teleoperated mode of operation
 *
 ******************************************************************************/
void ChipsBase::TeleopPeriodic(void)
{
	OIController::update();
	publish();
}

/******************************************************************************
 *
 * Called once when the robot enters the Test mode of operation
 *
 ******************************************************************************/
void ChipsBase::TestInit(void)
{
    m_robot_phase = TEST;
    Advisory::pinfo("Robot TestInit");

	for (ControlThread *control :  m_controls )
	{
		control->doTestInit();
	}
}

/******************************************************************************
 *
 * Called repeatedly while the robot is in the Test mode of operation
 *
 ******************************************************************************/
void ChipsBase::TestPeriodic(void)
{
	OIController::update();
	publish();
}

/*******************************************************************************
 *
 * Called only if an analog input is connected to this object during the
 * loading of the robot. At this time that does not happen so this will
 * not be called.
 *
 * Required by OIObserver
 *
 ******************************************************************************/
void ChipsBase::setAnalog(int id, float val)
{
//    try
//    {
//        switch(id)
//        {
//            default:
//            {
                Advisory::pinfo("Robot setFloat(%d, %f) -- no case for id", id, val);
//            } break;
//        }
 //   }
 //   catch (...)
 //   {
 //       Advisory::pinfo("EXCEPTION: caught in RobotMain::setFloat");
 //   }
}

/*******************************************************************************
 *
 *
 * Called only if a digital input is connected to this object during the
 * loading of the robot. That could happen if any macros are defined with
 * buttons that start/stop them.
 *
 * Required by OIObserver
 *
 *
 ******************************************************************************/
void ChipsBase::setDigital(int id, bool val)
{
    try
    {
//        switch(id)
//        {
//            default:
//            {
                if ((id >= CMD_START_MACRO) && (id < CMD_START_MACRO_MAX))
                {
                    if (val && (m_robot_phase != INIT) && (m_robot_phase != DISABLED))
                    {
                        m_macro_controller->startMacro(m_macro_name_list[id - CMD_START_MACRO]);
                    }
                }
                else if ((id >= CMD_ABORT_MACRO) && (id < CMD_ABORT_MACRO_MAX))
                {
                    if (val)
                    {
                        m_macro_controller->abortMacro(m_macro_name_list[id - CMD_ABORT_MACRO]);
                    }
                }
                else
                {
                    Advisory::pinfo("Robot setDigital(%d, %s) -- no case for id", id, (val?"true":"false"));
                }

//            } break;
//        }
    }
    catch (...)
    {
        Advisory::pinfo("EXCEPTION: caught in PacbotBase::setDigital");
    }
}

/*******************************************************************************
 *
 *
 * Called only if an integer input is connected to this object during the
 * loading of the robot. At this time that does not happen so this will
 * not be called.
 *
 * Required by OIObserver
 *
 *
 ******************************************************************************/
void ChipsBase::setInt(int id, int val)
{
//    try
//    {
//        switch(id)
//        {
//           default:
//           {
                Advisory::pinfo("Robot setInt(%d, %d) -- no case for id", id, val);
//           } break;
//        }
//    }
//    catch (...)
//    {
//        Advisory::pinfo("EXCEPTION: caught in RobotMain::setInt");
//    }
}

/******************************************************************************
 *
 * return true if at least one auton file was loaded
 *
 ******************************************************************************/
inline bool ChipsBase::isAutonAvailable()
{
    return m_auton_file_count > 0;
}

/******************************************************************************
 *
 * This method is called from within each of the periodic methods to allow
 * this class and all controls to publish values to the dashboard.
 *
 * Because the periodic methods should be getting called about 20 times a
 * second and the dashboard only needs to be updated every half second,
 * this method only does work every tenth time it is called.
 *
 ******************************************************************************/
void ChipsBase::publish(void)
{
    if (m_publish_count++ >= 20)
    {
        SmartDashboard::PutNumber("Macro Starts: ", this->m_macro_controller->getMacroStarts());
        SmartDashboard::PutNumber("Macro Completes: ", this->m_macro_controller->getMacroCompletes());
        SmartDashboard::PutNumber("Macro Aborts: ", this->m_macro_controller->getMacroAborts());
        SmartDashboard::PutNumber("Macros Running: ", this->m_macro_controller->getMacrosRunning());

        m_publish_count = 0;
    }

    if (m_publish_count < m_controls.size())
    {
        m_controls[m_publish_count]->doPublish();
    }
}
