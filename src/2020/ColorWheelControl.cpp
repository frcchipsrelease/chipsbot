/*******************************************************************************
 *
 * File: ColorWheelControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "2020//ColorWheelControl.h"
#include "rfutilities/MacroStepFactory.h"
#include "frc/DriverStation.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
ColorWheelControl::ColorWheelControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
#ifdef DISABLED
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Color Wheel Control [%s] =========================",
	            control_name.c_str());
	m_color_motor = nullptr;

	m_color_sensor = nullptr;

	motor_min_control = -1.0;
	motor_max_control = 1.0;

    color_max_current = 100.0;

	motor_increment_step = 0.1;
	motor_decrement_step = 0.1;

	m_run_color = false;
	m_run_number = false;
	m_run_one_slice = false; 

	m_on_power = 1.0;
	m_state_power = 1.0;

	motor_rotate_num_speed = 0.0;
	motor_rotate_color_speed = 0.0;

	motor_max_cmd_delta = 0.25;

	motor_target_power = 0.0;
	motor_command_power = 0.0;

	delta_position = 0.01;

	m_is_ready = false;

    m_max_velocity = 0.0;
    m_desired_acceleration = 0.0;
    m_desired_deceleration = 0.0;

	raw_position = 0.0;
	raw_velocity = 0.0;
	actual_position = 0.0;
    actual_velocity = 0.0;
	target_power = 0.0;
	target_position = 0.0;
	command_power = 0.0;

	m_confidence = 0.0;
	m_colorString = "";
	colorWheelRotations = 0.0;
	m_game_data = "";
	m_is_cw_running = false; 

	const char *name = nullptr;

	//
	// Parse XML
	//

	//
	// @TODO: get "comp" from xml and pass comp to this method
	//comp = xml-> FirstChildElement("color_sensor");
	//while (comp != nullptr)
	//{
		m_color_sensor = HardwareFactory::createColorSensor(nullptr);
	//}

	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "color_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for motor");
				m_color_motor = HardwareFactory::createMotor(comp);
				comp->QueryUnsignedAttribute("max_current", &color_max_current);
				Advisory::pinfo("    max_current=%u", color_max_current);				
			}
			else
			{
				Advisory::pwarning("  found unexpected motor with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp-> NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "off") == 0)
			{
				Advisory::pinfo("  connecting off channel");
				OIController::subscribeDigital(comp, this, CMD_MOTOR_OFF);
			}
			else if (strcmp(name, "on") == 0)
			{
				Advisory::pinfo("  connecting on channel");
				comp->QueryFloatAttribute("value", &m_on_power);
				OIController::subscribeDigital(comp, this, CMD_MOTOR_ON);
			}
			else if (strcmp(name, "state") == 0)
			{
				Advisory::pinfo("  connecting state channel");
				comp->QueryFloatAttribute("value", &m_state_power);
				OIController::subscribeDigital(comp, this, CMD_MOTOR_STATE);
			}
			else if (strcmp(name, "int") == 0)
			{
				Advisory::pinfo("  connecting int channel");
				OIController::subscribeInt(comp, this, CMD_MOTOR_INT);
			}
			else if (strcmp(name, "rotate_num") == 0)
			{
				Advisory::pinfo("  connecting rotate_num channel");
				comp->QueryFloatAttribute("value", &motor_rotate_num_speed);
				OIController::subscribeDigital(comp, this, CMD_ROTATE_NUM);
			}
			else if (strcmp(name, "rotate_color") == 0)
			{
				Advisory::pinfo("  connecting rotate_color channel");
				comp->QueryFloatAttribute("value", &motor_rotate_color_speed);
				OIController::subscribeDigital(comp, this, CMD_ROTATE_COLOR);
			}
			else if (strcmp(name, "rotate_slice") == 0)
			{
				Advisory::pinfo("  connecting rotate_slice channel");
				comp->QueryFloatAttribute("value", &motor_rotate_color_speed);
				OIController::subscribeDigital(comp, this, CMD_ROTATE_COLOR);
			}

		}
		
		comp = comp->NextSiblingElement("oi");
	}
#endif	
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
ColorWheelControl::~ColorWheelControl(void)
{
	if (m_color_motor != nullptr)
	{
		delete m_color_motor;
		m_color_motor = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void ColorWheelControl::controlInit(void)
{
    if ((m_color_motor != nullptr) && (color_max_current < 100.0))
    {
        m_color_motor->setCurrentLimit(color_max_current);
        m_color_motor->setCurrentLimitEnabled(true);
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void ColorWheelControl::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void ColorWheelControl::disabledInit(void)
{
	motor_target_power = 0.0;
	motor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void ColorWheelControl::autonomousInit(void)
{
	motor_target_power = 0.0;
	motor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void ColorWheelControl::teleopInit(void)
{
	motor_target_power = 0.0;
	motor_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void ColorWheelControl::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void ColorWheelControl::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id            	the control id passed to the subscribe
 * @param button_pressed	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void ColorWheelControl::setDigital(int id, bool button_pressed)
{
	switch (id)
	{
		case CMD_MOTOR_OFF:
		{
			if (button_pressed)
			{
				motor_target_power = 0.0;
			}
		} break;

		case CMD_MOTOR_ON:
		{
			if(button_pressed)
			{
				motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, m_on_power);
			}
		} break;

		case CMD_MOTOR_STATE:
		{
			if(button_pressed)
			{
				motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, m_state_power);
			}
			else
			{
				motor_target_power = 0.0;
			}
		} break;

		case CMD_ROTATE_NUM:
		{
			if (button_pressed)
			{
				m_run_number = true;
			}
		} break;

		case CMD_ROTATE_COLOR://The purpose of Rotate Color is to rotate the Color Wheel to a predetermined color
		{                     //which is determined by the game data
			if (button_pressed) 
			{
				m_run_color = true; 
			}
		} break;

		case CMD_ROTATE_SLICE:
		{
			if (button_pressed)
			{
				m_run_one_slice = true;
			}
		} break;

		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void ColorWheelControl::setInt(int id, int val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	if (id == CMD_MOTOR_INT)
	{
		if (val >= 0)
		{
			if ((val > 315) || (val < 45)) // up was pressed
			{
				m_run_number = true;
				//Advisory::pinfo("aaaaaaaaaaaa");
			}
			else if ((val > 45) && (val < 135)) // right was pressed
			{
				motor_target_power = 0.0;
				//Advisory::pinfo("bbbbbbbbbbb");
			} 
			else if ((val > 135) && (val < 225)) // down was pressed
			{
				m_run_color = true; 
			}
			else if ((val > 225) && (val < 315)) // left was pressed
			{
				motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, m_on_power);
				//Advisory::pinfo("dddddddddddddd");
			}
		}
	}
}

/*******************************************************************************
 * Returns the color needed to be sensed by the robot based on the 
 * color the field wants (parameter)
 ******************************************************************************/
std::string ColorWheelControl::getRequiredColor(std::string _fieldColor)
{
	if( strcmp(_fieldColor.c_str(), "Red") == 0)
	{
		return "Blue";		
	}
	if( strcmp(_fieldColor.c_str(), "Green") == 0)
	{
		return "Yellow";		
	}
	if( strcmp(_fieldColor.c_str(), "Blue") == 0)
	{
		return "Red";		
	}
	if( strcmp(_fieldColor.c_str(), "Yellow") == 0)
	{
		return "Green";		
	}
	return "Unknown";
}

/*******************************************************************************
 * Returns the detected color from the color sensor , only if it has an
 * acceptable tolerance level
 * 
 * The confidence value may need to be changed
 ******************************************************************************/
std::string ColorWheelControl::getDetectedColor(void)
{
	if( strcmp(m_colorString.c_str(), "Red") == 0   &&  m_confidence > .85 )
	{
		return "Red";		
	}
	if( strcmp(m_colorString.c_str(), "Green") == 0   &&  m_confidence > .95 )
	{
		return "Green";		
	}
	if( strcmp(m_colorString.c_str(), "Blue") == 0   &&  m_confidence > .95 )
	{
		return "Blue";		
	}
	if( strcmp(m_colorString.c_str(), "Yellow") == 0   &&  m_confidence > .91 )
	{
		return "Yellow";		
	}
	return "Unknown";
}


/*******************************************************************************
 * Returns true if the color wheel has rotated enough times to get the points
 ******************************************************************************/
bool ColorWheelControl::isDoneRotating(void)
{
	if(colorWheelRotations > 3.25)
	{
		return true;
	}
	else
	{
		return false;
	}	
}
/*******************************************************************************
 * Interprets the game data and returns a string of the required color by the field
 ******************************************************************************/
std::string ColorWheelControl::getGameDataColorString()
{
	switch (m_game_data[0])
  	{
		case 'B' :
			return "Blue";
		break;
		case 'G' :
			return "Green";
		break;
		case 'R' :
			return "Red";
		break;
		case 'Y' :
			return "Yellow";
		break;
		default :
			return "Unknown";
		break;
  	}
}

/*******************************************************************************
 * 
*******************************************************************************/
void ColorWheelControl::setClosedLoop(bool closed)
{
	if (m_closed_loop != closed)
	{
		m_closed_loop = closed;

		if (m_closed_loop)
		{
		    Advisory::pinfo("setting color_wheel closed loop mode");
		}
		else
		{
            Advisory::pinfo("setting color_wheel open loop mode");
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
bool ColorWheelControl::isClosedLoop(void)
{
	return m_closed_loop;
}

/*******************************************************************************
 *
 ******************************************************************************/
bool ColorWheelControl::isAtTarget(float tolerance)
{
	return (fabs(actual_position - target_position) < tolerance);
}

/*******************************************************************************	
 *
 ******************************************************************************/
void ColorWheelControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " target_power: ", motor_target_power);
	SmartDashboard::PutNumber(getName() + " command_power: ", motor_command_power);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void ColorWheelControl::doPeriodic()
{
#ifdef DISABLED
	m_color_motor->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	m_game_data = frc::DriverStation::GetInstance().GetGameSpecificMessage();
	motor_command_power = m_color_motor->getPercent();

	Color detectedColor; 
	m_colorString = "";
	if (m_color_sensor != nullptr)
	{
		m_color_sensor->update(detectedColor, m_confidence, m_colorString);	
	}
	SmartDashboard::PutNumber("Red", detectedColor.red);
	SmartDashboard::PutNumber("Green", detectedColor.green);
	SmartDashboard::PutNumber("Blue", detectedColor.blue);
	SmartDashboard::PutNumber("Confidence", m_confidence);
	SmartDashboard::PutString("Detected Color", m_colorString);
	SmartDashboard::PutString("Game Data", m_game_data);

	//
	// All processing happens in the motor class
	//
	if (m_run_color == true)/////////////////////////
	{
		m_run_color = false;

		if(m_is_cw_running == false)
		{
			m_is_cw_running = true;
			//Advisory::pinfo("Rotate Color");
			m_fieldColor = getGameDataColorString();
			currentColor = getDetectedColor().c_str();
			m_neededColor = getRequiredColor(m_fieldColor);
			if(  strcmp(m_fieldColor.c_str(), "Unknown") == 0  ) // If there is no game data color
			{
				//do nothing
			}
			else if(  strcmp(currentColor.c_str(), m_neededColor.c_str()) != 0  )
			{
				motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_rotate_color_speed);
				
				while(  strcmp(currentColor.c_str(), m_neededColor.c_str()) != 0  )
				{
					currentColor = getDetectedColor().c_str();
				}
				motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, 0); // motor off
			}
		}

	}
	else if (m_run_number == true)/////////////////////////
	{	
		m_run_number = false; 
		if(m_is_cw_running == false)
		{
			m_is_cw_running = true;
			Advisory::pinfo("Rotate Number");	
			previousColor = ColorWheelControl::getDetectedColor().c_str();
			motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_rotate_num_speed); // Motor On
			while(colorWheelRotations < 5)
			{
				currentColor = ColorWheelControl::getDetectedColor().c_str();

				if(  strcmp(currentColor.c_str(), "Unknown") == 0  )
				{
					//Advisory::pinfo("Unknown Color");
				}
				else if(  strcmp(currentColor.c_str(), previousColor.c_str()) != 0  )
				{
					colorWheelRotations += .125;
					previousColor = currentColor;
					if(colorWheelRotations >= 3.125)
					{
						motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, 0); // Motor Off
						colorWheelRotations = 0;
						break;
					}
				}
			}
			colorWheelRotations = 0;
			m_is_cw_running = false;
			motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, 0); // Motor Off
		}
		
		
	}
	else if (m_run_one_slice == true)/////////////////////////
	{
		m_run_one_slice = false; 
		if(m_is_cw_running == false)
		{
			m_is_cw_running = true;
			Advisory::pinfo("Rotate Number");	
			previousColor = ColorWheelControl::getDetectedColor().c_str();
			motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_rotate_num_speed); // Motor On
			while(colorWheelRotations < 5)
			{
				currentColor = ColorWheelControl::getDetectedColor().c_str();

				if(  strcmp(currentColor.c_str(), "Unknown") == 0  )
				{
					//Advisory::pinfo("Unknown Color");
				}
				else if(  strcmp(currentColor.c_str(), previousColor.c_str()) != 0  )
				{
					colorWheelRotations += .125;
					previousColor = currentColor;
					if(colorWheelRotations >= .125)
					{
						motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, 0); // Motor Off
						colorWheelRotations = 0;
						break;
					}
				}
			}
			colorWheelRotations = 0;
			m_is_cw_running = false;
			motor_target_power = RobotUtil::limit(motor_min_control, motor_max_control, 0); // Motor Off
		}


	}

	//
	// Set Outputs
	//
	m_color_motor->setPercent(motor_target_power);
#endif
}