<control type="motor" name="Talon">
       <motor type="TalonSrxCan" port="5" invert="false" />
       <motor type="TalonSrxCan" port="1" invert="true" />
       <oi name="momentary_a"	device="pilot" chan="6" value="0.2" invert="false"/>
       <oi name="momentary_b"	device="pilot" chan="8" value="0.35" invert="false"/>
       <oi name="momentary_c"	device="pilot" chan="5" value="0.375" invert="false"/>
       <oi name="momentary_d"	device="pilot" chan="7" value="0.4" invert="false"/>
       <oi name="momentary_e"	device="pilot" chan="9" value="0.8" invert="false"/>
       <oi name="momentary_f"	device="pilot" chan="10" value="1.0" invert="false"/>
</control> 