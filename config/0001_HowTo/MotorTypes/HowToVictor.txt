-<control name="bird_Beak" type="motor" period="0.05" max_cmd_delta="0.2">
	<motor type="Victor" invert="true" fuse="6" port="4"/>
	<oi name="momentary_a" invert="false" chan="6" device="pilot" value="-1.0"/>
	<oi name="momentary_b" invert="false" chan="8" device="pilot" value="1.0"/>
</control>