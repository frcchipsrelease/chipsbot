<!--
 * ****************************************************************
 * This is for using the "generic" motor control in rfControls.
 * This is good for prototyping as it can use 1 or 2 single motors
 * in open loop
 *
 * It can also be used with motor that has 1 (or more) followers
 *
 * The Motor Control can be used to control one or two motors that 
 * will run at the same time. 
 *
 * If this is controlling two motors they will normally be 
 * physically connected together. You must be careful to make
 * sure the inverts on the motors are set such that the motors
 * are working together instead of against each other. Good 
 * practice would be to run with just one motor plugged in 
 * first, then the other motor plugged in to make sure both
 * are running in the same direction before plugging both in
 * and running them together. 
 *
 * There are several ways to control the motor(s), you can 
 *    - you can use an analog input to set the speed based
 *      on a stick deflection or dial position with the
 *      analog command.
 *
 *    - step through the range of speeds with a specified 
 *      increment and decrement step sizes using the 
 *      increment, decrement, and stop commands
 *  
 *    - you can use the four momentary commads (a, b, c, and d) 
 *      to set the power for the motor(s) to preset values. With
 *      the momentary commands the power will be applied on thead
 *      button press an set back to zero on the button release.
 *
 * All of these command options can be specified for a single motor but 
 * operations might be a little confusing.
 *
 * All inputs (increments, decrements, analog, and momentaries) will
 * be limited to values between min_control and max_control.
 *
 *  <control type="motor" [name="unnamed"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [port="1"] [invert="false"] />]
 *
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 * The Motor control supports a single macro step so it 
 * can be used in autonomous scripts.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 * Macro Step SetPower Example:
 *
 *	<step name="lift_up" control="lift_motor" type="SetPower" power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>
 *  </step>
 *
 * All motors require either a device_id or a port. Either can be specified for
 * any type of motor controller although device_id is more common for CAN and
 * port is more common for PWM.
