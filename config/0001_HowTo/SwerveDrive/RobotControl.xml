<?xml version="1.0"?>
<!--
 * The Swerve Drive Control is to control a four wheeled 
 * drive base using one stick for speed and a second stick for turning.
 *
 * NOTE: Following the right hand rule, if +X is forward, and +Z is up,
 *       +Y must be to the left, so positive turn means turn left.
 *
 * The forward command is an analog input and is used to set the power
 * being applied to both sides of the drive base. The turn command is
 * an analog input and is applied to the right side of the drive base
 * and applied backwards to the left side of the drive base -- a positive
 * turn value will result in turning left. If both are commanded at the
 * same time, they are added together.
 * 
 * The drive base is expected to have either two or four motors, if it
 * has four motors the motors on one side of the robot can be, but are not
 * required to be physically connected together.
 * 
 * If four motors are connected to four mecanum wheels and not connected
 * to any of the other motors this class can support strafing. A third
 * analog input can be assigned to the strafe command. Assuming the 
 * wheels are installed in the typical way, A positive strafe command 
 * will strafe left. 
 *
 * The XXX_on, XXX_off, and XXX_toggle commands responde to a button
 * press but do nothing for a button release. The XXX_state will turn
 * XXX on with a button press and will turn it of with a button release.
 * 
 * This class also provides support for solenoid actuated brakes and a
 * solenoid actuated gear shifter. If connected, these solenoids are
 * controlled with the matching on, off, toggle, and state commands.
 *
 * It is also possible to use a low power mode, this makes it easier to
 * make fine adjustments to a base that is configured to be fast. A low power
 * scale can be used as a multiplier for the normal wheel commands when 
 * in low power mode -- so if a full forward command is given with a 
 * low power scale of 0.25 while in low power mode, the base will move
 * forward at 25% of it's normall full stick power. The low power on, off,
 * toggle, and state commands can be used to control the low power mode.
 *
 * This class also has support for both an accelerometer and an IMU. neither
 * of these are used by this class at this time, but if attached they can
 * be used to report and collect data.
 *
 *  <control type="swerve_drive" name="drive" [low_power_scale="0.5"]>
 *      [<motor name="front_left"   [type="Victor"] [port="1"] [invert="false"] />]
 *      [<motor name="front_right"  [type="Victor"] [port="2"] [invert="false"] />]
 *      [<motor name="back_left"  	[type="Victor"] [port="3"] [invert="false"] />]
 *      [<motor name="back_right" 	[type="Victor"] [port="4"] [invert="false"] />]
 *
 *      [<solenoid name="brake" 	[module="1"] 	[port="1"] />]
 *      [<solenoid name="gear"  	[module="1"] 	[port="2"] />]
 *
 *      [<accelerometer />]
 *      [<imu />]
 *
 *      [<oi name="forward"      	device="pilot_js" chan="2" 	[scale="-0.8"]/>]
 *      [<oi name="turn"         	device="pilot_js" chan="3" 	[scale="0.8"]/>]
 *      [<oi name="strafe"         	device="pilot_js" chan="1" 	[scale="0.8"]/>]
 *
 *      [<oi name="brake_on"     	device="pilot_js" chan="1" 	[invert="false"]/>]
 *      [<oi name="brake_off"    	device="pilot_js" chan="2" 	[invert="false"]/>]
 *      [<oi name="brake_toggle" 	device="pilot_js" chan="3" 	[invert="false"]/>]
 *      [<oi name="brake_state"  	device="pilot_js" chan="4" 	[invert="false"]/>]
 *
 *      [<oi name="gear_on"      	device="pilot_js" chan="5" 	[invert="false"]/>]
 *      [<oi name="gear_off"     	device="pilot_js" chan="7" 	[invert="false"]/>]
 *      [<oi name="gear_toggle"  	device="pilot_js" chan="6" 	[invert="false"]/>]
 *      [<oi name="gear_state"   	device="pilot_js" chan="8" 	[invert="false"]/>]
 *
 *      [<oi name="low_power_on"	 device="pilot_js" chan="9"	[invert="false"]/>]
 *      [<oi name="low_power_off"	 device="pilot_js" chan="10" 	[invert="false"]/>]
 *      [<oi name="low_power_toggle" device="pilot_js" chan="11" 	[invert="false"]/>]
 *      [<oi name="low_power_state"	 device="pilot_js" chan="12" 	[invert="false"]/>]
 *  </control>
 *
-->

<robot name = "HowToSwerveDrive">
	<interface>
		<device name = "pilot_js"   type = "joystick" num="0" />
	</interface>
	
    <control type="swerve_drive" name="merve_the_swerve" low_power_scale="0.25"
      drive_length="1.0"  drive_width="0.5" >
        <SwerveModule name="front_left">
            <motor name="rotational"    type="TalonSrxCan" port="1" invert false />
            <motor name="translational" type="TalonSrxCan" port="2" invert false />
        </SwerveModule>
        <SwerveModule name="back_left">
            <motor name="rotational"    type="TalonSrxCan" port="3" invert false />
            <motor name="translational" type="TalonSrxCan" port="4" invert false />
        </SwerveModule>
        <SwerveModule name="front_right">
            <motor name="rotational"    type="TalonSrxCan" port="5" invert false />
            <motor name="translational" type="TalonSrxCan" port="6" invert false />
        </SwerveModule>
        <SwerveModule name="back_right">
            <motor name="rotational"    type="TalonSrxCan" port="7" invert false />
            <motor name="translational" type="TalonSrxCan" port="8" invert false />
        </SwerveModule>
       
        <gyro name="the_gyro" type="ADXRS450" />
	   
        <oi name="forward"      device="pilot_js" chan="1" scale="-1.0"	/>
        <oi name="lateral"      device="pilot_js" chan="2" scale="-1.0" />
        <oi name="turn"         device="pilot_js" chan="3" scale="-1.0" />
        <oi name="speed_toggle" device="pilot_js" chan="4" invert="false" />
        <oi name="auto_attitude_toggle" device="pilot_js" chan="5" invert="false" />
        <oi name="drive_mode_toggle"	device="pilot_js" chan="6"	invert="false" />
    </control>
</robot>
