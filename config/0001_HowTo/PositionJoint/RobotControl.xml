<?xml version="1.0"?>
<!--
 * The Position Joint Control can be used to control a motor to move to multile
 * preset positions.
 *
 * The position joint control uses a single motor (or a motor with zero or more followers),
 * at least one sensor to provide a closed loop position control, and a pid control. It uses
 * a trapizodal velocity trajectory to move the joint to the disired position quickly with
 * with little overshoot.
 *
 * The trapizodal velocity is defined with the trajectory_max_velocity, trajectory_acceleration,
 * and trajectory_deceleration values. The min_position and max_position values help limit
 * the commanded values to a predetermined range.
 *
 * A list of setpoints must be specified so the control has some target positions. This 
 * list can contain up to 10 setpoints (index 0-9). each setpoint must specify an
 * index and a position value, each setpoint can have a name.
 * 
 * The closed loop oi allows you to switch between closed loop control and open 
 * looped control. This is very importent while configuring and testing the joint,
 * it can also be helpful if a sensor is damaged or disconnected during a match.
 *
 * The nudge_up, nudge_down, and analog ois are used for both open loop and 
 * closed loop control. The nudge commands take a power (for open loop) and abbrev
 * step (for closed loop). Tha analog input will use the analog value as is 
 * for open loop and will multiply the analog value by the delta_scale to deterine
 * how much the target position should be changed each time through the control loop.
 *
 * setpoint0 through stpoint9 oi's can be used to tie a button to a setpoint. This 
 * works well if there are only a few setpoints being used or you have a lot of
 * buttons available.
 *
 * The setpoint_idx oi gets tied to an integer input, this is normally tied 
 * to a dpad or hat control giving up to 8 unique values.
 * 
 * The setpoint_analog_idx in general requires some special input device
 * to be useful. If you have an input device that can set an analog input to
 * specific values than this can be very useful. The XKeys keypad can do this, 
 * also, a collection of resistors and a multi-positin switch could do this.
 * It expects a value between -1.0 and 1.0 will convert the provided value to
 * an index using the step and zero values.
 *  idx = (int)((value - zero  + (0.3 * step)) / (step)) so if the incoming 
 * value is close to a multiple of the step, it will find the correct index.
 *
 *  <control type="position_joint" name="turret" closed_loop="true"
 *		trajectory_max_velocity="220.0" trajectory_acceleration="440.0" trajectory_deceleration="220.0"
 *		min_position="-120" max_position="120"
 *		
 *  	<motor type="TalonFxCan" device_id="6" brake_mode="true" invert="false" >
 *  	    <limit_switch name="lower" port = "lower" normally_open="true" reset="false" reset_value="-120.0"  />
 *  	    <limit_switch name="upper" port = "upper" normally_open="true" reset="false" reset_value="120.0"   />
 *  		
 *  		<encoder type="integrated" scale="0.000879" invert="false" />
 *  		<pid kf="0.0" kp="0.25" ki="0.00125" kd="2.5" kz="200" cntl_min="-1.0" cntl_max="1.0" is_angle="false" />
 *      </motor>
 *
 *  	<setpoints>
 *  		<setpoint index="0" name="sp0" position="-120" />
 *  		<setpoint index="1" name="sp1" position="-80" />
 *  		<setpoint index="2" name="sp2" position="-50" />
 *  	</setpoints>
 *
 *  	<oi name="closed_loop_state"  device="keypad"  chan="15" />
 *  	<oi name="nudge_up"           device="keypad"  chan="5"  power="0.1"    step="5"  />
 *  	<oi name="nudge_down"         device="keypad"  chan="7"  power="-0.1"   step="-5" />		
 *  	<oi name="analog"             device="copilot" chan="3"  delta_scale="0.003" />		
 *  	<oi name="setpoint<n>"        device="keypad"  chan="12" />     where <n> is replaced with 0 to 9
 *  	<oi name="setpoint_idx"       device="keypad"  chan="0"  />
 *      <oi name="setpoint_analog_idx" device="keypad" chan="4" step="0.078125" zero="-0.937500" />
 *  </control>
 *
 *
 *  How to Pick the Numbers
 *  ----------------------- 
 *	For this I'm going to use an example. The example is a turret that rotates 240 
 *	degrees. It is driven with a Falcon 500 motor that has an integrated encode which 
 *	measures 4096 ticks per revolution. The drive is through a 100:1 gear ratio.
 *	
 *  Set the min_position and max_position, these are real world numbers that should
 *  have meaning to your robot, pick the ends of travel for your joint, normally 
 *  hardstop positions will work best. For this example I'm picking -120 degrees
 *  and 120 degrees, that would make the center point (forward) 0 degrees.
 * 
 *  Always start tuning in open loop mode. Set some inputs so you can control 
 *  the joint slowly. I'm starting with nudge buttons at +/- 0.1 (so +/- 10%) 
 *  motor power.
 *
 *  Do a little testing, make sure the nudge up (0.1) makes the turret move in 
 *  the positive direction. In my example turning left is positive (+X forward, 
 *  +Z up means the right hand rule dictates +Y is left).
 *
 *  Set the motor invert such that positive command power should increase the 
 *  real world numbers. Make sure you are looking at the command power, many 
 *  joysticks are opposite of what you would expect (in this case you should
 *  invert the joystick if needed, it will make autonomous more logical)
 * 
 *  Set sensor invert. Move the joint through it's range of motion, make sure 
 *  that as the motor is commanded forward, as the real world values increase, 
 *  the sensor values are increasing if not, change the invert on the sensor
 *  
 *  Set the sensor scale [and offset]. Move the joint to physical min_position
 *  location, record the raw position at this location. Repeat for the max_position.
 *  for encoders scale = (max_position - min_position) / (max_raw - min_raw)
 *  for pots, set the p1_raw, p1_cal, p2_raw, and p2_cal values to these numbers
 *  for my example 240/273000 = 0.000879
 *
 *  Set the trajectory numbers to a starting point. Move the joint to the minimum
 *  physical position. Then (in open loop) drive it to the midway position as
 *  fast as possible (100% power), time how long it takes to reach the midpoint. Be careful 
 *  to not damage the robot. Do this several times to get an average. Using math,
 *  calculate a maximum velocity. In my example it takes about 0.5 seconds to move
 *  120 degrees so about 240 degrees a second. Note: the Falcon no load speed is 
 *  6380 RPM, so ((6380 / 60 seconds) * 360 degrees) / 100 = 382.8 degrees per second
 *  on the output. You will want to set the trajectory velocity to about 90% of this
 *  so the control loop has space to work. Then double that value for initial
 *  acceleration and deceleration values.
 *
 *  PID Constants
 *  -------------
 *  For position control kf should always be 0.0.
 * 
 *  Start with kd = 0.0 and ki = 0.0 I'm going to say I want to correct a 0.1%
 *  error using 5.0% of the motor power. A .1% error is (max - min) * 0.001, in this
 *  example that is 0.24 degrees. Dividing by the scale gives us 273 encoder ticks.
 *  Internally the CTRE hardware gets commanded with -1023 to 1023 values, so we 
 *  get kp = (0.05 * 1023) / 273 = 0.22. 
 *
 *  kp = (0.05 * 1023) / (((max-min) * 0.001) / scale)
 *
 *  This is an initial kp, test with it, if you	are getting oscillation divide it 
 *  by 2 and test again, if you are not getting	oscillations double it and test. 
 *  Repeat until you have a value that is close	to but not resulting in oscillations.
 *
 *  Next kd can be adjusted to smooth accelerations, start with a kd that is about 
 *  ten times your kp, incrementaly adjust it until you get a behavior that you like.
 *  After kd, while enable, move the joint by hand and watch how much the raw value 
 *  changes while applying gental pressure in the forward and reverse directions. About
 *  twice the difference of the raw values should be your kz value (this is called the
 *  izone by CTRE). finally you can add a small ki, normally about kp/100 maybe even 
 *  smaller. The ki value should help the value get closer to the target value but will
 *  tend to introduce oscillations if too large.
 *
-->
<robot name = "HowToPositionJoint">
    <interface>
        <device name="keypad" type="hid" num="1" analogs="5" digitals="32" povs="1" />
    </interface>

    <control type="position_joint" name="turret" closed_loop="true"
			trajectory_max_velocity="220.0" trajectory_acceleration="440.0" trajectory_deceleration="220.0"
			min_position="-120" max_position="120" >
			
		<motor type="TalonFxCan" device_id="6" brake_mode="true" invert="false" >
		    <dlimit_switch name="lower" port = "lower" normally_open="true" reset="false" reset_value="-120.0"  />
		    <dlimit_switch name="upper" port = "upper" normally_open="true" reset="false" reset_value="120.0"   />
			
			<encoder type="integrated" scale="0.000879" invert="false" />
			<pid kf="0.0" kp="0.25" ki="0.00125" kd="2.5" kz="200" cntl_min="-1.0" cntl_max="1.0" is_angle="false" />
        </motor>

		<setpoints>
			<setpoint index="0" name="sp0" position="-120" />
			<setpoint index="1" name="sp1" position="-80" />
			<setpoint index="2" name="sp2" position="-50" />
			<setpoint index="3" name="sp3" position="-30" />
			<setpoint index="4" name="sp4" position="-15" />
			<setpoint index="5" name="sp5" position="0" />
			<setpoint index="6" name="sp6" position="15" />
			<setpoint index="7" name="sp7" position="30" />
			<setpoint index="8" name="sp8" position="50" />
			<setpoint index="9" name="sp9" position="80" />
		</setpoints>
		
		<oi name="nudge_up"    device="keypad" chan="5" power="0.1"    step="5"  />
		<oi name="nudge_down"  device="keypad" chan="7" power="-0.1"   step="-5" />
		
        <oi name="setpoint_analog_idx" device="keypad" chan="4" step="0.078125" zero="-0.937500" />
    </control>
</robot>
