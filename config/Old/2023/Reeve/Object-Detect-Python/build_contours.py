import cv2 as cv
import numpy as np


def build_contours(mask, min_hull, max_hull, ar_thresh_low, ar_thresh_high, eps):
    """This function will take in a color threshold image as a numpyarray and output contours for found convex hulls
     on said image that contain the given parameters pixel locations on the input image.

    Input:
    - mask - Threshold mask frame
    - min_hull - the min number of lengths that define a convex hull
    - max_hull - THe max number of lengths that define a convex hull
    - ar_thresh_low - the min aspect ratio threshold for detection
    - ar_thresh_high - the max aspect ratio threshold for detection
    - game_flag - a flag that says we are looking for a cone if 0 and a cube if 1
    Output:
    - cone_hull - the hull of the cone
    - """
    cone_edges = cv.Canny(mask, 80, 160)
    cone_contours, _ = cv.findContours(np.array(cone_edges), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    approx_contours = []
    for c in cone_contours:
        approx = cv.approxPolyDP(c, eps, closed=True)
        approx_contours.append(approx)

    # Loop through convex hulls and areas:
    all_convex_hulls = []
    for ac in approx_contours:
        all_convex_hulls.append(cv.convexHull(ac))
    # Pull out non circular polygons
    cone_hulls = []
    for ch in all_convex_hulls:
        if min_hull <= len(ch) <= max_hull:
            x, y1, z = ch.shape
            xy = np.reshape(ch, (z, x))
            # determine eigs
            eigvals, eigvecs = np.linalg.eig(np.cov(xy))
            ar = max(eigvals) / min(eigvals)  # get the aspect ratio of the convex hull
            if ar_thresh_low <= ar <= ar_thresh_high:  # and ar <= ar_max_thresh:  # if we are long and skinny....
                # print(min(eigvals))
                # print(max(eigvals))
                # print(ar)
                # print(len(ch))
                cone_hulls.append(cv.convexHull(ch))
                # print("="*25)
            #     print("=" * 50)
            # show resultant images'
    return cone_hulls
