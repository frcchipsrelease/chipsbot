import matplotlib.pyplot as plt
import numpy as np

# Random data
num = 3
xy = np.random.random((2,num)) + 0.01 * np.arange(num)
print(xy)
eigvals, eigvecs = np.linalg.eig(np.cov(xy))
ar = max(eigvals) / min(eigvals)  # get the aspect ratio of the convex hull
print(ar)

fig, (ax1, ax2) = plt.subplots(nrows=2)
x,y = xy
center = xy.mean(axis=-1)
for ax in [ax1, ax2]:
    ax.plot(x,y, 'ro')
    ax.axis('equal')

for val, vec in zip(eigvals, eigvecs.T):
    val *= 2
    x,y = np.vstack((center + val * vec, center, center - val * vec)).T
    ax2.plot(x,y, 'b-', lw=3)

plt.show()