# -------------------------------------------
# Creator: Reeve Lambert
# Date: 1/27/2023
# Python Version: 3.10.9
# Description: Use OpenCV to detect and estimate location of Game peices
# -------------------------------------------
import matplotlib.pyplot as plt
import cv2 as cv
import numpy as np
from build_contours import build_contours
############################
# Open and configure  Camera
############################
cam_port = 2  # select camera to use (if multiple)
camera = cv.VideoCapture(cam_port, cv.CAP_DSHOW)  # initialize cv2 camera
camera.set(cv.CAP_PROP_FRAME_WIDTH, 1280)
camera.set(cv.CAP_PROP_FRAME_HEIGHT, 720)


# Color Parameters
##############################################
lph = 203
lps = 37 # needs to be 20 for a bright blob... but gets april tag... otherwise 44
lpv = 38
###########################################
dph = 255
dps = 100
dpv = 100
########################################
lyh = 100
lys = 100
lyv = 100
###########################################
dyh = 18
dys = 60
dyv = 25
########################################
# Filter by Color
hsv_yellow_light = np.array([lyh/2, lys*2.55 + 1, lyv*2.55 + 1], dtype=np.uint8)
hsv_yellow_dark = np.array([dyh/2, dys*2.55+1, dyv*2.55+1], dtype=np.uint8)
hsv_purple_light = np.array([lph/2, lps*2.55+1, lpv*2.55+1], dtype=np.uint8)
hsv_purple_dark = np.array([dph/2, dps*2.55+1, dpv*2.55+1], dtype=np.uint8)

# set base parameters
kernel_size = 5  # the size of the square kernel
kernel = np.ones((kernel_size, kernel_size))  # setup contour kernel
##############################
# Contour Detection for cone #
##############################
ar_cone_max = 250  # what should the aspect ratio be
ar_cone_min = 50
cone_min = 3
cone_max = 30
cone_eps = 10

blob_min = 18
blob_max = 40
ar_blob_max = 1000
ar_blob_min = 18
blob_eps = 0.5  # epsilon changes the step length of poly edges

# Filter Color on Cone Image
frame = 0
# image = cv.imread("Test_images/Cone_and_april_Field.jpg")
# image = cv.imread("Test_images/Bright_cone.jpg")
# image = cv.imread("Test_images/Cone_and_blob_dark.jpg")
# image = cv.imread("Test_images/blob_reflection_2.jpg")
image = cv.imread("Test_images/Blob_cone_april_afar_4.jpg")

while True:
    result, image = camera.read()
    if result:
        frame += 1
        image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
        cone_mask = cv.inRange(image_hsv, hsv_yellow_dark, hsv_yellow_light)
        cone_mask = cv.erode(cone_mask, None, iterations=0)
        cone_mask = cv.dilate(cone_mask, kernel, iterations=0)
        cone_mask = cv.medianBlur(cone_mask, kernel_size)
        cone_frame = cv.bitwise_and(image, image, mask= cone_mask)
        # filter color on Blob Image
        blob_mask = cv.inRange(image_hsv, hsv_purple_light, hsv_purple_dark)
        blob_mask = cv.erode(blob_mask, None, iterations=0)
        blob_mask = cv.dilate(blob_mask, kernel, iterations=0)
        blob_mask = cv.medianBlur(blob_mask, kernel_size)
        blob_frame = cv.bitwise_and(image, image, mask = blob_mask)

        cone_hulls = build_contours(cone_mask, cone_min, cone_max, ar_cone_min, ar_cone_max, cone_eps)
        print("="*50)
        cube_hulls = build_contours(blob_mask, blob_min, blob_max, ar_blob_min, ar_blob_max, blob_eps)

        pieces = image.copy()
        cv.drawContours(image=pieces, contours=cone_hulls, contourIdx=-1, color=(0, 0, 255), thickness=4,
                        lineType=cv.LINE_AA)
        cv.drawContours(image=pieces, contours=cube_hulls, contourIdx=-1, color=(0, 255, 0), thickness=4,
                        lineType=cv.LINE_AA)
        cv.imshow("current frame", pieces)
        cv.waitKey(1)
        # if frame%100 == 0:
            # cv.imshow("image about to be saved", image)
            # cv.waitKey(0)
            # cv.imwrite("./Test_images/image_test_"+ str(frame)+".png", image)