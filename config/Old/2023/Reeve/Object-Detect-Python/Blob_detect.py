# -------------------------------------------
# Creator: Reeve Lambert
# Date: 1/27/2023
# Python Version: 3.10.9
# Description: Use OpenCV to detect and estimate location of Game peices
# -------------------------------------------
import cv2
import cv2 as cv
import numpy as np
############################
# Open and configure  Camera
############################
cam_port = 2  # select camera to use (if multiple)
camera = cv.VideoCapture(cam_port, cv.CAP_DSHOW)  # initialize cv2 camera
camera.set(cv.CAP_PROP_FRAME_WIDTH, 1280)
camera.set(cv.CAP_PROP_FRAME_HEIGHT, 720)

#####################################
# setup blob detectors
#####################################
#setup blob blob dectors
params_blob = cv.SimpleBlobDetector_Params()
# filter by area
params_blob.filterByArea = True
params_blob.minArea = 400
# filter by Inertia
params_blob.filterByInertia = False
params_blob.maxInertiaRatio = 0.5
# Filter by circularity
params_blob.filterByCircularity = True
params_blob.minCircularity = 0.2
# filter by Convexity
params_blob.filterByConvexity = False
params_blob.minConvexity = 0.6

# Setup Cone Blob Detector
params_cone = cv.SimpleBlobDetector_Params()
# Filter by Area.
params_cone.filterByArea = False
params_cone.minArea = 40**2
# Filter by Circularity
params_cone.filterByCircularity = False
params_cone.minCircularity = 0.1
# Filter by Convexity
params_cone.filterByConvexity = False
params_cone.minConvexity = 0.8
params_cone.maxConvexity = 1.0
# Filter by Inertia
params_cone.filterByInertia = True
params_cone.maxInertiaRatio = 0.5


# Color Parameters
##############################################
lph = 203
lps = 44  # needs to be 20 for a bright blob... but gets april tag... otherwise 44
lpv = 38
###########################################
dph = 255
dps = 100
dpv = 100
########################################
lyh = 100
lys = 100
lyv = 100
###########################################
dyh = 18
dys = 45
dyv = 25
########################################
# Filter by Color
hsv_yellow_light = np.array([lyh/2, lys*2.55 + 1, lyv*2.55 + 1], dtype=np.uint8)
hsv_yellow_dark = np.array([dyh/2, dys*2.55+1, dyv*2.55+1], dtype=np.uint8)
hsv_purple_light = np.array([lph/2, lps*2.55+1, lpv*2.55+1], dtype=np.uint8)
hsv_purple_dark = np.array([dph/2, dps*2.55+1, dpv*2.55+1], dtype=np.uint8)


# set up blob dector
cone_detect = cv.SimpleBlobDetector_create(params_cone)
blob_detect = cv.SimpleBlobDetector_create(params_blob)


# Filter Color on Cone Image
image_cone = cv.imread("Test_images/blob_test_2.png")
image_cone_hsv = cv.cvtColor(image_cone, cv.COLOR_BGR2HSV)
cone_mask = cv.inRange(image_cone_hsv, hsv_yellow_dark, hsv_yellow_light)
cone_mask = cv.erode(cone_mask, None, iterations=0)
cone_mask = cv.dilate(cone_mask, None, iterations=0)
cone_frame = cv.bitwise_and(image_cone, image_cone, mask= cone_mask)

#filter color on Blob Image
image_blob = cv.imread("Test_images/blob_and_april.jpg")
image_blob_hsv = cv.cvtColor(image_blob, cv.COLOR_BGR2HSV)
blob_mask = cv.inRange(image_blob_hsv, hsv_purple_light, hsv_purple_dark)
blob_mask = cv.erode(blob_mask, None, iterations=0)
blob_mask = cv.dilate(blob_mask, None, iterations=0)
blob_frame = cv.bitwise_and(image_blob, image_blob, mask = blob_mask)

# set up the blob detector for the cones and setup points
cone_blobs = cone_detect.detect(cone_frame)
cone_keypoints = cv.drawKeypoints(cone_mask, cone_blobs, np.array([]), (0, 0, 255),
                                      cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# set up the blob detector for the purple cubes and setup points
# purple_blobs = blob_detect.detect(blob_frame)
purple_blobs = blob_detect.detect(cv.bitwise_not(blob_mask))
# blob_keypoints = cv.drawKeypoints(image_blob, purple_blobs, np.array([]), (0, 0, 255), cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
# blob_keypoints = cv.drawKeypoints(blob_frame, purple_blobs, np.array([]), (0, 0, 255), cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
blob_keypoints = cv.drawKeypoints(cv.bitwise_not(blob_mask), purple_blobs, np.array([]), (0, 0, 255), cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
#
#
# show resultant images
cv.imshow("current frame", blob_keypoints)
cv.waitKey(0)

# set up a simple color mask:

if frame_num % 100 == 0:
    cv.imwrite("blob_test.png", image_cone)


