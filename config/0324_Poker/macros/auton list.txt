	
   	<auton name="00 do nothing" file="/robot/config/macros/do_nothing.xml"/>
	<auton name="01 taxi" file="/robot/config/macros/taxi.xml"/>
	<auton name="02 (center) 1 note taxi" file="/robot/config/macros/1_note_taxi.xml"/>
	<auton name="03 (right) 1 note taxi" file="/robot/config/macros/right_1_note_taxi.xml"/>
	<auton name="04 (left) 1 note taxi" file="/robot/config/macros/left_1_note_taxi.xml"/>
	<auton name="05 RED AMP (right) 2 note" file="/robot/config/macros/red_amp_2_note.xml"/>
	<auton name="06 RED FENDER (speaker) 2 note" file="/robot/config/macros/red_fender_2_note.xml"/>
	<auton name="07 RED CENTER (left of speaker) 2 note" file="/robot/config/macros/red_center_2_note.xml"/>
	<auton name="08 BLUE AMP (left) 2 note" file="/robot/config/macros/blue_amp_2_note.xml"/>
	<auton name="09 BLUE FENDER (speaker) 2 note" file="/robot/config/macros/blue_fender_2_note.xml"/>
	<auton name="10 BLUE CENTER (right of speaker) 2 note" file="/robot/config/macros/blue_center_2_note.xml"/>
	<auton name="11 3 note (real)" file="/robot/config/macros/red_amp_3_note.xml"/>
	<auton name="13 3 note blue (real)" file="/robot/config/macros/blue_amp_centerline.xml"/>
