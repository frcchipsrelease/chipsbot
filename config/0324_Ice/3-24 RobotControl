<?xml version="1.0"?>
<robot name = "Ice">

    <interface>
        <device name="pilot"    type="joystick" num="0" />
        <device name="copilot"  type="joystick" num="1" />
    </interface>
    
    <control type="camera_server" name="cameras">
        <camera id="0" res_x="320" res_y="240" fps="15"/>
        <camera id="1" res_x="320" res_y="240" fps="15"/>
    </control>

	<!-- BASE -->
    <control type="swerve_drive" name="oldschool_swerve" drive_length="1.0" drive_width="1.0">
		<!-- ACTUATORS  -->
        <SwerveModule name="front_right">
            <motor name="rotational"       type="TalonFxCan" brake_mode="true"    port="3" invert="true"  gear_ratio="12.8"  >
                <pid kp="0.15" ki="0.0005" kd="0.005"/>
            </motor>
            <motor name="translational"    type="TalonFxCan" brake_mode="true"    port="4" invert="false"     />    
            <cancoder name="absolute_rot_cc" port="3" north="109.5"/>        
        </SwerveModule>
        <SwerveModule name="back_right">
            <motor name="rotational"       type="TalonFxCan" brake_mode="true"    port="7" invert="true"  gear_ratio="12.8"  >
                <pid kp="0.15" ki="0.0005" kd="0.005"/>
            </motor>
            <motor name="translational"    type="TalonFxCan" brake_mode="true"    port="8" invert="true"     />    
            <cancoder name="absolute_rot_cc" port="7" north="18.088"/>        
        </SwerveModule>
        <SwerveModule name="back_left">
            <motor name="rotational"       type="TalonFxCan" brake_mode="true"    port="5" invert="true"  gear_ratio="12.8" >
                <pid kp="0.15" ki="0.0005" kd="0.005"/>
            </motor>
            <motor name="translational"    type="TalonFxCan" brake_mode="true"    port="6" invert="false"     />    
            <cancoder name="absolute_rot_cc" port="5" north="-11.5"/>        
        </SwerveModule>
        <SwerveModule name="front_left">
            <motor name="rotational"       type="TalonFxCan" brake_mode="true"    port="1" invert="true"  gear_ratio="12.8"  >
                <pid kp="0.15" ki="0.0005" kd="0.005"/>
            </motor>
            <motor name="translational"    type="TalonFxCan" brake_mode="true"    port="2" invert="true"     />    
            <cancoder name="absolute_rot_cc" port="1" north="149.2"/>        
        </SwerveModule>

		<!-- SENSORS  -->
		<limelight name="VisionDrive"/>
		<gyro name="base_gyro" type="spi" port="CS0" />

		<!-- CONTROLS  -->
		<oi name="lateral" device="pilot" chan="0"  scale="1.0"  deadband="0.02" />
		<oi name="forward" device="pilot" chan="1"  scale="-1.0" deadband="0.02" />
		<oi name="rotate"  device="pilot" chan="2"  scale="1.0"  deadband="0.02" />
		<oi name="ll_control_hold"  device="copilot" chan="5"/>
		
    </control>

    <!-- INTAKE -->
    <control type="intake" name="intake" >
        <motor type="TalonSrxCan"  name="outer_motor" port="18"  invert="true" />
        <motor type="TalonSrxCan"  name="inner_motor" port="16"  invert="false" />
        
        <oi name="stop"	device="copilot" chan="1" />
        <oi name="dump" device="copilot" chan="3" inner="-0.5" outer="-0.7"/>
		<doi name="inner_backward"	device="copilot" chan="7" value="-0.25" />
        <doi name="outer_backward"	device="copilot" chan="8" value="-0.7" />
		<oi name="nat_intake"		device="copilot" chan="2" inner="0.2" outer="1.0" />
        <oi name="nat_intake"		device="pilot"   chan="8" inner="0.25" outer="0.75" />
		<oi name="shoot"            device="copilot" chan="4" inner="0.5" outer="1.0" />
	
        <!-- TODO doublecheck that this works -->
        <digital_input name="light_sensor" port="6"/>
    </control>
	
	<!-- SHOOTER -->
	<control type="Shooter" name="ice_shooter" closed_loop="false" limelight_targeting_state="false">

		<!-- ACTUATORS  -->
		<motor type="TalonSrxCan" name="kicker_motor" device_id="19" nominal="0.45" idle="-0.15" min_control="-0.75" max_control="0.75" />
        <motor type="TalonFxCan" name="fly_motor_1" device_id="9" invert="true" power_step_size="0.025" velocity_step_size="2500" nominal_power="0.45">
            <follower type="TalonFxCan" device_id="10" invert="true"/>
            <pid kf="0.0" kp="0.25" ki="0.0" kd="0.0" kz="0" cntl_min="-1.0" cntl_max="1.0" is_angle="true"/>
            <setpoints active="0" >
                <setpoint index="0" value="73000" name="vclose"/>
                <setpoint index="1" value="77000" name="auto"/>
                <setpoint index="2" value="142000" name="fly_vision"/>
                <setpoint index="3" value="137000" name="fly_terminal"/>
            </setpoints>
        </motor>
        <motor type="TalonSrxCan" name="hood_motor"   device_id="20" invert="false" min_control="-0.5" max_control="0.5"
			gear_ratio="0.0045"
            step_size="5.0"
            low_stop="35.508"
            high_stop="171.826" >
			<!-- Hood angle at back (lower) hard stop is 0.0 degrees -->
            <limit_switch name="fwd" port="7" normally_open="true" reset="false" reset_value="false" />
            <limit_switch name="bck" port="8" normally_open="true" reset="false" reset_value="false" />
            <setpoints active="0" >
                <setpoint index="0" value="10.000" name="vclose"/>
                <setpoint index="1" value="15.650" name="auto"/>
                <setpoint index="2" value="27.800" name="hood_vision"/>
                <setpoint index="3" value="27.000" name="hood_terminal"/>
            </setpoints>
        </motor>

		<!-- SENSORS  -->
        <cancoder name="cc_hood" port="00" north="40.16"/>
        <digital_input name="light_sensor" port="6"/>
        <hood_pid kf="0.0" kp="0.5" ki="0.003" kd="0.1" kz="5.0" cntl_min="-1.0" cntl_max="1.0" is_angle="true"/>
		<limelight name="VisionShoot" target_height="105.0" limelight_height="29.5" limelight_angle="45.0" target_hz_offset="0.0"/>
        <limelight_velocity_curve>
            <line distance="30.9" velocity="11050" angle="10.5"/>
			<line distance="42.0" velocity="107000" angle="15.7"/>
			<line distance="85.0" velocity="120250" angle="23.0"/>
			<line distance="131.0" velocity="144000" angle="27.7"/>
            <line distance="169.5" velocity="175000" angle="29.5"/>

        </limelight_velocity_curve>
        
		<!-- CONTROLS  -->
        <oi name="kicker"  device="copilot" chan="4" value="1.0	" />

        <oi name="fly_step_up"   	device="copilot" chan="6" />    
        <oi name="fly_step_down" 	device="copilot" chan="8" />

        <oi name="hood_control" 	device="copilot" chan="3" invert="true" scale="0.4" deadband="0.02" />

        <oi name="closed_loop_toggle"       	device="copilot" chan="12" />
        <oi name="limelight_targeting_toggle"   device="copilot" chan="10" />
    </control>

    <control type="hang" name="Hang" >
	  <motor type="TalonFxCan"  name="motor_1" port="12"  invert="true" >
        	</motor>
   	<motor type="TalonFxCan"  name="motor_2" port="11"  invert="false" >
	      <encoder invert="false" scale="0.00190083"/>
              <pid kf="0.0" kp="0.25" ki="0.0" kd="0.0" kz="0" cntl_min="-1.0" cntl_max="1.0" is_angle="true"/>
    		</motor>   
    <setpoints>
  	<setpoint name="first"   position="-90" index="0" />
    	<dsetpoint name="ross"   pjs="-45" index="1" />
	<dsetpoint name="yihan"  pjs="45" index="2" />
    	<dsetpoint name="rushi"  pjs="90" index="3" />
    </setpoints>

    <solenoid name="lock" module="0" port="2"  type="REV"/>
    <solenoid name="pivot" module="0" port="0"  type="REV"/>

    <oi name="setpoint0"            device="pilot"  chan="1" />
		
    <oi name="up"		device="pilot" chan="4" value="-0.75"  />
    <oi name="down"	device="pilot" chan="2"  value="1.0"  />
    <oi name="closed_loop_state"       device="pilot" chan="11" />
    <oi name="lock_engage"       device="pilot" chan="9" />
    <oi name="lock_release"       device="pilot" chan="10" />
    <oi name="pivot_forward"       device="pilot" chan="1" />
    <oi name="pivot_backward"       device="pilot" chan="3" />
   
    </control>
    
    <auton name="basic_auton" file="/robot/config/macros/basic_auton.xml" />
    <auton name="2_ball_auton" file="/robot/config/macros/2_ball_auton.xml" />
    <auton name="2_ball_shoot_auton" file="/robot/config/macros/2_ball_shoot_auton.xml" />

</robot>
