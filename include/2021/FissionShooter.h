/*******************************************************************************
 *
 * File: FissionShooter.h
 *  
 * Fission CHIPS shooter control class. Mirrors the generic ShooterControl class
 * with specialized implementations specific to the 2021 Fission bot
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"
#include "rfhardware/LimitSwitch.h"
#include "rfhardware/Limelight.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "gsutilities/PiecewiseLinear.h"

/*******************************************************************************	
 * 
 * Create an instance of a shooter control
 *
 ******************************************************************************/
class FissionShooter : public PeriodicControl, public OIObserver
{
	/***************************************
	 *      ATTRIBUTES                     * 
	 ***************************************/
	public:
		// Command data
		enum Command
		{
			CMD_CLOSED_LOOP_STATE = 0, CMD_CLOSED_LOOP_TOGGLE, CMD_CLOSED_LOOP_ON, CMD_CLOSED_LOOP_OFF,
			CMD_LIMELIGHT_STATE, CMD_LIMELIGHT_TOGGLE, CMD_LIMELIGHT_ON, CMD_LIMELIGHT_OFF,
			CMD_KICKER_FWD, CMD_KICKER_BCK, CMD_KICKER_TOGGLE, 
			CMD_HOOD_STEP_UP, CMD_HOOD_STEP_DN, CMD_HOOD_ANALOG,
			CMD_FLY_ON, CMD_FLY_OFF, CMD_FLY_STEP_UP, CMD_FLY_STEP_DN,
			CMD_SETPOINT_0, CMD_SETPOINT_1, CMD_SETPOINT_2, CMD_SETPOINT_3, CMD_DPAD_SETPOINTS,
			NUM_COMMANDS
		};
		static const uint8_t NUM_SETPOINTS = 4;


		// Allocation
		FissionShooter(std::string control_name, tinyxml2::XMLElement *xml);
		~FissionShooter();

		// Initialization
  		void controlInit();
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void initLogFile();
		int  parseSetPoints( tinyxml2::XMLElement* comp, float* setp );

		// Cyclical
		void doPeriodic();
		void updateConfig();
		void updateLogFile();
		void publish();

		// Control functions
		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setClosedLoop(bool closedLoopOn);
		bool isClosedLoop();

		void  setKickerPercent(float target);
		float getKickerPercent();
		float getKickerVelocity();

		void  setFlywheelPercent(float target);
		float getFlywheelPercent();
		float getFlywheelVelocity();

		float getHoodPosition();
		float getHoodVelocity();
        void setPid(BasicPid* pid);

		bool isAtTarget( float fly_tolerance, float hood_tolerance );

		float applyMomentary( bool mom, float val); // stick/unstick control
		void applySetPoint(int setpt);
		int   getSetPointIndex();

		void setLimeLightState (bool value);
		bool getLimeLightState (void);


	private:
		Motor* kicker_motor; // Always commanded on percent basis
		float  kicker_percent_target;
		float  kicker_percent_command;
		float  kicker_percent_nominal;
		float  kicker_velocity_actual;
		float  kicker_percent_actual;


		Motor* fly_motor;
		float  fly_percent_step; // open loop
		float  fly_percent_target; // [-1.0,1.0] open loop
		float  fly_percent_command; // [-1.0,1.0] open loop
		float  fly_percent_nominal; // default when turning on
		float  fly_percent_min;
		float  fly_percent_max;

		float  fly_setpoint[4];  // [-1.0,1.0] closed loop


		float  fly_velocity_step; // closed loop
		float  fly_velocity_actual; // closed loop
		float  fly_velocity_target; // closed loop
		float  fly_velocity_command;
		float  fly_velocity_error; // closed loop
		float  fly_velocity_max;
		float  fly_velocity_min;

		// TODO linke Srx's internal PID to CC external sensor for closed loop control
		Motor* hood_motor;

		rfh::Cancoder* hood_encoder;
	    BasicPid* hood_pid;
        LimitSwitch* hood_ls_upper;
		LimitSwitch* hood_ls_lower;
		bool hood_ls_upper_state;
		bool hood_ls_lower_state;
		int cc_resolution;

		float  hood_percent_target;
		float  hood_percent_command;
		float  hood_percent_nudge;
		float  hood_position_actual; // cc_hood
		float  hood_position_target; // cc_hood
		float  hood_position_error;  // cc_hood
		float  hood_position_kp;
		float  hood_position_internal_actual; // TalonSrxCan
		float  hood_position_internal_target; // TalonSrxCan
		float  hood_setpoint[4];
		float  hood_position_step;
		float  hood_gear_ratio;
		float  hood_position_stop_hi;
		float  hood_position_stop_lo;


		// TODO testing closed loop control
		// TODO replace mount height with some other geometry
		Limelight* ll_limelight;
		gsu::PiecewiseLinear ll_target_curves; // interpolant for setting targets
		std::vector<double> m_vertical_angle_table; // Set points: Limelight vertical angle - degrees
		std::vector<double> m_hood_position_table; // Set points: Hood position

		bool   lime_light_is_on;    // input. Do closed loop control via limelight or don't.		
        double ll_actual_angle_vertical; // measurement: vertical angle from center of lens (deg)
		double ll_is_visible; // measurement: bool as float due to LL's weird API
		double ll_actual_area; // measurement: steradians? Who knows, didn't see in LL documentation 
		double ll_actual_angle_horizontal;  // measurement: horizontal angle from center of lens (deg)
		double ll_actual_angle_roll; // measurement: roll angle of target from vertical datum (deg)
		double ll_target_height; // input: from the ground the bottom of the target
		double ll_mount_height_minimum; // input: from the ground to the center of lens when the hood is all the way forward (facing floor)
		double ll_mount_height_current;  // calculated: current height of limelight lens from the ground. 
										 // function of hood position, mount height minimum, and mount radius
		double ll_mount_angle_vertical; // measurement: current limelight mount angle relative to floor
		double ll_hood_error; // measurement: limelight's current rotational offset from target
		double ll_hood_target; // calculation: what value should closed loop drive ll_hood_error to
		double ll_mount_planar_offset; // Distance in some unspecified units from the calibration point
		double ll_height_difference; // The diiference between the target height and the limelight mounted height.
		                             // This may not be a permanent variable. May be replaced once we account for the 
         // TODO The limelight angle to drive to
        double ll_target_angle_vertical;
		// Logs
		DataLogger *log_auton; 
		DataLogger *log_teleop;
		DataLogger *log_active;

		// Logic
		bool active;
		bool closed_loop_on;

		int    setpoint_index; // {0,1,2,3} closed loop

		std::map<std::string,float&> setpoint_lookup; 

		// ??
		float RAD_TO_DEG = 57.2958;

};


/* ============================================================================================ */
/* ============================================================================================ */
/* ============================================================================================ */
class MSFissionShooter : public MacroStepSequence
{
	public:
		MSFissionShooter(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	protected:
		FissionShooter *m_parent_control;
		float fly_velocity_command;
		float hood_position_command;
		float kicker_percent_command;
		int setpoint_index;
		bool m_closed_loop;


		static const uint8_t NUM_SETPOINTS = FissionShooter::NUM_SETPOINTS;
		std::string m_setpoint_name[NUM_SETPOINTS];
		float m_setpoint_velocity[NUM_SETPOINTS];
		bool m_wait;
};

/* ============================================================================================ */
/* ============================================================================================ */
/* ============================================================================================ */
class MSFissionFlywheelPower : public MacroStepSequence
{
	public:
		MSFissionFlywheelPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	protected:
		FissionShooter *m_parent_control;
		float fly_percent_command;
};

/* ============================================================================================ */
/* ============================================================================================ */
/* ============================================================================================ */
class MSFissionKickerPower : public MacroStepSequence
{
	public:
		MSFissionKickerPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	protected:
		FissionShooter *m_parent_control;
		float kicker_percent_command;
};