/*******************************************************************************
 *
 * File: RobotMain.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "2024/ChipsBase.h"

/*******************************************************************************
 *
 * The main class for this robot. This is a thin wrapper for the Chips Base
 * Robot that allows additional types of controls to be added without
 * requiring changes to the base class.
 * 
 ******************************************************************************/
class RobotMain : public ChipsBase
{
    public:
 		RobotMain(void);
		~RobotMain();
};
