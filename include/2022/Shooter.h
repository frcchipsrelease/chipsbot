/*******************************************************************************
 *
 * File: Shooter.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>
#include <vector>

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"
#include "rfhardware/LimitSwitch.h"
#include "rfhardware/Limelight.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "gsutilities/PiecewiseLinear.h"

#include "frc/smartdashboard/SendableChooser.h"
#include "frc/AddressableLED.h"

/*******************************************************************************	
 * 
 * Create an instance of a shooter control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class Shooter : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_CLOSED_LOOP_STATE = 0, CMD_CLOSED_LOOP_TOGGLE, CMD_CLOSED_LOOP_ON, CMD_CLOSED_LOOP_OFF,
		CMD_LIMELIGHT_STATE, CMD_LIMELIGHT_TOGGLE, CMD_LIMELIGHT_ON, CMD_LIMELIGHT_OFF,
		CMD_HOOD_CONTROL, CMD_KICKER,
		CMD_FLY_TOGGLE, CMD_FLY_STEP_UP, CMD_FLY_STEP_DOWN,
		CMD_DPAD_SETPOINTS,
		NUM_COMMANDS};
		static const uint8_t NUM_SETPOINTS = 4;
		
		Shooter(std::string control_name, tinyxml2::XMLElement *xml);
		~Shooter(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();
		void updateLogFile();
		void initLogFile();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		// class-specific methods
		void setKickerPower(float power);
		void setFlywheelPower(float power);
		void setHoodPower(float power);
		void setFlywheelVelocity(float velocity);
		void setHoodPosition(float position);
		bool isAtTarget(int vel_tolerance, float pos_tolerance);

		void setClosedLoop(bool closedLoopOn);
		bool isClosedLoop();
		bool getLimeLightState();
		void setLimeLightState(bool value);

		int parseSetPoints(tinyxml2::XMLElement* comp, float* setp);
		void applySetPoint(int index);
		
        void setPid(BasicPid* pid);

		void publish(void);

		void setLedData(void);
		void setLedPattern(int pattern, bool silly);
		
		void setLedSolid(int r, int g, int b, bool silly);
		void setLedDashed(int r1, int g1, int b1, int r2, int g2, int b2, int length, bool silly);
		void setLedGradient(int start, int s, int v, int end, bool silly);
		void setLedRepeating(int r1, int g1, int b1, int r2, int g2, int b2, int length, bool silly);
		
		void setLedRainbow(bool silly);

	private:	

		/*
			775 motor for kicker - talon srx motor controller
			2x falcon 500s for flywheel - one is a follower
			snowblower motor for hood
			cancoder for hood
			2x limit switches for hood
			light sensor - detects if ball in intake
		*/
		// ------ HARDWARE -------------------
        Motor* m_kicker_motor;
		Motor* m_fly1_motor;
		Motor* m_hood_motor;

	    LimitSwitch* m_limit_fwd;
		LimitSwitch* m_limit_back;

		rfh::Cancoder* m_hood_cancoder;

		BasicPid* hood_pid;

		frc::AddressableLED *m_led;

		// ------ FLYWHEEL -------------------
		// open loop
		float fly_power_step; // % power for step up/down
		float fly_target_power; // target power for the fly motor(s) to run at
		float fly_nominal_power;
		float fly1_command_power; // power the fly motor(s) are being commanded to run at
		float fly_min_power;
		float fly_max_power;
		bool is_fly_on;
		float prev_fly_target;

		// closed loop
		float fly_target_velocity;
		float fly_command_velocity;
		float  fly1_actual_velocity;
		float  fly_velocity_step;
		float  fly_min_velocity;
		float  fly_max_velocity;
		float  fly_velocity_error;
		float fly_target_rpm;
		float fly_actual_rpm;
		const float ENCODER_TO_RPM;

		float  fly_setpoint[4];
		int setpoint_index;
		std::map<std::string,float&> setpoint_lookup; 
		

		// ------ HOOD -------------------
		float hood_setpoint[4];

		float hood_target_pos;
		float hood_actual_pos;
		bool closed_loop_on;
		float hood_position_error;
		float hood_target_power;
		float hood_command_power;

		bool limit_fwd_state; // is the fwd limit switch pressed? (true / false)
		bool limit_back_state; // is the back limit switch pressed? (true / false)


		// ------ MISC -------------------
        uint32_t m_kicker_max_current;
        uint32_t m_fly_max_current;

		float kicker_nominal_power; // input: kicker motor power when we press the "shoot" button
		float kicker_idle_power; // input: kicker motor power when a ball is in the intake

		// Open loop motor commands
		// Target power is the power you want the motor to run at
		float kicker_target_power;

		// Command power is the power that the motor is commanded to
		// This can be different than target power due to current limiting,
		// power ramp up and down and min and max motor commands
		float kicker_command_power;



		
		// ----------- LIMELIGHT TARGETING -------
		Limelight* ll_limelight;
		gsu::PiecewiseLinear ll_velocity_curves; // interpolant for setting targets
		gsu::PiecewiseLinear ll_angle_curves; // interpolant for setting targets
		std::vector<double> m_distance_table; // Set points: Limelight distance from target
		std::vector<double> m_velocity_table; // Set points: Flywheel velocity
		std::vector<double> m_angle_table; // Set points: Hood angle

		bool   lime_light_is_on;    // input: is closed loop limelight control on?		
        double ll_actual_angle_vertical; // measurement: vertical angle from center of lens (deg)
		double ll_is_visible; // measurement: bool as float due to LL's weird API. (0 = false, 1 = true)
		double ll_actual_area; // measurement: steradians? Who knows, didn't see in LL documentation 
		double ll_actual_angle_horizontal;  // measurement: horizontal angle from center of lens (deg)
		double ll_actual_angle_roll; // measurement: roll angle of target from vertical datum (deg)
		double ll_target_height; // input: from the ground to the bottom of the target (inches)
		double ll_mount_height; // input: ground to ll camera (inches)
		double ll_mount_angle_vertical; // input: current limelight mount angle FROM CAMERA relative to floor. (deg)
		double ll_target_velocity;		// target flywheel velocity according to limelight area
		double ll_mount_planar_offset; // distance from robot to target provided by tan formula (inches)
		double ll_distance_to_target; // distance from robot to target (inches) - can either be straight from the formula or corrected using angle offset 
		double ll_angle_offset; // optional, makes ll distance calculations more accurate. don't try to figure out how this works pls
		double ll_height_difference; // The difference between the target height and the limelight mounted height.
		const float RAD_TO_DEG = 57.2958;

		// Logs
		DataLogger *log_auton; 
		DataLogger *log_teleop;
		DataLogger *log_active;

		bool active;

		// LEDs
		int m_led_length;
		std::vector <frc::AddressableLED::LEDData> m_led_buffer;
		int led_offset;
		int move_limit;

		bool silly_mode; // (xml) enable this if you have two led strips linked together.

		int p_main;
		int p_sec;
		int current_pattern; // by default, this is the pattern selected from the smartdashboard menu
		int main_pattern; // from xml
		int sec_pattern; // from xml, used if a condition is triggered 
		std::vector<int> pattern_type; // solid, gradient, repeating_gradient = 1, 2, 3
		std::vector<bool> pattern_moving;
		std::vector<bool> pattern_dashed;

		std::vector<int> start_1;
		std::vector<int> start_2;
		std::vector<int> start_3;
		std::vector<int> end_1;
		std::vector<int> end_2;
		std::vector<int> end_3;
		std::vector<int> pattern_length;

		std::vector<std::string> main_pattern_name;
		frc::SendableChooser<int> main_pattern_chooser;
		std::vector<std::string> sec_pattern_name;
		frc::SendableChooser<int> sec_pattern_chooser;

		bool dis_led_lock;


};



// ---------------------------------------------------------------------------------------------------


class MSShooter : public MacroStepSequence
{
	public:
		MSShooter(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter *m_parent_control;
		float flywheel_target_velocity;
		float kicker_target_power;
		float hood_target_position;
		int setpoint_index;
		bool m_closed_loop;
		bool m_ll_control;


		static const uint8_t NUM_SETPOINTS = 7;
		std::string m_setpoint_name[NUM_SETPOINTS];

		uint8_t num_setpoints;
		float m_setpoint_velocity[NUM_SETPOINTS];
		bool m_wait;
};


/*******************************************************************************
 *
 * This Macro Step sets the target velocity for the flywheel.
 * NOTE: PID should probably?? be tuned more at some point, just know that for
 *		 now you should set the velocity to ~20k more than what you want
 *
 * WARNING: This just sets the velocity, it does not turn it off unless the
 *          provided value for velocity is 0.0.
 * WARNING 2: You MUST turn closed loop on before using this.
 *
 *  Example XML:
 *
 *	<step name="close_velocity" control="ice_shooter" type="ShooterFlywheelVelocity" velocity="90000" >
 *  	<connect type="next" step="spinup_wait"/>>
 *  </step>
 *
 ******************************************************************************/

class MSShooterFlywheelVelocity : public MacroStepSequence
{
	public:
    	MSShooterFlywheelVelocity(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter *m_parent_control;
		float m_flywheel_target_velocity;
};


/*******************************************************************************
 *
 * This Macro Step sets the power for the kicker motor.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="kicker_on" control="ice_shooter" type="ShooterKickerPower"	power="0.85" >
 *  	<connect type="next" step="kicker_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSShooterKickerPower : public MacroStepSequence
{
	public:
	MSShooterKickerPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter *m_parent_control;
		float m_kicker_target_power;

};

/*******************************************************************************
 *
 * This Macro Step sets the target position for the hood.
 *
 *
 *  Example XML:
 *
 *	<step name="angle_close" control="ice_shooter" type="ShooterHoodPosition"	angle="23.0" >
 *  	<connect type="next" step="kicker_on"/>>
 *  </step>
 *
 ******************************************************************************/
class MSHoodPosition : public MacroStepSequence
{
	public:
	MSHoodPosition(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter *m_parent_control;
		float m_hood_target_position;

};


/*******************************************************************************
 *
 * This Macro Step sets the limelight targeting state. (true / false)
 * NOTE: limelight targeting automatically sets the target flywheel velocity and
 *		 hood angle.
 *
 *  Example XML:
 *
 *	<step name="ll_target_on" control="ice_shooter" type="LimelightState"	state="true" >
 *  	<connect type="next" step="ll_spinup_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSLimelightState : public MacroStepSequence
{
	public:
	MSLimelightState(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter *m_parent_control;
		bool m_ll_state;

};

/*******************************************************************************
 *
 * This Macro Step sets the flywheel closed loop state. (true / false)
 *
 *
 *  Example XML:
 *
 *	<step name="closed_loop_on" control="ice_shooter" type="ClosedLoopState"	state="true" >
 *  	<connect type="next" step="fly_close"/>>
 *  </step>
 *
 ******************************************************************************/
class MSClosedLoopState : public MacroStepSequence
{
	public:
	MSClosedLoopState(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter *m_parent_control;
		bool m_closed_loop;

};
