/*******************************************************************************
 *
 * File:  Hang.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "units/pressure.h"
#include "rfhardware/Motor.h"
#include "rfhardware/BasicPid.h"
#include "rfutilities/DataLogger.h"
#include "frc/Solenoid.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class Hang : public PeriodicControl, public OIObserver
{
	public:
		enum Command
		{
			CMD_CLOSED_LOOP_STATE = 0,
			CMD_SETPOINT_0,
			CMD_SETPOINT_1,
			CMD_SETPOINT_2,
			CMD_SETPOINT_3,
			CMD_SETPOINT_4,
			CMD_SETPOINT_5,
			CMD_SETPOINT_6,
			CMD_SETPOINT_7,
			CMD_SETPOINT_IDX,
			CMD_HANG_UP,
			CMD_STOP,
			CMD_HANG_DOWN,
			CMD_INCREMENT_UP,
			CMD_DECREMENT_DOWN,
			CMD_INCREMENT_FWD,
			CMD_DECREMENT_BACK,
			CMD_LOCK_ENGAGE,
			CMD_LOCK_RELEASE,
			CMD_PIVOT_FORWARD,
			CMD_PIVOT_BACKWARD,
			CMD_STATE
		 };
    	enum
    	{
         	LOCK_ENGAGED = 0,
        	LOCK_RELEASED, // mapped to a boolean, keep only two options
   	};
    	enum
		{
        	PIVOT_BACKWARD = 0,
         	PIVOT_FORWARD, // mapped to a boolean, keep only two options
	   	};
		static const uint8_t NUM_SETPOINTS = 10;
		
		Hang(std::string control_name, tinyxml2::XMLElement *xml);
		~Hang(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setPosition(float position);
		float getPosition(void);

		void initLogFile(void);
		void updateLogFile();

		void setClosedLoop(bool closed);
		bool isClosedLoop(void);
//		int8_t getSetpointIndex(std::string setpoint_name);
		void applySetpoint(bool on, int idx);
		// bool isAtTarget(float tolerance); // jly: this is a shooter method why is it here lmao
		// Add other methods specific for your class
		void publish(void);

	private:	
	// Be sure to give your motors names that are easy to figure out	
        Motor* motor1;

		DataLogger* hang_log; 
    	bool is_ready;

    	frc::Solenoid *lock_solenoid;
		bool lock_state;
    	frc::Solenoid *pivot_solenoid;
		bool pivot_state;  
		float motor_min_control; // sdr what
		float motor_max_control;

		float motor_up_power;
		float motor_down_power; 

		//Position Variables

		std::string setpoint_name [NUM_SETPOINTS];
		float setpoint_position [NUM_SETPOINTS];
		bool setpoint_defined [NUM_SETPOINTS];

		//Closed Loop Stuff - Variables

		const float ENCODER_TO_IN;
		float min_position;
		float max_position;
		bool is_closed_loop; 
		float hang_actual_position; // encoder units
		float hang_target_position;	// INCHES
		float hang_position_inches; // INCHES; bottom of robot to top of hooks
		float hang_height_offset; // xml input, INCHES

        uint32_t motor_max_current;

		float motor_max_cmd_delta; // sdr what

		// Open loop motor commands
		// Target power is the power you want the motor to run at
		float hang_target_power;
		// Command power is the power that the motor is commanded to
		// This can be different thatn target power due to current limiting,
		// power ramp up and down and min and max motor commands
		float hang_command_power;
};

/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSHang : public MacroStepSequence
{
	public:
    	MSHang(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Hang *m_parent_control;

		float m_motor_power;
};


/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSHangMacroSTep2 : public MacroStepSequence
{
	public:
	MSHangMacroSTep2(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Hang *m_parent_control;

		double something;

};
