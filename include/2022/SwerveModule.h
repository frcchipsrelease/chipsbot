/*******************************************************************************
 *
 * File: SwerveModule.h
 *  
 * Drive and Rotation Motor with Closed Loop control
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"
#include "rfhardware/Cancoder.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

/*******************************************************************************	
 * 
 * Create an instance of a shooter control with velocity joint srx utilities and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 * 
 *  <control type="ShooterControl" [name="unnamed"] [closed_loop="false"] [period="0.1"]
 *  		[setpoint0="0.0"] [setpoint1="0.0"] [setpoint2="0.0"] [setpoint3="0.0"] [setpoint4="0.0"]
 *  		[setpoint5="0.0"] [setpoint6="0.0"]  >
 *
 *  	<motor name="front_right" [type="CanTalon"] [port="2"] [control_period=”10”] [invert="false"]>
 *   		[<encoder [invert=”false”] [scale=”1.0”] />]
 *  		[<pid [kf=”0.001”] [kp=”0.0”] [ki=”0.0”] [kd=”0.0”] />]
 *		</motor>
 *
 *
 *      [<oi name="closed_loop_state"  device="switches" chan="1" [invert="false"]/>]
 *
 *      [<oi name="analog"    device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" step="0.1" device="pilot" chan="5" [invert="false"]/>]
 *      [<oi name="decrement" step="0.1" device="pilot" chan="7" [invert="false"]/>]
 *      [<oi name="stop"      device="pilot" chan="1" [invert="false"]/>]
 *
 *      [<oi name="setpoint_idx" device="pilot" chan="0" [scale="0.2222222"]/>
 *
 *      [<oi name="setpoint0" device="pilot" chan="2" [invert="false"]/>]
 *      [<oi name="setpoint1" device="pilot" chan="3" [invert="false"]/>]
 *      [<oi name="setpoint2" device="pilot" chan="3" [invert="false"]/>]   
 *
 *      [<oi name="momentary0" device="pilot" chan="5" ol_power="0.6" cl_step="0.1" [invert="false"]/>]
 *      [<oi name="momentary1" device="pilot" chan="7" ol_power="-0.6" cl_step="-0.1" [invert="false"]/>]
 *      [<oi name="momentary2" device="pilot" chan="6" ol_power="1.0" cl_step="0.2" [invert="false"]/>]
 *      [<oi name="momentary3" device="pilot" chan="8" ol_power="-1.0" cl_step="-0.2" [invert="false"]/>]
 *
 *  </control>
 *  
 *  Note: either pot or aps can be used, if both are specified the pot will be
 *        used.
 *
 ******************************************************************************/
class SwerveModule : public PeriodicControl, public OIObserver
{
	friend class SwerveDriveControl;

	public:
		enum Command
		{
			CMD_FORWARD = 0, CMD_ROTATE
		};
		enum Mode
		{
			MODE_FULL = 0, // Standalone: acts as a top level control class
			MODE_SLAVE,    // Subsystem: master control must call doPeriodic
		};


		static const uint8_t NUM_SETPOINTS = 8;
		static const uint8_t NUM_MOMENTARIES = 4;
		Mode mode;

		SwerveModule(std::string control_name, tinyxml2::XMLElement *xml);
		~SwerveModule(void);

  		void controlInit(void);
		void updateConfig(void);
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();
		void doControl();

		void publish();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setPosition(float val);
		float getPostion(void);

		void setVelocity(float val);
		float getVelocity(void);
		
		float getAngle();
		float getAbsoluteAngle();
		float getRate();

		float getTranslationMotorPosition();
		float getTranslationMotorVelocity();
		float getRotationMotorPosition();
		float getRotationMotorVelocity();
		float getCancoderCurrentPosition();
		float getCancoderTargetPosition();
		float getCancoderErrorPosition();
		float getRotationMotorGearRatio();
		int getTranslationMotorReverse();

	protected:		
		void applyMomentary(bool on, int idx);

		Motor *m_rotational_motor;
		float m_rotational_gear_ratio;
		unsigned m_rotational_resolution;
		float m_rotational_position_max;
		float m_rotational_position_min;
		float m_rotational_position_target;
		float m_rotational_position_current;
		float m_rotational_position_error;
		float m_rotational_velocity_max;
		float m_rotational_velocity_current;
		float m_rotational_acceleration_max;
		float m_rotational_acceleration_current;

		Motor* m_translational_motor; 
		float m_translational_gear_ratio;
		float m_translational_velocity_max;
		float m_translational_velocity_current;
		float m_translational_velocity_target;
		float m_translational_velocity_error;
		float m_translational_acceleration_max;
		float m_translational_acceleration_current;
		float m_translational_acceleration_target;
		float m_translational_acceleration_error;
		int   m_translational_reverse;

		rfh::Cancoder* absolute_rot_cc;
		float cc_to_m_rot_scale;
		float cc_position_target;
		float cc_position_current;
		float cc_position_error;
		float cc_position_min;
		float cc_position_max;
		float cc_velocity_target;
		float cc_velocity_current;
		float cc_velocity_error;
		float cc_velocity_max;

		bool m_is_ready;
		bool m_closed_loop;
		
		float momentary_power[NUM_MOMENTARIES];
		float momentary_step[NUM_MOMENTARIES];
};
