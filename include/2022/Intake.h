/*******************************************************************************
 *
 * File: Intake.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

#include "rfhardware/Motor.h"
// Maybe, maybe not
#include "rfhardware/LimitSwitch.h"
#include "frc/Solenoid.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class Intake : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_INNERFORWARD=0, CMD_OUTERFORWARD, CMD_INNERBACKWARD,
		CMD_OUTERBACKWARD, CMD_STOP, 
		CMD_SHOOT_INANDOUT, CMD_SHOOT, CMD_NAT_INTAKE, CMD_DUMP, CMD_DEPLOY};
		
		Intake(std::string control_name, tinyxml2::XMLElement *xml);
		~Intake(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setInnerPower(float power);
		void setOuterPower(float power);
        void setIntakePower(float power);
		void setSolenoidState(bool state);

		// Add other methods specific for your class

		void publish(void);

	private:	
	// Be sure to give your motors names that are easy to figure out	
        Motor* innerMotor;
        Motor* outerMotor;

		frc::Solenoid *m_solenoid;
		bool solenoid_state;

		bool intakeMode;

		frc::DigitalInput *lightSensor;
		bool lightSensorState;

		float motor_min_control; // sdr what
		float motor_max_control;

        uint32_t innerMotor_max_current;
        uint32_t outerMotor_max_current;

		float motor_max_cmd_delta; // sdr what

		float innerMotor_forward_power;
		float innerMotor_backward_power;
		float outerMotor_forward_power;
		float outerMotor_backward_power;
		float innerMotor_nat_power;
		float outerMotor_nat_power;
		float innerMotor_shoot_power;
		float outerMotor_shoot_power;


		// Open loop motor commands
		// Target power is the power you want the motor to run at
		float innerMotor_target_power;
		float outerMotor_target_power;
		// Command power is the power that the motor is commanded to
		// This can be different thatn target power due to current limiting,
		// power ramp up and down and min and max motor commands
		float innerMotor_command_power;
		float outerMotor_command_power;
};

/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSIntakePower : public MacroStepSequence
{
	public:
    	MSIntakePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *m_parent_control;

		float inner_power;
		float outer_power;
};


/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSIntakeShoot : public MacroStepSequence
{
	public:
	MSIntakeShoot(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *m_parent_control;

		float inner_power;
		float outer_power;
};

class MSIntakeDeploy : public MacroStepSequence
{
	public:
	MSIntakeDeploy(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *m_parent_control;

		bool state;
};
