/*******************************************************************************
 *
 * File: SwerveDriveBaseControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>
#include <vector>
#include <cstdint>

#include "gsutilities/tinyxml2.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"

#include "frc/BuiltInAccelerometer.h"

#include "rfhardware/Motor.h"
#include "rfhardware/Gyro_Spi.h"
#include "rfhardware/Limelight.h"
// #include "rfhardware/ADIS16448_IMU.h"

#include "rfcontrols/PeriodicControl.h"
#include "2022/SwerveModule.h"


/*******************************************************************************
 *
 *  Provides a PeriodicControl implementation for driving the base of a robot
 *  using an a swerve drive interface:
 *  - fwd: Forward/reverse translation
 *  - lat: Left/right translation
 *  - rot: Rotational drive
 *
 * Note well the difference between drive methods.
 *
 * In JUKE mode, define rotation about the robot's static center (e.g. pivot
 * rotation about the vehicle center).
 *
 * In TOKYO mode, define rotation about the robot's
 * center of rotation such that the robot steers like a car, and is also able
 * to "drift" due to lateral input (thus the name: Tokyo Drift).
 *
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="swerve_drive" name="drive">
 *      [<position value="2.3 2.3 2.3" />]
 *      [<velocity value="2.3 2.3 2.3" />]
 *      [<acceleration value="2.3 2.3 2.3" />]
 *      [<rot_matrix value="1.2 2.1 3.1 4.1 5.1 6.1 7.1 8.1 9.1" />]
 *      [<quaternion value="1.1 2.1 3.1 4.1" />]
 *      [<motor name="drive_front_right" [type="Falcon"] [module="1"] [port="1"] [invert="false"] />]
 *      [<motor name="drive_front_left"  [type="Falcon"] [module="1"] [port="2"] [invert="false"] />]
 *      [<motor name="drive_back_left"   [type="Falcon"] [module="1"] [port="3"] [invert="false"] />]
 *      [<motor name="drive_back_right"  [type="Falcon"] [module="1"] [port="4"] [invert="false"] />]
 *      [<motor name="rotor_front_right" [type="Falcon"] [module="1"] [port="5"] [invert="false"] />]
 *      [<motor name="rotor_front_left"  [type="Falcon"] [module="1"] [port="6"] [invert="false"] />]
 *      [<motor name="rotor_back_left"   [type="Falcon"] [module="1"] [port="7"] [invert="false"] />]
 *      [<motor name="rotor_back_right"  [type="Falcon"] [module="1"] [port="8"] [invert="false"] />]
 *      TODO: FIMXE with sensor inclusions.
 *      [<accelerometer />]
 *      [<imu />]
 * 		TODO: FIXME with channels. These are copy paste artifacts!
 *      [<oi name="forward"      	device="pilot" chan="2" 	[scale="-0.8"|invert="false"]/>]
 *      [<oi name="lateral"      	device="pilot" chan="0" 	[scale="-0.8"|invert="false"]/>]
 *      [<oi name="turn"         	device="pilot" chan="3" 	[scale="0.8"|invert="false"]/>]
 *      [<oi name="brake_on"     	device="pilot" chan="2" 	[invert="false"]/>]
 *      [<oi name="brake_off"    	device="pilot" chan="0" 	[invert="false"]/>]
 *      [<oi name="brake_toggle" 	device="pilot" chan="3" 	[invert="false"]/>]
 *      [<oi name="brake_state"  	device="pilot" chan="4" 	[invert="false"]/>]
 *      [<oi name="speed_hi"      	device="pilot" chan="5" 	[invert="false"]/>]
 *      [<oi name="speed_lo"     	device="pilot" chan="7" 	[invert="false"]/>]
 *      [<oi name="speed_toggle"  	device="pilot" chan="6" 	[invert="false"]/>]
 *      [<oi name="speed_state"   	device="pilot" chan="8" 	[invert="false"]/>]
 *      [<oi name="autoatt_on"		device="pilot" chan="9"		[invert="false"]/>]
 *      [<oi name="autoatt_off"		device="pilot" chan="10" 	[invert="false"]/>]
 *      [<oi name="autoatt_toggle"	device="pilot" chan="11" 	[invert="false"]/>]
 *      [<oi name="autostrafe_state"	device="pilot" chan="12" 	[invert="false"]/>]
 *  </control>
 * 
 ******************************************************************************/
class SwerveDriveScheme
{
	public:
	enum AUTO_TYPE
	{
		AUTO_OFF_JUKE = 0,  // robot relative drive orientation
		AUTO_GLOBAL_JUKE,   // "global" cartesian coordinates, i.e. field-relative drive orientation
		AUTO_OFF_TOKYO,     // "global" cartesian coordinates, i.e. field-relative drive orientation  (TODO)
		AUTO_ROTATE_STATIC, // track a "static" rotation target while translating in cartesian
                            // coordinates e.g. strafe while facing a goal or 
                            // static field item  (TODO)
		AUTO_ROTATE_MOBILE, // track a "mobile" rotation target while translating in cartesian
		                    // coordinates e.g. strafe while facing another robot or moving ball
		AUTO_ROTATE_SENSOR,  // Track a rotation target based on sensor input (i.e. center camera)
        AUTO_ORBIT_STATIC, // track a "static" target state via cylindrical
                            // coordinates e.g. move along 3pt arc of basketball court while facing
                            // hoop  (TODO)
        AUTO_ORBIT_MOBILE // track a "mobile" target via cylindrical coordinates
                            // e.g. orbit around another robot as it moves (TODO)

	};

	std::string name;
	AUTO_TYPE type;

	float pos_wrt_global[3];   // wrt is "with respect to"
	float vel_wrt_global[3];
	float acc_wrt_global[3];
	float T_wrt_global[3][3]; // transformation matrix from global frame to swerve drive reference frame
	float Q_wrt_global[4];    // a + b*i + c*j + d*k Quaternion scalar first
	float omg_wrt_local[3];   // rotation rate wrt self reference frame
	float alpha_wrt_local[3];

	// TODO wrt robot members
	// TODO calculate relative states/update functions
	// TODO replace these members with a node class when complete coordinate
	//      system architecture
	SwerveDriveScheme(std::string target_name, tinyxml2::XMLElement *xml);
	SwerveDriveScheme();
};

class SwerveDriveControl : public PeriodicControl, public OIObserver
{
	// Data Types -------------------------------------------------------------
public:

	enum Command
	{
		// Basic actuation
	    CMD_FORWARD = 0,  CMD_LATERAL,     CMD_ROTATE, // analog inputs
		CMD_ROT_STEP_LEFT, CMD_ROT_STEP_RIGHT, // Tweak rotation
		// behavior mods
		CMD_BRAKE_ON,     CMD_BRAKE_OFF,   CMD_BRAKE_TOGGLE,   CMD_BRAKE_STATE,   // Attempt to hold position: fight against pushing (TODO)
		CMD_SPEED_HI, 	  CMD_SPEED_LO,    CMD_SPEED_TOGGLE,   CMD_SPEED_STATE,   // Switch between high/low gear (coarse/fine)
		// Control scheme algorithm switching
		CMD_SCHEME_INCR, CMD_SCHEME_DECR, CMD_SCHEME_INT,
		CMD_JUKE,        CMD_GLOBAL_JUKE,  CMD_TOKYO,	   CMD_LL_HOLD,
		CMD_SCHEME_3,    CMD_SCHEME_4,    CMD_SCHEME_5,    CMD_SCHEME_6, // For higher indexes use CMD_SCHEME_INT
        // Sensors
		CMD_GYRO_CALIBRATE, CMD_MODULE_CALIBRATE, // Swerve module calibration should really only happen in testing

		CMD_DPAD_SETPOINTS,

		NUM_COMMANDS   // NOTE: This one must always be last as it holds the number of Commands in this enum
	};
	enum MOTOR_ID // Assumes rectangular drive base
	{
		FRONT_RIGHT = 0,
		BACK_RIGHT,
		BACK_LEFT,
		FRONT_LEFT
	};

	//
	// METHODS ----------------------------------------------------------------
	//
	
	// --- Mem management ---

public:
	/**
	 * Explicit Constructor from XML block
	 */
	SwerveDriveControl(std::string name, tinyxml2::XMLElement *xml);
	/**
	 * Destructor
	 */
	~SwerveDriveControl(void);

	// --- Initialization Methods ---

public:
	/**
	 * @brief Initialize all control inputs to a default state
	 */
	void controlInit();
	/**
	 * @brief (Re)Initialize all control inputs to default state and clear log
	 */
	virtual void disabledInit();
	/**
	 * @brief Initialize all control inputs to a default state for autonomous
	 */
	virtual void autonomousInit();
	/**
	 * @brief Initialize all control inputs to a default state for teleoperation
	 */
	virtual void teleopInit();
	/**
	 * @brief Initialize all control inputs to a default state for testing
	 */
	virtual void testInit();
	/**
	 * @brief Calibrate cancoders wheel modules' north as current attitude
	 */
	virtual void calibrate_modules();

protected:
	/**
	 * @brief Open a log file
	 * 
	 * @param[in] phase   name of the log file
	 */
	virtual void logFileInit(std::string phase);

	// --- Interface Methods ---

public:
	/**
	 * @brief Set analog value from input
	 * 
	 * @param[in] id int cast of Command, identifies context of input val
	 * @param[in] val value commanded for the analog destination
	 */
	void setAnalog(int id, float val);
	/**
	 * @brief Set digital value from input
	 * @param[in] id int cast of Command, identifies context of input val
	 * @param[in] val bool commanded for the digital destination
	 */
	void setDigital(int id, bool val);
	/**
	 * @brief UNUSED!!!!!
	 * 
	 * @param[in] id int cast of Command, identifies context of input val
	 * @param[in] val int commanded for the integer destination
	 */
	void setInt(int id, int val);

	void setDriveScheme(uint32_t scheme);

	void rotToAngle(float angle);

	// --- Update Methods ---

public:
	/**
	 * @brief Calls all functions that must be processed every software frame.
	 */
	virtual void doPeriodic();
	/**
	 * @brief Communicate top level diagnostic data to live feeds
	 */
	 void publish();
protected:
	/**
	 * @brief Log all raw diagnostic data
	 */
	virtual void logFileAppend(void);
	/**
	 * @brief Update swerve motors via tokyo method
	 */
	void update_drive_tokyo();
	/**
	 * @brief Update swerve motors via juke method
	 */
	void update_drive_juke();

	//
	// ATTRIBUTES -------------------------------------------------------------
	//

	// --- Hardware Setup ---

public:
	/**
	 * Front right module (includes motors, sensors, etc.)
	 */
	SwerveModule* front_right;
	/**
	 * Front left module (includes motors, sensors, etc.)
	 */
	SwerveModule* front_left;
	/**
	 * Back left module (includes motors, sensors, etc.)
	 */
	SwerveModule* back_left;
	/**
	 * Back right module (includes motors, sensors, etc.)
	 */
	SwerveModule* back_right;
	/**
	 * distance from front wheel center to back wheel center
	 */
	float distance_front_to_back; 
	/**
	 * distance from left wheel center to right wheel center
	 */
	float distance_left_to_right;
	/**
	 * distance robot center to wheel center
	 */
	float distance_robot_radius;
	/**
	 * Linear acceleration sensor TODO
	 */
	frc::Accelerometer* m_accelerometer;

	/**
	 * Gyroscope: linear and acceleration sensor
	 */
	//ADIS16448_IMU *m_imu;
    /**
     * Gyroscope: ADXRS450 
     */
    rfh::Gyro* gyro;
	/**
	 * Logger for printing diagnostic data
	 */
	DataLogger *m_data_logger;
    /**
     * Limelight: detects angles to target
     */
    Limelight* ll_limelight;
    double ll_target_is_visible;
    double ll_target_angle_horizontal;
	double ll_target_angle_vertical;
    double ll_target_area;
	double ll_target_angle_roll;
	double ll_target_hz_offset;
		
	// --- Inputs ---

public:
	/** 
	 * Engage status of the software brake
	 */
	bool brake_engaged; 
	/**
	 * Drive speed modifier
	 * 
	 * true:  actuate drive motors at full speed 
	 * false: actuate drive motors scaled by low_power_scale
	 */
	bool speed_high; 
	/**
	 *  attitude target
	 */
	SwerveDriveScheme* drive_scheme_current;
	/**
	 * index of attitude targets
	 */
	unsigned drive_scheme_index;
	/**
	 * List of attitude targets
	 */
	std::vector<SwerveDriveScheme> drive_scheme;
	/**
	 * Commanded forward power percent (analog)
	 */
	float fwd_cmd;
	/** 
	 * Commanded lateral power percent (analog)
	 */
	float lat_cmd;
	/**
	 * Commanded rotational power percent (analog)
	 */
	float rot_cmd;
	/**
	 * Hand control deadzone: analog inputs less than this value are ignored
	 */
	float hc_deadzone;
	/**
	 * Robot-relative forward magnitude (calculated)
	 */
	float fwd_drive;
	/**
	 * Robot-relative lateral magnitude (calculated)
	 */
	float lat_drive;
	/**
	 * Commanded drive omega percent (calculated)
	 */
	float rot_drive;
	/**
	 * Scale multiplier for drive motor output when speed_high is set false
	 */
	float low_power_scale;

	float gyro_angle_current;
	float gyro_angle_target;
	float gyro_angle_error;
	float gyro_rate_current;
	float gyro_rate_target;
	float gyro_rate_error;
	
	double m_accelerometer_x;
	double m_accelerometer_y;
	double m_accelerometer_z;


	double m_imu_accel_x;
	double m_imu_accel_y;
	double m_imu_accel_z;

	double m_imu_mag_x;
	double m_imu_mag_y;
	double m_imu_mag_z;

	double m_imu_rate_x;
	double m_imu_rate_y;
	double m_imu_rate_z;

	double m_imu_angle_x;
	double m_imu_angle_y;
	double m_imu_angle_z;

	double m_imu_roll;
	double m_imu_pitch;
	double m_imu_yaw;

	double m_imu_quat_w;
	double m_imu_quat_x;
	double m_imu_quat_y;
	double m_imu_quat_z;

	double m_imu_bar_press;
	double m_imu_temperature;

private:
	bool m_in_auton_mode;
};


// ============================================================================
// ============================================================================

/******************************************************************************
 *
 * This Macro Step sets the forward power, lateral power, turn power, and 
 * strafe state of the Swerve Drive PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn them off unless the
 *          provided values for forward, turn, and strafe turn the drive off.
 *
 *  Example XML:
 *
 *	<step name="drive_1" control="drive" type="DrivePower"
 *			forward="0.5" lateral="0.1" turn="0.1" strafe="false">
 *  	<connect type="next" step="drive_wait"/>>
 *  </step>
 *
 *****************************************************************************/
class MSSwerveDriveDrivePower : public MacroStepSequence
{
	public:
		MSSwerveDriveDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep* update(void);

	private:
		SwerveDriveControl *parent_control;

		float forward_power;
		float lateral_power;
		float rotate_power;
};

class MSSwerveDriveSetScheme : public MacroStepSequence
{
	public:
		MSSwerveDriveSetScheme(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep* update(void);

	private:
		SwerveDriveControl *parent_control;

		uint32_t drive_scheme;    // TODO: Is this the right type?
};

class MSSwerveDriveRotateAngle : public MacroStepSequence
{
	public:
		MSSwerveDriveRotateAngle(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep* update(void);

	private:
		SwerveDriveControl *parent_control;

		float degrees;
		float target_angle;
};
