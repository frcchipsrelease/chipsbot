/*******************************************************************************
 *
 * File: YourClassName.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once


#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "gsutilities/tinyxml2.h"
#include "rfutilities/DataLogger.h"

#include "rfhardware/Motor.h"
// Maybe, maybe not
#include "rfhardware/LimitSwitch.h"


/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class Intake : public PeriodicControl, public OIObserver
{
	public:
		enum {PIVOT_UP, PIVOT_UP_SLOW, PIVOT_DOWN, ROLLER_STOP, ROLLER_OUT, ROLLER_IN, LIFT_UP, LIFT_DOWN, BLOOPER_IN, BLOOPER_OUT, CLOSED_LOOP_STATE, SETPOINT_0, SETPOINT_1, SETPOINT_2, SETPOINT_3, Intake_Analog_SetPoint=11, Lift_Analog_SetPoint=12, setLiftPosition_0, setLiftPosition1, setLiftPosition2};
		
		static const uint8_t NUM_SETPOINTS = 20;

		Intake(std::string control_name, tinyxml2::XMLElement *xml);
		~Intake (void);
		void setRollerPower(float, int);
		void setKickerPower(float, int);

  		void controlInit(void);
		void updateConfig(void);
		//void PeriodicControl(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		// Add other methods specific for your class

		void publish(void);
		void togglePosition();
		void setClosedLoop(bool closed);
        bool isClosedLoop(void);
        void applySetPoint(bool on, int idx);
		void setLiftPosition(float position);
		float getLiftPosition(void);
		void setPivotPosition(float position);
		float getPivotPosition(void);
		int getSetpointIndex();

		void initLogFile(void);
		void updateLogFile();

		Motor* pivot_motor;
		Motor* roller_motor;
		Motor* lift_motor;
		Motor* blooper_motor;
		Motor* kicker_motor;

		BasicPid* lift_pid;
		BasicPid* pivot_pid;

		LimitSwitch* lift_limit_up;
		LimitSwitch* lift_limit_down;

		LimitSwitch* pivot_limit_down;
		LimitSwitch* pivot_limit_up;


	private:	
	// Be sure to give your motors names that are easy to figure out	
	

// Please use variable names that are easy to decipher

		DataLogger* lift_log;
		DataLogger* pivot_log;

		bool lift_upper_limit_pressed;
		bool lift_lower_limit_pressed;
		bool pivot_upper_limit_pressed;
		bool pivot_lower_limit_pressed;


		float motor_min_control; // sdr what
		float motor_max_control;

		float motor_roller_out_max_power;
		float motor_roller_in_max_power;

		float lift_up_max_power;
		float lift_down_max_power;
		float pivot_up_max_power;
		float pivot_down_max_power;
		float pivot_up_slow;



		


		float motor_max_cmd_delta; // sdr what

		// Open loop motor commands
		// Target power is the power you want the motor to run at
		float pivot_motor_target_power;
		float roller_motor_target_power;
		float lift_motor_target_power;
		float blooper_motor_target_power;
		float kicker_target_power;
		// Command power is the power that the motor is commanded to
		// This can be different than target power due to current limiting,
		// power ramp up and down and min and max motor commands
		float pivot_motor_command_power;
		float roller_motor_command_power;
		float lift_motor_command_power;
		float blooper_motor_command_power;
		float kicker_command_power;


		//Position Variables
		std::string setpoint_name [NUM_SETPOINTS];
		float lift_setpoint_position [NUM_SETPOINTS];
		float pivot_setpoint_position [NUM_SETPOINTS];
		bool setpoint_defined [NUM_SETPOINTS];
	
		//Closed Loop Stuff - Variables
		bool is_closed_loop;
		bool is_ready;
		int setpoint_index;

		float min_position_lift;
		float max_position_lift;
		float lift_actual_position;
		float lift_target_position;
		float lift_position_error;
		float lift_offset_angle;

		float min_position_pivot;
		float max_position_pivot;
		float pivot_actual_position;
		float pivot_target_position;
		float pivot_position_error;
		float pivot_offset_angle;

		bool light_sensor_adjusted;
		int light_sensor_count;
		float light_sensor_delay; // seconds


};

/*******************************************************************************
 * commands rollers in
 ******************************************************************************/
class MSRollersIn: public MacroStepSequence
{
	public:
		MSRollersIn(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands rollers out
 ******************************************************************************/
class MSRollersOut: public MacroStepSequence
{
	public:
		MSRollersOut(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands rollers off
 ******************************************************************************/
class MSRollersOff: public MacroStepSequence
{
	public:
		MSRollersOff(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands pivot down (open loop)
 ******************************************************************************/
class MSPivotDown: public MacroStepSequence
{
	public:
		MSPivotDown(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands pivot up (open loop)
 ******************************************************************************/
class MSPivotUp: public MacroStepSequence
{
	public:
		MSPivotUp(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands pivot power to 0 (open loop)
 ******************************************************************************/
class MSPivotStop: public MacroStepSequence
{
	public:
		MSPivotStop(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};


/*******************************************************************************
 * commands lift to desired setpoint
 * @param idx setpoint index
 ******************************************************************************/
class MSLiftSetpoint: public MacroStepSequence
{
	public:
		MSLiftSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
		int idx = 0;
};

/*******************************************************************************
 * commands blooper in
 ******************************************************************************/
class MSBlooperIn: public MacroStepSequence
{
	public:
		MSBlooperIn(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands blooper out
 ******************************************************************************/
class MSBlooperOut: public MacroStepSequence
{
	public:
		MSBlooperOut(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};

/*******************************************************************************
 * commands blooper off
 ******************************************************************************/
class MSBlooperOff: public MacroStepSequence
{
	public:
		MSBlooperOff(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Intake *parent_control;
};
