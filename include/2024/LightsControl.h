/*******************************************************************************
 *
 * File: LightsControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *  Copywrite and License information can be found in the LICENSE.md file 
 *  distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfhardware/LightsStrip.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

/*******************************************************************************	
 * 
 ******************************************************************************/
class LightsControl : public PeriodicControl, public OIObserver
{
	public:
		enum Pattern {
			DISABLED = 0, 	AUTON, 	TELEOP,
			INTAKE,		ENDGAME
		};

		LightsControl(std::string control_name, tinyxml2::XMLElement *xml);
		~LightsControl(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void publish(void);

		static bool getIntakeSensorState(void);

		void setPattern(int p);

	private:	
        LightsStrip* lights;

		int RED		[3]	= {128, 0, 0};  // im not using wpilib color class its so goofy
		int ORANGE	[3]	= {160, 24, 0};
		int YELLOW	[3]	= {64, 48, 0};
		int GREEN	[3]	= {0, 96, 0};
		int CYAN	[3] = {0, 48, 32};
		int BLUE	[3]	= {0, 0, 96};
		int PURPLE	[3]	= {32, 0, 64};
		int MAGENTA	[3] = {64, 0, 32};
		int WHITE	[3]	= {48, 44, 40};
		int NONE	[3]	= {0, 0, 0};
		// plus year specific colors idk

		int length = 0;
		int pattern = 0;
		int move_count = 0;
		int intake_count = 0;
		float intake_pattern_time = 1.0;

		frc::DigitalInput* intake_sensor;
		static bool intake_sensor_state;

};