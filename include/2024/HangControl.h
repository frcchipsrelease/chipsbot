/*******************************************************************************
 *
 * File: HangControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "frc/Servo.h"

#include "rfhardware/Motor.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="hang" [name="unnamed"] >
 *
 *      <motor name="hooks_motor" 
 *          [type="Victor"] [module="1"] [port="1"] [invert="false"]
 * 				[hooks_down_position="-3.0"]
 *				[hooks_stow_position="0.0"]
 *				[hooks_up_position="3.0"]
 *				[hooks_step_size="0.5"]
 *				[hooks_unlock_step_size="0.75"]
 *				[hooks_position_tolerance="0.25"]
 *				[hooks_enabled="true"]               // set this to false for testing the servo
 *      />
 * 
 * 		<servo name="brake_servo" [port="1"] 
 * 
 *      />
 * 
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class HangControl : public PeriodicControl, public OIObserver
{
	public:
		enum 
		{
			CMD_BRAKE_ENGAGE, CMD_BRAKE_RELEASE,
			CMD_BRAKE_STEP_UP, CMD_BRAKE_STEP_DOWN,
			// CMD_HOOKS_RAISE, CMD_HOOKS_LOWER, CMD_HOOKS_STOW, 
			CMD_HOOKS_STEP_RAISE, CMD_HOOKS_STEP_LOWER,
			// CMD_DPAD_TRANSLATION
		};
		
		HangControl(std::string control_name, tinyxml2::XMLElement *xml);
		~HangControl(void);

  		void controlInit(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);
		
		void publish(void);

	private:	
		// Be sure to give your motors names that are easy to figure out	
    	bool m_is_ready;
	    Motor* hooks_motor;
		frc::Servo	*brake_servo;

		// Closed Loop Stuff - Variables
		float hooks_step_size;
		float hooks_unlock_step_size;

		float hooks_down_position;  // hooks as far down as they go
		float hooks_up_position;  // hooks as far up as they go
		float hooks_stow_position;   // hooks just in stow
		float hooks_actual_position;
		float hooks_target_position;
		float hooks_command_position;
		float hooks_position_tolerance;
		float hooks_position_error;
		float hooks_unlock_position;

		bool hooks_enabled; 

	    // Servo brake to keep the hang locked at power off  
		float brake_command_position;
		float brake_target_position;
		float brake_step_size;
		float brake_max_position;
		float brake_min_position;
		float brake_release_position;
		float brake_engage_position;

		// bool do_brake_release;
};
