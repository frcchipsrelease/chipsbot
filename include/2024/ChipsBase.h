/*******************************************************************************
 *
 * File: ChipsBase.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <vector>
#include <string>

#include "frc/TimedRobot.h"
#include "frc/smartdashboard/SendableChooser.h"

#include "rfcontrols/ControlThread.h"
#include "rfutilities/MacroController.h"
#include "rfutilities/OIObserver.h"

/*******************************************************************************
 *
 * The main class for this robot controls the creation of the robot, loading
 * the configuration from file, and controlling the transition between
 * operation phases.
 * 
 ******************************************************************************/
class ChipsBase : public frc::TimedRobot, public OIObserver
{
    public:
        enum Phase
        {
            INIT, DISABLED, AUTON, TELEOP, TEST
        };

        enum Command
        {
            CMD_START_MACRO = 200, CMD_START_MACRO_MAX = 299,
            CMD_ABORT_MACRO = 300, CMD_ABORT_MACRO_MAX = 399
        };

        ChipsBase(void);
        virtual ~ChipsBase();

		virtual void RobotInit(void);
		virtual void LoadRobot(std::string file);
		virtual void UpdateConfig(void);
		virtual void UpdateAutonConfig(void);

		virtual void DisabledInit(void);
		virtual void DisabledPeriodic(void);

		virtual void AutonomousInit(void);
		virtual void AutonomousPeriodic(void);

		virtual void TeleopInit(void);
		virtual void TeleopPeriodic(void);

		virtual void TestInit(void);
		virtual void TestPeriodic(void);

		virtual void setAnalog(int id, float val);
		virtual void setDigital(int id, bool val);
		virtual void setInt(int id, int val);

    protected:
        virtual void RegisterControls(void);

	private:
        class AutonIndex
        {
            public:
                AutonIndex(int i) : index(i) {}
                int value(void) {return index;}
            private:
                int index;
        };

        virtual bool isAutonAvailable();
        virtual void publish(void);

        std::vector<ControlThread*> m_controls;

        MacroController *m_macro_controller;
		std::vector<std::string> m_macro_name_list;
		std::vector<std::string> m_macro_file_list;

        std::vector<std::string> m_auton_name_list;
        std::vector<std::string> m_auton_file_list;

        int         m_auton_file_count;
        int         m_auton_select;
        int         m_auton_select_loaded;
        int         m_auton_select_mismatch;
        bool        m_auton_enabled;
        float       m_auton_delay;
        bool        m_auton_delay_enabled;
        double      m_auton_start_time;
        bool        m_auton_started;
        std::string m_auton_description;

        frc::SendableChooser<AutonIndex *> m_auton_chooser;

        uint8_t m_publish_count;

        Phase m_robot_phase;
};
