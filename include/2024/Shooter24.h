/*******************************************************************************
 *
 * File: Shooter24.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Notes:
 * 	Remember to replace "Shooter24" with the name of your subsystem
 * 	throughout the entire file.
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

// Do not remove - all subsystems
#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfhardware/Limelight.h"
#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"
#include "gsutilities/PiecewiseLinear.h"


// Add as needed


/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class Shooter24 : public PeriodicControl, public OIObserver
{
	public:
		// enumerates (assigns a positive integer value to) all the commands that can be sent from the controller
		enum {	CMD_FLY_STEP_UP, CMD_FLY_STEP_DOWN,
				CMD_LOOP_TOGGLE, CMD_FLY_TOGGLE, CMD_LIMELIGHT_TOGGLE,
				CMD_SETPOINT_0, CMD_SETPOINT_1, CMD_SETPOINT_2, CMD_SETPOINT_3, 
				CMD_DPAD_SETPOINTS,
				CMD_KICK_FWD, CMD_KICK_BACK, CMD_SPEED_UP, CMD_SPEED_DOWN}; // commands that can be sent from the joystick to the subsystem. OK to edit
		static const uint8_t NUM_SETPOINTS = 20;

		// These methods are in every class. Do not remove!
		Shooter24(std::string control_name, tinyxml2::XMLElement *xml);
		~Shooter24(void);

		bool flyOn(void);
		bool isClosedLoop(void);
		void setClosedLoop(bool state);
  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void shootToggle();
		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);
		void getKickerPower(float power);
		
		void publish(void);

		// Add other methods specific for your class
		void setMotorPower(float power);
		void setSolenoidState(bool state);
		void applySetpoint(int i);
		int getSetpointIndex();
		
		void setLimelightPipeline(int idx);
		bool isHome();

		static bool isKickerRunning(void);


	private:	
		// Be sure to give your motors names that are easy to figure out
		// Even if you know what a certain codename means, other people don't!
		// Usually we name motors in camelCaps: for example, frontMotor and backMotor
        Motor *fly_motor;
		Motor *joint_motor;
		Motor *kicker_motor;
		LimitSwitch *bottom_limit;
		Limelight *lime_light;
		frc::DigitalInput *innerLightSen;
		float fly1_powers[NUM_SETPOINTS];
		float setpoint_speeds[NUM_SETPOINTS]; 
		std::string setpoint_names[NUM_SETPOINTS];
		bool setpoint_defined[NUM_SETPOINTS];

		float joint_position[NUM_SETPOINTS];
		
		bool homed;

		bool bottomPressed;
		bool LightOn;
		bool LightOnAdjusted;
		int lightCycleCount;
		int lightCycleMaxCount;

		gsu::PiecewiseLinear ll_velocity_curves; // interpolant for setting targets
		gsu::PiecewiseLinear ll_angle_curves; // interpolant for setting targets
		std::vector<double> m_distance_table; // Set points: Limelight distance from target
		std::vector<double> m_velocity_table; // Set points: Flywheel velocity
		std::vector<double> m_angle_table; 
		const float RAD_TO_DEG = 57.2958;
		float ll_target_distance;
		float ll_target_vel;
		float ll_target_pos;
		
		float ll_height_diff;
		double ll_target_height;
		double ll_mount_height;
		double ll_mount_angle_vert;

		bool is_ll_target;

		double ll_is_visible;
		double ll_actual_ang_vert;
		double ll_actual_ang_hori;
		double ll_actual_area;
		double ll_actual_ang_roll;

		static float kicker_command_power;
		static float kicker_power;
		float kicker_actual_power;
		float kicker_target_power;

		float joint_target_position;
		float joint_actual_position;
		float joint_target_power;
		float joint_actual_power;
		float joint_min_position;
		float joint_max_position;
		float joint_command_power;
		float joint_power_step;
		float joint_max_current;
		float joint_command_position;
		float position_step;

		// Open Loop motor commands
		// Open Loop: We command the motor to run at a certain % power (expressed as a decimal from 0.0 to 1.0)
		float fly1_actual_power; // The power that the motor is currently running at
		float fly1_target_power; // Target power is the power you want the motor to run at
		float fly1_command_power; // Command power is the power that the motor is commanded to
		float prev_power;
				// power ramp up and down and min and max motor commands
		float fly1_forward_power; // These 2 are target powers for when a button is pressed
		float fly1_backward_power;
		float prev_velocity;
		float fly1_velocity_step;
		float fly1_power_step;
		// Closed Loop motor commands
		// Closed Loop: We command the motor to spin at a certain velocity and it will adjust the power output to stay at that velocity
		// It does this through PID (proportional, integral, derivative) values which we set in the XML
		// Take a look at the Control Systems PPT to learn more :)
		float fly1_actual_velocity; // The current velocity of the motor. Talon FXs report velocity in encoder units, but we convert it to RPM for ease and consistency
		float fly1_target_velocity;
		float fly1_command_velocity;
		float fly1_max_velocity;
		float fly1_min_velocity;
		float fly1_velocity_error; // only used for troubleshooting
		
		bool isFlyOn;
		bool active;
		bool is_closed_loop;
		int setpoint_index;
		
};


/*******************************************************************************
 * sets kicker state
 * @param state is kicker on? (true/false)
 ******************************************************************************/
class MSKickerState: public MacroStepSequence
{
	public:
		MSKickerState(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		bool state = false;
};

/*******************************************************************************
 * sets shooter closed loop on/off
 * @param state is closed loop? (true/false)
 ******************************************************************************/
class MSShooterClosedLoop: public MacroStepSequence
{
	public:
		MSShooterClosedLoop(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		bool state = false;
};

/*******************************************************************************
 * commands the shooter to a setpoint
 * @param setpoint setpoint index [0-3]
 ******************************************************************************/
class MSShooterSetpoint : public MacroStepSequence
{
	public:
    	MSShooterSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		float setpoint;
};


/*******************************************************************************
 * toggles flywheel on/off
 ******************************************************************************/
class MSShooterToggle: public MacroStepSequence
{
	public:
		MSShooterToggle(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
};

/*******************************************************************************
 * sets limelight pipeline
 * @param pipeline pipeline index [0-9]
 ******************************************************************************/
class MSLimelightPipeline: public MacroStepSequence
{
	public:
		MSLimelightPipeline(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		int pipeline = 0;
};
class MSShooterHome: public MacroStepSequence
{
	public:
		MSShooterHome(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		
};

class MSShooterPivotDown: public MacroStepSequence
{
	public:
		MSShooterPivotDown(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		
};

class MSShooterPivotStop: public MacroStepSequence
{
	public:
		MSShooterPivotStop(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		Shooter24 *parent_control;
		
};