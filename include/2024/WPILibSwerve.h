/*******************************************************************************
 *
 * File: WPILibSwerve.h
 *
 * Written by:
 * 	 Clear Creek Independent School District FIRST Robotics
 *   FRC Team 324, Chips
 * 	 NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "2024/WPILibSwerveModule.h"

#include "rfhardware/Gyro.h"
#include "rfhardware/Limelight.h"

#include "rfutilities/SimpleTrapCntl.h"

#include "frc/kinematics/SwerveDriveKinematics.h"
#include "frc/kinematics/SwerveDriveOdometry.h"
#include "frc/geometry/Translation2d.h"

using namespace frc;

/*******************************************************************************	
 * 
 * chips coordinate system
 * 	x: + forward
 * 	y: + right
 * 	heading: + clockwise (cw)
 * 
 * wpilib coordinate system
 * 	x: + forward
 * 	y: + left
 * 	heading: + counterclockwise (ccw)
 *
 ******************************************************************************/
class WPILibSwerve : public PeriodicControl, public OIObserver
{
	public:
		enum Commands
		{
			CMD_FORWARD = 0, 	CMD_STRAFE, 	CMD_TURN,		CMD_POWER,
			CMD_GYRO_RESET, 	CMD_X_WHEELS,	CMD_RESET_WHEELS,
			CMD_FIELD_ORIENTED_MODE,
			CMD_LL_ROT_HOLD,	CMD_LL_STRAFE_HOLD
		};
		
		enum DriveScheme
		{
			DRIVE_FIELD_RELATIVE = 0,	DRIVE_ROBOT_RELATIVE,
			DRIVE_STRAFE_TO_LIMELIGHT,	DRIVE_ROTATE_TO_LIMELIGHT,
			DRIVE_X_WHEELS,				DRIVE_RESET_WHEELS,
			DRIVE_AUTON,				DRIVE_PATH_FOLLOWING
		};
		
		WPILibSwerve(std::string control_name, tinyxml2::XMLElement *xml);
		~WPILibSwerve(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void robotRelativeDrive(float xSpeed, float ySpeed, float rot);
		void fieldRelativeDrive(float xSpeed, float ySpeed, float rot);
		wpi::array<frc::SwerveModuleState, 4> chassisToModuleStates(float xSpeed, float ySpeed, float rot);
		wpi::array<frc::SwerveModuleState, 4> chassisToModuleStates(frc::ChassisSpeeds speeds);
		frc::ChassisSpeeds moduleToChassisSpeeds(wpi::array<frc::SwerveModuleState, 4> states);
		frc::ChassisSpeeds fieldToChassisSpeeds(float x, float y, float omega, float robot_angle);
		void normalizeSpeeds(wpi::array<frc::SwerveModuleState, 4> states);

		void resetOdometry(frc::Pose2d pose);
		frc::Pose2d getCurrentPose(void);

		bool isGyroConnected();
		void resetHeading();
		float getHeading();
		frc::Rotation2d getHeadingRotation2d();
		float getPitch();
		float getRoll();

		void strafeToHeading(float heading);
		void rotateToHeading(float heading);
		void xWheels();
		void resetWheels();
		void setPipeline(int index);
		int getDefaultDriveScheme();
		void setDriveScheme(int index);
		void updateModules();

		void publish(void);

	private:	
        const float METERS_TO_INCHES = 39.3700787402;
        const float INCHES_TO_METERS = 0.0254;

		WPILibSwerveModule* front_left;
		WPILibSwerveModule* front_right;
		WPILibSwerveModule* back_left;
		WPILibSwerveModule* back_right;
		float max_drive_velocity;
		float max_rot_velocity;

		rfh::Gyro* gyro;
		bool is_gyro_connected;
		float heading;
		float pitch;
		float roll;

		frc::ChassisSpeeds chassis_speeds; // default values are 0.0, no need to initialize in cpp
		wpi::array<frc::SwerveModuleState, 4> module_states = {
			frc::SwerveModuleState{units::meters_per_second_t(0.0), frc::Rotation2d(units::degree_t(0.0))}, 
			frc::SwerveModuleState{units::meters_per_second_t(0.0), frc::Rotation2d(units::degree_t(0.0))}, 
			frc::SwerveModuleState{units::meters_per_second_t(0.0), frc::Rotation2d(units::degree_t(0.0))}, 
			frc::SwerveModuleState{units::meters_per_second_t(0.0), frc::Rotation2d(units::degree_t(0.0))}
		};

		// swervedrivekinematics and swervedriveodometry are templates, so they must be initialized when created
		// the actual values don't really matter, just the ratio between them
        float wheel_base = 23.0; // distance between front and back wheels
        float track_width = 23.0; // distance between left and right wheels
		float corner_to_corner = std::sqrt(wheel_base*wheel_base + track_width*track_width);
		frc::SwerveDriveKinematics<4> kinematics { // wheel locations: [front left, front right, back left, back right]
			frc::Translation2d(units::meter_t(wheel_base/2 * INCHES_TO_METERS), units::meter_t(track_width/2 * INCHES_TO_METERS)),
			frc::Translation2d(units::meter_t(wheel_base/2 * INCHES_TO_METERS), units::meter_t(-track_width/2 * INCHES_TO_METERS)),
			frc::Translation2d(units::meter_t(-wheel_base/2 * INCHES_TO_METERS), units::meter_t(track_width/2 * INCHES_TO_METERS)),
			frc::Translation2d(units::meter_t(-wheel_base/2 * INCHES_TO_METERS), units::meter_t(-track_width/2 * INCHES_TO_METERS)),
		};

		frc::Pose2d robot_pose;
		frc::SwerveDriveOdometry<4> odometry {
			kinematics,
			frc::Rotation2d(units::degree_t(0.0)),
			{
				frc::SwerveModulePosition{units::meter_t(0.0), frc::Rotation2d(units::degree_t(0.0))},
				frc::SwerveModulePosition{units::meter_t(0.0), frc::Rotation2d(units::degree_t(0.0))},
				frc::SwerveModulePosition{units::meter_t(0.0), frc::Rotation2d(units::degree_t(0.0))},
				frc::SwerveModulePosition{units::meter_t(0.0), frc::Rotation2d(units::degree_t(0.0))}
			}
		};
		
		float forward_cmd;
		float strafe_cmd;
		float turn_cmd;

		float forward_input; // joystick input
		float strafe_input; // joystick input
		float forward_axis; // (new input mode) stored joystick input
		float strafe_axis; // (new input mode) stored joystick input
		float power_cmd;

		bool using_new_input_mode;

		int drive_scheme;
		int default_drive_scheme;

		Limelight* limelight;
		double ll_target_visible;
		double ll_target_angle_horizontal;
		double ll_target_angle_vertical;
		double ll_target_area;
		double ll_target_skew;
		int ll_pipeline;

		float ll_forward_cmd; // maybe unused
		float ll_strafe_cmd;
		float ll_turn_cmd;
		float ll_rot_kp;
		float ll_strafe_kp;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDrivetoPosition : public MacroStepSequence
{
	public:
		MSDrivetoPosition(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
        const float METERS_TO_INCHES = 39.3700787402;
        const float INCHES_TO_METERS = 0.0254;

		SimpleTrapCntl *x_control;
		SimpleTrapCntl *y_control;
		SimpleTrapCntl *rot_control;

		float initial_x = 0.0;
		float initial_y = 0.0;
		float initial_heading = 0.0;

		float current_x = 0.0;
		float current_y = 0.0;
		float current_heading = 0.0;

		float target_x = 0.0;
		float target_y = 0.0;
		float target_heading = 0.0;

		float x_cmd = 0.0;
		float y_cmd = 0.0;
		float rot_cmd = 0.0;

		float tol_x = 2.0; // tolerance (in)
		float tol_y = 2.0;
		float tol_heading = 2.0; // tolerance (deg)
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveFieldRelative : public MacroStepSequence
{
	public:
		MSDriveFieldRelative(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
		float start_time;
		float current_time;

		float forward = 0.0;
		float strafe = 0.0;
		float turn = 0.0;
		float time = 0.5;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveRobotRelative : public MacroStepSequence
{
	public:
		MSDriveRobotRelative(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
		float start_time;
		float current_time;

		float forward = 0.0;
		float strafe = 0.0;
		float turn = 0.0;
		float time = 0.5;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSRotatetoHeading : public MacroStepSequence
{
	public:
		MSRotatetoHeading(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
		SimpleTrapCntl *rot_control;

		bool is_at_endpoint = false;

		float current = 0.0;
		float ending = 0.0;
		float cmd = 0.0;
		float tol = 2.0; // tolerance (deg)
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSSetDriveScheme : public MacroStepSequence
{
	public:
		MSSetDriveScheme(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
		int scheme;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSSetLLPipeline : public MacroStepSequence
{
	public:
		MSSetLLPipeline(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
		int pipeline;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSXWheels : public MacroStepSequence
{
	public:
		MSXWheels(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSGyroReset : public MacroStepSequence
{
	public:
		MSGyroReset(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		WPILibSwerve *parent_control;
};