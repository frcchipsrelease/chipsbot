/*******************************************************************************
 *
 * File: HangControl.h
 *  
 * Motor with Closed Loop control, feedback from a Pot and limit switches
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"
#include "frc/Servo.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

/*******************************************************************************	
 * 
 * Create an instance of a hang control with position joint srx utilities and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 * 
 *  <control type="hangControl" [name="unnamed"] [closed_loop="false"] [period="0.1"]
 *  		[setpoint0="0.0"] [setpoint1="0.0"] [setpoint2="0.0"] [setpoint3="0.0"] [setpoint4="0.0"]
 *  		[setpoint5="0.0"] [setpoint6="0.0"]  >
 *
 *  	<motor name="front_right" [type="CanTalon"] [port="2"] [control_period=”10”] [invert="false"]>
 *   		[<encoder [invert=”false”] [scale=”1.0”] />]
 *  		[<pid [kf=”0.001”] [kp=”0.0”] [ki=”0.0”] [kd=”0.0”] />]
 *		</motor>
 *
 *
 *      [<oi name="closed_loop_state"  device="switches" chan="1" [invert="false"]/>]
 *
 *      [<oi name="analog"    device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" step="0.1" device="pilot" chan="5" [invert="false"]/>]
 *      [<oi name="decrement" step="0.1" device="pilot" chan="7" [invert="false"]/>]
 *      [<oi name="stop"      device="pilot" chan="1" [invert="false"]/>]
 *
 *      [<oi name="setpoint_idx" device="pilot" chan="0" [scale="0.2222222"]/>
 *
 *      [<oi name="setpoint0" device="pilot" chan="2" [invert="false"]/>]
 *      [<oi name="setpoint1" device="pilot" chan="3" [invert="false"]/>]
 *      [<oi name="setpoint2" device="pilot" chan="3" [invert="false"]/>]   
 *
 *  </control>
 *  
 *  Note: either pot or aps can be used, if both are specified the pot will be
 *        used.
 *
 ******************************************************************************/
class HangControl : public PeriodicControl, public OIObserver
{
	public:
		enum Command
		{
			CMD_CLOSED_LOOP_STATE = 0,
			CMD_SETPOINT_0,
			CMD_SETPOINT_1,
			CMD_SETPOINT_2,
			CMD_SETPOINT_3,
			CMD_SETPOINT_4,
			CMD_SETPOINT_5,
			CMD_SETPOINT_6,
			CMD_SETPOINT_7,
			CMD_SETPOINT_IDX,
			CMD_HANG_UP,
			CMD_HANG_DOWN,
			CMD_INCREMENT_POS,
			CMD_DECREMENT_POS,
			CMD_RELEASE,
			CMD_ENGAGE,
			CMD_STATE
		};


		static const uint8_t NUM_SETPOINTS = 7;
		static const uint8_t NUM_MOMENTARIES = 4;

		HangControl(std::string control_name, tinyxml2::XMLElement *xml);
		~HangControl(void);

  		void controlInit(void);
		void updateConfig(void);
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void publish(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setPosition(float val);
		float getPostion(void);

		void initLogFile(void);

		void setClosedLoop(bool closed);
		bool isClosedLoop(void);
//		int8_t getSetpointIndex(std::string setpoint_name);
		void applySetpoint(bool on, int idx);
		bool isAtTarget(float tolerance);


	private:		

		DataLogger *hang_log;

		TrapezoidProfile3 m_traj_profile;

		Motor *motor;

		LimitSwitch *m_upper_limit_sw;
        LimitSwitch *m_lower_limit_sw;

		frc::Servo *m_servo;

		float hang_up_power;
		float hang_down_power;
		float hang_up_step;
		float hang_down_step;
        float initial_position;
        float min_position;
        float max_position;

		float m_servo_target_position;
		float m_servo_release_position;
		float m_servo_engage_position;
		float m_servo_step_size;

		bool m_upper_limit_pressed;
		bool m_lower_limit_pressed;
		bool reset_on_limit_press;

		float m_max_velocity;
		float m_desired_acceleration;
		float m_desired_deceleration;

		float increment_step;
		float decrement_step;
		float delta_position;
		
		bool m_is_ready;

		float raw_position;
		float actual_position;
		float actual_velocity;
		int32_t raw_velocity;

		float target_power;
		float target_position;
		float command_power;
		float max_power_delta;
		float commanded_position;

		uint8_t setpoint_index;
		int8_t getSetpointIndex(std::string setpoint_name);
		std::string setpoint_name[NUM_SETPOINTS];
		float setpoint_position[NUM_SETPOINTS];
        bool setpoint_defined[NUM_SETPOINTS];

		bool m_closed_loop;

};
class MSHang : public MacroStepSequence
{
	public:
		MSHang(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		HangControl *m_parent_control;
		float m_power;
		bool m_closed_loop;
		int setpoint_index;

		static const uint8_t NUM_SETPOINTS = 7;
		std::string m_setpoint_name[NUM_SETPOINTS];

		uint8_t num_setpoints;
		float m_pjs_setpoint[NUM_SETPOINTS];
		bool m_wait;
};

