/*******************************************************************************
 *
 * File: ColorWheelControl.h
 * 
 * 
 * 
 * 
 * As of 08/03/2022, this system has been disable by commenting out the references
 * to the color sensor which I believe lives in a Rev Robotics library that we do not 
 * seem to have at the moment.
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "2020/ColorSensor.h"

#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 *      [<oi name="off"          device="pilot" chan="4" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class ColorWheelControl : public PeriodicControl, public OIObserver
{
	public:
		enum 
		{
			CMD_MOTOR_STATE=0,
			CMD_MOTOR_ON,
			CMD_MOTOR_OFF,
			CMD_MOTOR_INT,
			CMD_ROTATE_NUM,
			CMD_ROTATE_COLOR,
			CMD_ROTATE_SLICE,

			CMD_CLOSED_LOOP_STATE
		};
		
		ColorWheelControl(std::string control_name, tinyxml2::XMLElement *xml);
		~ColorWheelControl(void);

		static const uint8_t NUM_SETPOINTS = 7;   //// down needs to change (prob)
		static const uint8_t NUM_MOMENTARIES = 4; ////up needs to change (prob)

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void publish(void);

		void setClosedLoop(bool closed);
		bool isClosedLoop(void);
		bool isAtTarget(float tolerance);                     //// up

		//Logic
		std::string getDetectedColor(void);
		std::string getRequiredColor(std::string _fieldColor);
		std::string getGameDataColorString(void);
		bool isDoneRotating(void);

	private:		
        Motor* m_color_motor;
//		ColorSensor* m_color_sensor;
		
		float motor_min_control;
		float motor_max_control;

        uint32_t color_max_current;

		float motor_increment_step;
		float motor_decrement_step;

		float m_on_power;
		float m_state_power;

		float motor_rotate_num_speed; 
		float motor_rotate_color_speed; 

		float motor_max_cmd_delta;

		float motor_target_power;
		float motor_command_power;

		float m_max_velocity;          ////down
		float m_desired_acceleration;
		float m_desired_deceleration;

		float increment_step;
		float decrement_step;
		float delta_position;
		
		bool m_is_ready;

		float raw_position;
		float actual_position;
		float actual_velocity;
		int32_t raw_velocity;

		float target_power;
		float target_position;
		float command_power;
		float max_power_delta;
		float commanded_position;

		float momentary_power[NUM_MOMENTARIES];
		float momentary_step[NUM_MOMENTARIES];

		bool m_closed_loop;                           //// up


		bool m_run_color;
		bool m_run_number;
		bool m_run_one_slice;

		//Logic Vars
		std::string m_fieldColor;
		std::string m_neededColor;
		std::string previousColor;
		std::string currentColor;
		std::string m_colorString;
		std::string m_game_data;

		bool m_is_cw_running;

		double m_confidence;
		double colorWheelRotations; 
		
};
