/*******************************************************************************
 *
 * File: ColorSensor.h -- Color IR Sensor
 *
 * 
 * As of 08/03/2022, this system has been disable by commenting out the references
 * to the color sensor which I believe lives in a Rev Robotics library that we do not 
 * seem to have at the moment.
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <frc/util/color.h>

//#include "rev/ColorSensorV3.h"
//#include "rev/ColorMatch.h"

// never put a using in include file, prefix needed items with frc::
// using namespace frc;


/**
 * 
 */



class ColorSensor
{
#ifdef DISABLED

 public:

  enum ThresholdType { kGreaterThan, kLessThan };

  static constexpr frc::Color BLUE_COLOR = frc::Color(0.143, 0.427, 0.429);
  static constexpr frc::Color GREEN_COLOR = frc::Color(0.197, 0.561, 0.240);
  static constexpr frc::Color RED_COLOR = frc::Color(0.561, 0.232, 0.114);
  static constexpr frc::Color YELLOW_COLOR = frc::Color(0.361, 0.524, 0.113);
  
  ColorSensor();
  ~ColorSensor();

  void setThreshold(int threshold);
  void setLatchCycles(int cycles);
  void setThresholdType(ThresholdType type);
  bool onLine();
  void update(frc::Color &detectedColor, double &confidence, std::string &colorString);



 private:
   /**
   * Change the I2C port below to match the connection of your color sensor
   */
  static constexpr auto i2cPort = frc::I2C::Port::kOnboard;

  /**
   * A Rev Color Sensor V3 object is constructed with an I2C port as a 
   * parameter. The device will be automatically initialized with default 
   * parameters.
   */
  rev::ColorSensorV3 m_colorSensor{i2cPort};

  /**
   * A Rev Color Match object is used to register and detect known colors. This can 
   * be calibrated ahead of time or during operation.
   * 
   * This object uses a simple euclidian distance to estimate the closest match
   * with given confidence range.
   */
  rev::ColorMatch m_colorMatcher;
#endif
};
