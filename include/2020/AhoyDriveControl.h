/*******************************************************************************
 *
 * File: AhoyDriveControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>

#include "gsutilities/tinyxml2.h"

#include "frc/Solenoid.h"
#include "frc/BuiltInAccelerometer.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ADIS16448_IMU.h"
#include "rfhardware/Limelight.h"

#include "rfcontrols/PeriodicControl.h"

/*******************************************************************************
 *
 *  Provides a PeriodicControl implementation for driving the base of a robot
 *  using an Arcade-like driver interface, that is a forward and turn command
 *  are used to make the robot move.
 *  
 * This class is designed to be created from an XML element.
 *  
 * 
 ******************************************************************************/
class AhoyDriveControl : public PeriodicControl, public OIObserver
{
	public:
		enum
		{
			CMD_FORWARD = 0, 	CMD_TURN,           CMD_STRAFE,
			CMD_BRAKE_ON, 		CMD_BRAKE_OFF, 	    CMD_BRAKE_TOGGLE, 	    CMD_BRAKE_STATE,
			CMD_GEAR_HIGH, 		CMD_GEAR_LOW, 	    CMD_GEAR_TOGGLE, 	    CMD_GEAR_STATE,
			CMD_LOW_POWER_ON,	CMD_LOW_POWER_OFF, 	CMD_LOW_POWER_TOGGLE, 	CMD_LOW_POWER_STATE,
			CMD_LIMELIGHT_STATE,
		};

		AhoyDriveControl(std::string name, tinyxml2::XMLElement *xml);
		~AhoyDriveControl(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void controlInit();

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();
		void publish();

	private:
        void logFileInit(std::string phase);
        void logFileAppend(void);
		void setLimelightState(bool value);

        DataLogger *m_data_logger;
	 	Limelight* m_limelight;

		bool is_limelight_control; 
		float m_targetHeight;
		float m_limelightHeight;
		float m_heightDifference;
		float m_calibratedDistance;
		float m_distanceOffset;
		double m_target_visible;
		double m_targetOffsetAngle_Horizontal;
		double m_targetOffsetAngle_Vertical;
		double m_targetArea;
		double m_targetSkew;

        Motor* fl_drive_motor;
        Motor* fr_drive_motor;
        Motor* bl_drive_motor;
        Motor* br_drive_motor;

		frc::Solenoid *brake_solenoid;
		frc::Solenoid *gear_solenoid;
		
		float fl_drive_motor_cmd;
		float fr_drive_motor_cmd;
		float bl_drive_motor_cmd;
		float br_drive_motor_cmd;

		bool brake_engaged;
		bool brake_solenoid_invert;

		bool gear_high;
		bool gear_solenoid_invert;
		
		float trn_power;
		float fwd_power;
		float strafe_power;

		float m_low_power_scale;
		bool m_low_power_active;

        frc::Accelerometer *m_accelerometer;
        double m_accelerometer_x;
        double m_accelerometer_y;
        double m_accelerometer_z;

        ADIS16448_IMU *m_imu;
        double m_imu_accel_x;
        double m_imu_accel_y;
        double m_imu_accel_z;

        double m_imu_mag_x;
        double m_imu_mag_y;
        double m_imu_mag_z;

        double m_imu_rate_x;
        double m_imu_rate_y;
        double m_imu_rate_z;

        double m_imu_angle_x;
        double m_imu_angle_y;
        double m_imu_angle_z;

        double m_imu_roll;
        double m_imu_pitch;
        double m_imu_yaw;

        double m_imu_quat_w;
        double m_imu_quat_x;
        double m_imu_quat_y;
        double m_imu_quat_z;

        double m_imu_bar_press;
        double m_imu_temperature;
		
};


// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 * This Macro Step sets the forward power, turn power, and strafe power of the
 * Drive Arcade PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn them off unless the
 *          provided values for forward, turn, and strafe turn the drive off.
 *
 *  Example XML:
 *
 *	<step name="drive_1" control="drive" type="DrivePower"
 *			forward="0.5" turn="0.1" strafe="0.0">
 *  	<connect type="next" step="drive_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSDriveAhoyDrivePower : public MacroStepSequence
{
	public:
		MSDriveAhoyDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		AhoyDriveControl *parent_control;

		float forward_power;
		float turn_power;
		float strafe_power;
};

