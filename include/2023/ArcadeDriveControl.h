/*******************************************************************************
 *
 * File: ArcadeDriveControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once
// TODO
// - clean up code and remove unused includes, variables

#include <string>

#include "gsutilities/tinyxml2.h"

#include "frc/DoubleSolenoid.h"
#include "frc/drive/DifferentialDrive.h"
#include "frc/geometry/Pose2d.h"
#include "frc/kinematics/DifferentialDriveOdometry.h"
#include "frc/kinematics/DifferentialDriveWheelSpeeds.h"
#include "units/voltage.h"
#include "units/velocity.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/CartTrajectory.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ADIS16448_IMU.h"
#include "rfhardware/Gyro_Spi.h"
#include "rfhardware/Gyro_NavX.h"
#include "rfhardware/Limelight.h"

#include "rfcontrols/PeriodicControl.h"

using namespace frc;

#define TRAJ2023

/*******************************************************************************
 *
 *  Provides a PeriodicControl implementation for driving the base of a robot
 *  using an Arcade-like driver interface, that is a forward and turn command
 *  are used to make the robot move.
 *  
 * This class is designed to be created from an XML element.
 *  
 * 
 ******************************************************************************/
class ArcadeDriveControl : public PeriodicControl, public OIObserver
{
	public:
		enum Commands
		{
			CMD_FORWARD = 0, 	CMD_TURN,           CMD_STRAFE,
			CMD_BRAKE_ON, 		CMD_BRAKE_OFF, 	    CMD_BRAKE_TOGGLE, 	    CMD_BRAKE_STATE,
			CMD_GEAR_HIGH, 		CMD_GEAR_LOW, 	    CMD_GEAR_TOGGLE, 	    CMD_GEAR_STATE,
			CMD_LOW_POWER_ON,	CMD_LOW_POWER_OFF, 	CMD_LOW_POWER_TOGGLE, 	CMD_LOW_POWER_STATE,
			CMD_LIMELIGHT_HOLD, CMD_PIPELINE_TAPE, 	CMD_PIPELINE_APRILTAG, 	CMD_PIPELINE_GAME_PIECE,
			CMD_RESET_GYRO
		};
		
		enum CartCompleteStatus
		{
			CCS_RUNNING = 0,
			CCS_DONE,
			CCS_TIMEOUT,
			CCS_ABORT
		};

		enum DriveScheme
		{
			DRIVE_ARCADE = 0, 	DRIVE_LIMELIGHT, 	DRIVE_CARTESIAN, 	DRIVE_CLOSED_LOOP
		};

		ArcadeDriveControl(std::string name, tinyxml2::XMLElement *xml);
		~ArcadeDriveControl(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void controlInit();

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();
		void publish();

		void arcadeDrive(float fwd, float turn); // rename
		void driveDistance(float distance);
		void driveDistanceMotor(float distance);
		void rotateDegrees(float degrees);
		void rotateToAngle(float angle);
		void balancePitch(float target);
		void distAndRotate(float distance, float angle); // maybe

		bool isAtHeadingTarget(float tolerance);
		bool isAtDistanceTarget(float tolerance);
		bool isAtPitchTarget(float tolerance);
		bool isGyroConnected(void);

		float getDistanceFromLLTarget(); // please dear god rename these
		float getLLHZAngle();

		void tankDriveVolts(units::volt_t left, units::volt_t right); // JLY unused
		void resetEncoders();
		float getAvgEncoderDistance();
  		float getHeading(); // deg
		float getTurnRate(); // deg/s		
		float getPitch(); // deg		
		float getRoll(); // deg
		frc::Pose2d getPose(); // JLY unused
		frc::DifferentialDriveWheelSpeeds getWheelSpeeds(); // JLY unused
		void resetOdometry(frc::Pose2d pose); // JLY unused

		void setBrakeState(bool state);

		// -------------- CARTESIAN TRAJECTORY (from 118 2019)
		#ifdef TRAJ2023
		void initCartesian(float x, float y, float theta);
		void doCartesianMode();
		void setCartesianMode();
		void setArcadeMode();
		void setLimelightMode();
		void setClosedLoopMode();
		void getRobotState();
		void driveSegment(float dist, float vel, float accel, float decel);
		void doSegmentMode();
		void updateCartesian(float heading, float d_dist, float *x, float *y);
		float getShortestHeadingCorrection(float *desired, float measured, float mag);
		float unwindAngleError(float des, float mea);
		float getCartesianX();
		float getCartesianY();
		float adjustHeadingFromCartesian(float x_act, float y_act, float x_cmd, float y_cmd);
		int cartesianSegmentDone();
		void calcCartesianSegmentDone();
		void initializeCartesianModeCmd(float timeout, std::vector<float> *x_cmd, std::vector<float> *y_cmd,
                                             std::vector<float> *end_vel, std::vector<bool> *blend,
                                             std::vector<int> *round_pts, std::vector<float> *round_dist,
                                             float tolerance, float ignore_start, float ignore_end,
                                             bool reset_coordinates);
		void initializeCartesianModeExec();
		void clearCartTrajectory();
		void clearCartTrajectoryStage(bool reset_coordinates, float x, float y);
		bool errorFromCartesian(float x_act, float y_act, float x_cmd, float y_cmd, float vel,
                                     float vel_prev, float pos, float end, float *desired_angle, float *mag);
		void doVelocityControlLaw(float left_target_vel, float right_target_vel, float gyro_contrib);
		
		#endif
		void setPipeline(int pipeline);


	private:
        void logFileInit(std::string phase);
        void logFileAppend(void);
		void setLimelightState(bool value);

        DataLogger *m_data_logger;
	 	Limelight* m_limelight;

		const float RAD_TO_DEG = 57.2958;

		bool is_limelight_control; 
		float m_targetHeight; // inches
		float m_limelightHeight; // inches
		float m_limelightAngle; // degrees
		float m_heightDifference;
		float m_distance_to_target; // inches
		float m_calibratedDistance;
		float m_distanceOffset;
		double m_target_visible;
		double m_targetOffsetAngle_Horizontal;
		double m_targetOffsetAngle_Vertical;
		double m_targetArea;
		double m_targetSkew;
		BasicPid* velocity_pid;
		BasicPid *angle_pid;
		

        Motor* fl_drive_motor;
        Motor* fr_drive_motor;
		uint32_t drive_max_current;
 		//frc::DifferentialDriveOdometry m_odometry;
		float drive_width;
		// TODO maybe hardcode scale as a const for more precise value
		float encoder_scale; // encoder value * scale = inches

		frc::DoubleSolenoid *brake_solenoid;
		
		float fl_drive_motor_cmd;
		float fr_drive_motor_cmd;

		bool brake_engaged;
		bool brake_solenoid_invert;
		bool brake_on_disable;

		bool gear_high;
		bool gear_solenoid_invert;
		
		float trn_power;
		float fwd_power;
		float strafe_power;

		float m_low_power_scale;
		bool m_low_power_active;

		float distance_error;
		float heading_error;
		float pitch_error;

		float pitch;
		float pitch_target;
		float pitch_offset;

	    rfh::Gyro* gyro;

		// TODO organize this
		uint8_t drive_scheme; // maps to a DriveScheme enum
		// ---------------------- CARTESIAN TRAJECTORY
		#ifdef TRAJ2023
		// gyro variables
		float m_heading; // degrees
		float m_heading_raw; // degrees
		float m_heading_offset; 

		float left_distance; // inches
		float right_distance;
		float avg_distance; // avg of left and right
		float avg_distance_prev;
		float avg_distance_target;

		float left_velocity; // m/s
		float right_velocity;
		float avg_velocity; // avg of left and right
		
		float m_encoders_reset_less_than;
		bool m_resetting_encoders;
		int m_resetting_encoder_cnt;
		int m_resetting_encoder_limit;

		float m_vel_cmd_prev;

		float m_prev_time; // seconds

		float *m_output_voltage_l, *m_output_voltage_r, m_output_voltage_strafe;
		float *m_output_current_l, *m_output_current_r, m_output_current_strafe;

		int m_output_voltage_current_count;  // kept to get the data, but overburden the CAN bus, read one per cycle

		bool m_drive_ready;

		float m_heading_target;
	   	float m_heading_target_prev;
		float m_gyro_cmd_adjusted;

		float m_drive_segment_end_distance;
		float m_drive_cmd_distance; // inches

		float m_drive_dist_kp;
		float m_drive_dist_kd;

		// ********************* BEGIN: variables for cartesian mode *********************
		float m_timeout;
		float m_timeout_time;
		std::vector<float> *m_x_cmd_vec;
		std::vector<float> *m_y_cmd_vec;
		std::vector<float> *m_end_vel_vec;
		std::vector<bool> *m_blend_vec;
		std::vector<float> *m_round_dist;
		std::vector<int> *m_round_pts;
		bool m_initialize_cart;
		float m_tolerance;
		float m_cart_init_time;
		float m_cart_offset_time; // currently only for SplitRotate, could be used for other things also

		float m_timeout_staged;
		std::vector<float> *m_x_cmd_vec_staged;
		std::vector<float> *m_y_cmd_vec_staged;
		std::vector<float> *m_end_vel_vec_staged;
		std::vector<bool> *m_blend_vec_staged;
		std::vector<float> *m_round_dist_staged;
		std::vector<int> *m_round_pts_staged;
		float m_tolerance_staged;
		bool m_reset_coordinates_staged;

		float m_adjust_heading_start;
		float m_adjust_heading_end;

		CartTrajectory *m_cart_traj;  // for 2018 offseason beyond type cartesian segments
		uint8_t m_cart_traj_done_state;  // maps to a CartCompleteStatus

		// cartesian variables
		float m_x_cmd;
		float m_x_meas;
		float m_y_cmd;
		float m_y_meas;
		float m_cart_error;
		float m_x_offset;
		float m_y_offset;

		float m_cart_tolerance_nominal;
		float m_cart_close_enough_to_end;

		// ********************* BEGIN: driving velocity mode variables *********************
		float target_avg_velocity;
		float target_left_velocity;
		float target_right_velocity;
		float m_delta_th;   // offset velocity based on gyro (for driving straight or following a desired angle)



		// miscellaneous

		#endif
		
};


// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDrivePower : public MacroStepSequence
{
	public:
		MSDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float fwd;
		float trn;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveDistance : public MacroStepSequence
{
	public:
		MSDriveDistance(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		bool m_wait;
		float dist;
		float dist_error;
		float fwd;
		int at_dist_count;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSSimpleDriveDistance : public MacroStepSequence
{
	public:
		MSSimpleDriveDistance(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float dist; // inches
		float power;
		float timeout; // seconds
		float tol; // inches
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveAngle : public MacroStepSequence
{
	public:
		MSDriveAngle(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		bool m_wait;
		float angle;
		float angle_error;
		float trn;
		float kp;
		float tol;
		int at_angle_count;
		float timeout;
		float start_time;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSSimpleDriveAngle : public MacroStepSequence
{
	public:
		MSSimpleDriveAngle(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float angle; // degrees
		float power;
		float tol; // degrees
		
		float starting_angle;
		float angle_traveled;
		float angle_error;

		int count;
};


/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveLLRotate : public MacroStepSequence
{
	public:
		MSDriveLLRotate(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float trn;
		float angle_error;

		float angle_traveled;
		float angle;
		float tol;
		float power;
		int count;
		int cycle_count;
		float fwd;
		float kp;
};


/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveBridge : public MacroStepSequence
{
	public:
		MSDriveBridge(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		bool m_wait;
		bool reverse;
		float fast_power;
		float slow_power;
		float pitch_on_bridge;
		float pitch_balanced;

		float fwd;
		float pitch;
		int count;
		int max_count;
		int state;

		float start_time;
		float current_time;
};


/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveBrakeState : public MacroStepSequence
{
	public:
		MSDriveBrakeState(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		bool state;
};

class MSSetPipeline : public MacroStepSequence
{
	public:
		MSSetPipeline(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		int pipeline;
		
};

class MSDriveAngleDistance : public MacroStepSequence
{
	public:
		MSDriveAngleDistance(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float dist;
		float angle;
		float dist_tol;
		float angle_tol;
		float dist_power;
		float angle_power;
		
};

class MSDriveDistanceOnHeading : public MacroStepSequence
{
	public:
		MSDriveDistanceOnHeading(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float target_heading;
		float heading_error;
		float max_fwd_power;
		float target_dist;
		float dist_error;
		float fwd;
		float trn;
		float tol;
		float kp;
		float trn_kp;
		
};
class MSDriveDistanceOnCurrentHeading : public MacroStepSequence
{
	public:
		MSDriveDistanceOnCurrentHeading(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float target_heading;
		float heading_error;
		float max_fwd_power;
		float target_dist;
		float dist_error;
		float fwd;
		float trn;
		float tol;
		float kp;
		float trn_kp;
		
};

// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSCartesianMode : public MacroStepSequence
{
	public:
		MSCartesianMode(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveCartesian : public MacroStepSequence
{
	public:
		MSDriveCartesian(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArcadeDriveControl *parent_control;
		float m_tolerance;
		float m_timeout;
		float m_init_time;

		float m_ignore_start_inches;
		float m_ignore_end_inches;

		float m_vision_exit_distance;
		bool m_vision_exit_criteria;

		bool m_reset_coordinates;
//		ArcadeDriveControl *m_drive;
		std::vector<float> *m_x_cmd;
		std::vector<float> *m_y_cmd;
		std::vector<float> *m_end_vel;
		std::vector<bool>   *m_blend;
		std::vector<int>    *m_round_pts;
		std::vector<float> *m_round_dist;
};
