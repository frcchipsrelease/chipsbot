/*******************************************************************************
 *
 * File: SwerveModule.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"
#include "rfhardware/Cancoder.h"

#include "gsutilities/tinyxml2.h"

/*******************************************************************************	
 * 
 * Create an instance of a swerve module and connect it to the specified
 * motors and sensors
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <swerve_module name="front_left">
 *      <motor name="drive" type="TalonFxCan" device_id="1" invert="false" current_limit="60" peak_current="80" peak_current_dur="1"/>
 * 		<motor name="Steering" type="TalonFxCan" device_id="2" invert="false" current_limit="60" peak_current="80" peak_current_dur="1">
 *          <pid kf="0.0" kp="0.4" ki="0.000" kd="0.00" kz="0" cntl_min="-1.0" cntl_max="1.0" is_angle="true"/>    
 *      </motor>
 *      <cancoder name="steering_cancoder" port="3" north="40.16"/>        
 *  </swerve_module>
 *
 ******************************************************************************/
class SwerveModule
{
	public:	
		SwerveModule(tinyxml2::XMLElement *xml);
		~SwerveModule(void);

		// Add other methods specific for your class
        float getDrivePower(void);
        float getDriveTargetPower(void);
        float getSteeringPower(void);
        float getDrivePosition(void);
        float getSteeringPosition(void);
        float getSteeringPositionTarget(void);
        float getSteeringPositionError(void);
        float getDriveVelocity(void);
        float getSteeringVelocity(void);
        float getEncoderPosition(void);
        float getEncoderPositionTarget(void);

        void setDrivePower(float pwr);
        void setSteeringPower(float pwr);
        void setDriveVelocity(float vel);
        void setSteeringPosition(float pos);
        void set(float pwr, float pos);
        void resetEncoders(void);

        void update(void);

	private:
        Motor* drive_motor;
        Motor* steering_motor;

        float wheel_diam; // inches

        float drive_gear_ratio;
        float steering_gear_ratio;

        float drive_encoder_scale; // encoder ticks * scale = inches
        float steering_encoder_scale; // encoder ticks * scale = degs

		// Open loop
		float drive_target_power;
		float steering_target_power;
		float drive_command_power;
		float steering_command_power;

        // Closed loop
        float drive_target_velocity;
		float steering_target_velocity;
		float drive_command_velocity; // talon returns vel per second
		float steering_command_velocity;

        float drive_target_position;
        float steering_target_position; // deg
        float drive_actual_position;
        float steering_actual_position;

        rfh::Cancoder* cancoder;
        float cc_actual;
        float cc_target;
        float cc_error;

        BasicPid* steering_pid;
};
