/*******************************************************************************
 *
 * File: LightsControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

// Do not remove - all subsystems
#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"


// Add as needed
#include "rfhardware/LightsStrip.h"
#include "frc/smartdashboard/SendableChooser.h"

/*******************************************************************************	
 * 
 * Create an instance of a light strip control
 *
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *	TODO
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class LightsControl : public PeriodicControl, public OIObserver
{
	public:
		//enum {CMD_PATTERN_1}; // unused
		
		LightsControl(std::string control_name, tinyxml2::XMLElement *xml);
		~LightsControl(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void publish(void);

		void setLedData(void);
		void setLedPattern(int idx);	

		void setSelectedPattern(int idx);


	private:	
		LightsStrip *led_strip;
		frc::SendableChooser<int> pattern_chooser;

		int m_led_length;
		int front_length;
		int back_length;

		int port;

		int selected_pattern_idx;
		int autonomous_pattern;
		int disabled_pattern;

		int move_count;

		// palette[color idx][r g or b]	
		std::vector<std::vector<int>> ColorPalette;

		std::vector<int> pattern_type;
		std::vector<int> color1_idx;
		std::vector<int> color2_idx;
		

};

/*******************************************************************************
 *
 * This Macro Step sets the light pattern :)
 *
 *  Example XML:
 *
 *	<step name="lights_blue" control="lights" type="SetPattern"	pattern="UHHH TODO" >
 *  	<connect type="next" step="wait"/>
 *  </step>
 *
 ******************************************************************************/
class MSSetPattern : public MacroStepSequence
{
	public:
    	MSSetPattern(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		LightsControl *m_parent_control;

		int pattern;
};