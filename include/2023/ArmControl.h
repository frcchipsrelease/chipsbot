/*******************************************************************************
 *
 * File: ArmControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"
#include "rfhardware/Motor_TalonSrx.h"
#include "rfhardware/Motor.h"
#include "rfhardware/AbsPosSensor.h"
// Maybe, maybe not
#include "rfhardware/LimitSwitch.h"
#include "rfhardware/Cancoder.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class ArmControl : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_WRISTFORWARD=0, CMD_WRISTBACKWARD, CMD_STOP, CMD_SHOULDERFORWARD, CMD_SHOULDERBACKWARD,
        CMD_EXTENSIONFORWARD, CMD_EXTENSIONBACKWARD, CMD_CLOSED_LOOP_STATE, CMD_CLOSED_LOOP_TOGGLE, CMD_CLOSED_LOOP_ON,
		CMD_CLOSED_LOOP_OFF, CMD_SETPOINT_0, CMD_SETPOINT_1, CMD_SETPOINT_2, CMD_SETPOINT_IDX, CMD_ANALOG_SETPOINT,
		CMD_NUDGE_SHOULDER_FWD, CMD_NUDGE_SHOULDER_BCK, CMD_NUDGE_WRIST_FWD, CMD_NUDGE_WRIST_BCK, CMD_NUDGE_EXTENSION_FWD,
		CMD_NUDGE_EXTENSION_BCK};

		static const uint8_t NUM_SETPOINTS = 20;

		ArmControl(std::string control_name, tinyxml2::XMLElement *xml);
		~ArmControl(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();
		void initLogFile();
		void updateLogFile();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setExtensionPosition(float position);
		float getPosition(void);
		float getShoulderPosition(void);
		float getShoulderRaw(void);

		void setWristPower(float power);
        void setShoulderPower(float power);
		void setExtensionPower(float power);

		void setClosedLoop(bool closed);
		bool isClosedLoop();

		void applySetPoint(bool on, int idx);
		//int parseSetPoints(tinyxml2::XMLElement* comp, float* setp);

		void setPid(BasicPid* pid);

		void publish(void);

	private:		
        Motor* wrist_motor;
        Motor* shoulder_motor;
        Motor* extension_motor;

		DataLogger* arm_log;

	    bool m_wristFwdLimitPressed;
		bool m_wristBackLimitPressed;
		bool m_shoulderFwdLimitPressed;
		bool m_shoulderBackLimitPressed;
		bool m_extensionFwdLimitPressed;
		bool m_extensionBackLimitPressed;
		bool arm_homed;
		int32_t home_count;
		
		LimitSwitch* m_limit_shoulder_fwd;
		LimitSwitch* m_limit_shoulder_back;

		rfh::Cancoder* m_wrist_cancoder;

		AbsPosSensor *shoulder_pos_sensor;

		BasicPid* wrist_pid;
		BasicPid* shoulder_pid;

		bool shoulder_fwd_limit_state;
		bool shoulder_back_limit_state;

		float motor_min_control; // sdr what
		float motor_max_control;

        uint32_t wrist_motor_max_current;
        uint32_t shoulder_motor_max_current;
        uint32_t extension_motor_max_current;

		float motor_max_cmd_delta; // sdr what

        float wrist_motor_forward_power;
		float wrist_motor_backward_power;
        float shoulder_motor_forward_power;
		float shoulder_motor_backward_power;
        float extension_motor_forward_power;
		float extension_motor_backward_power;

		// Open loop motor commands
		// Target power is the power you want the motor to run at
		float wrist_motor_target_power;
		float shoulder_motor_target_power;
        float extension_motor_target_power;
		// Command power is the power that the motor is commanded to
		// This can be different thatn target power due to current limiting,
		// power ramp up and down and min and max motor commands
		float wrist_motor_command_power;
		float shoulder_motor_command_power;
        float extension_motor_command_power;

		//Position Variables


		std::string setpoint_name [NUM_SETPOINTS];
		float wrist_setpoint_position [NUM_SETPOINTS];
		float shoulder_setpoint_position [NUM_SETPOINTS];
		float extension_setpoint_position [NUM_SETPOINTS];
		bool setpoint_defined [NUM_SETPOINTS];

		//Closed Loop Stuff - Variables

		bool is_closed_loop; 

		const float ENCODER_TO_IN;
		float min_position_wrist;
		float max_position_wrist;
		float wrist_actual_position; // encoder units
		float wrist_target_position;
		float wrist_position_error;
		float wrist_offset_angle;

		float min_position_extension;
		float max_position_extension; 
		float extension_actual_position;
		float extension_encoder_actual_position;
		float extension_encoder_raw_position;
		float extension_target_position;	// INCHES

		float min_position_shoulder;
		float max_position_shoulder; 
		float shoulder_target_position;
		float shoulder_calibrated_position;
		float shoulder_raw_position;
		float shoulder_position_error;

		float wrist_nudge_forward_step;
		float wrist_nudge_backward_step;
		float shoulder_nudge_forward_step;
		float shoulder_nudge_backward_step;
		float extension_nudge_forward_step;
		float extension_nudge_backward_step;
			

	bool active;
};

/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSWrist : public MacroStepSequence
{
	public:
    	MSWrist(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArmControl *m_parent_control;

		float wrist_power;
};


/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSShoulder : public MacroStepSequence
{
	public:
	MSShoulder(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArmControl *m_parent_control;

		float shoulder_power;
    
};

class MSExtension : public MacroStepSequence
{
	public:
	MSExtension(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArmControl *m_parent_control;

		float extension_power;

};

class MSChooseSetpoint : public MacroStepSequence
{
	public:
	MSChooseSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ArmControl *m_parent_control;

		int setpoint_index;

};