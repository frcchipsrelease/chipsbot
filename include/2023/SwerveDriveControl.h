/*******************************************************************************
 *
 * File: SwerveDriveControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "2023/SwerveModule.h"

#include "rfhardware/Gyro_NavX.h"
#include "rfhardware/Limelight.h"

using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="summer_swerve" name="yippee!!!" [period="0.1"]>
 *      <swerve_module name="front_left" />
 *      <swerve_module name="front_right" />
 *      <swerve_module name="back_left" />
 *      <swerve_module name="back_right" />
 * 		<gyro name="base_gyro" type="NavX" port="MXP"/>
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class SwerveDriveControl : public PeriodicControl, public OIObserver
{
	public:
		enum Commands {
			CMD_FORWARD = 0, 	CMD_STRAFE, 	CMD_TURN,		CMD_POWER,
			CMD_GYRO_RESET, 	CMD_X_WHEELS,	CMD_RESET_WHEELS,
			CMD_FIELD_ORIENT_TOGGLE,
			CMD_LL_ROT_HOLD,	CMD_LL_STRAFE_HOLD
		};
		
		enum DriveScheme
		{
			DRIVE_FIELD = 0, 	DRIVE_ROBOT, 	
			DRIVE_LL_ROT, 		DRIVE_LL_STRAFE, DRIVE_X_WHEELS, DRIVE_RESET_WHEELS
		};
		
		SwerveDriveControl(std::string control_name, tinyxml2::XMLElement *xml);
		~SwerveDriveControl(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void robotRelativeDrive(float xSpeed, float ySpeed, float rot);
		void fieldRelativeDrive(float xSpeed, float ySpeed, float rot);
		void toWheelSpeeds(float x, float y, float rot, float (&speeds)[4], float (&angles)[4]);
		void normalizeSpeeds(float (speeds)[4]);

		bool isGyroConnected();
		float getHeading();
		float getPitch();
		float getRoll();

		void strafeToHeading(float heading);
		void rotateToHeading(float heading);
		void xWheels();
		void resetWheels();
		void setPipeline(int index);
		void setDriveScheme(int index);
		void updateModules();

		void publish(void);

	private:	
		const float RAD_TO_DEG = 57.2958;
        const float DEG_TO_RAD = 0.0174533;

		SwerveModule* front_right;
		SwerveModule* front_left;
		SwerveModule* back_left;
		SwerveModule* back_right;
		rfh::Gyro* gyro;

		bool gyro_connected;
		float heading; // navx api returns degrees, cw positive
		float pitch; // positive when front of robot is up
		float roll; // positive when ?

		float l; // distance btwn front and back wheels
		float w; // distance btwn left and right wheels
		float r; // distance from corner to corner of drivebase: sqrt(l^2 + w^2)

		float wheelSpeeds[4]; // 0: front right, 1: front left, 2: back left, 3: back right
		float wheelAngles[4];
		
		float forward_axis;
		float strafe_axis;
		float forward_cmd;
		float strafe_cmd;
		float turn_cmd;
		float power_cmd;
		float forward_input;
		float strafe_input;

		bool using_new_input_mode; // rename!!!!!!!

		int drive_scheme;
		int default_drive_scheme;

		Limelight* limelight;
		double ll_target_visible;
		double ll_target_angle_horizontal;
		double ll_target_angle_vertical;
		double ll_target_area;
		double ll_target_skew;
		int ll_pipeline;

		float ll_forward_cmd;
		float ll_strafe_cmd;
		float ll_turn_cmd;


};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSFieldDrivePower : public MacroStepSequence
{
	public:
		MSFieldDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
		float fwd;
		float strafe;
		float rot;
		float time;
		float start_time;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSFieldDriveHeading : public MacroStepSequence
{
	public:
		MSFieldDriveHeading(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
		float target_heading;
		float actual_heading;
		float heading_error;
		float rot_calc;
		float timeout;
		float start_time;
		float tolerance;
		float kp;
		float max_power;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSFieldRotateByAngle : public MacroStepSequence
{
	public:
		MSFieldRotateByAngle(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
		float angle;
		float target_heading;
		float actual_heading;
		float heading_error;
		float rot_calc;
		float timeout;
		float start_time;
		float tolerance;
		float kp;
		float max_power;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSXWheels : public MacroStepSequence
{
	public:
		MSXWheels(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSSetLLPipeline : public MacroStepSequence
{
	public:
		MSSetLLPipeline(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
		int pipeline;
};

/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSSetDriveScheme : public MacroStepSequence
{
	public:
		MSSetDriveScheme(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
		int scheme;
};


/*******************************************************************************
 *
 *
 ******************************************************************************/
class MSDriveBalance : public MacroStepSequence
{
	public:
		MSDriveBalance(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SwerveDriveControl *parent_control;
		bool reverse;
		float fast_power;
		float slow_power;
		float pitch_on_bridge;
		float pitch_balanced;

		float fwd;
		float pitch;
		int count;
		int max_count;
		int state;

		float start_time;
		float current_time;
};

