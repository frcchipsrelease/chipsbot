/*******************************************************************************
 *
 * File: ChipsShooter.h
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Notes:
 * 	
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

#include "gsutilities/PiecewiseLinear.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class ChipsShooter : public PeriodicControl, public OIObserver
{
	public:
		// enumerates (assigns a positive integer value to) all the commands that can be sent from the controller
		enum {	CMD_FLY_STEP_UP, CMD_FLY_STEP_DOWN, CMD_PIVOT_UP, CMD_PIVOT_DOWN, CMD_LIMELIGHT_TOGGLE, CMD_SPEED_UP, 
		CMD_SPEED_DOWN, CMD_LOOP_TOGGLE, CMD_FLY_TOGGLE, CMD_SETPOINT_0, CMD_DPAD_SETPOINTS,
		 CMD_SETPOINT_1, CMD_SETPOINT_2, CMD_SETPOINT_3}; 
		 // commands that can be sent from the joystick to the subsystem. OK to edit
		static const uint8_t NUM_SETPOINTS = 20;

		// These methods are in every class. Do not remove!
		ChipsShooter(std::string control_name, tinyxml2::XMLElement *xml);
		~ChipsShooter(void);

		bool flyOn(void);
		bool isClosedLoop(void);
  		void controlInit(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setClosedLoop(bool state, bool pressed=true);
		void setSetpointShift(bool pressed);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);
		
		void publish(void);

		// Add other methods specific for your class
		void setMotorPower(float power);
		void applySetpoint(int i);
		int getSetpointIndex();
		
		void setLimelightPipeline(int idx);

	private:	
		// Be sure to give your motors names that are easy to figure out
		// Even if you know what a certain codename means, other people don't!
		// Usually we name motors in camelCaps: for example, frontMotor and backMotor
        Motor* fly_motor;
		Motor* joint_motor;

		LimitSwitch* bottom_limit;
		LimitSwitch* top_limit;

		AbsPosSensor* joint_sensor;

		BasicPid* joint_pid;

		bool setpoint_defined[NUM_SETPOINTS];
		std::string setpoint_name[NUM_SETPOINTS];
		float setpoint_fly_velocity[NUM_SETPOINTS];
		float setpoint_joint_position[NUM_SETPOINTS];
		bool setpoint_shift;

		bool bottomPressed;
		bool topPressed;

		float joint_target_power;
		float joint_command_power;
		float joint_actual_power;
		float joint_min_power;
		float joint_max_power;
		float joint_step_power;

		float joint_target_position;
		float joint_command_position;
		float joint_actual_position;
		float Joint_actual_raw;
		int   joint_actual_wraps;

		float joint_position_step;
		float joint_min_position;
		float joint_max_position;
		float joint_max_current;

		// Open Loop motor commands
		// Open Loop: We command the motor to run at a certain % power (expressed as a decimal from 0.0 to 1.0)
		float fly_target_power; // Target power is the power you want the motor to run at
		float fly_command_power; // Command power is the power that the motor is commanded to
		float fly_actual_power; // actual power that the motor is currently running at
		float fly_max_power;
		float fly_min_power;
		float fly_power_step;

		// Closed Loop motor commands
		// Closed Loop: We command the motor to spin at a certain velocity and it will adjust the power output to stay at that velocity
		// It does this through PID (proportional, integral, derivative) values which we set in the XML
		// Take a look at the Control Systems PPT to learn more :)
		float fly_target_velocity;
		float fly_command_velocity;
		float fly_actual_velocity; // The current velocity of the motor. Talon FXs report velocity in encoder units, but we convert it to RPM for ease and consistency
		float fly_max_velocity;
		float fly_min_velocity;
		float fly_velocity_step;
		float fly_velocity_raw;

		bool isFlyOn;
		bool active;
		bool is_closed_loop;
		int setpoint_index;	
};


/*******************************************************************************
 * sets shooter closed loop on/off
 * @param state is closed loop? (true/false)
 ******************************************************************************/
class MSShooterClosedLoop: public MacroStepSequence
{
	public:
		MSShooterClosedLoop(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ChipsShooter *parent_control;
		bool state = false;
};

/*******************************************************************************
 * commands the shooter to a setpoint
 * @param setpoint setpoint index [0-3]
 ******************************************************************************/
class MSShooterSetpoint : public MacroStepSequence
{
	public:
    	MSShooterSetpoint(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ChipsShooter *parent_control;
		int setpoint;
};

#if 0
/*******************************************************************************
 * toggles flywheel on/off
 ******************************************************************************/
class MSShooterToggle: public MacroStepSequence
{
	public:
		MSShooterToggle(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ChipsShooter *parent_control;
};

/*******************************************************************************
 * sets limelight pipeline
 * @param pipeline pipeline index [0-9]
 ******************************************************************************/
class MSLimelightPipeline: public MacroStepSequence
{
	public:
		MSLimelightPipeline(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ChipsShooter *parent_control;
		int pipeline = 0;
};

class MSShooterPivotDown: public MacroStepSequence
{
	public:
		MSShooterPivotDown(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ChipsShooter *parent_control;
		
};

class MSShooterPivotStop: public MacroStepSequence
{
	public:
		MSShooterPivotStop(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ChipsShooter *parent_control;
		
};
#endif