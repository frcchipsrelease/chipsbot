/*******************************************************************************
 *
 * File: ChipsIntake.h
 *
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "gsutilities/tinyxml2.h"
#include "rfutilities/DataLogger.h"
#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"

/******************************************************************************/

class TortillaIntake : public PeriodicControl
{
    public:
    	TortillaIntake(std::string control_name, tinyxml2::XMLElement *xml);
		~TortillaIntake (void);

  		void controlInit(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();

		void setRollerPower(float value);
		void setMomentaryRollerPower(float value, bool pressed=true);
		void setHandoffPower(float value);
		void setMomentaryHandoffPower(float value, bool stop_at_light=true, bool pressed=true);

		void publish(void);

    private:
        Motor* motor_roller;
        Motor* motor_handoff;

		LimitSwitch* light_sensor;

        float motor_roller_in_max_power;
        float motor_roller_out_max_power;
        float motor_handoff_in_max_power;
        float motor_handoff_out_max_power;
        
        float motor_roller_target_power;
        float motor_handoff_target_power;

        float motor_roller_command_power;
        float motor_handoff_command_power;

        float motor_roller_actual_power;
        float motor_handoff_actual_power;

		bool light_is_triggered;
		bool stop_handoff_at_light;

        bool light_sensor_adjusted;
		int light_sensor_count;
		float light_sensor_delay; // seconds

        bool hardware_is_ready;
};


/*******************************************************************************
 * commands rollers in
 ******************************************************************************/
class MSTortillaIntakePower: public MacroStepSequence
{
	public:
		MSTortillaIntakePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		TortillaIntake *parent_control;
		float power = 0.0;
};

/*******************************************************************************
 * commands rollers out
 ******************************************************************************/
class MSTortillaHandoffPower: public MacroStepSequence
{
	public:
		MSTortillaHandoffPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		TortillaIntake *parent_control;
		float power = 0.0;
		bool stop_at_light = false;
};
