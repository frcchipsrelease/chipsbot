/*******************************************************************************
 *
 * File: SwerveModule.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfhardware/Motor.h"
#include "rfhardware/Cancoder.h"

#include "gsutilities/tinyxml2.h"

#include "frc/kinematics/SwerveModulePosition.h"
#include "frc/kinematics/SwerveModuleState.h"
#include "frc/geometry/Pose2d.h"
#include "frc/geometry/Rotation2d.h"
#include "units/angular_velocity.h"
#include "units/time.h"
#include "units/velocity.h"

/*******************************************************************************	
 * 
 * @TODO
 * Create an instance of a swerve module and connect it to the specified
 * motors and sensors
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <swerve_module name="front_left">
 *      <motor name="drive" type="TalonFxCan" device_id="1" invert="false" current_limit="60" peak_current="80" peak_current_dur="1"/>
 * 		<motor name="Steering" type="TalonFxCan" device_id="2" invert="false" current_limit="60" peak_current="80" peak_current_dur="1">
 *          <pid kf="0.0" kp="0.4" ki="0.000" kd="0.00" kz="0" cntl_min="-1.0" cntl_max="1.0" is_angle="true"/>    
 *      </motor>
 *      <cancoder name="steering_cancoder" port="3" north="40.16"/>        
 *  </swerve_module>
 *
 ******************************************************************************/
class WPILibSwerveModule
{
  public:	
		WPILibSwerveModule(uint8_t idx, tinyxml2::XMLElement *xml);
		~WPILibSwerveModule(void);  

      uint8_t module_index; 
      
        void setMaxDriveVelocity(float vel);
        void setMaxRotVelocity(float vel);
        float getMaxDriveVelocity(void);
        float getMaxRotVelocity(void);
        void setDrivePower(float pwr);
        void setSteeringPower(float pwr);
        void setDriveVelocity(float vel);
        void setSteeringPosition(float pos);
        void setState(frc::SwerveModuleState target_state);
        void setState(float target_pwr, float target_pos);
        void resetEncoders(void);

        void update(void);
        float getEncoderPosition(void);
        float getEncoderTarget(void);
        float getEncoderError(void);

        float getDrivePower(void);
        float getDriveVelocity(void);
        float getSteeringPower(void);

        frc::SwerveModulePosition getModulePosition(void);

        frc::SwerveModuleState optimizeState(frc::SwerveModuleState state);
        frc::SwerveModuleState optimizeState(float state_vel, float state_pos);
        frc::SwerveModuleState getState(void);


        private:
        const float METERS_TO_INCHES = 39.3700787402; //change values when dimensions of base is found
        const float INCHES_TO_METERS = 0.0254;
        const float RAD_TO_DEG = 57.29578;
        const float DEG_TO_RAD = 0.0174532925;

        Motor* drive_motor;
        Motor* steering_motor;

        float wheel_diameter; // inches
        float drive_gear_ratio;
        float drive_encoder_scale; // inches per encoder tick
        float max_drive_velocity; // in/s
        float max_rot_velocity; // deg/s
        float steering_gear_ratio;
        float steering_encoder_scale; // degrees per encoder tick

        frc::SwerveModuleState module_target_state;
        frc::SwerveModuleState module_actual_state;

        // velocities in in/s, positions in deg
        float drive_target_power;
        float drive_command_power;
        float drive_target_velocity;
        float drive_command_velocity;
        float steering_target_power;
        float steering_command_power;
        float steering_target_position;
        float steering_actual_position;

        rfh::Cancoder* cancoder;
        float cc_actual;
        float cc_target;
        float cc_error;

        BasicPid* steering_pid;
};